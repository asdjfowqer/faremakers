//
//  AppDelegate.m
//  Faremakers
//
//  Created by Mac1 on 16/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "AppDelegate.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import "FMRootViewController.h"
#import "UILabel+FontName.h"
#import "AirLinesNameData.h"
#import "FBLoginManager.h"
#import "LoadingView.h"
#import "Constants.h"
#import "FMUser.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

-(UIStoryboard*)getManStoryBoard
{
    return [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
}
- (UINavigationController*)setUpNavigationControllerWithRootView:(id)rootView
{
    UINavigationController *navigationController = [[UINavigationController alloc] initWithNavigationBarClass:[FMNavigationBar class] toolbarClass:nil];
    [navigationController setViewControllers:@[rootView]];
    return navigationController;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    [AirLinesNameData defaultAirLinesData];
    [[FMUser defaultUser] loadLoggedInUser];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    
    [[UINavigationBar appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:KCustomeRagularFont size:15.0],NSFontAttributeName,
      nil]];

    [[UIBarButtonItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                          [UIFont fontWithName:KCustomeFont size:16.0],NSFontAttributeName,
                                                          nil] forState:UIControlStateNormal];
    
    [FIRApp configure];
    
    //Add an observer for handling a token refresh callback
    
    [[NSNotificationCenter defaultCenter ] addObserver:self selector:@selector(tokenRefreshCallback:)
                                                            name: kFIRInstanceIDTokenRefreshNotification
                                                            object:nil];
    
    
    // Request Permission for notification from user nabeel coding
    
    UIUserNotificationType allNotificationTypes = (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
    
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes: allNotificationTypes categories: nil];
    
    [application registerUserNotificationSettings:settings];
    
    // request for the apple push notification service
    
    [application registerForRemoteNotifications];
    
    return [[FBSDKApplicationDelegate sharedInstance]application:application didFinishLaunchingWithOptions:launchOptions];
    
    return YES;
}
+ (AppDelegate*)sharedDelegate
{
    return (AppDelegate*)[UIApplication sharedApplication].delegate;
}

#pragma spiningView

-(void)showLoadingViewForView:(UIViewController*)viewController withText:(NSString*)text
{
    if (self.loadingView == nil)
        self.loadingView = [[[NSBundle mainBundle] loadNibNamed:@"LoadingView" owner:viewController options:nil] lastObject];
    self.loadingView.frame = viewController.view.frame;
    self.loadingView.typeLabel.text = text;
    [self.loadingView loadAnimation];
    [self.loadingView setBackgroundColor:[UIColor clearColor]];
    [viewController.view addSubview:self.loadingView];
    [viewController.view bringSubviewToFront:self.loadingView];
}
-(void)hideLoadingViewForView:(UIViewController*)viewController
{
    [self.loadingView removeFromSuperview];
}
-(void)ShowActivityView:(UIViewController*)viewController
{
    UIView *view = [viewController.view viewWithTag:707];
    if (view)
    {
        [view removeFromSuperview];
        [[AppDelegate sharedDelegate] hideLoadingViewForView:viewController];
        
    }
    else
    {
        view = [[UIView alloc] initWithFrame:viewController.view.bounds];
        view.tag = 707;
        view.alpha = 0.5;
        
        [view setBackgroundColor:[UIColor whiteColor]];
        [viewController.view addSubview:view];
        [viewController.view bringSubviewToFront:view];
        [[AppDelegate sharedDelegate] showLoadingViewForView:viewController withText:@"Loading Umrah Packages..."];
    }
}


#pragma firebasePushNotification

-(void) tokenRefreshCallback:(NSNotification *)notification{
    
    NSString *refreshedToken = [[FIRInstanceID instanceID] token];
    NSLog(@"instance iD Tocken %@",refreshedToken);
    
    //Connect to  FCM SINCE CONNECTION may have failed when attempted before having a token
    
    [self connectToFirebase];
    
}

-(void)connectToFirebase{
    [[FIRMessaging messaging] connectWithCompletion:^(NSError * _Nullable error) {
        if(error != nil){
            NSLog(@"unable to connect to FCM . %@",error);
        }
        else{
            NSLog(@"Connect to fcm.");
        }
    }];
}





#pragma PushNotifcation

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSString *token = [[[[deviceToken description]
                         stringByReplacingOccurrencesOfString:@"<" withString:@""]
                        stringByReplacingOccurrencesOfString:@">" withString:@""]
                       stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    [FMUser defaultUser].deviceToken = token;
    DLog(@"device token: %@", token);
}
-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(nonnull void (^)(UIBackgroundFetchResult))completionHandler
{
    NSLog(@"Message ID",userInfo[@"gcm.message.id"]);
    NSLog(@"%@",userInfo);
}
//   [FBLoginManager checkFBUser];
/*
 if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
 [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
 [[UIApplication sharedApplication] registerForRemoteNotifications];
 }
 else {
 DLog(@"Unable To Register");
 }
 
 [application setApplicationIconBadgeNumber:0];
 */
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    [[FIRMessaging messaging] disconnect];
    NSLog(@"disconnect from fcm");
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    [FBSDKAppEvents activateApp];
    [self connectToFirebase];
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "com.Focusteck.Faremakers" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Faremakers" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Faremakers.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        DLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            DLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation
            ];
}

//NSError * err;
//NSData * jsonData = [NSJSONSerialization dataWithJSONObject:loginData options:0 error:&err];
//NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];

@end
