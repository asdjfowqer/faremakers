//
//  FMWebViewController.h
//  Faremakers
//
//  Created by Mac1 on 29/03/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FMWebViewDelegate <NSObject>

-(void)fmWebViewDidRedirectedToPaymentURL;

@end

@interface FMWebViewController : UIViewController<UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (nonatomic,assign) id<FMWebViewDelegate> adelegate;
@property (assign,nonatomic) NSString *html;


@end
