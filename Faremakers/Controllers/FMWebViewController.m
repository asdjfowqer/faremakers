//
//  FMWebViewController.m
//  Faremakers
//
//  Created by Mac1 on 29/03/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "FMWebViewController.h"
#import "Constants.h"

@interface FMWebViewController ()

@end

@implementation FMWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //[self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"Google"]]];
    [self.webView loadHTMLString:self.html baseURL:nil];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    DLog(@"%@",webView.request.URL.absoluteString);
    [self compareBaseUrlWithWebViewUrl:webView.request.URL.absoluteString];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self compareBaseUrlWithWebViewUrl:[self findErorBaseURL:error.userInfo]];
    DLog(@"%@",webView.request.URL.absoluteString);
}

- (IBAction)doneButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];

}

- (NSString*)findErorBaseURL:(NSDictionary*)userInfo
{
    if (userInfo) {
        return  [userInfo objectForKey:@"NSErrorFailingURLStringKey"];
    }
    return @"";
}
-(void)compareBaseUrlWithWebViewUrl:(NSString*)urlString
{
    if ([urlString containsString:@"http://localhost:55336/Home/PaymentGatewayReturnUrl"])
    {
        [self dismissViewControllerAnimated:YES completion:nil];
        if([self.adelegate respondsToSelector:@selector(fmWebViewDidRedirectedToPaymentURL)])
        {
            [self.adelegate fmWebViewDidRedirectedToPaymentURL];
        }
        DLog(@"got it");
    }
    else
        DLog(@"Still working");
   // if
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
