//
//  AppDelegate.h
//  Faremakers
//
//  Created by Mac1 on 16/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import <UIKit/UIKit.h>
@import Firebase;
@import FirebaseMessaging;
#import <CoreData/CoreData.h>

@class LoadingView;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) LoadingView *loadingView;
@property (strong, nonatomic) NSString *transactionId;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

+ (AppDelegate*)sharedDelegate;
- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;
- (void)showLoadingViewForView:(UIViewController*)viewController withText:(NSString*)text;
- (void)hideLoadingViewForView:(UIViewController*)viewController;
- (UIStoryboard*)getManStoryBoard;
- (UINavigationController*)setUpNavigationControllerWithRootView:(id)rootView;

@end

