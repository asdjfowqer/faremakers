//
//  AirLinesNameData.h
//  Faremakers
//
//  Created by Mac1 on 14/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AirLinesNameData : NSObject

@property (nonatomic) NSArray *airLinesName;

@property (nonatomic) NSArray *airPortsName;


+ (id)defaultAirLinesData;

- (NSString*)getAirlineNameByCode:(NSString*)code;
- (NSString*)getAirPortNameByCode:(NSString*)code;

@end
