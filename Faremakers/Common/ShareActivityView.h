//
//  ShareActivityView.h
//  Faremakers
//
//  Created by Mac1 on 01/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShareActivityView : UIActivityViewController

- (void)showItSelf;

@end
