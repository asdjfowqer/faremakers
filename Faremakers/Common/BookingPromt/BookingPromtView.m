//
//  BookingPromtView.m
//  Faremakers
//
//  Created by Mac1 on 10/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "BookingPromtView.h"
#import "NSString+Email.h"
#import "CheckOutItems.h"
#import "Travellers.h"
#import "FMUser.h"

@interface BookingPromtView ()<UITextFieldDelegate>

@end

@implementation BookingPromtView

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUserInfo];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)crossBtnSelector:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
	[self informDelegateWithResponse:NO];
	
}

-(void)informDelegateWithResponse:(BOOL)submit
{
    [self dismissViewControllerAnimated:YES completion:nil];

	if (_delegate)
	{
		if ([_delegate respondsToSelector:@selector(userSubmitResponse:)])
		{
			[self.delegate  userSubmitResponse:submit];
		}
	}
}

-(IBAction)submitBtnSelector:(id)sender
{
    
    if (self.nameField.text.length < 3) {
        [self showErrorAlert:@"Name must Contains 3 letters"];
    }
//    else if (![self validatePhoneNumber:self.phoneField.text]) {
//        [self showErrorAlert:@"Please enter valid Phone Number"];
//    }
    else if (![self.emailField.text isValidEmail]) {
        [self showErrorAlert:@"Please enter valid Email Address"];
    }
    else
    {
        [self.view endEditing:YES];
//        [self showSuccessAlert];
	    [self informDelegateWithResponse:YES];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.nameField) {
        [self.phoneField becomeFirstResponder];
    }
    else if (textField == self.phoneField) {
        [self.emailField becomeFirstResponder];
    }
    else
    {
        [self.emailField becomeFirstResponder];
    }
    return YES;
}

-(void)showSuccessAlert
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Submitted" message:@"Thank you for submitting query we will get back to you soon!" preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:YES completion:nil];

    }]];
    [self presentViewController:alertController animated:YES completion:nil];

}
-(void) showErrorAlert :(NSString*)message
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:message preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
    
}
- (void)setUserInfo
{
    self.emailField.text = [FMUser defaultUser].email;
    self.phoneField.text = [FMUser defaultUser].phoneNumber;
    if ([self.passenger isPassengerInfoAvaialble])
        self.nameField.text =  [self.passenger convertIntoString];
    else
        self.nameField.text =  [self.checkOutItem.passenger convertIntoString];

}
-(BOOL)validatePhoneNumber:(NSString*)phoneNumber
{
   // NSString *phoneRegex = @"[789][0-9]{9}";
    NSString *phoneRegex = @"[03][0-9]{3}([0-9]{7})?";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    return [test evaluateWithObject:phoneNumber];
}
@end
