//
//  BookingPromtView.h
//  Faremakers
//
//  Created by Mac1 on 10/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CheckOutItems;
@class Passengers;

@protocol BookingPromtViewDelegate <NSObject>

-(void)userSubmitResponse:(BOOL)submit;

@end


@interface BookingPromtView : UIViewController

@property (nonatomic,weak) id <BookingPromtViewDelegate> delegate;

@property (weak, nonatomic) IBOutlet UITextField *phoneField;
@property (weak, nonatomic) IBOutlet UITextField *emailField;
@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (nonatomic) CheckOutItems *checkOutItem;
@property (nonatomic) Passengers *passenger;



-(IBAction)crossBtnSelector:(id)sender;
-(IBAction)submitBtnSelector:(id)sender;
- (void)setUserInfo;

@end
