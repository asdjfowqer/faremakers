//
//  Constants.h
//  ElectionApp
//
//  Created by Mac1 on 09/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#ifndef Constant_h
#define Constant_h

#define BASE_URL_PROMO_PROD @"https://www.faremakers.com/admin/"
#define BASE_URL_RELEASE    @"https://www.faremakers.com/api/"

#define BASE_URL_DEVELOPMENT @"http://192.168.0.119:8080/api/"
#define BASE_URL_PROMO_DEV @"http://192.168.0.119:8080/admin/"

#define KMAXWIDTH [UIScreen mainScreen].bounds.size.width
#define KMAXHEIGHT [UIScreen mainScreen].bounds.size.height
#define KCurrencyCode [[NSLocale currentLocale] objectForKey:NSLocaleCurrencyCode]

#if DEBUG
//#define BASE_URL BASE_URL_DEVELOPMENT
//#define BASE_URL_2 BASE_URL_DEVELOPMENT
//#define BASE_URL_PROMO BASE_URL_PROMO_DEV
#define BASE_URL BASE_URL_RELEASE
#define BASE_URL_2 BASE_URL_RELEASE
#define BASE_URL_PROMO BASE_URL_PROMO_PROD

#else
#define BASE_URL BASE_URL_RELEASE
#define BASE_URL_2 BASE_URL_RELEASE
#define BASE_URL_PROMO BASE_URL_PROMO_PROD
#endif

//#define _LOG_TO_CONSOLE_ //#undef _LOG_TO_CONSOLE_
#ifdef _LOG_TO_CONSOLE_
#define DLog(format, ...) NSLog(format, ##__VA_ARGS__)
#else
#define DLog(format, ...)
#endif


#define Api_Key              @"API_KEY_TOBE_CHANGE"
#define Get_Cities_API_URL   @"Activity/getCities?searchString="
#define Flights_API_URL      @"Activity/SearchFlightsModel"
#define Promotions_API_URL   @"promotions/"
#define Hotel_API_URL        @"Hotels/SearchHotels/"
#define Cars_API_URL         @"Hotels/SearchCars/"
#define FlightsRule_API_URL  @"Activity/AirRules"
#define Umrah_API_URL        @"packages/umrahs/"
#define FareCalendar_API_URL @"Activity/fareCalender"
#define SignUp_UserName      @"isusernameexists?username="
#define SignUp_EmailCheck    @"isemailexists?email="
#define SignUp_PhoneCheck    @"isphonenumberexists?phonenumber="
#define Pass_Recover_URL     @"reqpwdrecovery?email="
#define Holiday_API_URL      @"Activity/getholidaypackages"
#define UserProfil_URL       @"getuserprofile"
#define UserBooking_URL      @"userBooking"
#define Edit_Profile_Pic     @"edituserprofilepicture"
#define Login_URL            @"login"
#define SignUp_URL           @"signup"
#define FBLogin_URL          @"fblogin"
#define Update_Profile_URL   @"edituserprofile"

#define Booking_URL_Release          @"Activity/BookFlight"
#define Booking_URL_Development      Booking_Payment_URL
#define Booking_URL				  Booking_URL_Development

#define Promocode_URL	    @"GetPromotionPercentage?code="
#define Book_Packages_URL	    @"Hotels/BookPackages"

#define Listings_URL	    @"Hotels/getBookingList?authToken="
#define Listings_Package_URL	    @"Hotels/getPackageBookingList?authToken="

#define Cancel_booking		@"Hotels/cancelBooking"
#define Cancel_Package_booking		@"Hotels/cancelPackageBooking"


#define Booking_Paid_URL     Booking_Paid_Flight_URL

#define Booking_Payment_URL      @"Activity/GetWebView"
#define Booking_Paid_Flight_URL     @"Hotels/bookflight"
#define Booking_Paid_Hotel_URL     @"Hotels/bookHotel"
#define Booking_Paid_Car_URL     @"Hotels/bookCar"
#define Booking_User_Exists_URL     @"Activity/CheckEmailExistence?emailAddress="

#define KDoubleDateTableCell    @"DoubleDateTableCell"
//#define KCustomeFont    @"HelveticaNeue-Thin"
//#define KCustomeRagularFont  @"HelveticaNeue"

#define KCustomeFont    @"DIN-Regular"
#define KCustomeRagularFont  @"DIN-Medium"

#define kLineHeightSpacing  1.0f

#define KFBUserDefualtKey @"fbuser"
#define FMUserDefualtKey @"user-key"
#define FMUserEmailKey @"user-email"


#define HotelFilterPriceStrings  @[@"Below 5K",@"5K - 10K",@"Above 10K"]
#define HolidayFilterPriceStrings  @[@"Below 30K",@"30K - 70K",@"Above 70K"]
#define CarsFilterPriceStrings  @[@"Below 200",@"200 - 500",@"Above 500"]
#define KDayTimingIntervals    @[@"0", @"39600", @"61200",@"75600",@"86400"]
#define KTimeInterval 86400
#define KFareCalanderBarWidth 40.f


#endif /* Constant_h */
