//
//  HotelFilterColletionCell.h
//  Faremakers
//
//  Created by Mac1 on 17/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//


#import <UIKit/UIKit.h>

@interface PriceFilterColletionCell : UICollectionViewCell

@property (nonatomic , weak) IBOutlet UILabel *priceLabel;

-(void)loadCellForPrice:(NSString*)price withAllPrices:(NSArray*)prices atIndexPath:(NSIndexPath*)indexPath;

@end
