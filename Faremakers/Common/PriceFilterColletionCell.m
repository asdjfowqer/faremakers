//
//  PriceFilterColletionCell.m
//  Faremakers
//
//  Created by Mac1 on 17/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "PriceFilterColletionCell.h"
#import "UILabel+FontName.h"
#import "Constants.h"
#import "HotelFilterItem.h"

@implementation PriceFilterColletionCell

- (void)awakeFromNib
{
    [self.priceLabel changeFontFamily];
}
-(void)loadCellForPrice:(NSString*)price withAllPrices:(NSArray*)prices atIndexPath:(NSIndexPath*)indexPath
{
    
    _priceLabel.text = prices[indexPath.row];
    NSInteger index = [prices indexOfObject:price];
    if (index == indexPath.row) {
        _priceLabel.textColor = [UIColor blueColor];
    }
    else
    {
        _priceLabel.textColor = [UIColor lightGrayColor];

    }
    
}


@end
