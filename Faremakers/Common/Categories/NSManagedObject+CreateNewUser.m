//
//  NSManagedObject+CreateNewUser.m
//  Faremakers
//
//  Created by Mac1 on 03/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "NSManagedObject+CreateNewUser.h"

@implementation NSManagedObject (CreateNewUser)

+(instancetype)createNewUserWithKey:(NSString*)key andValue:(NSString*)value inContext:(NSManagedObjectContext*)context
{
    NSString *entityName = NSStringFromClass(self);
    id object = [self findUserWithKey:key andValue:value inContext:context];
    if (object == nil) {
        object = [NSEntityDescription insertNewObjectForEntityForName:entityName inManagedObjectContext:context];
    }
    return object;
    //return [self findUserWithKey:key andValue:value inContext:context];
}

+(instancetype)findUserWithKey:(NSString*)key andValue:(NSString*)value inContext:(NSManagedObjectContext*)context
{
    NSString *entityName = NSStringFromClass(self);
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entityName];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"SELF.%@ = %@",key,value];
    fetchRequest.fetchLimit = 1;
    NSError *error;
    NSArray *array = [context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        return nil;
    }
    return  [array firstObject];
    
}
+(instancetype)FetchAlll:(NSString*)key andValue:(NSString*)value inContext:(NSManagedObjectContext*)context
{
    NSString *entityName = NSStringFromClass(self);
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entityName];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"%@ CONTAINS[cd] %@",key,@".com"];
    fetchRequest.fetchLimit = 1;
    NSError *error;
    NSArray *array = [context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        return nil;
    }
    return  [array firstObject];
    
}

@end
