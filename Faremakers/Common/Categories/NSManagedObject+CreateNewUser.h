//
//  NSManagedObject+CreateNewUser.h
//  Faremakers
//
//  Created by Mac1 on 03/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSManagedObject (CreateNewUser)

+(instancetype)createNewUserWithKey:(NSString*)key andValue:(NSString*)value inContext:(NSManagedObjectContext*)context;
+(instancetype)findUserWithKey:(NSString*)key andValue:(NSString*)value inContext:(NSManagedObjectContext*)context;
+(instancetype)FetchAlll:(NSString*)key andValue:(NSString*)value inContext:(NSManagedObjectContext*)context;
@end
