//
//  NSString+Height.m
//  Faremakers
//
//  Created by MaheepK on 26/08/15.
//  Copyright (c) 2015 Ergun Development LLC. All rights reserved.
//

#import "NSString+Height.h"

@implementation NSString (Height)

+ (CGRect ) setAttributeWithString : (NSString *) string withLineSpacing:(int) lineSpacing withSize:(CGSize )sizeMake withFont:(UIFont *)font
{
    if (string == nil) {
        return CGRectZero;
    }
    NSDictionary *attributeDictionary = @{NSFontAttributeName:font};
    
    NSMutableAttributedString * attributeString = [self setAttributeWithString:string withLineSpacing:lineSpacing withFont:font setTextAlignment:NSTextAlignmentLeft attributedString:nil];
    NSStringDrawingContext * con = [NSStringDrawingContext new];
    
    CGRect descriptionRect = [attributeString.string
                              boundingRectWithSize:sizeMake
                              options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
                              attributes:attributeDictionary
                              context:con];
    
    CGFloat additionHeight = 0.0f;
    CGFloat noOfLine = ceilf(descriptionRect.size.height / font.pointSize);
    
    CGRect newRect = descriptionRect;
    newRect.size.height = ceilf(noOfLine * lineSpacing + descriptionRect.size.height + additionHeight);
    descriptionRect = newRect;
    
    return descriptionRect;
}

+ (NSMutableAttributedString *)setAttributeWithString:(NSString *)string withLineSpacing:(int)lineSpacing withFont:(UIFont *)font setTextAlignment:(NSTextAlignment)textAlignment attributedString:(NSAttributedString *)argAttributedString
{
    NSDictionary *attributeDictionary = @{NSFontAttributeName:font};
    
    NSMutableAttributedString *attributedString;
    
    if (argAttributedString)
    {
        attributedString = [[NSMutableAttributedString alloc] initWithAttributedString:argAttributedString];
        
        [attributedString addAttributes:attributeDictionary range:NSMakeRange(0, [argAttributedString.string length])];
        NSMutableParagraphStyle *paragrahStyle = [[NSMutableParagraphStyle alloc] init];
        [paragrahStyle setLineSpacing:lineSpacing];
        [paragrahStyle setAlignment:textAlignment];
        
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragrahStyle range:NSMakeRange(0, [argAttributedString.string length])];
    }
    else
    {
        attributedString = [[NSMutableAttributedString alloc] initWithString:string attributes:attributeDictionary];
        
        [attributedString addAttributes:attributeDictionary range:NSMakeRange(0, [string length])];
        NSMutableParagraphStyle *paragrahStyle = [[NSMutableParagraphStyle alloc] init];
        [paragrahStyle setLineSpacing:lineSpacing];
        [paragrahStyle setAlignment:textAlignment];
        
        [attributedString removeAttribute:NSParagraphStyleAttributeName range:NSMakeRange(0, [string length])];
        
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragrahStyle range:NSMakeRange(0, [string length])];
    }
    
    return attributedString;
}

+ (CGRect ) setAttributeWithString : (NSString *) string withLineSpacing:(int) lineSpacing withSize:(CGSize )sizeMake withFont:(UIFont *)font withLabel:(UILabel *)label setLabelTextColor:(UIColor *)color;
{

    
    NSMutableAttributedString * attributeString = [self setAttributeWithString:nil withLineSpacing:lineSpacing withFont:font setTextAlignment:NSTextAlignmentLeft attributedString:label.attributedText];
    label.font = font;
    label.attributedText = attributeString;
    
    NSStringDrawingContext * con = [NSStringDrawingContext new];
    
    CGRect descriptionRect = [attributeString.string
                              boundingRectWithSize:sizeMake
                              options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
                              attributes: @{NSFontAttributeName:font}
                              context:con];
    
    CGFloat additionHeight = 0.0f;
    CGFloat noOfLine = ceilf(descriptionRect.size.height / font.pointSize);
    
    CGRect newRect = descriptionRect;
    newRect.size.height = ceilf(noOfLine * lineSpacing + descriptionRect.size.height + additionHeight);
    descriptionRect = newRect;
    
    return descriptionRect;
    
}


+ (CGRect ) setAttributeWithString : (NSString *) string withLineSpacing:(int) lineSpacing withSize:(CGSize )sizeMake withFont:(UIFont *)font withLabel:(UILabel *)label setLabelTextColor:(UIColor *)color setTextAlignment:(NSTextAlignment)textAlignment
{
    
    NSMutableAttributedString * attributeString = [self setAttributeWithString:nil withLineSpacing:lineSpacing withFont:font setTextAlignment:textAlignment attributedString:label.attributedText];
    
    label.font = font;
    label.attributedText = attributeString;
    
    NSStringDrawingContext * con = [NSStringDrawingContext new];
    
    CGRect descriptionRect = [attributeString.string
                              boundingRectWithSize:sizeMake
                              options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
                              attributes: @{NSFontAttributeName:font}
                              context:con];
    
    CGFloat additionHeight = 0.0f;
    CGFloat noOfLine = floorf(descriptionRect.size.height / font.pointSize);
    
    CGRect newRect = descriptionRect;
    newRect.size.height = ceilf((noOfLine - 1) * lineSpacing + descriptionRect.size.height + additionHeight); // subtracting one from no of lines because line spacing is not added to line if noOfLine is 1.
    descriptionRect = newRect;
    
    return descriptionRect;
    
}

@end
