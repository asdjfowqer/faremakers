//
//  NSDate+DateComponents.h
//  Faremakers
//
//  Created by Mac1 on 16/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (DateComponents)

-(NSString*)getDayOfTheWeek;
-(NSString*)getMonth;
-(NSString*)getYear;
-(NSString*)getDayOfTheMonth;
-(NSString*)getFourDigitYear;

@end
