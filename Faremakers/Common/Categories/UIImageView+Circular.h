//
//  UIImageView+Circular.h
//  Faremakers
//
//  Created by Mac1 on 16/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (Circular)

-(void)addCircularRadius;
@end
