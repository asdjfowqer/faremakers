//
//  NSString+PhoneNumber.h
//  Faremakers
//
//  Created by Mac1 on 25/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (PhoneNumber)

-(BOOL)isValidPhoneNumber;

@end
