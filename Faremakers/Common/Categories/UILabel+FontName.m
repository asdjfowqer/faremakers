//
//  UILabel+FontName.m
//  Faremakers
//
//  Created by Mac1 on 06/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "UILabel+FontName.h"
#import "Constants.h"

@implementation UILabel (FontName)

- (void)changeFontToRagular
{
    [self setFont:[UIFont fontWithName:KCustomeRagularFont size:self.font.pointSize]];
}
- (void)changeFontFamily
{
    [self setFont:[UIFont fontWithName:KCustomeFont size:self.font.pointSize]];
}
- (void)changeFontFamilyWithSize:(float)fontSize
{
    [self setFont:[UIFont fontWithName:KCustomeFont size:fontSize]];
}
- (void)changeFontFamily:(NSString*)fontName WithSize:(float)fontSize
{
    [self setFont:[UIFont fontWithName:fontName size:fontSize]];
}
- (void)setSubstituteFontName:(NSString *)name UI_APPEARANCE_SELECTOR
{
    self.font = [UIFont fontWithName:name size:self.font.pointSize];
}
@end
