//
//  UILabel+FontName.h
//  Faremakers
//
//  Created by Mac1 on 06/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//


#import <UIKit/UIKit.h>

@interface UILabel (FontName)

- (void)changeFontFamily;

- (void)changeFontFamilyWithSize:(float)fontSize;

- (void)changeFontToRagular;

- (void)changeFontFamily:(NSString*)fontName WithSize:(float)fontSize;

- (void)setSubstituteFontName:(NSString *)name UI_APPEARANCE_SELECTOR;

@end
