//
//  NSDate+DateComponents.m
//  Faremakers
//
//  Created by Mac1 on 16/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "NSDate+DateComponents.h"

@implementation NSDate (DateComponents)

-(NSDateFormatter*)dateFormater
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterNoStyle];
    return formatter;
}
// Return Day of the Week 'Wed'
-(NSString*)getDayOfTheWeek
{
    NSDateFormatter *dateFormatter = [self dateFormater];
    [dateFormatter setDateFormat:@"EEE"];
    return [[dateFormatter stringFromDate:self] uppercaseString];
}
// Return Month NAME 'Dec'
-(NSString*)getMonth
{
    NSDateFormatter *dateFormatter = [self dateFormater];
    [dateFormatter setDateFormat:@"MMM"];
    return [[dateFormatter stringFromDate:self] capitalizedString];
}
// Return Year '15'
-(NSString*)getYear
{
    NSDateFormatter *dateFormatter = [self dateFormater];
    [dateFormatter setDateFormat:@"yy"];
    return [dateFormatter stringFromDate:self];
}
// Return Year '2015'
-(NSString*)getFourDigitYear
{
    NSDateFormatter *dateFormatter = [self dateFormater];
    [dateFormatter setDateFormat:@"yyyy"];
    return [dateFormatter stringFromDate:self];
}
// Return Date Of The Month '16'
-(NSString*)getDayOfTheMonth
{
    NSDateFormatter *dateFormatter = [self dateFormater];
    [dateFormatter setDateFormat:@"dd"];
    return [dateFormatter stringFromDate:self];
}
- (NSString*)getTimeOnlyFromDate:(NSDate*)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    return [dateFormatter stringFromDate:date];
    
}

@end
