//
//  NSString+Email.h
//  ElectionApp
//
//  Created by Mac1 on 09/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Email)

- (BOOL)isValidEmail;

@end
