//
//  UIImageView+Circular.m
//  Faremakers
//
//  Created by Mac1 on 16/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "UIImageView+Circular.h"

@implementation UIImageView (Circular)

-(void)addCircularRadius
{
    self.layer.cornerRadius = self.frame.size.height/2;
    self.layer.masksToBounds = YES;

}

@end
