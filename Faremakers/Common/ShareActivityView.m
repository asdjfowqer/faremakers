//
//  ShareActivityView.m
//  Faremakers
//
//  Created by Mac1 on 01/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "ShareActivityView.h"

@interface ShareActivityView ()

@end

@implementation ShareActivityView

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)showItSelf
{
    UIViewController *rootController =(UIViewController*)[[[[UIApplication sharedApplication]delegate] window] rootViewController];
    [rootController presentViewController:self animated:YES completion:nil];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
