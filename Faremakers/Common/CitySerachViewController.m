//
//  CitySerachViewController.m
//  Faremakers
//
//  Created by Mac1 on 21/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "CitySerachViewController.h"
#import "CitiesApiManager.h"
#import <AFNetworking/AFHTTPSessionManager.h>
#import "Cities.h"
#import "Constants.h"

@interface CitySerachViewController () <UISearchBarDelegate,GetAllCitiesDelegate >
{
    GetAllCitiesManager *citiesDataManager;
    UIActivityIndicatorView *activityIndicator;
    NSInteger totalRequests;
    NSArray *citiesItems;
}


@end

@implementation CitySerachViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    totalRequests = 0;
    citiesDataManager = [[GetAllCitiesManager alloc] init];
    citiesDataManager.adelegate = self;
    [self barButtonWithActivityIndicator];
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return (citiesItems.count)?citiesItems.count:1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = (UITableViewCell *)[self.tableView dequeueReusableCellWithIdentifier:@"CitySerachCell" forIndexPath:indexPath];
    if (citiesItems.count) {
        
        Cities *city = citiesItems[indexPath.row];
        cell.textLabel.text = city.airPortName;
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ , %@",[[city.text componentsSeparatedByString:@","] firstObject],city.countryName];
    }
    else
    {
        cell.textLabel.text = @"No Record";
        cell.detailTextLabel.text = @"";
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    if ([self.adelegate respondsToSelector:@selector(CitySearchViewController:didSelectCity:)]) {
        totalRequests = 0;
        [self.adelegate CitySearchViewController:self didSelectCity:citiesItems[indexPath.row]];
    }
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (searchBar.text.length > 0)
    {
        [self getCityListWithQueryString:searchBar.text];
    }
    else
    {
        [activityIndicator stopAnimating];
    }
    
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    if (searchBar.text.length > 0)
    {
        [self getCityListWithQueryString:searchBar.text];
    }
    [searchBar resignFirstResponder];
    
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}

-(void)getCityListWithQueryString:(NSString*)str
{
    totalRequests++;
    [citiesDataManager getCityListWithQueryString:str];
    [self showActivityView:YES];
}
-(void)getAllCitiesDelegateDidRecieveResponse:(id)object
{
    totalRequests--;
    [self showActivityView:NO];
    citiesItems = object;
    [self.tableView reloadData];
}

- (void)barButtonWithActivityIndicator
{
    activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    [activityIndicator hidesWhenStopped];
    UIBarButtonItem *activityItem = [[UIBarButtonItem alloc] initWithCustomView:activityIndicator];
    self.navigationItem.rightBarButtonItem = activityItem;
}
- (void)showActivityView:(BOOL)show
{
    [activityIndicator setHidden:NO];
    if (show)
        [activityIndicator startAnimating];
    else
        (totalRequests != 0)?:[activityIndicator stopAnimating];
}
-(void) viewWillDisappear:(BOOL)animated
{
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound)
    {
        // Navigation button was pressed. Do some stuff
        totalRequests = 0;
        [self CancleAllTask];
    }
    [super viewWillDisappear:animated];
}
-(void) CancleAllTask
{
    [activityIndicator stopAnimating];
    [citiesDataManager CancleAllTask];
}
@end
