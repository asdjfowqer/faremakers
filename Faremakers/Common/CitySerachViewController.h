//
//  CitySerachViewController.h
//  Faremakers
//
//  Created by Mac1 on 21/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//



@protocol CityNameSearchDelegate <NSObject>

-(void) CitySearchViewController:(id)viewController didSelectCity:(id)citie;

@end

@interface CitySerachViewController : UITableViewController

@property (nonatomic , weak) id<CityNameSearchDelegate> adelegate;


@end
