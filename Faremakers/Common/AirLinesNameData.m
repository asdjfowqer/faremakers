//
//  AirLinesNameData.m
//  Faremakers
//
//  Created by Mac1 on 14/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "AirLinesNameData.h"

@implementation AirLinesNameData

static AirLinesNameData *defaultAirLinesData = nil;

+ (id)defaultAirLinesData
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        defaultAirLinesData = [[self alloc] init];
    });
    return defaultAirLinesData;
}

- (id)init
{
    self = [super init];
    [self loadAirLinesNameDataBase];
    [self loadAirPortsNameDataBase];
    return self;
}

- (NSString*)getAirlineNameByCode:(NSString*)code
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"airlineCode == %@ ",code];
    NSArray *filteredArray = [_airLinesName filteredArrayUsingPredicate:predicate];
    if (filteredArray.count)
        return [[filteredArray objectAtIndex:0] objectForKey:@"airlineName"];
    return code;
    
}
- (NSString*)getAirPortNameByCode:(NSString*)code
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"iata == %@ ",code];
    NSArray *filteredArray = [_airPortsName filteredArrayUsingPredicate:predicate];
    if (filteredArray.count)
        return [[filteredArray objectAtIndex:0] objectForKey:@"name"];
    return code;
}

- (void)loadAirLinesNameDataBase
{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"airlines" ofType:@"json"];
    NSData *JSONData = [NSData dataWithContentsOfFile:filePath options:NSDataReadingMappedIfSafe error:nil];
    NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:JSONData options:NSJSONReadingMutableContainers error:nil];
    
    _airLinesName = [[NSArray alloc] initWithArray:[jsonObject objectForKey:@"data"]];
    
}
- (void)loadAirPortsNameDataBase
{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"airports" ofType:@"json"];
    NSData *JSONData = [NSData dataWithContentsOfFile:filePath options:NSDataReadingMappedIfSafe error:nil];
    NSArray *jsonObject = [NSJSONSerialization JSONObjectWithData:JSONData options:NSJSONReadingMutableContainers error:nil];
    
    _airPortsName = [[NSArray alloc] initWithArray:jsonObject];
    
}
@end
