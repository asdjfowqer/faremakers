//
//  LoginManager.h
//  ElectionApp
//
//  Created by Mac1 on 09/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LoginManager : NSObject

+(void)loginUserWithEmail:(NSString*)email
              andPassword:(NSString*)password
      withCompletionBlock:(void(^)(id response,NSError* error))completionBlock;
+(void)loginUserWithFBData:(id)fbData
       withCompletionBlock:(void(^)(id response,NSError* error))completionBlock;

+(void)updateUserPasswordWithData:(id)passWords
             withCompletionBlock:(void(^)(id response,NSError* error))completionBlock;

+(void)recoverUserPasswordWithEmail:(NSString*)email
               withCompletionBlock:(void(^)(id response,NSError* error))completionBlock;

+(void)getUserPrfileDataFromServerWithCompletionBlock:(void(^)(id response,NSError* error))completionBlock;

@end
