//
//  SignUpManger.h
//  ElectionApp
//
//  Created by Mac1 on 09/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SignUpManger : NSObject

+(void)createUserAccountWithParameters:(NSDictionary*)parameters
                   withcompletionBlock:(void(^)(id response,NSError *error))completionBlock;
+(void)checkPhoneForUserSignUp:(NSString*)phoneNumber
           withcompletionBlock:(void(^)(id response,NSError *error))completionBlock;
+(void)checkEmailForUserSignUp:(NSString*)email
           withcompletionBlock:(void(^)(id response,NSError *error))completionBlock;
+(void)checkUserNameForUserSignUp:(NSString*)userName
              withcompletionBlock:(void(^)(id response,NSError *error))completionBlock;
@end
