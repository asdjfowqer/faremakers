//
//  FBLoginManager.h
//  ElectionApp
//
//  Created by Mac1 on 10/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FBLoginManager : NSObject

+(void)loginUsingFacebookFromViewController:(id)viewController withCompletionBlock:(void(^)(id loginData,NSError* error))completionBlock;

+ (void)checkFBUser;

+ (void)logoutFacebook;

+ (void)getUserLikes;

@end
