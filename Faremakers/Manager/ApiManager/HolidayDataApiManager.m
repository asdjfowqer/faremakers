//
//  HolidayDataApiManager.m
//  Faremakers
//
//  Created by Mac1 on 09/03/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "HolidayDataApiManager.h"
#import <AFNetworking/AFHTTPSessionManager.h>
#import "HolidayItem.h"
#import "Constants.h"

@implementation HolidayDataApiManager

+ (NSArray*)parseHolidayResponseData:(NSArray*)JSONData
{
    
    NSMutableArray *packages = [[NSMutableArray alloc] init];
    
    for (NSDictionary *dict in JSONData)
    {
        HolidayItem*package = [HolidayItem itemWithDictionary:dict];
        [packages addObject:package];
    }
    
    return packages;
}
+ (void)getHolidayListWithSuccessBlock:(void (^)(id response))success faliureBlock:(void(^)(NSError* error))failure
{
    NSString *url = [NSString stringWithFormat:@"%@%@",BASE_URL,Holiday_API_URL];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setTimeoutInterval:20.0];
    
    [manager POST:url parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
    {
        dispatch_async(dispatch_get_main_queue(), ^
        {
            if ([self checkStatus:responseObject] && [self checkResponseResult:responseObject])
                success([self parseHolidayResponseData:[[responseObject objectForKey:@"data"] objectForKey:@"serviceResponse"]]);
            else
                failure([NSError errorWithDomain:@"com.faremakersApp" code:14 userInfo:[NSDictionary dictionaryWithObject:@"No Data Found" forKey:NSLocalizedDescriptionKey]]);
            
        });
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            failure(error);
        });
    }];
}
+ (BOOL) checkStatus:(NSDictionary*)jsonObject
{
    return [[jsonObject objectForKey:@"status"] isEqualToNumber:[NSNumber numberWithInt:200]];
}
+ (BOOL) checkResponseResult:(NSDictionary*)jsonObject
{
    NSArray *result = [[jsonObject objectForKey:@"data"] objectForKey:@"serviceResponse"];
    return result.count;
}
@end
