//
//  FlightRulesApiManger.m
//  Faremakers
//
//  Created by Mac1 on 03/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "FlightRulesApiManger.h"
#import <AFNetworking/AFHTTPSessionManager.h>
#import "AirLineRules.h"
#import "Constants.h"


@implementation FlightRulesApiManger

#pragma mark AirRules


- (BOOL) checkStatus:(NSDictionary*)jsonObject
{
    return [[jsonObject objectForKey:@"status"] isEqualToNumber:[NSNumber numberWithInt:200]];
}
- (BOOL) checkJsonData:(NSDictionary*)jsonObject
{
    NSDictionary *dict = [[[jsonObject objectForKey:@"data"]objectForKey:@"serviceResponse"] objectForKey:@"applicationResults"];
    NSArray *allKeys = [dict allKeys];
    return ([allKeys containsObject:@"error"])?NO:YES;
}
- (NSArray*)getAllFlightsRulesFromResponse:(NSDictionary*)JSONData
{
    NSArray *rulesArray = [[[[JSONData objectForKey:@"data"] objectForKey:@"serviceResponse"] objectForKey:@"fareRuleInfo"] objectForKey:@"rules"];
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    for (NSDictionary *dict in rulesArray)
    {
        AirLineRules *airLinePricedItinerary = [AirLineRules itemWithDictionary:dict];
        [array addObject:airLinePricedItinerary];
    }
    
    return [[NSArray alloc] initWithArray:array];
}
- (void) getFlightRulesDataWithRequestData:(NSDictionary*)dict
{
    NSString *Urlstr = [NSString stringWithFormat:@"%@%@",BASE_URL,FlightsRule_API_URL];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [manager.requestSerializer setTimeoutInterval:120.0];
    
    _currentTask = [manager POST:Urlstr parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
     {
         
         BOOL status = [self checkStatus:responseObject];
         if (status)
         {
             if ([self checkJsonData:responseObject])
                 [self.adelegate flightRulesApiMangerRecieveResponse:[self getAllFlightsRulesFromResponse:responseObject]];
             else
                 [self.adelegate flightRulesApiMangerDidFail:nil];
         }
         else
         {
             [self.adelegate flightRulesApiMangerDidFail:nil];
         }
         
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
     {
         [self.adelegate flightRulesApiMangerDidFail:nil];
     }];
   
    
}
-(void)cancleAllTask
{
    [_currentTask cancel];
}

@end
