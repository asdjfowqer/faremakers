//
//  GetAllCitiesManager.m
//  Faremakers
//
//  Created by Mac1 on 11/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "CitiesApiManager.h"
#import <AFNetworking/AFHTTPSessionManager.h>
#import "Cities.h"
#import "Constants.h"


@implementation GetAllCitiesManager


- (NSArray*)getAllCitesDataFromResponse:(NSDictionary*)JSONData
{
    
    BOOL status = [self checkStatus:JSONData];
    if (status)
    {
        NSArray *CitiesArray = [JSONData objectForKey:@"data"];
        return [[NSArray alloc] initWithArray:[Cities itemsWithArray:CitiesArray]];
    }
    return [[NSArray alloc] init];
    
}

- (BOOL) checkStatus:(NSDictionary*)jsonObject
{
    return [[jsonObject objectForKey:@"status"] isEqualToNumber:[NSNumber numberWithInt:200]];
}

-(void)getCityListWithQueryString:(NSString*)urlStr
{
    if (_currentTask) {
        [_currentTask cancel];
        _currentTask = nil;
    }
    NSString *Urlstr = [NSString stringWithFormat:@"%@%@%@",BASE_URL,Get_Cities_API_URL,urlStr];
    Urlstr = [Urlstr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager.requestSerializer setTimeoutInterval:20.0];
    
    _currentTask = [manager GET:Urlstr parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject)
     {
         [self.adelegate getAllCitiesDelegateDidRecieveResponse:[self getAllCitesDataFromResponse:responseObject]];
     }
         failure:^(NSURLSessionTask *operation, NSError *error)
     {
         DLog(@"Error: %@", error);
         [self.adelegate getAllCitiesDelegateDidRecieveResponse:[[NSArray alloc] init]];

     }];
}
-(void)CancleAllTask
{
    [_currentTask cancel];
}

@end
