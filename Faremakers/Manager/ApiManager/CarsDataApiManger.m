//
//  CarsDataApiManger.m
//  Faremakers
//
//  Created by Mac1 on 01/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "CarsDataApiManger.h"
#import <AFNetworking/AFHTTPSessionManager.h>
#import "FMHotels.h"
#import "Constants.h"
#import "Cities.h"
#import "CarsItem.h"

@implementation CarsDataApiManger


- (NSArray*) parseCarsSearchData:(id)item
{
    NSArray *responseArray = [[item objectForKey:@"data"] objectForKey:@"serviceResponse"];
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    for (NSDictionary *dict in responseArray)
    {
        [array addObject:[CarsItem itemWithDictionary:dict]];
    }
    return array;
}

- (BOOL) checkJsonData:(NSDictionary*)jsonObject
{
    NSDictionary *dict = [jsonObject objectForKey:@"data"];
    return ([[dict objectForKey:@"error"] isEqualToString:@"0"])?YES:NO;
}

- (void) searchCarsDataForItem:(NSDictionary*)dict;
{
    
    NSString *Urlstr = [NSString stringWithFormat:@"%@%@",BASE_URL,Cars_API_URL];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager.requestSerializer setTimeoutInterval:20.0];
    
    _currentTask =[manager POST:Urlstr parameters:dict progress:nil success:^(NSURLSessionTask *task, id responseObject)
                   {
                       if ([self checkJsonData:responseObject])
                       {
                           [self.adelegate CarsDataManagerDidRecieveResponse:[self parseCarsSearchData:responseObject]];
                       }
                       else
                           [self.adelegate CarsDataManagerDidFail:nil];
                       
                   } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
                   {
                       DLog(@"Error: %@", error);
                       [self.adelegate CarsDataManagerDidFail:nil];
                       
                   }];
    [manager setDataTaskDidReceiveDataBlock:^(NSURLSession * _Nonnull session, NSURLSessionDataTask * _Nonnull dataTask, NSData * _Nonnull data) {
        
        [self.adelegate CarsDataManagerStartProgressing:(float)dataTask.countOfBytesReceived / (float)dataTask.countOfBytesExpectedToReceive];
    }];
}



+ (void) uploadPaidBookingDetail:(NSDictionary*)data success:(void (^)(id responseobject))successBlock failureBlock:(void (^)(NSError *error))failureBlock
{
	DLog(@"%@",data);
	
	NSString *Urlstr = [NSString stringWithFormat:@"%@%@",BASE_URL_2,Booking_Paid_Car_URL];
	
	AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
	[manager.requestSerializer setTimeoutInterval:20.0];
	
	[manager POST:Urlstr parameters:data progress:nil success:^(NSURLSessionTask *task, id responseObject){
		
		if (successBlock) {
			dispatch_async(dispatch_get_main_queue(), ^{
				
				DLog(@"Is of type: %@", [[responseObject objectForKey:@"status"] class]);
				
				if ([[responseObject objectForKey:@"status"] isEqualToNumber:[NSNumber numberWithInteger:200]])
				{
					successBlock(responseObject);
				}
				
			});
		}
		
	} failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error){
		if (failureBlock) {
			dispatch_async(dispatch_get_main_queue(), ^{
				failureBlock(error);
			});
		}
	}];
	
}

-(void)CancleAllTask
{
    [_currentTask cancel];
}

@end
