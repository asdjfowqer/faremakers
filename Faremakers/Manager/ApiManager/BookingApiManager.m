//
//  BookingApiManager.m
//  Faremakers
//
//  Created by Afnan Ahmad on 22/04/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "BookingApiManager.h"
#import <AFNetworking/AFHTTPSessionManager.h>
#import "Constants.h"


@implementation BookingApiManager


+ (void)postPackageBooking:(TransactionBookingPaidPackage *)packageTransaction withSuccessBlock:(void (^)(id responseobject))successBlock failureBlock:(void (^)(NSError *error))failureBlock
{
    NSString *urlString = [NSString stringWithFormat:@"%@%@", BASE_URL, Book_Packages_URL];
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

    DLog(@"%@", urlString);

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setTimeoutInterval:180.0];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];


    [manager POST:urlString parameters:[packageTransaction postPaymentDictionary] progress:nil success: ^(NSURLSessionTask *task, id responseObject)
     {
         DLog(@"Error: %@", responseObject);
         successBlock(responseObject);
     }
          failure: ^(NSURLSessionTask *operation, NSError *error)
     {
         DLog(@"Error: %@", error);
         failureBlock(error);
     }];
}

@end