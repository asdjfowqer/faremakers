//
//  PaymentApiManager.h
//  Faremakers
//
//  Created by Mac Mini - 2 on 13/04/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaymentApiManager : NSObject

+ (void) uploadBookingDetailData:(NSDictionary*)data success:(void (^)(id responseobject))successBlock failureBlock:(void (^)(NSError *error))failureBlock;

+(void) getPromoCode:(NSString*)promocode type:(NSString*)type withSuccessBlock:(void (^)(id responseobject))successBlock failureBlock:(void (^)(NSError *error))failureBlock;

+ (void) prepareNewUSerForBooking:(NSString*)email success:(void (^)(id responseobject))successBlock failureBlock:(void (^)(NSError *error))failureBlock;
@end
