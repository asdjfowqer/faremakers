//
//  HolidayDataApiManager.h
//  Faremakers
//
//  Created by Mac1 on 09/03/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HolidayDataApiManager : NSObject

+ (void)getHolidayListWithSuccessBlock:(void (^)(id response))success faliureBlock:(void(^)(NSError* error))failure;

@end
