//
//  ListingsApiManager.m
//  Faremakers
//
//  Created by Mac Mini - 2 on 15/04/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "ListingsApiManager.h"
#import <AFNetworking/AFHTTPSessionManager.h>
#import "Constants.h"
#import "FMUser.h"

@implementation ListingsApiManager


+(void)getBookingsListWithSuccess:(void (^)(id responseobject))successBlock failureBlock:(void (^)(NSError *error))failureBlock
{
	NSString* authToken = [FMUser defaultUser].authToken;
	NSString *urlString = [NSString stringWithFormat:@"%@%@%@",BASE_URL,Listings_URL,authToken];
	urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	
	
	DLog(@"%@",urlString);
	
	AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
	[manager.requestSerializer setTimeoutInterval:20.0];
	
	[manager GET:urlString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject)
     {
		DLog(@"Error: %@", responseObject);
		successBlock(responseObject);
		
	}
		failure:^(NSURLSessionTask *operation, NSError *error)
     {
		DLog(@"Error: %@", error);
		failureBlock(error);
	}];
}

+(void)getPackageBookingsListWithSuccess:(void (^)(id responseobject))successBlock failureBlock:(void (^)(NSError *error))failureBlock
{
    NSString* authToken = [FMUser defaultUser].authToken;
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@",BASE_URL,Listings_Package_URL,authToken];
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];


    DLog(@"%@",urlString);

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setTimeoutInterval:20.0];

    [manager GET:urlString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject)
     {
         DLog(@"Error: %@", responseObject);
         successBlock(responseObject);

     }
         failure:^(NSURLSessionTask *operation, NSError *error)
     {
         DLog(@"Error: %@", error);
         failureBlock(error);
     }];
}

+(void)cancelBookingWithBookingId:(NSString*)bookingid withSuccessBlock:(void (^)(id responseobject))successBlock failureBlock:(void (^)(NSError *error))failureBlock
{
	NSString *urlString = [NSString stringWithFormat:@"%@%@",BASE_URL,Cancel_booking];
	urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	
	NSDictionary* dict = @{@"bookingId":bookingid,@"authToken":[FMUser defaultUser].authToken};

	DLog(@"%@",urlString);
	
	AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
	[manager.requestSerializer setTimeoutInterval:20.0];
	
	[manager GET:urlString parameters:dict progress:nil success:^(NSURLSessionTask *task, id responseObject)
	 {
		 DLog(@"Error: %@", responseObject);
		 successBlock(responseObject);
		 
	 }
		failure:^(NSURLSessionTask *operation, NSError *error)
	 {
		 DLog(@"Error: %@", error);
		 failureBlock(error);
	 }];
}
+(void)cancelPackageBookingWithBookingId:(NSDictionary *)data withSuccessBlock:(void (^)(id responseobject))successBlock failureBlock:(void (^)(NSError *error))failureBlock
{
    NSString *urlString = [NSString stringWithFormat:@"%@%@",BASE_URL,Cancel_Package_booking];
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:data];
    [dict setObject:[FMUser defaultUser].authToken forKey:@"authToken"];
    
    DLog(@"%@",urlString);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setTimeoutInterval:20.0];
    
    [manager GET:urlString parameters:dict progress:nil success:^(NSURLSessionTask *task, id responseObject)
     {
         DLog(@"Error: %@", responseObject);
         successBlock(responseObject);
         
     }
         failure:^(NSURLSessionTask *operation, NSError *error)
     {
         DLog(@"Error: %@", error);
         failureBlock(error);
     }];
}

@end
