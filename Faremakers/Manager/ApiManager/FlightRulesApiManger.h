//
//  FlightRulesApiManger.h
//  Faremakers
//
//  Created by Mac1 on 03/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FlightRulesApiMangerDelegate <NSObject>

-(void) flightRulesApiMangerRecieveResponse:(id)object;
-(void) flightRulesApiMangerDidFail:(id)object;

@end

@interface FlightRulesApiManger : NSObject


@property (nonatomic, weak) id<FlightRulesApiMangerDelegate> adelegate;

@property (nonatomic) NSURLSessionDataTask *currentTask;

- (void) getFlightRulesDataWithRequestData:(NSDictionary*)dict;

- (void) cancleAllTask;

@end
