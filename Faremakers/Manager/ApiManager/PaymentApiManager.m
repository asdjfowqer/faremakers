//
//  PaymentApiManager.m
//  Faremakers
//
//  Created by Mac Mini - 2 on 13/04/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "PaymentApiManager.h"
#import <AFNetworking/AFHTTPSessionManager.h>
#import "Constants.h"

@interface PaymentApiManager ()

@end

@implementation PaymentApiManager

+ (void) uploadBookingDetailData:(NSDictionary*)data success:(void (^)(id responseobject))successBlock failureBlock:(void (^)(NSError *error))failureBlock
{
	DLog(@"%@",data);
	
		//	phoneNumber
		//	amount
		//	firstName
		//	lastName
		//	Currency
		//	OrderInfo
		//	OrderName
	
	NSString *Urlstr = [NSString stringWithFormat:@"%@%@",BASE_URL_2,Booking_URL];
	
	AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
//	[manager.requestSerializer setTimeoutInterval:20.0];
	
	[manager POST:Urlstr parameters:data progress:nil success:^(NSURLSessionTask *task, id responseObject){
		
		if (successBlock) {
			dispatch_async(dispatch_get_main_queue(), ^{
				
				DLog(@"Is of type: %@", [[responseObject objectForKey:@"status"] class]);
				
				if ([[responseObject objectForKey:@"status"] isEqualToNumber:[NSNumber numberWithInteger:200]])
				{
					successBlock(responseObject);
				}
				
			});
		}
		
	} failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error){
		if (failureBlock) {
			dispatch_async(dispatch_get_main_queue(), ^{
				failureBlock(error);
			});
			
			DLog(@"%@",error);
		}
	}];
	
}

+(void)getPromoCode:(NSString*)promocode type:(NSString*)type withSuccessBlock:(void (^)(id responseobject))successBlock failureBlock:(void (^)(NSError *error))failureBlock
{
	NSString *urlString = [NSString stringWithFormat:@"%@%@%@",BASE_URL_PROMO,Promocode_URL,promocode];
	urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	
	DLog(@"%@",urlString);
	
	AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
	[manager.requestSerializer setTimeoutInterval:20.0];
	manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];

	
	[manager GET:urlString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject)
	 {
		 DLog(@"Error: %@", responseObject);
		 successBlock(responseObject);
		 
	 }
		failure:^(NSURLSessionTask *operation, NSError *error)
	 {
		 DLog(@"Error: %@", error);
		 failureBlock(error);
	 }];
}

+ (void) prepareNewUSerForBooking:(NSString*)email success:(void (^)(id responseobject))successBlock failureBlock:(void (^)(NSError *error))failureBlock
{
    DLog(@"%@",email);
    
    NSString *Urlstr = [NSString stringWithFormat:@"%@%@%@",BASE_URL,Booking_User_Exists_URL,email];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setTimeoutInterval:20.0];
    
    [manager GET:Urlstr parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject){
        
        if (successBlock) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                DLog(@"Is of type: %@", [[responseObject objectForKey:@"status"] class]);
                
                if ([[responseObject objectForKey:@"status"] isEqualToNumber:[NSNumber numberWithInteger:200]])
                {
                    successBlock(responseObject);
                }
                
            });
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error){
        if (failureBlock) {
            dispatch_async(dispatch_get_main_queue(), ^{
                failureBlock(error);
            });
            
            DLog(@"%@",error);
        }
    }];
    
}

@end
