//
//  AirLineApiManager.h
//  Faremakers
//
//  Created by Mac1 on 06/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ASProgressPopUpView/ASProgressPopUpView.h>

@protocol SearchFlightDataDelegate <NSObject>

-(void) flightDataManagerDidRecieveResponse:(id)object;
-(void) flightDataManagerDidFail:(id)object;
-(void) flightDataManagerStartProgressing:(float)progress;

@end

@interface FlightDataApiManager : NSObject

@property (nonatomic, weak)id<SearchFlightDataDelegate> adelegate;

@property (nonatomic) NSURLSessionDataTask *currentTask;

+ (void) getFareTrendForFlights:(NSDictionary*)dict withBaseCurrency:(NSString*)currency success:(void (^)(id responseobject))successBlock failureBlock:(void (^)(NSError *error))failureBlock;

+ (void) uploadBookingDetailData:(NSDictionary*)data success:(void (^)(id responseobject))successBlock failureBlock:(void (^)(NSError *error))failureBlock;
+ (void) uploadPaidBookingDetail:(NSDictionary*)data success:(void (^)(id responseobject))successBlock failureBlock:(void (^)(NSError *error))failureBlock;

- (void) getFlightListingDataWithRequestData:(NSDictionary*)dict withProgressView:(ASProgressPopUpView*)progressView;

- (void) getAirLineDataFromAPI;

- (void) CancleAllTask;

@end
