//
//  AirLineApiManager.h
//  Faremakers
//
//  Created by Mac1 on 06/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^PromotionUmrahSuccessBlock)(id responseobject);
typedef void (^PromotionUmrahFailureBlock)(NSError *error);

@protocol PromotionListingsDataDelegate <NSObject>

-(void) promotionDataManagerDidRecieveResponse:(id)object;
-(void) promotionDataManagerDidFail:(id)object;

@end

@class AFHTTPSessionManager;

@interface PromotionDataApiManager : NSObject

@property (nonatomic) NSURLSessionDataTask *currentTask;

@property (nonatomic, weak)id<PromotionListingsDataDelegate> adelegate;

+ (void)getUmrahPackagesWithsuccessBlock:(PromotionUmrahSuccessBlock)successBlock failureBlock:(PromotionUmrahFailureBlock)failureBlock;
- (void)getPromotionListingData;
- (void)CancleAllTask;

@end
