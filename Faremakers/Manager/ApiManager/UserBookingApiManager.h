//
//  UserBookingApiManager.h
//  Faremakers
//
//  Created by Mac1 on 16/03/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserBookingApiManager : NSObject

+(void)getUserBookinglistForUserID:(NSString*)userID withSuccessBolck:(void(^) (id response))success andFailureBlock:(void (^) (NSError *error))failure;
                                                                       
@end
