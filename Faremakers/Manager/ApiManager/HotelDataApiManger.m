//
//  HotelDataApiManger.m
//  Faremakers
//
//  Created by Mac1 on 01/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "HotelDataApiManger.h"
#import <AFNetworking/AFHTTPSessionManager.h>
#import "FMHotels.h"
#import "Constants.h"
#import "Cities.h"


@implementation HotelDataApiManger


- (NSArray*) parseHotelSearchData:(id)item
{
	DLog(@"hotel search %@",item);
	
    NSArray *responseArray = [[item objectForKey:@"data"] objectForKey:@"serviceResponse"];
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    for (NSDictionary *dict in responseArray)
    {
        [array addObject:[FMHotels itemWithDictionary:dict]];
    }
    return array;
}

- (BOOL) checkJsonData:(NSDictionary*)jsonObject
{
    NSDictionary *dict = [jsonObject objectForKey:@"data"];
    return ([[dict objectForKey:@"error"] isEqualToString:@"0"])?YES:NO;
}

- (void) searchHotelDataForItem:(NSDictionary*)dict
{
    
    NSString *Urlstr = [NSString stringWithFormat:@"%@%@",BASE_URL,Hotel_API_URL];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager.requestSerializer setTimeoutInterval:180.0];
	
	DLog(@"%@",dict);
    
    _currentTask =[manager POST:Urlstr parameters:dict progress:nil success:^(NSURLSessionTask *task, id responseObject)
    {
	    
	    DLog(@"%@",responseObject);

        if ([self checkJsonData:responseObject])
        {
            [self.adelegate HotelDataManagerDidRecieveResponse:[self parseHotelSearchData:responseObject]];
        }
        else
            [self.adelegate HotelDataManagerDidFail:nil];
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
                   {
                       DLog(@"Error: %@", error);
                       [self.adelegate HotelDataManagerDidFail:nil];
                   }];
    [manager setDataTaskDidReceiveDataBlock:^(NSURLSession * _Nonnull session, NSURLSessionDataTask * _Nonnull dataTask, NSData * _Nonnull data) {
        
        [self.adelegate HotelDataManagerStartProgressing:(float)dataTask.countOfBytesReceived / (float)dataTask.countOfBytesExpectedToReceive];
        DLog(@"countOfBytes: %f ",(float)dataTask.countOfBytesReceived / (float)dataTask.countOfBytesExpectedToReceive);
    }];
}



+ (void) uploadPaidBookingDetail:(NSDictionary*)data success:(void (^)(id responseobject))successBlock failureBlock:(void (^)(NSError *error))failureBlock
{
	DLog(@"%@",data);
	
	NSString *Urlstr = [NSString stringWithFormat:@"%@%@",BASE_URL_2,Booking_Paid_Hotel_URL];
	
	AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
	[manager.requestSerializer setTimeoutInterval:180.0];
	
	[manager POST:Urlstr parameters:data progress:nil success:^(NSURLSessionTask *task, id responseObject){
		
		if (successBlock) {
			dispatch_async(dispatch_get_main_queue(), ^{
				
				DLog(@"Is of type: %@", [[responseObject objectForKey:@"status"] class]);
				
				if ([[responseObject objectForKey:@"status"] isEqualToNumber:[NSNumber numberWithInteger:200]])
				{
					successBlock(responseObject);
				}
				
			});
		}
		
	} failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error){
		if (failureBlock) {
			dispatch_async(dispatch_get_main_queue(), ^{
				failureBlock(error);
			});
		}
	}];
	
}

-(void)CancleAllTask
{
    [_currentTask cancel];
}

@end
