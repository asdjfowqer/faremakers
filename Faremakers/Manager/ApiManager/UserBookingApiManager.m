//
//  UserBookingApiManager.m
//  Faremakers
//
//  Created by Mac1 on 16/03/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "UserBookingApiManager.h"
#import <AFNetworking/AFHTTPSessionManager.h>
#import "Constants.h"

@implementation UserBookingApiManager

+ (AFHTTPSessionManager *)getSessionManager
{
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] init];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:Api_Key forHTTPHeaderField:@"api-key"];
    return manager;
}

+ (void)getUserBookinglistForUserID:(NSString *)userID withSuccessBolck:(void (^) (id response))success andFailureBlock:(void (^) (NSError *error))failure;
{
    AFHTTPSessionManager *manger = [self getSessionManager];
    [manger.requestSerializer setTimeoutInterval:2.0];
    NSString *urlString = [NSString stringWithFormat:@"%@%@", BASE_URL, UserBooking_URL];

    [manger POST:urlString parameters:nil
        progress: ^(NSProgress *_Nonnull uploadProgress) {
        }
         success: ^(NSURLSessionDataTask *_Nonnull task, id _Nullable responseObject) {
             success(responseObject);
         }
         failure: ^(NSURLSessionDataTask *_Nullable task, NSError *_Nonnull error) {
             failure(error);
         }];
}

@end