//
//  AirLineApiManager.m
//  Faremakers
//
//  Created by Mac1 on 06/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "PromotionDataApiManager.h"
#import "Promotions.h"
#import "UmrahPackage.h"
#import <AFNetworking/AFHTTPSessionManager.h>
#import "Constants.h"

@implementation PromotionDataApiManager


- (NSDictionary*)filterPromotionData:(NSMutableArray*)array
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"promotionType Contains[cd] %@",@"Flight"];
    NSArray *flightData = [array filteredArrayUsingPredicate:predicate];
    predicate = [NSPredicate predicateWithFormat:@"promotionType Contains[cd] %@",@"Holiday"];
    NSArray *HolidayData = [array filteredArrayUsingPredicate:predicate];
    predicate = [NSPredicate predicateWithFormat:@"promotionType Contains[cd] %@",@"Car"];
    NSArray *CarsData = [array filteredArrayUsingPredicate:predicate];
    
    return [NSDictionary dictionaryWithObjectsAndKeys:flightData,@"Flight",HolidayData,@"Holiday",CarsData,@"Cars", nil];
}

- (BOOL) checkStatus:(NSDictionary*)jsonObject
{
    return [[jsonObject objectForKey:@"status"] isEqualToNumber:[NSNumber numberWithInt:200]];
}

- (NSDictionary*)parsePromotionsDataResponse:(NSDictionary*)JSONData
{
    
    NSArray *allKeys = [JSONData allKeys];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    for (NSString *key in allKeys)
    {
        NSArray *promotionArray = [Promotions itemWithArray:[JSONData objectForKey:key]];
        [dict setObject:promotionArray forKey:key];
    }
	
	DLog(@"%@",JSONData);
    
    return dict;
}

-(void)getPromotionListingData
{
    NSString *Urlstr = [NSString stringWithFormat:@"%@%@",BASE_URL,Promotions_API_URL];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setTimeoutInterval:20.0];
    _currentTask =[manager GET:Urlstr parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject){
        
        [self.adelegate promotionDataManagerDidRecieveResponse:[self parsePromotionsDataResponse:responseObject]];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
                   {
                       DLog(@"Error: %@", error);
                       [self.adelegate promotionDataManagerDidFail:nil];
                   }];
}

#pragma mark Umrah Packages

+ (void)getUmrahPackagesWithsuccessBlock:(PromotionUmrahSuccessBlock)successBlock failureBlock:(PromotionUmrahFailureBlock)failureBlock
{
    NSString *Urlstr = [NSString stringWithFormat:@"%@%@",BASE_URL,Umrah_API_URL];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager.requestSerializer setTimeoutInterval:20.0];
    
    [manager GET:Urlstr parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject){
        
        if (successBlock) {
            dispatch_async(dispatch_get_main_queue(), ^{
                successBlock([self parseUmrahResponse:responseObject]);
            });
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error){
        if (failureBlock) {
            dispatch_async(dispatch_get_main_queue(), ^{
                failureBlock(error);
            });
        }
    }];
}
+ (NSArray*)parseUmrahResponse:(NSArray*)JSONData
{
    NSMutableArray *packages = [[NSMutableArray alloc] init];
    
    for (NSDictionary *dict in JSONData)
    {
        UmrahPackage *umrahPackage = [UmrahPackage itemWithDictionary:dict];
        [packages addObject:umrahPackage];
    }
    
    return packages;
}
-(void)CancleAllTask
{
    [_currentTask cancel];    
}

@end
