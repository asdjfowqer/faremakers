//
//  AirLineApiManager.m
//  Faremakers
//
//  Created by Mac1 on 06/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "FlightDataApiManager.h"
#import "AirLinePricedItinerary.h"
#import <AFNetworking/AFHTTPSessionManager.h>
#import <ASProgressPopUpView/ASProgressPopUpView.h>
#import "FlightFareCalendar.h"
#import "Constants.h"


@implementation FlightDataApiManager

- (BOOL)checkStatus:(NSDictionary *)jsonObject
{
    return [[jsonObject objectForKey:@"status"] isEqualToNumber:[NSNumber numberWithInt:200]];
}

- (BOOL)checkJsonData:(NSDictionary *)jsonObject
{
    NSDictionary *dict = [jsonObject objectForKey:@"data"];
    return ([[dict objectForKey:@"error"] isEqualToString:@"0"]) ? NO : YES;
}

- (NSArray *)getAllFlightsDataFromResponse:(NSDictionary *)JSONData
{
    NSArray *ItineraryArray = [[JSONData objectForKey:@"data"] objectForKey:@"serviceResponse"];

    NSMutableArray *array = [[NSMutableArray alloc] init];

    for (NSDictionary *dict in ItineraryArray)
    {
        AirLinePricedItinerary *airLinePricedItinerary = [AirLinePricedItinerary itemWithDictionary:dict];
        [array addObject:airLinePricedItinerary];
    }

    return [[NSArray alloc] initWithArray:array];
}

- (void)getFlightListingDataWithRequestData:(NSDictionary *)dict withProgressView:(ASProgressPopUpView *)progressView
{
    //     dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    //         [self getAirLineDataFromAPI];
    //
    //     });
    //     return;
    NSString *Urlstr = [NSString stringWithFormat:@"%@%@", BASE_URL, Flights_API_URL];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [manager.requestSerializer setTimeoutInterval:120.0];
    
    progressView.progress = 0.01;
    _currentTask = [manager POST:Urlstr parameters:dict progress:nil success: ^(NSURLSessionDataTask *_Nonnull task, id _Nullable responseObject)
                    {
                        
                        DLog(@"%@", responseObject);

                        BOOL status = [self checkStatus:responseObject];
                        if (status)
                        {
                            if ([self checkJsonData:responseObject])
                            {
                                [self.adelegate flightDataManagerDidFail:nil];
                            }
                            else
                            {
                                [self.adelegate flightDataManagerDidRecieveResponse:[self getAllFlightsDataFromResponse:responseObject]];
                            }
                        }
                        else
                        {
                            [self.adelegate flightDataManagerDidFail:nil];
                        }
                    }
                       failure  : ^(NSURLSessionDataTask *_Nullable task, NSError *_Nonnull error)
                    {
                        DLog(@"Error: %@", error);
                        [self.adelegate flightDataManagerDidFail:nil];
                    }];
    [manager setDataTaskDidReceiveDataBlock: ^(NSURLSession *_Nonnull session, NSURLSessionDataTask *_Nonnull dataTask, NSData *_Nonnull data) {
        [self.adelegate flightDataManagerStartProgressing:(float)dataTask.countOfBytesReceived / (float)dataTask.countOfBytesExpectedToReceive];
        DLog(@"countOfBytes: %f ", (float)dataTask.countOfBytesReceived / (float)dataTask.countOfBytesExpectedToReceive);
    }];
}

+ (NSArray *)getFareRulesFromLocalFile
{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"getCities" ofType:@"json"];
    NSData *JSONData = [NSData dataWithContentsOfFile:filePath options:NSDataReadingMappedIfSafe error:nil];
    NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:JSONData options:NSJSONReadingMutableContainers error:nil];

    NSArray *response = [FlightFareCalendar itemWithArray:[[[jsonObject objectForKey:@"data"] objectForKey:@"serviceResponse"] objectForKey:@"FareInfo"]];
    return response;
}

+ (NSArray *)getFareRulesFromResponse:(id)jsonObject
{
    NSArray *response = [FlightFareCalendar itemWithArray:[[[jsonObject objectForKey:@"data"] objectForKey:@"serviceResponse"] objectForKey:@"FareInfo"]];
    return response;
}

+ (BOOL)validFareCalendarJson:(NSDictionary *)jsonObject
{
    return ![[[[jsonObject objectForKey:@"data"] objectForKey:@"serviceResponse"] allKeys] containsObject:@"errorCode"];
}

+ (void)getFareTrendForFlights:(NSDictionary *)dict withBaseCurrency:(NSString *)currency success:(void (^)(id responseobject))successBlock failureBlock:(void (^)(NSError *error))failureBlock
{
    NSString *Urlstr = [NSString stringWithFormat:@"%@%@", BASE_URL, FareCalendar_API_URL];

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setTimeoutInterval:180.0];
    [manager POST:Urlstr parameters:dict progress:nil success: ^(NSURLSessionTask *task, id responseObject) {
        if (successBlock)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                if ([self validFareCalendarJson:responseObject])
                {
                    successBlock([self getFareRulesFromResponse:responseObject]);
                }
                else
                {
                    failureBlock([[[responseObject objectForKey:@"data"] objectForKey:@"serviceResponse"] objectForKey:@"errorCode"]);
                }
            });
        }
    }
          failure: ^(NSURLSessionDataTask *_Nullable task, NSError *_Nonnull error) {
              if (failureBlock)
              {
                  dispatch_async(dispatch_get_main_queue(), ^{
                      //successBlock([self getFareRulesFromLocalFile]);
                      failureBlock(error);
                  });
              }
          }];
}

+ (void)uploadBookingDetailData:(NSDictionary *)data success:(void (^)(id responseobject))successBlock failureBlock:(void (^)(NSError *error))failureBlock
{
    DLog(@"%@", data);

    NSString *Urlstr = [NSString stringWithFormat:@"%@%@", BASE_URL_2, Booking_URL];

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setTimeoutInterval:180.0];

    [manager POST:Urlstr parameters:data progress:nil success: ^(NSURLSessionTask *task, id responseObject) {
        if (successBlock)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                DLog(@"Is of type: %@", [[responseObject objectForKey:@"status"] class]);

                if ([[responseObject objectForKey:@"status"] isEqualToNumber:[NSNumber numberWithInteger:200]])
                {
                    successBlock(responseObject);
                }
            });
        }
    }
          failure: ^(NSURLSessionDataTask *_Nullable task, NSError *_Nonnull error) {
              if (failureBlock)
              {
                  dispatch_async(dispatch_get_main_queue(), ^{
                      failureBlock(error);
                  });
              }
          }];
}

+ (void)uploadPaidBookingDetail:(NSDictionary *)data success:(void (^)(id responseobject))successBlock failureBlock:(void (^)(NSError *error))failureBlock
{
    DLog(@"%@", data);

    NSString *Urlstr = [NSString stringWithFormat:@"%@%@", BASE_URL_2, Booking_Paid_Flight_URL];

    DLog(@"%@", Urlstr);

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setTimeoutInterval:180.0];
    
    NSLog(@"parameters: %@", [self jsonStringWithDictionary:data prettyPrint:YES]);
    
    [manager POST:Urlstr parameters:data progress:nil success: ^(NSURLSessionTask *task, id responseObject) {
        if (successBlock)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                DLog(@"Is of type: %@", [[responseObject objectForKey:@"status"] class]);
                successBlock(responseObject);
            });
        }
    }
          failure: ^(NSURLSessionDataTask *_Nullable task, NSError *_Nonnull error) {
              if (failureBlock)
              {
                  dispatch_async(dispatch_get_main_queue(), ^{
                      failureBlock(error);
                  });
              }
          }];
}

+ (NSString*)jsonStringWithDictionary:(NSDictionary *)dictionary prettyPrint:(BOOL) prettyPrint {
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary
                                                       options:(NSJSONWritingOptions)    (prettyPrint ? NSJSONWritingPrettyPrinted : 0)
                                                         error:&error];
    
    if (! jsonData) {
        NSLog(@"bv_jsonStringWithPrettyPrint: error: %@", error.localizedDescription);
        return @"{}";
    } else {
        return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
}

- (void)CancleAllTask
{
    [_currentTask cancel];
}

@end
