//
//  ListingsApiManager.h
//  Faremakers
//
//  Created by Mac Mini - 2 on 15/04/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ListingsApiManager : NSObject

+(void)getBookingsListWithSuccess:(void (^)(id responseobject))successBlock failureBlock:(void (^)(NSError *error))failureBlock;
+(void)getPackageBookingsListWithSuccess:(void (^)(id responseobject))successBlock failureBlock:(void (^)(NSError *error))failureBlock;
+(void)cancelBookingWithBookingId:(NSString*)bookingid withSuccessBlock:(void (^)(id responseobject))successBlock failureBlock:(void (^)(NSError *error))failureBlock;
+(void)cancelPackageBookingWithBookingId:(NSDictionary*)data withSuccessBlock:(void (^)(id responseobject))successBlock failureBlock:(void (^)(NSError *error))failureBlock;

@end
