//
//  BookingApiManager.h
//  Faremakers
//
//  Created by Afnan Ahmad on 22/04/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TransactionBookingPaidPackage.h"

@interface BookingApiManager : NSObject

+ (void)postPackageBooking:(TransactionBookingPaidPackage *)packageTransaction withSuccessBlock:(void (^)(id responseobject))successBlock failureBlock:(void (^)(NSError *error))failureBlock;

@end
