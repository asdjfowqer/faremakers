//
//  GetAllCitiesManager.h
//  Faremakers
//
//  Created by Mac1 on 11/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol GetAllCitiesDelegate <NSObject>

-(void) getAllCitiesDelegateDidRecieveResponse:(id)object;

@end

@interface GetAllCitiesManager : NSObject

@property (nonatomic, weak)id<GetAllCitiesDelegate> adelegate;

@property (nonatomic) NSURLSessionDataTask *currentTask;


-(void)getCityListWithQueryString:(NSString*)urlStr;

-(void)CancleAllTask;

@end
