//
//  HotelDataApiManger.h
//  Faremakers
//
//  Created by Mac1 on 01/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol HotelDataApiSearchDelegate <NSObject>

-(void) HotelDataManagerDidRecieveResponse:(id)object;
-(void) HotelDataManagerDidFail:(id)object;
-(void) HotelDataManagerStartProgressing:(float)progress;

@end

@interface HotelDataApiManger : NSObject

@property (nonatomic, weak)id<HotelDataApiSearchDelegate> adelegate;

@property (nonatomic) NSURLSessionDataTask *currentTask;

- (void) searchHotelDataForItem:(NSDictionary*)dict;

- (void) CancleAllTask;

+ (void) uploadPaidBookingDetail:(NSDictionary*)data success:(void (^)(id responseobject))successBlock failureBlock:(void (^)(NSError *error))failureBlock;

	//Hotels/bookCar

@end
