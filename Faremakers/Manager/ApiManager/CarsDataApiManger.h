//
//  CarsDataApiManger.h
//  Faremakers
//
//  Created by Mac1 on 01/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CarsDataApiSearchDelegate <NSObject>

-(void) CarsDataManagerDidRecieveResponse:(id)object;
-(void) CarsDataManagerDidFail:(id)object;
-(void) CarsDataManagerStartProgressing:(float)progress;

@end

@interface CarsDataApiManger : NSObject

@property (nonatomic, weak)id<CarsDataApiSearchDelegate> adelegate;

@property (nonatomic) NSURLSessionDataTask *currentTask;

- (void) searchCarsDataForItem:(NSDictionary*)dict;

+ (void) uploadPaidBookingDetail:(NSDictionary*)data success:(void (^)(id responseobject))successBlock failureBlock:(void (^)(NSError *error))failureBlock;


- (void) CancleAllTask;

@end
