//
//  TransactionBookingManager.h
//  Faremakers
//
//  Created by Mac Mini - 2 on 13/04/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "TransactionBookingManager.h"
#import "TransactionBooking.h"

@implementation TransactionBookingManager

+(TransactionBookingManager*)shared
{
	static TransactionBookingManager* shareInstance;
	
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		shareInstance = [[TransactionBookingManager alloc] init];
	});
	
	return shareInstance;
}

-(instancetype)init
{
	if (self = [super init])
	{
		_currentTransaction = [[TransactionBookingPaid alloc] init];
	}
	
	return self;
}

@end
