//
//  TransactionBookingManager.h
//  Faremakers
//
//  Created by Mac Mini - 2 on 13/04/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TransactionBookingPaid;

@interface TransactionBookingManager : NSObject

@property (nonatomic,strong) TransactionBookingPaid* currentTransaction;

+(TransactionBookingManager*)shared;

@end
