//
//  LoginManager.m
//  ElectionApp
//
//  Created by Mac1 on 09/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "LoginManager.h"
#import <AFNetworking/AFHTTPSessionManager.h>
#import "Constants.h"
#import "FMUser.h"

@implementation LoginManager


+ (AFHTTPSessionManager *)getSessionManager
{
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] init];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:Api_Key forHTTPHeaderField:@"api-key"];
    return manager;
}

+ (void)loginUserWithEmail:(NSString *)email
               andPassword:(NSString *)password
       withCompletionBlock:(void (^)(id response, NSError *error))completionBlock
{
    NSString *urlString = [NSString stringWithFormat:@"%@%@", BASE_URL, Login_URL];

    AFHTTPSessionManager *manager = [self getSessionManager];

    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:email, @"Email", password, @"Password", @"IPhoneApp", @"ApplicationType", nil];

    [manager.requestSerializer setTimeoutInterval:20.0];

    [manager POST:urlString parameters:dict progress: ^(NSProgress *_Nonnull uploadProgress) {
    }
          success: ^(NSURLSessionDataTask *_Nonnull task, id _Nullable responseObject) {
              dispatch_async(dispatch_get_main_queue(), ^{
                  DLog(@"%@", responseObject);

                  if ([self checkJSonForErrorType:responseObject])
                  {
                      DLog(@"%@", responseObject[@"data"][@"serviceResponse"][@"authToken"]);
                      [[FMUser defaultUser] setAuthToken:responseObject[@"data"][@"serviceResponse"][@"authToken"]];
                      completionBlock(responseObject, nil);
                  }
                  else
                  {
                      completionBlock(nil, [NSError errorWithDomain:@"com.faremakersApp" code:14 userInfo:[NSDictionary dictionaryWithObject:[[responseObject objectForKey:@"data"] objectForKey:@"serviceResponse"] forKey:NSLocalizedDescriptionKey]]);
                  }
              });
          }
          failure: ^(NSURLSessionDataTask *_Nullable task, NSError *_Nonnull error) {
              dispatch_async(dispatch_get_main_queue(), ^{
                  completionBlock(nil, error);
                  completionBlock(nil, error);                                //ompletionBlock([self getUserFromJSonFile],nil);//
              });

              DLog(@"%@", error);
          }];
}

+ (void)loginUserWithFBData:(id)fbData
        withCompletionBlock:(void (^)(id response, NSError *error))completionBlock
{
    NSString *urlString = [NSString stringWithFormat:@"%@%@", BASE_URL, FBLogin_URL];

    AFHTTPSessionManager *manager = [self getSessionManager];

    [manager POST:urlString parameters:fbData progress: ^(NSProgress *_Nonnull uploadProgress) {
    }
          success: ^(NSURLSessionDataTask *_Nonnull task, id _Nullable responseObject) {
              dispatch_async(dispatch_get_main_queue(), ^{
                  if ([self checkJSonForErrorType:responseObject])
                  {
                      completionBlock(responseObject, nil);
                  }
                  else
                  {
                      completionBlock(nil, [NSError errorWithDomain:@"com.faremakersApp" code:14 userInfo:[NSDictionary dictionaryWithObject:[[responseObject objectForKey:@"data"] objectForKey:@"serviceResponse"] forKey:NSLocalizedDescriptionKey]]);
                  }
              });
          }
          failure: ^(NSURLSessionDataTask *_Nullable task, NSError *_Nonnull error) {
              dispatch_async(dispatch_get_main_queue(), ^{
                  completionBlock(nil, error);
              });
          }];
}

+ (void)updateUserPasswordWithData:(id)passWords
               withCompletionBlock:(void (^)(id response, NSError *error))completionBlock
{
    NSString *urlString = [NSString stringWithFormat:@"%@%@", BASE_URL, Login_URL];

    AFHTTPSessionManager *manager = [self getSessionManager];

    [manager POST:urlString parameters:passWords progress: ^(NSProgress *_Nonnull uploadProgress) {
    }
          success: ^(NSURLSessionDataTask *_Nonnull task, id _Nullable responseObject) {
              dispatch_async(dispatch_get_main_queue(), ^{
                  if ([self checkJSonForErrorType:responseObject])
                  {
                      completionBlock(responseObject, nil);
                  }
                  else
                  {
                      completionBlock(nil, [NSError errorWithDomain:@"com.faremakersApp" code:14 userInfo:[NSDictionary dictionaryWithObject:[[responseObject objectForKey:@"data"] objectForKey:@"serviceResponse"] forKey:NSLocalizedDescriptionKey]]);
                  }
              });
          }
          failure: ^(NSURLSessionDataTask *_Nullable task, NSError *_Nonnull error) {
              dispatch_async(dispatch_get_main_queue(), ^{
                  completionBlock(nil, error);
              });
          }];
}

+ (void)recoverUserPasswordWithEmail:(NSString *)email
                 withCompletionBlock:(void (^)(id response, NSError *error))completionBlock
{
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@", BASE_URL, Pass_Recover_URL, email];

    AFHTTPSessionManager *manager = [self getSessionManager];

    [manager GET:urlString parameters:nil progress: ^(NSProgress *_Nonnull uploadProgress) {
    }
         success: ^(NSURLSessionDataTask *_Nonnull task, id _Nullable responseObject) {
             dispatch_async(dispatch_get_main_queue(), ^{
                 if ([self checkJSonForErrorType:responseObject])
                 {
                     completionBlock(responseObject, nil);
                 }
                 else
                 {
                     completionBlock(nil, [NSError errorWithDomain:@"com.faremakersApp" code:14 userInfo:[NSDictionary dictionaryWithObject:[[responseObject objectForKey:@"data"] objectForKey:@"serviceResponse"] forKey:NSLocalizedDescriptionKey]]);
                 }
             });
         }
         failure: ^(NSURLSessionDataTask *_Nullable task, NSError *_Nonnull error) {
             dispatch_async(dispatch_get_main_queue(), ^{
                 completionBlock(nil, error);
             });
         }];
}

+ (void)getUserPrfileDataFromServerWithCompletionBlock:(void (^)(id response, NSError *error))completionBlock
{
    NSString *UrlString = [NSString stringWithFormat:@"%@%@", BASE_URL, UserProfil_URL];

    AFHTTPSessionManager *manager = [self getSessionManager];

    [manager.requestSerializer setValue:[FMUser defaultUser].authToken forHTTPHeaderField:@"auth-token"];

    [manager GET:UrlString parameters:nil
        progress: ^(NSProgress *_Nonnull uploadProgress) {}
         success: ^(NSURLSessionDataTask *_Nonnull task, id _Nullable responseObject) {
             dispatch_async(dispatch_get_main_queue(), ^{
                 if ([self checkJSonForErrorType:responseObject])
                 {
                     completionBlock(responseObject, nil);
                 }
                 else
                 {
                     completionBlock(nil, [NSError errorWithDomain:@"com.faremakersApp" code:14 userInfo:[NSDictionary dictionaryWithObject:@"Bad Request" forKey:NSLocalizedDescriptionKey]]);
                 }
             });
         }
         failure: ^(NSURLSessionDataTask *_Nullable task, NSError *_Nonnull error) {
             completionBlock(nil, error);
         }];
}

+ (BOOL)checkJSonForErrorType:(NSDictionary *)response
{
    NSNumber *statusCode = nil;
    if ((statusCode = [[response objectForKey:@"data"] objectForKey:@"statusCode"]))
    {
        return [statusCode isEqualToNumber:@200];
    }

    return true;
}

+ (NSDictionary *)getUserFromJSonFile
{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"getCities" ofType:@"json"];
    NSData *JSONData = [NSData dataWithContentsOfFile:filePath options:NSDataReadingMappedIfSafe error:nil];
    NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:JSONData options:NSJSONReadingMutableContainers error:nil];

    {
        return jsonObject;
    }
}

//+(NSError*)createCustomError:(NSDictionary*)dict
//{
//    [NSError errorWithDomain:@"com.faremakersApp" code:14 userInfo:[NSDictionary dictionaryWithObject:[[responseObject objectForKey:@"data"] objectForKey:@"serviceResponse"] forKey:NSLocalizedDescriptionKey]]
//}

@end