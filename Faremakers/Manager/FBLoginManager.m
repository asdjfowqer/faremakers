//
//  FBLoginManager.m
//  ElectionApp
//
//  Created by Mac1 on 10/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "FBLoginManager.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import "FMUser.h"

@implementation FBLoginManager

+(void)loginUsingFacebookFromViewController:(id)viewController withCompletionBlock:(void(^)(id loginData,NSError* error))completionBlock
{
    if (![FBSDKAccessToken currentAccessToken]){
        
        FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
        [loginManager logInWithReadPermissions:@[@"public_profile",@"email"] fromViewController:viewController handler:^(FBSDKLoginManagerLoginResult *result, NSError *error){
            
            if (!error)
            {
                
                if ([FBSDKAccessToken currentAccessToken])
                {
                    [SVProgressHUD show];
                    
                    [FBSDKAccessToken setCurrentAccessToken:result.token];
                    
                    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me?fields=id,name,email,picture.type(normal),likes" parameters:nil]
                     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                         completionBlock(result,error);
                     }];
                }
                else if (result.isCancelled)
                {
                    if (!error) {
                         error = [NSError errorWithDomain:@"com.faremakersApp" code:14 userInfo:[NSDictionary dictionaryWithObject:@"User Cancle FB Login" forKey:NSLocalizedDescriptionKey]];
                    }
                    completionBlock(result,error);
                }
            }
            else{
                completionBlock(result,error);
                
            }
        }];
    }
    else{
        if ([FBSDKAccessToken currentAccessToken])
        {
            [SVProgressHUD show];

            [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me?fields=id,name,email,picture.type(normal)" parameters:nil]
             startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                 completionBlock(result,error);
             }];
        }
    }
}
+ (void)getUserLikes
{
    if ([FBSDKAccessToken currentAccessToken])
    {
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me/likes/" parameters:nil]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             NSLog(@"%@",result);
         }];
    }
}
+ (void)checkFBUser
{
    if ([FBSDKAccessToken currentAccessToken])
    {
        [FMUser defaultUser];
    }

}
+ (void)logoutFacebook
{
    if ([FBSDKAccessToken currentAccessToken])
    {
        [FBSDKAccessToken setCurrentAccessToken:nil];
    }
    
}
@end
