//
//  SignUpManger.m
//  ElectionApp
//
//  Created by Mac1 on 09/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "SignUpManger.h"
#import <AFNetworking/AFHTTPSessionManager.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import "Constants.h"

@implementation SignUpManger

+(void)createUserAccountWithParameters:(NSDictionary*)parameters
                   withcompletionBlock:(void(^)(id response,NSError *error))completionBlock
{
    [SVProgressHUD show];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer setValue:Api_Key forHTTPHeaderField:@"api-key"];
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",BASE_URL,SignUp_URL];
    
    [manager POST:urlString parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    }
          success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
              dispatch_async(dispatch_get_main_queue(), ^{
                  
                  if ([SignUpManger checkJSonForErrorType:responseObject]) {
                      [SignUpManger sendConfirmationEmail:parameters[@"email"]];
                      completionBlock(responseObject,nil);
                  }
                  else
                      completionBlock(nil,[NSError errorWithDomain:@"com.faremakersApp" code:14 userInfo:[NSDictionary dictionaryWithObject:[[responseObject objectForKey:@"data"] objectForKey:@"serviceResponse"] forKey:NSLocalizedDescriptionKey]]);
                  [SVProgressHUD dismiss];
              });
          } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              dispatch_async(dispatch_get_main_queue(), ^{
                  completionBlock(nil,error);
                  [SVProgressHUD dismiss];
              });
          }];
}
+(void)sendConfirmationEmail:(NSString*)email
{
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer setValue:Api_Key forHTTPHeaderField:@"api-key"];
    
    NSString *urlString = [NSString stringWithFormat:@"%@/SendConfirmationEmail?email=%@",BASE_URL,email];
    
    [manager GET:urlString parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
        
    }
          success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
              dispatch_async(dispatch_get_main_queue(), ^{
                  DLog(@"%@",responseObject);
              });
          } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              dispatch_async(dispatch_get_main_queue(), ^{
                  DLog(@"%@",error);
              });
          }];
}

+(BOOL)checkJSonForErrorType:(NSDictionary*)response
{
    return ([[[response objectForKey:@"data"] objectForKey:@"error"] isEqualToString:@"0"]);
}

+ (NSDictionary*)getUserFromJSonFile
{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"my-2" ofType:@"json"];
    NSData *JSONData = [NSData dataWithContentsOfFile:filePath options:NSDataReadingMappedIfSafe error:nil];
    NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:JSONData options:NSJSONReadingMutableContainers error:nil];
    
    {
        return jsonObject;
    }
}
+(void)checkUserNameForUserSignUp:(NSString*)userName
              withcompletionBlock:(void(^)(id response,NSError *error))completionBlock
{

    NSString *Urlstr = [NSString stringWithFormat:@"%@%@%@",BASE_URL,SignUp_UserName,userName];
    Urlstr = [Urlstr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer setValue:Api_Key forHTTPHeaderField:@"api-key"];

    [manager.requestSerializer setTimeoutInterval:20.0];

    [manager GET:Urlstr parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject)
                    {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            if ([self checkJSonForErrorType:responseObject]) {
                                completionBlock(responseObject,nil);
                            }
                            else
                                completionBlock(nil,[NSError errorWithDomain:@"com.faremakersApp" code:14 userInfo:[NSDictionary dictionaryWithObject:[[responseObject objectForKey:@"data"] objectForKey:@"serviceResponse"] forKey:NSLocalizedDescriptionKey]]);
                        });
                    }
                        failure:^(NSURLSessionTask *operation, NSError *error)
                    {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            completionBlock(nil,error);
                        });

                    }];
}
+(void)checkEmailForUserSignUp:(NSString*)email
              withcompletionBlock:(void(^)(id response,NSError *error))completionBlock
{
    
    NSString *Urlstr = [NSString stringWithFormat:@"%@%@%@",BASE_URL,SignUp_EmailCheck,email];
    Urlstr = [Urlstr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer setValue:Api_Key forHTTPHeaderField:@"api-key"];
    
    [manager.requestSerializer setTimeoutInterval:20.0];
    
    [manager GET:Urlstr parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             
             if ([self checkJSonForErrorType:responseObject]) {
                 completionBlock(responseObject,nil);
             }
             else
                 completionBlock(nil,[NSError errorWithDomain:@"com.faremakersApp" code:14 userInfo:[NSDictionary dictionaryWithObject:[[responseObject objectForKey:@"data"] objectForKey:@"serviceResponse"] forKey:NSLocalizedDescriptionKey]]);
         });
     }
         failure:^(NSURLSessionTask *operation, NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             completionBlock(nil,error);
         });
         
     }];
}
+(void)checkPhoneForUserSignUp:(NSString*)phoneNumber
              withcompletionBlock:(void(^)(id response,NSError *error))completionBlock
{
    
    NSString *Urlstr = [NSString stringWithFormat:@"%@%@%@",BASE_URL,SignUp_PhoneCheck,phoneNumber];
    Urlstr = [Urlstr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer setValue:Api_Key forHTTPHeaderField:@"api-key"];
    
    [manager.requestSerializer setTimeoutInterval:20.0];
    
    [manager GET:Urlstr parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             
             if ([self checkJSonForErrorType:responseObject]) {
                 completionBlock(responseObject,nil);
             }
             else
                 completionBlock(nil,[NSError errorWithDomain:@"com.faremakersApp" code:14 userInfo:[NSDictionary dictionaryWithObject:[[responseObject objectForKey:@"data"] objectForKey:@"serviceResponse"] forKey:NSLocalizedDescriptionKey]]);
         });
     }
         failure:^(NSURLSessionTask *operation, NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             completionBlock(nil,error);
         });
     }];
}
@end
