//
//  FMNavigationBar.m
//  KonMari
//
//  Created by Afnan Ahmad on 23/02/2016.
//  Copyright © 2016 Focusteck. All rights reserved.
//

#import "FMNavigationBar.h"

#define CUSTOM_NAVIGATION_HEIGHT 60.f

@interface FMNavigationBar ()

@property (nonatomic, strong) UIImageView *logoImageView;

@end

@implementation FMNavigationBar

+ (void)load
{
    [super load];
}

- (CGSize)sizeThatFits:(CGSize)size
{
    CGSize navigationBarSize = [super sizeThatFits:size];
    navigationBarSize.height = CUSTOM_NAVIGATION_HEIGHT;
    return navigationBarSize;
}


- (void)layoutSubviews
{
    [super layoutSubviews];

    if (!self.logoImageView) {
        self.logoImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"nav-logo"]];
        //[self addSubview:self.logoImageView];
    }

    self.logoImageView.center = self.center;
    CGRect frame = self.logoImageView.frame;
    frame.origin.y = 0;
    self.logoImageView.frame = frame;
}

- (void)willMoveToWindow:(UIWindow *)newWindow
{

}

@end
