//
//  KMNavigationBar.h
//  KonMari
//
//  Created by Afnan Ahmad on 23/02/2016.
//  Copyright © 2016 Focusteck. All rights reserved.
//

#import <UIKit/UIKit.h>

#define CUSTOM_NAVIGATION_HEIGHT 60.f

@interface FMNavigationBar : UINavigationBar

@end
