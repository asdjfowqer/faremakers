//
//  SignUpWithEmailViewController.m
//  ElectionApp
//
//  Created by Mac1 on 09/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "SignUpWithEmailViewController.h"
#import "NSString+PhoneNumber.h"
#import "NSString+Email.h"
#import "SignUpManger.h"
#import "AppDelegate.h"
#import "FMUser.h"

@interface SignUpWithEmailViewController ()
{
    NSMutableArray *invlidFormsArray;
}

@end

@implementation SignUpWithEmailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.title = @"Sign Up";
    // self.emailField.text = @"abcd@efg.com";
    //  self.fNameField.text = @"fazal";
    //   self.passwordField.text = @"123456";
    //   self.lNameField.text = @"ios";
    //   self.userNameField.text = @"fazaios";
    //   self.mobileField.text = @"0900078601";

    invlidFormsArray = [[NSMutableArray alloc] initWithObjects:@"invalid", @"invalid", @"invalid", nil];

    UIBarButtonItem *rigthButtonClicked = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(DoneButtonPressed:)];
    [self.navigationItem setRightBarButtonItem:rigthButtonClicked];
    [self adjustMarginsForLagerHeights];

    [self.emailField addTarget:self
                        action:@selector(textFieldDidChange:)
              forControlEvents:UIControlEventEditingChanged];
    [self.userNameField addTarget:self
                           action:@selector(textFieldDidChange:)
                 forControlEvents:UIControlEventEditingChanged];
    [self.mobileField addTarget:self
                         action:@selector(textFieldDidChange:)
               forControlEvents:UIControlEventEditingChanged];
}

- (void)DoneButtonPressed:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITextFieldDelegate

- (void)textFieldDidChange:(UITextField *)textField
{
    if (textField == self.emailField && [self.emailField.text isValidEmail])
    {
        [invlidFormsArray replaceObjectAtIndex:0 withObject:@"invalid"];
        [self checkForEmail:self.emailField.text];
    }
    else if (textField == self.mobileField)
    {
        [invlidFormsArray replaceObjectAtIndex:2 withObject:@"invalid"];
        if (textField.text.length >= 6 && [self.mobileField.text isValidPhoneNumber])
        {
            [self checkForPhoneNumber:self.mobileField.text];
        }
    }
    else if (textField == self.userNameField)
    {
        [invlidFormsArray replaceObjectAtIndex:1 withObject:@"invalid"];
        if (textField.text.length >= 3)
        {
            [self checkForUserName:self.userNameField.text];
        }
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == self.emailField)
    {
        if (![self.emailField.text isValidEmail])
        {
            [self showErrorLabelMessage:@"Email is Invalid"];
        }
        else
        {
            [self checkForEmail:self.emailField.text];
        }
    }
    else if (textField == self.mobileField)
    {
        if ([self.mobileField.text isValidPhoneNumber])
        {
            [self checkForPhoneNumber:self.mobileField.text];
        }
        else
        {
            [self showErrorLabelMessage:@"Phone Number is Invalid! Correct Formate (03xx1234567)"];
        }
    }
    else if (textField == self.userNameField)
    {
        [self checkForUserName:self.userNameField.text];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.fNameField)
    {
        [self.lNameField becomeFirstResponder];
    }
    else if (textField == self.lNameField)
    {
        [self.emailField becomeFirstResponder];
    }
    else if (textField == self.emailField)
    {
        [self.mobileField becomeFirstResponder];
    }
    else if (textField == self.mobileField)
    {
        [self.userNameField becomeFirstResponder];
    }
    else if (textField == self.userNameField)
    {
        [self.passwordField becomeFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
        //[self SignUpButtonSelector:nil];
    }
    return YES;
}

- (IBAction)SignUpButtonSelector:(id)sender
{
    [self.view endEditing:YES];
    if (self.fNameField.text.length < 3)
    {
        [self showErrorMessage:@"First Name should be atleast 3 chracters"];
    }
    else if (self.lNameField.text.length < 3)
    {
        [self showErrorMessage:@"Last Name should be atleast 3 chracters"];
    }
    else if (![self.emailField.text isValidEmail])
    {
        [self showErrorMessage:@"Email is not Valid"];
    }
    else if (self.passwordField.text.length < 3)
    {
        [self showErrorMessage:@"Requires Password"];
    }
    else if (self.userNameField.text.length < 3)
    {
        [self showErrorMessage:@"User Name should be atleast 3 chracters"];
    }
    else if (self.mobileField.text.length < 6 || ![self.mobileField.text isValidPhoneNumber])
    {
        [self showErrorLabelMessage:@"Phone Number is Invalid! Correct Formate (03xx1234567)"];
    }
    else if ([invlidFormsArray indexOfObject:@"invalid"] == NSNotFound)
    {
        [self prepareDataForSignUp];
    }
    else
    {
        if ([invlidFormsArray indexOfObject:@"invalid"] == 0)
        {
            [self showErrorMessage:@"Email Already Exists"];
        }
        else if ([invlidFormsArray indexOfObject:@"invalid"] == 1)
        {
            [self showErrorMessage:@"UserName Already Exists"];
        }
        else if ([invlidFormsArray indexOfObject:@"invalid"] == 2)
        {
            [self showErrorMessage:@"Phone Already Exists"];
        }
    }
}

- (void)prepareDataForSignUp
{
    [self.errorLabel setText:@""];

    NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:self.fNameField.text, @"fName",
                          self.lNameField.text, @"lname",
                          self.userNameField.text, @"userName",
                          self.mobileField.text, @"phoneNumber",
                          @"", @"cell",
                          self.emailField.text, @"email",
                          self.passwordField.text, @"password", @"IPhoneApp", @"ApplicationType", nil];

    [SignUpManger createUserAccountWithParameters:dict withcompletionBlock: ^(id response, NSError *error) {
        if (!error)
        {
            [[FMUser defaultUser] initWithDataFromApi:[[response objectForKey:@"data"] objectForKey:@"serviceResponse"]];
            if ([self.adelegate respondsToSelector:@selector(SignUpWithEmailDidRegisterUser:)])
            {
                [self dismissViewControllerAnimated:YES completion:nil];
                [self.adelegate SignUpWithEmailDidRegisterUser:YES];
            }
            else
            {
                [self dismissViewControllerAnimated:YES completion:nil];
            }
        }
        else
        {
            [self showErrorMessage:[error.userInfo objectForKey:NSLocalizedDescriptionKey]];
        }
    }];
}

- (void)checkForUserName:(NSString *)userName
{
    [SignUpManger checkUserNameForUserSignUp:userName withcompletionBlock: ^(id response, NSError *error) {
        if (error)
        {
            [invlidFormsArray replaceObjectAtIndex:1 withObject:@"invalid"];
        }
        else
        {
            [invlidFormsArray replaceObjectAtIndex:1 withObject:@"valid"];
        }
        [self showErrorLabelMessage:[error.userInfo objectForKey:NSLocalizedDescriptionKey]];
    }];
}

- (void)checkForEmail:(NSString *)email
{
    [SignUpManger checkEmailForUserSignUp:email withcompletionBlock: ^(id response, NSError *error) {
        if (error)
        {
            [invlidFormsArray replaceObjectAtIndex:0 withObject:@"invalid"];
        }
        else
        {
            [invlidFormsArray replaceObjectAtIndex:0 withObject:@"valid"];
        }
        [self showErrorLabelMessage:[error.userInfo objectForKey:NSLocalizedDescriptionKey]];
    }];
}

- (void)checkForPhoneNumber:(NSString *)phoneNumber
{
    [SignUpManger checkPhoneForUserSignUp:phoneNumber withcompletionBlock: ^(id response, NSError *error) {
        if (error)
        {
            [invlidFormsArray replaceObjectAtIndex:2 withObject:@"invalid"];
        }
        else
        {
            [invlidFormsArray replaceObjectAtIndex:2 withObject:@"valid"];
        }
        [self showErrorLabelMessage:[error.userInfo objectForKey:NSLocalizedDescriptionKey]];
    }];
}

- (void)showErrorLabelMessage:(NSString *)str
{
    self.errorLabel.text = str;
}

- (void)showErrorMessage:(NSString *)message
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:message preferredStyle:UIAlertControllerStyleAlert];

    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler: ^(UIAlertAction *_Nonnull action) {
        DLog(@"cancled");
    }]];
    [self.navigationController presentViewController:alertController animated:YES completion:nil];
}

- (void)adjustMarginsForLagerHeights
{
    if ([UIScreen mainScreen].bounds.size.height != 480)
    {
        self.searchBtnBottomMargin.constant = 100;
        self.fNameTopMagin.constant = 20;
    }
}

@end