//
//  ProfileCellView.h
//  Faremakers
//
//  Created by Mac1 on 14/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol profileViewCellDelegate <NSObject>

- (void)textFieldDidStartEditingWithFrame:(CGRect)size;
- (void)textFieldDidEndEditingWithFrame;

@end

@interface ProfileCellView : UITableViewCell <UITextFieldDelegate>

@property (nonatomic , weak) id <profileViewCellDelegate> adelegate;

#pragma mark NewProfileCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITextField *valueTextField;

#pragma mark ProfileCellView
@property (weak, nonatomic) IBOutlet UITextField *fNameField;

#pragma mark ProfileCellView1
@property (weak, nonatomic) IBOutlet UITextField *lNameField;

#pragma mark ProfileCellView2
@property (weak, nonatomic) IBOutlet UITextField *dobField;

#pragma mark ProfileCellView3
@property (weak, nonatomic) IBOutlet UITextField *EmailField;

#pragma mark ProfileCellView4

@property (weak, nonatomic) IBOutlet UITextField *numberField;

- (void)settextForReuseIdentifie:(NSInteger)index;

@end
