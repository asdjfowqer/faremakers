//
//  UserManagementController.m
//  GoibiboStoryBoarding
//
//  Created by Mac1 on 15/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "UserManagementController.h"
#import "LogInWithEmailViewController.h"
#import "TPKeyboardAvoidingTableView.h"
#import "ResetPasswordViewController.h"
#import "FBLoginViewController.h"
#import "FBLoginManager.h"
#import "UIImageView+Circular.h"
#import "NSString+PhoneNumber.h"
#import "ProfileSignUpView.h"
#import "ProfileCellView.h"
#import "LoginManager.h"
#import "AppDelegate.h"
#import "FMUser.h"

@interface UserManagementController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate, profileViewCellDelegate, loginVCDidSuccessLogin>
{
    NSArray *profileDataArr;
    NSArray *listViewImageArr;
    NSArray *sectionHeaders;
    BOOL isEditing;
    UIImagePickerController *imagePickerController;
    BOOL isUserFetched;
    BOOL isPicChanged;
}

@end

#define KProfileViewCell @"NewProfileCell"

@implementation UserManagementController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (![[FMUser defaultUser] isSessionAlive])
    {
        [self showSignUpView];
    }
        
    isUserFetched = false;
    isEditing = NO;
    isPicChanged = false;
    sectionHeaders = @[@"Personal Information", @"Contact Information"];
    profileDataArr  = @[@[@"First Name", @"Last Name", @"Email", @"Contact Number"], @[@"Email", @"Contact Number"]];
    
    [self setUpProfilePic];
    
    [self.signUpButton setHidden:NO];
    [self.sliderLineView setHidden:YES];
    [self adjustMarginsForLagerHeights];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(UpdateUserNameLabel:) name:@"UpdateUserNameLabel" object:nil];
    
    // Do any additional setup after loading the view, typically from a nib. Gradient_HotelDetail//navbar_img
}
-(void)showSignUpView
{
    LogInWithEmailViewController *loginView = [self.storyboard instantiateViewControllerWithIdentifier:@"LogInWithEmailViewController"];
    [loginView setUpRightNavigationItem];
    loginView.isHomePageNeed = true;
    [self.navigationController presentViewController:[self setUpNavigationControllerWithRootView:loginView] animated:NO completion:nil];
}
- (void)UpdateUserNameLabel:(NSNotification *)notifi
{
    if ([[FMUser defaultUser] isSessionAlive])
    {
        [self updateUserData:YES];
    }
    else
    {
        [self updateUserData:NO];
    }
    
    if (self)
    {
        [self.tableView reloadData];
    }
}

- (void)updateUserData:(BOOL)isUserLogIn
{
    if (isUserLogIn)
    {
        self.profilePic.image = [FMUser defaultUser].displayPic;
        [self.signUpButton setTitle:@"Logout" forState:UIControlStateNormal];
        [self.signUpButton setTag:1];
    }
    else
    {
        self.profilePic.image = [UIImage imageNamed:@"noProfileIcon"];
        [self.signUpButton setTitle:@"Sign In / Sign Up" forState:UIControlStateNormal];
        [self.signUpButton setTag:0];
        [self.resetPasswordBtn setHidden:YES];
        isUserFetched = false;
    }
    [self.sliderLineView setHidden:!isUserLogIn];
    self.userDisplayName.text = ([FMUser defaultUser].displayName) ? [FMUser defaultUser].displayName : @"Guest User";
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self UpdateUserNameLabel:nil];
    
    [self.profilePic addCircularRadius];
    
    [self.tableView reloadData];
    
    self.navigationItem.rightBarButtonItem = [[FMUser defaultUser] isSessionAlive] ? self.editProfileButton : nil;
   // self.tableView.hidden = ![[FMUser defaultUser] isSessionAlive];
    
    [FBLoginManager getUserLikes];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *array = profileDataArr[section];
    return array.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger index = indexPath.row;
    if (indexPath.section == 1)
    {
        NSArray *array = profileDataArr[indexPath.section - 1];
        index = indexPath.row + array.count;
    }
    
    ProfileCellView *cell = [tableView dequeueReusableCellWithIdentifier:KProfileViewCell];
    cell.fNameField.placeholder = profileDataArr[indexPath.section][indexPath.row];
    [cell settextForReuseIdentifie:index];
    cell.adelegate = self;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (![[FMUser defaultUser] isSessionAlive])
    {
        [self showErrorMessage:@"You Need to Login First"];
        return;
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleNone;
}

- (BOOL)tableView:(UITableView *)tableview shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

- (BOOL)tableView:(UITableView *)tableview canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

#pragma imagePickerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary <NSString *, id> *)info
{
    UIImage *image = (UIImage *)[info objectForKey:UIImagePickerControllerOriginalImage];
    [FMUser defaultUser].displayPic = image;
    [self.profilePic setImage:image];
    [self.profilePic addCircularRadius];
    [picker dismissViewControllerAnimated:NO completion:^{
        UIBarButtonItem *barItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(settingsBtnSelector:)];
        [self.navigationItem setRightBarButtonItem:barItem];
    }];
    isPicChanged = true;
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:^{
        UIBarButtonItem *barItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(settingsBtnSelector:)];
        [self.navigationItem setRightBarButtonItem:barItem];
    }];
}

- (void)setUpImagePicker
{
    imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.delegate = self;
    
    if ([[FMUser defaultUser] isSessionAlive])
    {
        if (self.editing)
            [self showPhotoActionSheet];
    }
}

- (void)setUpProfilePic
{
    self.profilePic.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageViewTaped:)];
    tapGesture.numberOfTapsRequired  = 1;
    [self.profilePic addGestureRecognizer:tapGesture];
}

- (void)imageViewTaped:(id)gesture
{
    [self setUpImagePicker];
}

- (UINavigationController *)setUpNavigationControllerWithRootView:(id)rootView
{
    UINavigationController *navigationControlller = [[UINavigationController alloc] initWithNavigationBarClass:[FMNavigationBar class] toolbarClass:nil];
    [navigationControlller setViewControllers:@[rootView]];
    return navigationControlller;
}

- (void)DoneButtonPressed:(id)sender
{
}

- (IBAction)SignUpButtonPressed:(id)sender
{
    if ([self.signUpButton tag] == 0)
    {
        LogInWithEmailViewController *loginView = [self.storyboard instantiateViewControllerWithIdentifier:@"LogInWithEmailViewController"];
        [loginView setUpRightNavigationItem];
        loginView.loginDelegate = self;
        [self.navigationController presentViewController:[self setUpNavigationControllerWithRootView:loginView] animated:YES completion:nil];
    }
    else
    {
        if ([[FMUser defaultUser] isSessionAlive])
        {
            [[AppDelegate sharedDelegate] showLoadingViewForView:self withText:@"Signing out..."];
            self.view.alpha = 0.9;
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [[FMUser defaultUser] logOutUser];
                [self.tableView reloadData];
                self.view.alpha = 1;
                [[AppDelegate sharedDelegate] hideLoadingViewForView:self];
                self.navigationItem.rightBarButtonItem = [[FMUser defaultUser] isSessionAlive] ? self.editProfileButton : nil;
               // self.tableView.hidden = ![[FMUser defaultUser] isSessionAlive];
                //[self showLoginViewController];
            });
        }
    }
}

#pragma mark UserManagementHeaderDelegate

- (void)showUserLoginViewControllerCalled:(id)sender
{
}

- (void)showUserSignUpViewControllerCalled:(id)sender
{
}

- (void)showUserFacebookLoginCalled:(id)sender
{
}

- (IBAction)sliderLineTipsSelector:(id)sender
{
}

- (IBAction)settingsBtnSelector:(id)sender
{
    if (![[FMUser defaultUser] isSessionAlive])
    {
        [self showErrorMessage:@"You Need to Login First"];
        return;
    }
    
    if (!self.editing)
    {
        self.editing = YES;
        self.tableView.editing = YES;
        [self.tableView reloadData];
        [self.resetPasswordBtn setHidden:NO];
        [self checkUserProfileFromServer];
        UIBarButtonItem *barItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(settingsBtnSelector:)];
        [self.navigationItem setRightBarButtonItem:barItem];
    }
    else
    {
        if ([self validteUserData])
        {
            if (isUserFetched)
            {
                UIBarButtonItem *barItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(settingsBtnSelector:)];
                [self.navigationItem setRightBarButtonItem:barItem];
                [self.resetPasswordBtn setHidden:YES];
                //   [self saveProfileBtnSelector:sender];
                if (isPicChanged) {
                    [[FMUser defaultUser] UpdateUserProfileData];
                    isPicChanged = false;
                }
                [[FMUser defaultUser] updateUserData];
                self.editing = NO;
                self.tableView.editing = NO;
                [self.tableView reloadData];
            }
            else
            {
                [self showErrorMessage:@"Somthing went Wrong unable to save data right now!"];
            }
        }
    }
}

- (void)checkUserProfileFromServer
{
    [LoginManager getUserPrfileDataFromServerWithCompletionBlock: ^(id response, NSError *error)
     {
         if (!error)
         {
             isUserFetched = true;
             DLog(@"User Fetched");
         }
         else
         {
             isUserFetched = false;
         }
     }];
}

- (BOOL)validteUserData
{
    BOOL isValid = true;
    if ([FMUser defaultUser].firstName.length < 3)
    {
        [self showErrorMessage:@"First Name is too short"];
        isValid = false;
    }
    else if ([FMUser defaultUser].lastName.length < 3)
    {
        [self showErrorMessage:@"Last Name is too short"];
        isValid = false;
    }
    else if (![[FMUser defaultUser].phoneNumber isValidPhoneNumber])
    {
        if ([FMUser defaultUser].phoneNumber.length < 1)
        {
            [self showErrorMessage:@"Please Enter Phone Number"];
        }
        else
        {
            [self showErrorMessage:@"Phone Number is not valid"];
        }
        isValid = false;
    }
    //    else if ([[FMUser defaultUser] checkAgeValidity:[[FMUser defaultUser] getDateFromUserProfile]] < 12)
    //    {
    //        if ([FMUser defaultUser].dob.length < 1)
    //        {
    //            [self showErrorMessage:@"Please Enter Your Age"];
    //        }
    //        else
    //        {
    //            [self showErrorMessage:@"Age Cannot be less Than 12 Years"];
    //        }
    //
    //        isValid = false;
    //    }
    return isValid;
}

- (IBAction)saveProfileBtnSelector:(id)sender
{
    [self.tableView endEditing:YES];
    [[FMUser defaultUser] prepareUserforDefaults];
}

- (void)showErrorMessage:(NSString *)message
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:message preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler: ^(UIAlertAction *_Nonnull action) {
        DLog(@"cancled");
    }]];
    [self.navigationController presentViewController:alertController animated:YES completion:nil];
}

- (void)textFieldDidEndEditingWithFrame
{
    [self.view endEditing:YES];
    //[self.tableView setContentOffset:CGPointMake(0, 0)];
}

- (void)textFieldDidStartEditingWithFrame:(CGRect)size
{
}

- (void)adjustMarginsForLagerHeights
{
    if ([UIScreen mainScreen].bounds.size.height <= 586)
    {
        float heightMargin = [UIScreen mainScreen].bounds.size.height / 667;
        self.tableTopSpaceConstraint.constant = (self.tableTopSpaceConstraint.constant) * (heightMargin - (([UIScreen mainScreen].bounds.size.height == 480) ? 0.6 : 0.0));
        self.headerViewHightConstraint.constant = self.headerViewHightConstraint.constant * heightMargin;
        // self.profileImageHConstant.constant = 20 * (-heightMargin);
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

- (IBAction)resetPasswordBtnSelector:(id)sender
{
    ResetPasswordViewController *passwordView = [self.storyboard instantiateViewControllerWithIdentifier:@"ResetPasswordViewController"];
    [self.navigationController pushViewController:passwordView animated:YES];
}

- (void)UserDidSelectPhoto:(id)isCameraOption
{
    if (isCameraOption && [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    else
    {
        imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    
    [self presentViewController:imagePickerController animated:YES completion:nil];
}

- (void)showPhotoActionSheet
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Choose Photo"
                                                                   message:@""
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    UIAlertAction *lowestPriceAction = [UIAlertAction actionWithTitle:@"Choose Photo From Camera"
                                                                style:UIAlertActionStyleDefault handler: ^(UIAlertAction *action) {
                                                                    [self performSelector:@selector(UserDidSelectPhoto:) withObject:@"Camera" afterDelay:0.3];
                                                                }];                                                                                                                         // 2
    UIAlertAction *mostExpensiveAction = [UIAlertAction actionWithTitle:@"Choose Photo From Library"
                                                                  style:UIAlertActionStyleDefault handler: ^(UIAlertAction *action) {
                                                                      [self performSelector:@selector(UserDidSelectPhoto:) withObject:nil afterDelay:0.3];
                                                                  }];                                                                                                                             // 2
    UIAlertAction *cancleAction = [UIAlertAction actionWithTitle:@"Cancle"
                                                           style:UIAlertActionStyleCancel handler: ^(UIAlertAction *action) {
                                                           }];                                                                                                               // 3
    
    [alert addAction:lowestPriceAction]; // 5
    [alert addAction:mostExpensiveAction];
    [alert addAction:cancleAction];
    [self presentViewController:alert animated:YES completion:nil];
}
-(void)showLoginViewController
{
    LogInWithEmailViewController *loginView = [self.storyboard instantiateViewControllerWithIdentifier:@"LogInWithEmailViewController"];
    
    UINavigationController *navigationControlller = [[UINavigationController alloc] initWithNavigationBarClass:[FMNavigationBar class] toolbarClass:nil];
    [navigationControlller setViewControllers:@[loginView]];
    [self.sideMenuViewController setContentViewController:navigationControlller
                                                 animated:YES];
    [loginView setUpLeftNavigationItem];
    [self.sideMenuViewController hideMenuViewController];
    
}
-(void)LogInWithEmailViewControllerDidLoggedInSuccussfully:(LogInWithEmailViewController *)ViewController{
    [self showFlightSearchViewController];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [ViewController dismissViewControllerAnimated:YES completion:nil];
    });
}

-(void)showFlightSearchViewController
{
    NSString *viewContrIdentifier = @"FlightSearchViewController";
    [self presentViewControllerWithIdentifier:viewContrIdentifier];
}
-(void)presentViewControllerWithIdentifier:(NSString*)viewContrIdentifier{
    
    if (viewContrIdentifier!=nil)
    {
        UINavigationController *navigationControlller = [[UINavigationController alloc] initWithNavigationBarClass:[FMNavigationBar class] toolbarClass:nil];
        [navigationControlller setViewControllers:@[[self.storyboard instantiateViewControllerWithIdentifier:viewContrIdentifier]]];
        
        [self.sideMenuViewController setContentViewController:navigationControlller
                                                     animated:YES];
        [self.sideMenuViewController hideMenuViewController];
    }
}

@end