//
//  ResetPasswordViewController.h
//  Faremakers
//
//  Created by Mac1 on 25/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TPKeyboardAvoidingTableView.h>

@interface ResetPasswordViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingTableView *tableView;
- (IBAction)UpdatePasswordSelector:(id)sender;

@end
