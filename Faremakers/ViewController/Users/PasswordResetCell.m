//
//  PasswordResetCell.m
//  Faremakers
//
//  Created by Mac1 on 14/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "PasswordResetCell.h"
#import "IQActionSheetPickerView.h"
#import "NSString+Email.h"
#import "Constants.h"
#import "FMUser.h"

@interface PasswordResetCell()<IQActionSheetPickerViewDelegate>

@end
@implementation PasswordResetCell

- (void)awakeFromNib
{
    _neWPasswordField.delegate = self;
    _oldPasswordField.delegate = self;
    
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    if (textField == self.neWPasswordField)
    {
        [self.passwords replaceObjectAtIndex:0 withObject:textField.text];
    }
    else if (textField == self.oldPasswordField)
    {
        [self.passwords replaceObjectAtIndex:1 withObject:textField.text];

    }
}
- (void) updateUserData:(UITextField*)textfield
{
}

- (void) doneWithNumberPad
{
    // [self endEditing:YES];
    /// [self.adelegate textFieldDidEndEditingWithFrame];
}
- (void) cancelNumberPad
{
    // [self endEditing:YES];
    // [self.adelegate textFieldDidEndEditingWithFrame];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
@end
