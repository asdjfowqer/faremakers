//
//  ProfileSignUpView.h
//  GoibiboStoryBoarding
//
//  Created by Mac1 on 15/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//


#import "UserManagementHeaderDelegate.h"

@interface ProfileSignUpView : UIView

@property (nonatomic, weak) id <UserManagementHeaderDelegate> aDelegate;

- (IBAction)loginSelectorsPressed:(id)sender;

@end
