//
//  ProfileViewController.h
//  GoibiboStoryBoarding
//
//  Created by Mac1 on 15/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RESideMenu.h"

@class ProfileSignUpView;
@class TPKeyboardAvoidingTableView;

@interface UserManagementController : UIViewController <UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *userDisplayName;
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingTableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIImageView *profilePic;
@property (weak, nonatomic) IBOutlet ProfileSignUpView *profileSignUpView;
@property (weak, nonatomic) IBOutlet UIView *sliderLineView;
@property (weak, nonatomic) IBOutlet UIButton *signUpButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableTopSpaceConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headerViewHightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *profileImageHConstant;
@property (weak, nonatomic) IBOutlet UIButton *resetPasswordBtn;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *editProfileButton;

- (IBAction)sliderLineTipsSelector:(id)sender;
- (IBAction)settingsBtnSelector:(id)sender;
- (IBAction)saveProfileBtnSelector:(id)sender;
- (IBAction)SignUpButtonPressed:(id)sender;
- (IBAction)resetPasswordBtnSelector:(id)sender;

@end
