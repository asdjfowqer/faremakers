//
//  PasswordResetCell.h
//  Faremakers
//
//  Created by Mac1 on 14/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface PasswordResetCell : UITableViewCell <UITextFieldDelegate>

@property(nonatomic,strong) NSMutableArray *passwords;

#pragma mark PasswordResetCell0
@property (weak, nonatomic) IBOutlet UITextField *oldPasswordField;

#pragma mark PasswordResetCell1
@property (weak, nonatomic) IBOutlet UITextField *neWPasswordField;


@end
