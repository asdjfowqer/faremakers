//
//  ResetPasswordViewController.m
//  Faremakers
//
//  Created by Mac1 on 25/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "ResetPasswordViewController.h"
#import "LoginManager.h"
#import "PasswordResetCell.h"

@interface ResetPasswordViewController ()
{
    NSMutableArray *passwordsArray;
}

@end

@implementation ResetPasswordViewController

#define KPasswordResetCell @"PasswordResetCell"

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Update Password";
    passwordsArray = [[NSMutableArray alloc] initWithObjects:@"",@"",nil];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger index = indexPath.row;
    NSString *resuseIdentifier = [NSString stringWithFormat:@"%@%ld",KPasswordResetCell,index];
    
    PasswordResetCell *resetCell = [tableView dequeueReusableCellWithIdentifier:resuseIdentifier];
    resetCell.passwords = passwordsArray;
    return resetCell;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)UpdatePasswordSelector:(id)sender
{
    if ([self checkAllFieldsValidity])
    {
        NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:passwordsArray[0],@"old",passwordsArray[1],@"new", nil];
        [LoginManager updateUserPasswordWithData:dict withCompletionBlock:^(id response, NSError *error)
        {
            if (!error)
            {
                [self showErrorMessage:@"Success" shouldDissmiss:NO];
            }
            else
                [self showErrorMessage:[error.userInfo objectForKey:NSLocalizedDescriptionKey] shouldDissmiss:NO];
        }];
    }
    else
    {
        [self showErrorMessage:@"Password should be consists of Atleast 6 characters" shouldDissmiss:NO];
    }
}

- (BOOL)checkAllFieldsValidity
{
    BOOL isValid = true;
    for (NSString *str in passwordsArray) {
        
        if (str.length < 6) {
            
            isValid = false;
            break;
        }
    }
    
    return isValid;
}
-(void)showErrorMessage:(NSString*)message shouldDissmiss:(BOOL)dissmiss
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:message preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        if(dissmiss)
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }]];
    [self.navigationController presentViewController:alertController animated:YES completion:nil];
}
@end
