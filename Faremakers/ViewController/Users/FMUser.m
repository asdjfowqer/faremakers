//
//  User.m
//  Faremakers
//
//  Created by Mac1 on 31/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "FMUser.h"
#import <AFNetworking/AFImageDownloader.h>
#import "NSManagedObject+CreateNewUser.h"
#import <AFNetworking/AFHTTPSessionManager.h>
#import <KVNProgress/KVNProgress.h>
#import "FMDataStoreManager.h"
#import "FBLoginManager.h"
#import "LoginManager.h"
#import "Constants.h"
#import "User.h"


@implementation FMUser

@synthesize displayPic;
@synthesize displayName;

static FMUser *defaultUser = nil;

+ (FMUser *)defaultUser
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        defaultUser = [[self alloc] init];
    });
    return defaultUser;
}

- (id)init
{
    self = [super init];
    NSDictionary *fbdict ;//= [self getFBUserFromDefaults];
    
    if (fbdict)
    {
        [self initWithDictionary:fbdict];
        [self notify];
    }
    else
    {
        self.ID = nil;
        self.imageUrl = nil;
    }
    return self;
}

- (BOOL)isSessionAlive
{
    if (self.user != nil)
    {
        return YES;
    }
    return NO;
}

- (void)initUserWithFBData:(NSDictionary *)Fbdict
{
    self.ID = [Fbdict objectForKey:@"id"];
    self.displayName = [Fbdict objectForKey:@"name"];
    self.email = [Fbdict objectForKey:@"email"];
    self.imageUrl = [[[Fbdict objectForKey:@"picture"] objectForKey:@"data"] objectForKey:@"url"];
    
    if (self.displayName && (!self.firstName.length && !self.lastName.length))
    {
        self.firstName = [[self.displayName componentsSeparatedByString:@" "] firstObject];
        self.lastName = [[self.displayName componentsSeparatedByString:@" "] lastObject];
    }
    if (self.imageUrl != nil)
    {
        [self downloadImageFromFacebook];
    }
    
    [self notify];
    [self prepareUserforDefaults];
}

+ (instancetype)initWithFBDataDiction:(NSDictionary *)Fbdict
{
    FMUser *user = [[FMUser alloc] init];
    user.userName = [Fbdict objectForKey:@"username"];
    user.displayName = [Fbdict objectForKey:@"name"];
    user.email = [Fbdict objectForKey:@"email"];
    user.imageUrl = [Fbdict objectForKey:@"displayPic"];
    user.ID = [Fbdict objectForKey:@"id"];
    return user;
}

- (void)initWithDataFromApi:(NSDictionary *)dict
{
    [self saveUserIntoCoreData:dict];
    
    self.email = [dict objectForKey:@"email"];
    self.imageUrl = [dict objectForKey:@"imageUrl"];
    self.firstName = [dict objectForKey:@"firstName"];
    self.lastName = [dict objectForKey:@"lastName"];
    self.dob = [dict objectForKey:@"dob"];
    self.phoneNumber = [dict objectForKey:@"phoneNumber"];//pnumber
    self.authToken = [dict objectForKey:@"authToken"];
    self.userType = [dict objectForKey:@"userType"];

    if (self.firstName.length && self.lastName.length)
    {
        self.displayName = [NSString stringWithFormat:@"%@ %@", self.firstName, self.lastName];
    }
    
    if (self.imageUrl != nil)
    {
        //self.imageUrl =  [[BASE_URL stringByDeletingLastPathComponent] stringByAppendingPathComponent:self.imageUrl];
        [self downloadImageFromFacebook];
    }
    
    [self notify];
}

- (void)initWithDictionary:(NSDictionary *)dict
{
    self.userName = [dict objectForKey:@"username"];
    self.displayName = [dict objectForKey:@"name"];
    self.email = [dict objectForKey:@"email"];
    self.imageUrl = [dict objectForKey:@"displayPic"];
    self.ID = [dict objectForKey:@"id"];
    self.firstName = [dict objectForKey:@"fname"];
    self.lastName = [dict objectForKey:@"lname"];
    self.dob = [dict objectForKey:@"dob"];
    self.phoneNumber = [dict objectForKey:@"phoneNumber"];//pnumber
    
    if (self.displayName && (!self.firstName.length && !self.lastName.length))
    {
        self.firstName = [[self.displayName componentsSeparatedByString:@" "] firstObject];
        self.lastName = [[self.displayName componentsSeparatedByString:@" "] lastObject];
    }
    
    if (self.imageUrl != nil)
    {
        [self downloadImageFromFacebook];
    }
}

- (void)downloadImageFromFacebook
{
    [[AFImageDownloader defaultInstance] downloadImageForURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.imageUrl]] success: ^(NSURLRequest *_Nonnull request, NSHTTPURLResponse *_Nullable response, UIImage *_Nonnull responseObject) {
        self.displayPic = responseObject;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self notify];
        });
    }
                                                            failure: ^(NSURLRequest *_Nonnull request, NSHTTPURLResponse *_Nullable response, NSError *_Nonnull error) {
                                                                DLog(@"Unable To Download Image");
                                                                NSData *data = [NSData dataWithContentsOfFile:[self getProfilPicPath]];
                                                                if (data)
                                                                {
                                                                    self.displayPic = [UIImage imageWithData:data];
                                                                    DLog(@"found it on Local");
                                                                }
                                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                                    [self notify];
                                                                });
                                                            }];
}

- (void)logOutUser
{
    [FBLoginManager logoutFacebook];
    [self invalidateEveryThing];
    [self saveFBUserIntoDefaults:nil];
    self.user = nil;
    [self notify];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:FMUserDefualtKey];
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:FMUserEmailKey];
}

- (void)invalidateEveryThing
{
    self.ID = nil;
    self.displayName = @"Guest User";
    self.displayPic = nil;
    self.imageUrl = nil;
    self.email = nil;
    self.userName = nil;
    self.firstName = nil;
    self.lastName = nil;
    self.phoneNumber = nil;
    self.authToken = nil;
    self.dob = nil;
    self.userType = nil;
}

- (NSString *)getProfilPicPath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"Image.png"];
    return filePath;
}

- (void)saveProfilePicLocally
{
    NSString *str = [self getProfilPicPath];
    NSData *data = UIImagePNGRepresentation(self.displayPic);
    BOOL saved = [data writeToFile:str atomically:YES];
    if (saved)
    {
        self.imageUrl = [NSString stringWithString:str];
    }
}

- (void)prepareUserforDefaults
{
    if (self.displayPic)
    {
        [self saveProfilePicLocally];
    }
    NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:self.ID ? : [NSNull null], @"id",
                          self.email ? : @"", @"email",
                          self.imageUrl ? : @"", @"displayPic",
                          self.displayName ? : @"", @"name",
                          self.dob ? : @"", @"dob",
                          self.phoneNumber ? : @"", @"pnumber",
                          self.lastName ? : @"", @"lname",
                          self.firstName ? : @"", @"fname",
                          self.userName ? : @"", @"username",
                          nil];
    [self saveFBUserIntoDefaults:dict];
}

- (void)saveFBUserIntoDefaults:(NSDictionary *)Fbdict
{
    NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:Fbdict];
    
    [[NSUserDefaults standardUserDefaults] setObject:encodedObject forKey:KFBUserDefualtKey];
}

- (NSDictionary *)getFBUserFromDefaults
{
    NSDictionary *encodedObject = [NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:KFBUserDefualtKey]];
    return encodedObject;
}

- (void)notify
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateUserNameLabel" object:self];
}

- (NSInteger)checkAgeValidity:(NSDate *)date
{
    NSDateComponents *ageComponents = [[NSCalendar currentCalendar]
                                       components:NSCalendarUnitYear
                                       fromDate:date
                                       toDate:[NSDate date]
                                       options:0];
    NSInteger age = [ageComponents year];
    
    return age;
}

- (NSDate *)getDateFromUserProfile
{
    NSDateFormatter *dateFormater = [[NSDateFormatter alloc] init];
    [dateFormater setDateFormat:@"yyyy-MM-dd"];
    
    NSDate *date = [dateFormater dateFromString:self.dob];
    return date;
}

- (void)UpdateUserProfileData
{
    if (self.displayPic)
    {
        [KVNProgress showWithStatus:@"Uploading.."];
        NSString *UrlString = [NSString stringWithFormat:@"%@%@", BASE_URL, Edit_Profile_Pic];
        NSDictionary *postParameters = [[NSDictionary alloc]initWithObjectsAndKeys:
                                        @"", @"",
                                        nil];
        NSData *imageData = UIImageJPEGRepresentation(self.displayPic,0.6);
        
        NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:UrlString parameters:postParameters constructingBodyWithBlock: ^(id <AFMultipartFormData> formData)
                                        {
                                            NSString *fileName = [NSString stringWithFormat:@"%@.jpeg",self.uuid];
                                            [formData appendPartWithFileData:imageData name:@"file" fileName:fileName mimeType:@"image/jpeg"];
                                        }
                                                                                                      error:nil];
        [request setValue:Api_Key forHTTPHeaderField:@"api-key"];
        [request setValue:self.authToken forHTTPHeaderField:@"auth-token"];
        
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        
        NSURLSessionUploadTask *uploadTask;
        uploadTask = [manager
                      uploadTaskWithStreamedRequest:request
                      progress: ^(NSProgress *_Nonnull uploadProgress) {
                          
                          dispatch_async(dispatch_get_main_queue(), ^{
                              
                              float completed = uploadProgress.completedUnitCount;
                              float total = uploadProgress.totalUnitCount;
                              float val = completed/total;
                              //    float rounded_up = ceilf(val * 100.0f) / 100.0f;
                              // if (rounded_up >= 1) {
                              //      [KVNProgress showWithStatus:@"Saving."];
                              //  } else {
                              [KVNProgress updateProgress:val animated:YES];
                              //   }
                              
                          });
                      }
                      completionHandler: ^(NSURLResponse *_Nonnull response, id _Nullable responseObject, NSError *_Nullable error) {
                          [KVNProgress dismiss];
                          if (error)
                          {
                              [self showAlertWithTitle:@"Error" message:@"Unable to upload profile pic"];
                          }
                          else
                          {
                              [self UpdateUserProfilePicIntoCoreData:responseObject[@"data"][@"serviceResponse"][@"imageUrl"]];
                              [self showAlertWithTitle:@"Success" message:@"profile pic uploaded"];
                          }
                          
                          
                      }];
        
        [uploadTask resume];
    }
}

- (void)updateUserData
{
    NSString *Urlstr = [NSString stringWithFormat:@"%@%@", BASE_URL, Update_Profile_URL];
    Urlstr = [Urlstr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager.requestSerializer setTimeoutInterval:20.0];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer setValue:Api_Key forHTTPHeaderField:@"api-key"];
    [manager.requestSerializer setValue:self.authToken forHTTPHeaderField:@"auth-token"];
    
    
    NSDictionary *parameters = @{
                                 @"FirstName":self.firstName,
                                 @"LastName": self.lastName,
                                 @"Phone": self.phoneNumber
                                 };
    
    NSURLSessionDataTask *_currentTask = [manager POST:Urlstr parameters:parameters progress:nil success: ^(NSURLSessionTask *task, id responseObject)
                                          {
                                              DLog(@"response: %@", responseObject);
                                          }
                                             failure  : ^(NSURLSessionTask *operation, NSError *error)
                                          {
                                              DLog(@"error: %@", error);
                                          }];
}
//-(void)loadUSerFromServer
//{
//    [LoginManager getUserPrfileDataFromServerWithCompletionBlock: ^(id response, NSError *error)
//     {
//         if (!error)
//         {
//             isUserFetched = true;
//             DLog(@"User Fetched");
//         }
//         else
//         {
//             isUserFetched = false;
//         }
//     }];
//}
- (void)saveUserIntoCoreData:(NSDictionary *)dict
{
    NSManagedObjectContext *context = [[FMDataStoreManager sharedInstance] newManagedObjectContextWithConcurrencyType:NSPrivateQueueConcurrencyType];
    User *user = [User createNewUserWithKey:@"email" andValue:[dict objectForKey:@"email"] inContext:context];
    
    user.email = [dict objectForKey:@"email"];
    user.imageUrl = [dict objectForKey:@"imageUrl"];
    user.firstName = [dict objectForKey:@"firstName"];
    user.lastName = [dict objectForKey:@"lastName"];
    user.dateOfBirth = [dict objectForKey:@"dob"];
    user.phoneNumber = [dict objectForKey:@"phoneNumber"];//pnumber
    user.authToken = [dict objectForKey:@"authToken"];
    user.userType = [dict objectForKey:@"userType"];
    [context save:nil];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:FMUserDefualtKey];
    [[NSUserDefaults standardUserDefaults] setObject:[dict objectForKey:@"email"] forKey:FMUserEmailKey];
    
    
    [self setUser:user];
}
- (void)UpdateUserProfilePicIntoCoreData:(NSString *)url
{
    NSManagedObjectContext *context = [[FMDataStoreManager sharedInstance] newManagedObjectContextWithConcurrencyType:NSPrivateQueueConcurrencyType];
    User *newUser = [User findUserWithKey:@"email" andValue:self.email inContext:context];
    
    newUser.imageUrl = url;
    newUser.firstName = self.firstName;
    newUser.lastName = self.lastName;
    newUser.dateOfBirth = self.dob;
    newUser.phoneNumber = self.phoneNumber;
    newUser.authToken = self.authToken;
    newUser.email = self.email;
    newUser.userName = self.userName;
    newUser.userType = self.userType;
    
    [context save:nil];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:FMUserDefualtKey];
    [[NSUserDefaults standardUserDefaults] setObject:newUser.email forKey:FMUserEmailKey];
    
    [self setUser:newUser];
    self.imageUrl = url;
    if (self.imageUrl != nil)
    {
       // self.imageUrl =  [[BASE_URL stringByDeletingLastPathComponent] stringByAppendingPathComponent:self.imageUrl];
        [self downloadImageFromFacebook];
    }
}


- (void)loadLoggedInUser
{
    NSString *email = nil;
    
    BOOL isLoggedIn = [[NSUserDefaults standardUserDefaults] boolForKey:FMUserDefualtKey];
    email = [[NSUserDefaults standardUserDefaults] objectForKey:FMUserEmailKey];
    
    if (isLoggedIn && email)
    {
        NSManagedObjectContext *context = [[FMDataStoreManager sharedInstance] newManagedObjectContextWithConcurrencyType:NSPrivateQueueConcurrencyType];
        
        User *user = [User findUserWithKey:@"email" andValue:email inContext:context];
        [[FMUser defaultUser] setUser:user];
        self.displayName = [NSString stringWithFormat:@"%@ %@", user.firstName, user.lastName];
        self.imageUrl = user.imageUrl;
        self.lastName = user.lastName;
        self.firstName = user.firstName;
        self.email = user.email;
        self.dob = user.dateOfBirth;
        self.phoneNumber = user.phoneNumber;
        self.authToken = user.authToken;
        self.userType = user.userType;
        
        if (self.imageUrl != nil)
        {
           // self.imageUrl =  [[BASE_URL stringByDeletingLastPathComponent] stringByAppendingPathComponent:self.imageUrl];
        }
        [self downloadImageFromFacebook];
    }
}
- (BOOL)isUserCorporate{
    
    if (self.userType && [@"corporate" isEqualToString:[self.userType lowercaseString]]) {
        return true;
    }
    return false;
}
- (void)findUser
{
    NSManagedObjectContext *context = [[FMDataStoreManager sharedInstance] newManagedObjectContextWithConcurrencyType:NSPrivateQueueConcurrencyType];
    User *user = [User createNewUserWithKey:@"userID" andValue:@"userID" inContext:context];
}
- (void)showAlertWithTitle:(NSString *)title message:(NSString *)message
{
    UIAlertView *alertController = [[UIAlertView alloc]initWithTitle:title message:message delegate:nil cancelButtonTitle:nil otherButtonTitles:@"ok", nil];
    [alertController show];
}
- (NSString *)uuid
{
    CFUUIDRef uuidRef = CFUUIDCreate(NULL);
    CFStringRef uuidStringRef = CFUUIDCreateString(NULL, uuidRef);
    CFRelease(uuidRef);
    return (__bridge_transfer NSString *)uuidStringRef;
}
@end