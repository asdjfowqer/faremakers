//
//  ProfileSignUpView.m
//  GoibiboStoryBoarding
//
//  Created by Mac1 on 15/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "ProfileSignUpView.h"


@implementation ProfileSignUpView

- (IBAction)loginSelectorsPressed:(id)sender
{
    switch ([sender tag]) {
        case 0:{
            [self.aDelegate showUserSignUpViewControllerCalled:sender];
        }
            break;
        case 1:{
            [self.aDelegate showUserLoginViewControllerCalled :sender];

        }
            break;
        case 2:{
            [self.aDelegate showUserFacebookLoginCalled:sender];

        }
            break;
        default:
            break;
    }
    
}

@end
