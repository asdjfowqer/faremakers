//
//  LogInWithEmailViewController.m
//  ElectionApp
//
//  Created by Mac1 on 09/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "LogInWithEmailViewController.h"
#import "SignUpWithEmailViewController.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import <UIViewController+RESideMenu.h>
#import "LoginManager.h"
#import "FBLoginManager.h"
#import "NSString+Email.h"
#import "FMUser.h"

@interface LogInWithEmailViewController ()<SignUpWithEmailDelegate>

@end

@implementation LogInWithEmailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Sign In";
    
    // self.emailTextField.text = @"test@fest.com";
    // self.passwordTextField.text = @"123456";
}
-(void)setUpLeftNavigationItem
{
    UIBarButtonItem * leftButtonClicked = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Menu_icon"] style:UIBarButtonItemStylePlain target:self action:@selector(menuButtonPressed:)];
    [self.navigationItem setLeftBarButtonItem:leftButtonClicked];
    
}
-(void)setUpRightNavigationItem
{
    UIBarButtonItem *rigthButtonClicked=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(DoneButtonPressed:)];
    [self.navigationItem setRightBarButtonItem:rigthButtonClicked];
    
}
-(void)menuButtonPressed:(id)sender
{
    [self presentLeftMenuViewController:sender];
}

-(void)DoneButtonPressed:(id)sender
{
    if (_isHomePageNeed) {
        [self showHomeViewController];
    }
    else
        [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == _emailTextField) {
        [self.passwordTextField becomeFirstResponder];
    }
    else {
        [self.passwordTextField resignFirstResponder];
        [self LoginButtonSelector:nil];
    }
    return YES;
}
- (IBAction)LoginButtonSelector:(id)sender
{
    [self.view endEditing:YES];
    
    if (self.emailTextField.text.length < 1)
    {
        [self showErrorMessage:@"Email Field cannot be Empty"];
    }
    else if (![self.emailTextField.text isValidEmail])
    {
        [self showErrorMessage:@"Email is not Valid"];
    }
    else if (self.passwordTextField.text.length < 1)
    {
        [self showErrorMessage:@"Requires Password"];
    }
    else
    {
        [self loginUsingEmail];
    }
}
- (UINavigationController*)setUpNavigationControllerWithRootView:(id)rootView
{
    
    UINavigationController *navigationControlller = [[UINavigationController alloc] initWithNavigationBarClass:[FMNavigationBar class] toolbarClass:nil];
    [navigationControlller setViewControllers:@[rootView]];
    
    return navigationControlller;
}
- (void)loginUsingEmail
{
    [SVProgressHUD show];
    
    [LoginManager loginUserWithEmail:self.emailTextField.text andPassword:self.passwordTextField.text withCompletionBlock:^(id response, NSError *error) {
        if (!error)
        {
            [[FMUser defaultUser] initWithDataFromApi:[[response objectForKey:@"data"] objectForKey:@"serviceResponse"]];
            
            if (self.loginDelegate && [self.loginDelegate respondsToSelector:@selector(LogInWithEmailViewControllerDidLoggedInSuccussfully:)]) {
                [self.loginDelegate LogInWithEmailViewControllerDidLoggedInSuccussfully:self];
            }
            else if (_isHomePageNeed)
            {
                [self showFlightSearchViewController];
            }
            else
                [self dismissViewControllerAnimated:YES completion:nil];
        }
        else
        {
            [self showErrorMessage:[error.userInfo objectForKey:NSLocalizedDescriptionKey]];
        }
        [SVProgressHUD dismiss];
        
    }];
}
- (IBAction)showUserSignUpViewControllerCalled:(id)sender
{
    SignUpWithEmailViewController *signUpView = [self.storyboard instantiateViewControllerWithIdentifier:@"SignUpWithEmailViewController"];
    signUpView.adelegate = self;
    [self.navigationController presentViewController:[self setUpNavigationControllerWithRootView:signUpView] animated:YES completion:nil];
}

- (IBAction)forgotPasswordSelector:(id)sender {
}

- (IBAction)facebookloginSelector:(id)sender
{
    [FBLoginManager loginUsingFacebookFromViewController:self withCompletionBlock:^(id loginData, NSError *error){
        
        if (error)
        {
            
            [SVProgressHUD dismiss];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self showErrorMessage:error.description];
            });
        }
        else
        {
            NSError * err;
            NSData * jsonData = [NSJSONSerialization dataWithJSONObject:loginData options:0 error:&err];
            NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            
            [LoginManager loginUserWithFBData:loginData withCompletionBlock:^(id response, NSError *error)
             {
                 [SVProgressHUD dismiss];
                 if (!error)
                 {
                     [[FMUser defaultUser] initWithDataFromApi:[[response objectForKey:@"data"] objectForKey:@"serviceResponse"]];
                     if (self.loginDelegate && [self.loginDelegate respondsToSelector:@selector(LogInWithEmailViewControllerDidLoggedInSuccussfully:)]) {
                         [self.loginDelegate LogInWithEmailViewControllerDidLoggedInSuccussfully:self];
                     }
                     else if (_isHomePageNeed)
                     {
                         [self showFlightSearchViewController];
                     }
                     else
                         [self dismissViewControllerAnimated:YES completion:nil];
                 }
                 else
                 {
                     [self showErrorMessage:[error.userInfo objectForKey:NSLocalizedDescriptionKey]];
                 }
                 
             }];
            //            NSError * err;
            //            NSData * jsonData = [NSJSONSerialization dataWithJSONObject:loginData options:0 error:&err];
            //            NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            //            NSDictionary * response = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:[myString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
            //            NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:[loginData objectForKey:@"email"],@"email",[loginData objectForKey:@"id"],@"id",[loginData objectForKey:@"name"],@"name", [loginData objectForKey:@"picture"],@"picture",nil];
            //            DLog(@"%@",dict);
            //            DLog(@"%@",loginData);
            
            //            [[FMUser defaultUser] initUserWithFBData:loginData];
            //            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            //                [self dismissViewControllerAnimated:YES completion:nil];
            //            });
        }
        
    }];
}
-(NSString*) bv_jsonStringWithPrettyPrint:(BOOL) prettyPrint :(NSString*)str{
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:str
                                                       options:(NSJSONWritingOptions)    (prettyPrint ? NSJSONWritingPrettyPrinted : 0)
                                                         error:&error];
    if (! jsonData) {
        DLog(@"bv_jsonStringWithPrettyPrint: error: %@", error.localizedDescription);
        return @"{}";
    } else {
        return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
}
-(void)showErrorMessage:(NSString*)message
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:message preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        DLog(@"cancled");
    }]];
    if (alertController == nil) {
        return;
    }
    [self.navigationController presentViewController:alertController animated:YES completion:nil];
}
#pragma mark SignUpWithEmailDelegate

-(void)SignUpWithEmailDidRegisterUser:(BOOL)isRegistered
{
    if (isRegistered) {
        
        if (self.loginDelegate && [self.loginDelegate respondsToSelector:@selector(LogInWithEmailViewControllerDidLoggedInSuccussfully:)]) {
            [self.loginDelegate LogInWithEmailViewControllerDidLoggedInSuccussfully:self];
        }
        else {
            if (_isHomePageNeed)
            {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self showFlightSearchViewController];
                });
            }
            [self dismissViewControllerAnimated:NO completion:nil];
        }
    }
}
-(void)showHomeViewController
{
    NSString *viewContrIdentifier = @"MainScreenViewController";
    [self presentViewControllerWithIdentifier:viewContrIdentifier];
}
-(void)showFlightSearchViewController
{
    NSString *viewContrIdentifier = @"FlightSearchViewController";
    [self presentViewControllerWithIdentifier:viewContrIdentifier];
}

-(void)showUserViewController
{
    NSString *viewContrIdentifier = @"UserManagementController";
    [self presentViewControllerWithIdentifier:viewContrIdentifier];
}
-(void)presentViewControllerWithIdentifier:(NSString*)viewContrIdentifier{
    
    if (viewContrIdentifier!=nil)
    {
        UINavigationController *navigationControlller = [[UINavigationController alloc] initWithNavigationBarClass:[FMNavigationBar class] toolbarClass:nil];
        [navigationControlller setViewControllers:@[[self.storyboard instantiateViewControllerWithIdentifier:viewContrIdentifier]]];
        
        [self.sideMenuViewController setContentViewController:navigationControlller
                                                     animated:YES];
        [self.sideMenuViewController hideMenuViewController];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self dismissViewControllerAnimated:YES completion:nil];
        });
    }
}
@end
