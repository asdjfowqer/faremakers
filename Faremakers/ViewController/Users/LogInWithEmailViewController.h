//
//  LogInWithEmailViewController.h
//  ElectionApp
//
//  Created by Mac1 on 09/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RESideMenu.h"

@class LogInWithEmailViewController;

@protocol loginVCDidSuccessLogin <NSObject>
-(void)LogInWithEmailViewControllerDidLoggedInSuccussfully:(LogInWithEmailViewController*)ViewController;
@end

@interface LogInWithEmailViewController : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (nonatomic, readwrite) BOOL isHomePageNeed;
@property (nonatomic, assign) id<loginVCDidSuccessLogin> loginDelegate;

- (IBAction)showUserSignUpViewControllerCalled:(id)sender;
- (IBAction)forgotPasswordSelector:(id)sender;
- (IBAction)facebookloginSelector:(id)sender;
- (IBAction)LoginButtonSelector:(id)sender;
- (void)setUpLeftNavigationItem;
- (void)setUpRightNavigationItem;


@end
