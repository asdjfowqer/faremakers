//
//  ProfileCellView.m
//  Faremakers
//
//  Created by Mac1 on 14/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "ProfileCellView.h"
#import "IQActionSheetPickerView.h"
#import "NSString+Email.h"
#import "NSString+PhoneNumber.h"
#import "Constants.h"
#import "FMUser.h"

@interface ProfileCellView () <IQActionSheetPickerViewDelegate>

@end
@implementation ProfileCellView

- (void)awakeFromNib
{
    _fNameField.delegate = self;
    _lNameField.delegate = self;
    _numberField.delegate = self;
    _EmailField.delegate = self;
    _valueTextField.delegate = self;

    [self.fNameField setFont:[UIFont fontWithName:KCustomeFont size:self.fNameField.font.pointSize]];
    [self.lNameField setFont:[UIFont fontWithName:KCustomeFont size:self.lNameField.font.pointSize]];
    [self.numberField setFont:[UIFont fontWithName:KCustomeFont size:self.numberField.font.pointSize]];
    [self.EmailField setFont:[UIFont fontWithName:KCustomeFont size:self.EmailField.font.pointSize]];
    [self.dobField setFont:[UIFont fontWithName:KCustomeFont size:self.dobField.font.pointSize]];
}

- (void)settextForReuseIdentifie:(NSInteger)index
{
    if (self.editing)
    {
        self.valueTextField.enabled = true;
    }
    else
    {
        self.valueTextField.enabled = false;
    }

    self.valueTextField.tag = index;

    [self updatePlaceHolderText:index];

    switch (index)
    {
        case 0:
            [self loadFirstCell];
            break;

        case 1:
            [self loadSecondCell];
            break;

        case 2:
            [self loadFourthCell];
            break;

        case 3:
            [self loadFifthCell];
            break;
            
        case 4:
            break;
            
        default:
            break;
    }
    _fNameField.delegate = self;
}

- (void)loadFirstCell
{
    NSString *fname = [FMUser defaultUser].firstName;
    if ([[FMUser defaultUser] isSessionAlive])
    {
        self.valueTextField.text = fname;
    }
}

- (void)loadSecondCell
{
    if ([[FMUser defaultUser] isSessionAlive])
    {
        self.valueTextField.text = [FMUser defaultUser].lastName;
    }
}

- (void)loadThirdCell
{
    if ([[FMUser defaultUser] isSessionAlive])
    {
        self.valueTextField.text = [FMUser defaultUser].dob;
    }
}

- (void)loadFourthCell
{
    self.valueTextField.enabled = false;
    if ([[FMUser defaultUser] isSessionAlive])
    {
        self.valueTextField.text = [FMUser defaultUser].email;
    }
}

- (void)loadFifthCell
{
    if ([[FMUser defaultUser] isSessionAlive])
    {
        self.valueTextField.text = [FMUser defaultUser].phoneNumber;
    }
}

- (void)updatePlaceHolderText:(NSInteger)index
{
    self.valueTextField.text = @"";

    switch (index)
    {
        case 0:
            self.titleLabel.text = @"First Name";
            self.valueTextField.placeholder = (self.editing) ? @"Enter your first Name" : @"";
            break;

        case 1:
            self.titleLabel.text = @"Last Name";
            self.valueTextField.placeholder = (self.editing) ? @"Enter your last Name" : @"";
            break;

        case 2:
//            self.titleLabel.text = @"DOB";
//            self.valueTextField.placeholder = (self.editing) ? @"Enter your Date of Birth" : @"";
//            break;
//
//        case 3:
            self.titleLabel.text = @"Email";
            self.valueTextField.placeholder = (self.editing) ? @"Enter your Email" : @"";
            break;

        case 3:
            self.titleLabel.text = @"Phone";
            self.valueTextField.placeholder = (self.editing) ? @"Enter your Phone Number" : @"";
            break;

        case 4:
            self.titleLabel.text = @"User Name";
            self.valueTextField.placeholder = (self.editing) ? @"Enter your User Name" : @"";
            break;

        default:
            break;
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if ([textField tag] == 2)
    {
        [self.adelegate textFieldDidEndEditingWithFrame];
     //   [self datePickerViewClicked];
        return NO;
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField.text.length > 3)
    {
        [self updateUserData:textField];
    }
    else
    {
        // [self showErrorMessage:@"Please Enter Valid Data"];
    }
}

- (void)updateUserData:(UITextField *)textfield
{
    switch (textfield.tag)
    {
        case 0:
            [FMUser defaultUser].firstName = textfield.text;
            break;

        case 1:
            [FMUser defaultUser].lastName = textfield.text;
            break;

        case 2:
            if (![textfield.text isValidEmail])
            {
                [self showErrorMessage:@"Enter Valid Email"];
            }
            else
            {
                [FMUser defaultUser].email = textfield.text;
            }
            break;

        case 3:
            [FMUser defaultUser].phoneNumber = textfield.text;

            break;

        default:
            break;
    }
}

- (void)doneWithNumberPad
{
    // [self endEditing:YES];
    /// [self.adelegate textFieldDidEndEditingWithFrame];
}

- (void)cancelNumberPad
{
    // [self endEditing:YES];
    // [self.adelegate textFieldDidEndEditingWithFrame];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    if ([textField tag] == 707)
    {
        [self.adelegate textFieldDidEndEditingWithFrame];
    }
    return YES;
}

- (void)datePickerViewClicked
{
    IQActionSheetPickerView *picker = [[IQActionSheetPickerView alloc] initWithTitle:@"Date Picker" delegate:self];
    [picker setTag:6];
    [picker setActionSheetPickerStyle:IQActionSheetPickerStyleDatePicker];
    [picker setDate:[[NSDate date] dateByAddingTimeInterval:-(KTimeInterval * 360 * 18)] animated:YES];
    [picker show];
}

- (void)actionSheetPickerView:(IQActionSheetPickerView *)pickerView didSelectDate:(NSDate *)date
{
    switch (pickerView.tag)
    {
        case 6:
        {
            NSDateFormatter *dateFormater = [[NSDateFormatter alloc] init];
            [dateFormater setDateFormat:@"yyyy-MM-dd"];

            self.valueTextField.text = [dateFormater stringFromDate:date];
            [FMUser defaultUser].dob = [dateFormater stringFromDate:date];
        }
            break;

        default:
            break;
    }
}

- (void)showErrorMessage:(NSString *)message
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alertView show];
}

@end