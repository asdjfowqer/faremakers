//
//  User.h
//  Faremakers
//
//  Created by Mac1 on 31/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import <Foundation/Foundation.h>

@class User;

@interface FMUser : NSObject

@property (nonatomic, strong) User *user;

@property (nonatomic) NSString *userName;
@property (nonatomic) NSString *displayName;
@property (nonatomic) NSString *firstName;
@property (nonatomic) NSString *lastName;
@property (nonatomic) NSString *userType;
@property (nonatomic) NSString *email;
@property (nonatomic) NSString *phoneNumber;
@property (nonatomic) NSString *imageUrl;
@property (nonatomic,copy) UIImage *displayPic;
@property (nonatomic,copy) NSString *dob;
@property (nonatomic) NSString *ID;
@property (nonatomic) NSString *authToken;
@property (nonatomic) NSString *deviceToken;


+ (instancetype)initWithFBDataDiction:(NSDictionary*)Fbdict;
+ (instancetype)initWithDictionary:(NSDictionary*)dict;
+ (FMUser*)defaultUser;

- (void)loadLoggedInUser;
- (void)UpdateUserProfileData;
- (void)updateUserData;
- (BOOL)isSessionAlive;
- (void)initUserWithFBData:(NSDictionary*)Fbdict;
- (void)logOutUser;
- (void)prepareUserforDefaults;
- (void)initWithDataFromApi:(NSDictionary*)dict;
- (NSInteger)checkAgeValidity:(NSDate*)date;
- (NSDate*)getDateFromUserProfile;
- (void)saveUserIntoCoreData:(NSDictionary*)dict;
- (BOOL)isUserCorporate;

@end
