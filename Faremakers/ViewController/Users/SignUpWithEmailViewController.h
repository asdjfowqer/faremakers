//
//  SignUpWithEmailViewController.h
//  ElectionApp
//
//  Created by Mac1 on 09/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SignUpWithEmailDelegate <NSObject>

- (void) SignUpWithEmailDidRegisterUser:(BOOL)isRegistered;

@end

@interface SignUpWithEmailViewController : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) id<SignUpWithEmailDelegate> adelegate;
@property (weak, nonatomic) IBOutlet UITextField *fNameField;
@property (weak, nonatomic) IBOutlet UIImageView *nameImageView;
@property (weak, nonatomic) IBOutlet UITextField *lNameField;
@property (weak, nonatomic) IBOutlet UITextField *emailField;
@property (weak, nonatomic) IBOutlet UITextField *userNameField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UITextField *mobileField;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint* searchBtnBottomMargin;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *fNameTopMagin;
@property (weak, nonatomic) IBOutlet UILabel *errorLabel;

- (IBAction)SignUpButtonSelector:(id)sender;

@end
