//
//  HotelCellWithStepperDelegate.h
//  ElectionApp
//
//  Created by Mac1 on 09/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

//#ifndef HotelCellWithStepperDelegate_h
//#define HotelCellWithStepperDelegate_h

@protocol UserManagementHeaderDelegate <NSObject>

- (void) showUserLoginViewControllerCalled:(id)sender;
- (void) showUserSignUpViewControllerCalled:(id)sender;
- (void) showUserFacebookLoginCalled:(id)sender;

@end

//#endif /* HotelCellWithStepperDelegate_h */
