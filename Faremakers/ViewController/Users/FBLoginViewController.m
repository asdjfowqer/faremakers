//
//  FBLoginViewController.m
//  ElectionApp
//
//  Created by Mac1 on 08/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "FBLoginViewController.h"
#import "AppDelegate.h"
//#import "User.h"
#import "FBLoginManager.h"

@interface FBLoginViewController ()

@end

@implementation FBLoginViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    UIBarButtonItem *rigthButtonClicked=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(DoneButtonPressed:)];
    [self.navigationItem setRightBarButtonItem:rigthButtonClicked];
}
-(void)DoneButtonPressed:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHidden:NO];

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)showErrorMessage:(NSString*)message
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:message preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        DLog(@"cancled");
    }]];
    [self.navigationController presentViewController:alertController animated:YES completion:nil];
}
- (IBAction)FBSignUpButtonSelector:(id)sender {
    
    [FBLoginManager loginUsingFacebookFromViewController:self withCompletionBlock:^(NSDictionary *loginData, NSError *error){
        if (error) {
            [self showErrorMessage:error.description];
        }
        else
        {
        }
    }];
}
@end
