//
//  ForgotPasswordViewController.h
//  Faremakers
//
//  Created by Mac1 on 31/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotPasswordViewController : UIViewController <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *userEmailField;

- (IBAction)cancleBtnSelector:(id)sender;
- (IBAction)submitBtnSelector:(id)sender;

@end
