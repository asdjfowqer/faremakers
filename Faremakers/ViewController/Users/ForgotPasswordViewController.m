//
//  ForgotPasswordViewController.m
//  Faremakers
//
//  Created by Mac1 on 31/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "ForgotPasswordViewController.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "LoginManager.h"
#import "NSString+Email.h"

@implementation ForgotPasswordViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.userEmailField.delegate = self;    
}
- (IBAction)cancleBtnSelector:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
 
    [textField resignFirstResponder];
    return YES;
}
- (IBAction)submitBtnSelector:(id)sender
{
    [self.view endEditing:YES];
    
    if (![self.userEmailField.text isValidEmail])
    {
        [self showMessage:@"Email is Invalid" withTitle:@"Error" shouldDissmiss:NO];
    }
    else
    {
        [SVProgressHUD show];
        [LoginManager recoverUserPasswordWithEmail:self.userEmailField.text withCompletionBlock:^(id response, NSError *error)
         {
             [SVProgressHUD dismiss];
             if (error)
             {
                 [self showMessage:[error.userInfo objectForKey:NSLocalizedDescriptionKey] withTitle:@"Error" shouldDissmiss:NO];
             }
             else
                 [self showMessage:[[response objectForKey:@"data"] objectForKey:@"serviceResponse"] withTitle:@"Success" shouldDissmiss:YES];
         }];
    }
}
-(void)showMessage:(NSString*)message withTitle:(NSString*)title shouldDissmiss:(BOOL)shouldDissmiss
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        if (shouldDissmiss)
        {
            [self dismissViewControllerAnimated:YES completion:nil];
        }
        DLog(@"cancled");
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}
@end
