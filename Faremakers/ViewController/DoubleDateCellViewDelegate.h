//
//  Header.h
//  Faremakers
//
//  Created by Mac1 on 01/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//


#ifndef DoubleDateCellViewDelegate_h
#define DoubleDateCellViewDelegate_h

@protocol DoubleDateCellViewDelegate <NSObject>

-(void) doubleDateReturnComponentDidSelected:(id)sender;

@end


#endif /* Header_h */
