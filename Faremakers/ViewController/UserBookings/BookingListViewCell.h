//
//  BookingListViewCell.h
//  Faremakers
//
//  Created by Mac1 on 01/03/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BookingCollectionViewCell.h"

//@protocol BookingListViewCellDataSource <NSObject>
//
//-(NSArray*)arrayToLoad;
//
//@end

@interface BookingListViewCell : UITableViewCell<UICollectionViewDataSource,UICollectionViewDelegate,BookingCollectionCellDelegate>

@property (weak, nonatomic) IBOutlet UILabel *travelTypeLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic) NSInteger bookingType;
@property (nonatomic ,strong) NSArray *bookingItems;


//@property (nonatomic,weak) id<BookingListViewCellDataSource> dataSource;

//-(void)prepareCollectionView:(id)obj withIdentifier:(NSInteger)tag;
-(void)prepareCollectionView:(NSArray*)array withIdentifier:(NSInteger)tag;

@end
