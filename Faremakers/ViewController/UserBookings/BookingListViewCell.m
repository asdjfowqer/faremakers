//
//  BookingListViewCell.m
//  Faremakers
//
//  Created by Mac1 on 01/03/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "BookingListViewCell.h"
#import "BookingCollectionViewCell.h"
#import "BookingListTableView.h"
#import "BookingCollectionViewCellSimple.h"

@implementation BookingListViewCell

- (void)awakeFromNib
{
    // Initialization code
}

//-(void)prepareCollectionView:(id)obj withIdentifier:(NSInteger)tag
//{
//    _collectionView.dataSource = self;
//    _collectionView.delegate = self;
//    _bookingType = tag;
//	_bookingItems = [[NSArray alloc] initWithObjects:obj, nil];
//	[self.collectionView reloadData];
//}

-(void)prepareCollectionView:(NSArray*)array withIdentifier:(NSInteger)tag
{
	_collectionView.dataSource = self;
	_collectionView.delegate = self;
	_bookingType = tag;
	
	if (array)
		_bookingItems = [[NSArray alloc] initWithArray:array];
	else
		_bookingItems = nil;
	
	
	[self.collectionView reloadData];
	
	DLog(@"prepareCollectionView %d %d",array.count, tag);
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
	
//	if (self.dataSource)
//	{
//		if ([self.dataSource respondsToSelector:@selector(arrayToLoad)])
//		{
//			_bookingItems = [self.dataSource arrayToLoad];
//		}
//	}
    return _bookingItems.count;
	
//	return 1;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    BookingCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"BookingCollectionViewCell" forIndexPath:indexPath];
    switch (self.bookingType) {
        case KFlightBooking:
        {
//            cell.flightBookingItem = [[NSDictionary alloc]init];
		   
//		   if (_bookingItems && indexPath.row < _bookingItems.count)
//			   cell.flightBookingItem = [[NSDictionary alloc]initWithDictionary:[_bookingItems objectAtIndex:indexPath.row]];
//		   else
//			   cell.flightBookingItem = [[NSDictionary alloc]init];
		   
		   if (_bookingItems && indexPath.row < _bookingItems.count)
			   cell.flightBookingModel = [_bookingItems objectAtIndex:indexPath.row];

        }
            break;
        case KCarBooking:
        {
//            cell.carBookingItem = [[NSDictionary alloc]init];
		   

//		   if (_bookingItems && indexPath.row < _bookingItems.count)
//			   cell.carBookingItem = [[NSDictionary alloc]initWithDictionary:[_bookingItems objectAtIndex:indexPath.row]];
//		   else
//			   cell.carBookingItem = [[NSDictionary alloc]init];
		   
		   if (_bookingItems && indexPath.row < _bookingItems.count)
			   cell.carBookingModel = [_bookingItems objectAtIndex:indexPath.row];

        }
            break;
        case KHotelBooking:
        {
//            cell.hotelBookingItem = [[NSDictionary alloc]init];
//		   if (_bookingItems && indexPath.row < _bookingItems.count)
//			   cell.hotelBookingItem = [[NSDictionary alloc]initWithDictionary:[_bookingItems objectAtIndex:indexPath.row]];
//		   else
//			   cell.hotelBookingItem = [[NSDictionary alloc]init];
		   
		   if (_bookingItems && indexPath.row < _bookingItems.count)
			   cell.hotelBookingModel = [_bookingItems objectAtIndex:indexPath.row];

        }
            break;
        case KUmrahBooking:
        {

//            cell.umrahBookingItem = [[NSDictionary alloc]init];
		   
//		   if (_bookingItems && indexPath.row < _bookingItems.count)
//			   cell.umrahBookingItem = [[NSDictionary alloc]initWithDictionary:[_bookingItems objectAtIndex:indexPath.row]];
//		   else
//			   cell.umrahBookingItem = [[NSDictionary alloc]init];
		   
		   if (_bookingItems && indexPath.row < _bookingItems.count)
			   cell.umrahBookingModel = [_bookingItems objectAtIndex:indexPath.row];

        }
            break;
            case KHolidayBooking:
        {
//            cell.holidayBookingItem = [[NSDictionary alloc]init];
		   
//		   if (_bookingItems && indexPath.row < _bookingItems.count)
//			   cell.holidayBookingItem = [[NSDictionary alloc]initWithDictionary:[_bookingItems objectAtIndex:indexPath.row]];
//		   else
//			   cell.holidayBookingItem = [[NSDictionary alloc]init];
		   
		   if (_bookingItems && indexPath.row < _bookingItems.count)
			   cell.holidayBookingModel = [_bookingItems objectAtIndex:indexPath.row];


        }
            break;
        default:
            break;
    }
    cell.adelegate = self;
    cell.tag = self.bookingType;
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    // [self.aDelegate collectionViewDidSelectMenuItem:indexPath.row];
    
}
-(void)bookingCollectionCellDidPressCancelBtn:(UIButton*)sender
{
    NSIndexPath *indexPath = nil;
    indexPath = [self.collectionView indexPathForItemAtPoint:[self.collectionView convertPoint:sender.center fromView:sender.superview]];
    DLog(@"%ld",(long)indexPath.row);
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

@end
