//
//  BookingListingsViewController.h
//  Faremakers
//
//  Created by Mac1 on 02/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//


#import "UIViewController+RESideMenu.h"

@interface BookingListingsViewController : UIViewController

@property (nonatomic) NSArray *bookingList;

- (void)getUserBookingList;

@end
