//
//  BookingListingsViewController.m
//  Faremakers
//
//  Created by Mac1 on 02/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "BookingListingsViewController.h"
#import "UserBookingApiManager.h"
#import "BookingListTableView.h"
#import "UserBookingItem.h"
#import "AppDelegate.h"
#import "CarbonKit.h"
#import "Constants.h"
#import "ListingsApiManager.h"
#import "UserBookingUmrah.h"
#import "UserBookingHoliday.h"


@interface BookingListingsViewController () <CarbonTabSwipeNavigationDelegate>
{
    CarbonTabSwipeNavigation *carbonTabSwipeNavigation;
    UIActivityIndicatorView *activityIndicator;
    NSArray *viewControllers;
    NSArray *items;
    NSDictionary *bookingsDictionary;
}

@end

@implementation BookingListingsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    items = @[@"Flights", /*@"Cars",*/ @"Hotels", @"Umrah", @"Holiday"];
    self.title = @"Travel History";
    [self populateViewControllers];

    // Do any additional setup after loading the view.

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(cancelBooking:)
                                                 name:@"cancelBooking"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(cancelPackageBooking:)
                                                 name:@"cancelPackageBooking"
                                               object:nil];
}

- (void)viewDidAppear:(BOOL)animated
{
    [self getUserBookingList];
}
- (void)cancelPackageBooking:(NSNotification *)notification
{    
    NSDictionary *bookingObj = notification.object;
    
    if ([notification.name isEqualToString:@"cancelPackageBooking"])
    {
        NSDictionary* data = @{@"bookingId":bookingObj[@"bookingId"],@"type":bookingObj[@"type"]};
        [ListingsApiManager cancelPackageBookingWithBookingId:data withSuccessBlock: ^(id responseobject)
         {
             DLog(@"%@", responseobject);
             
             [self getUserBookingList];
         }
                                          failureBlock: ^(NSError *error) {
                                              UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Could not cancel booking" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                              [av show];
                                          }];
    }
}

- (void)cancelBooking:(NSNotification *)notification
{
    //	{name = cancelBooking; object = HXWDKT}

    DLog(@"%@", notification);
    DLog(@"%@", notification.object);
    NSString *bookingid = notification.object;

    if ([notification.name isEqualToString:@"cancelBooking"])
    {
        [ListingsApiManager cancelBookingWithBookingId:bookingid withSuccessBlock: ^(id responseobject)
         {
             DLog(@"%@", responseobject);

             [self getUserBookingList];
         }
                                          failureBlock: ^(NSError *error) {
                                              UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Could not cancel booking" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                              [av show];
                                          }];
    }
}

- (void)getUserBookingList
{
    //    [UserBookingApiManager getUserBookinglistForUserID:@"userID" withSuccessBolck:^(id response) {
    //
    //    } andFailureBlock:^(NSError *error) {
    //        [self ShowActivityView];
    //
    //    }];

    [self ShowActivityView];

    [ListingsApiManager getBookingsListWithSuccess: ^(id responseobject) {
        DLog(@"%@", responseobject);
        [self ShowActivityView];
        bookingsDictionary = [[NSDictionary alloc] initWithDictionary:responseobject[@"data"]];

        BookingListTableView *controllerFlight = [viewControllers objectAtIndex:0];
        BookingListTableView *controllerCars = [viewControllers objectAtIndex:1];
        BookingListTableView *controllerHotel = [viewControllers objectAtIndex:2];

        controllerFlight.bookingItems = [UserBookingFlight instanceArrayWithDictionary:responseobject];
        controllerCars.bookingItems = [UserBookingCar instanceArrayWithDictionary:responseobject];
        controllerHotel.bookingItems = [UserBookingHotel instanceArrayWithDictionary:responseobject];

        DLog(@"%@", controllerHotel.bookingItems);

        [controllerFlight reloadData];
        [controllerCars reloadData];
        [controllerHotel reloadData];
    }
                                      failureBlock: ^(NSError *error) {
                                          DLog(@"%@", error);
                                      }];

    [ListingsApiManager getPackageBookingsListWithSuccess: ^(id responseobject) {
        
        //Changed this part for hiding cars tab
        //BookingListTableView *controllerUmrah = [viewControllers objectAtIndex:3];
        //BookingListTableView *controllerHolidays = [viewControllers objectAtIndex:4];
        BookingListTableView *controllerUmrah = [viewControllers objectAtIndex:2];
        BookingListTableView *controllerHolidays = [viewControllers objectAtIndex:3];

        controllerUmrah.bookingItems = [UserBookingUmrah instanceArrayWithDictionary:responseobject];
        controllerHolidays.bookingItems = [UserBookingHoliday instanceArrayWithDictionary:responseobject];

        [controllerUmrah reloadData];
        [controllerHolidays reloadData];
    }
                                             failureBlock: ^(NSError *error) {
                                                 DLog(@"%@", error);
                                             }];
}

- (void)populateViewControllers
{
    BookingListTableView *viewController1 = [self.storyboard instantiateViewControllerWithIdentifier:@"BookingListTableView"];
    viewController1.bookingType = KFlightBooking;
    BookingListTableView *viewController2 = [self.storyboard instantiateViewControllerWithIdentifier:@"BookingListTableView"];
    viewController2.bookingType = KCarBooking;
    BookingListTableView *viewController3 = [self.storyboard instantiateViewControllerWithIdentifier:@"BookingListTableView"];
    viewController3.bookingType = KHotelBooking;
    BookingListTableView *viewController4 = [self.storyboard instantiateViewControllerWithIdentifier:@"BookingListTableView"];
    viewController4.bookingType = KUmrahBooking;
    BookingListTableView *viewController5 = [self.storyboard instantiateViewControllerWithIdentifier:@"BookingListTableView"];
    viewController5.bookingType = KHolidayBooking;

    viewControllers = @[viewController1, /*viewController2,*/ viewController3, viewController4, viewController5];

    carbonTabSwipeNavigation = [[CarbonTabSwipeNavigation alloc] initWithItems:items delegate:self];
    [carbonTabSwipeNavigation insertIntoRootViewController:self];
    [self style];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)promotionDataManagerDidFail:(id)object
{
    // [promotionDataManager getPromotionDataFromLocalFile];
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"No Data Found!" preferredStyle:UIAlertControllerStyleAlert];

    [alertController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler: ^(UIAlertAction *_Nonnull action) {
        [self.navigationController popViewControllerAnimated:YES];
    }]];
    [self.navigationController presentViewController:alertController animated:YES completion:nil];
}

- (void)style
{
    UIColor *color = [UIColor colorWithRed:255.0 / 255 green:255.0 / 255 blue:255.0 / 255 alpha:1];
    carbonTabSwipeNavigation.toolbar.translucent = NO;
    [carbonTabSwipeNavigation setIndicatorColor:color];
    [carbonTabSwipeNavigation setTabExtraWidth:30];

    for (int index = 0; index < items.count; index++)
    {
        [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:self.view.frame.size.width / items.count forSegmentAtIndex:index];
    }

    [carbonTabSwipeNavigation.carbonTabSwipeScrollView setBackgroundColor:[UIColor colorWithRed:24.0 / 255 green:74.0 / 255 blue:150.0 / 255 alpha:1]];

    // Custimize segmented control
    [carbonTabSwipeNavigation setNormalColor:[UIColor colorWithRed:255.0 / 255 green:255.0 / 255 blue:255.0 / 255 alpha:0.5]
                                        font:[UIFont fontWithName:KCustomeFont size:14]];
    [carbonTabSwipeNavigation setSelectedColor:color
                                          font:[UIFont fontWithName:KCustomeFont size:14]];
}

- (void)ShowActivityView
{
    UIView *view = [self.view viewWithTag:707];
    if (view)
    {
        [view removeFromSuperview];
        [activityIndicator stopAnimating];
        [[AppDelegate sharedDelegate] hideLoadingViewForView:self];
    }
    else
    {
        view = [[UIView alloc] initWithFrame:self.view.bounds];
        view.tag = 707;
        view.alpha = 0.5;

        [view setBackgroundColor:[UIColor whiteColor]];
        [self.view addSubview:view];
        [self.view bringSubviewToFront:view];
        [[AppDelegate sharedDelegate] showLoadingViewForView:self withText:@"Loading User's Booking..."];
    }
}

# pragma mark - CarbonTabSwipeNavigation Delegate
// required
- (nonnull UIViewController *)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbontTabSwipeNavigation
                                 viewControllerAtIndex:(NSUInteger)index
{
    return viewControllers[index];
}

// optional
- (void)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
                 willMoveAtIndex:(NSUInteger)index
{
}

- (void)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
                  didMoveAtIndex:(NSUInteger)index
{
    DLog(@"Did move at index: %ld", (unsigned long)index);
}

- (UIBarPosition)barPositionForCarbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
{
    return UIBarPositionTop; // default UIBarPositionTop
}

- (void)viewWillDisappear:(BOOL)animated
{
    if ([self.navigationController.viewControllers indexOfObject:self] == NSNotFound)
    {
        [self CancleAllTask];
    }
    [super viewWillDisappear:animated];
}

- (void)CancleAllTask
{
}

@end
