//
//  BookingCollectionViewCell.h
//  Faremakers
//
//  Created by Mac1 on 01/03/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BookingCollectionCellDelegate <NSObject>

-(void)bookingCollectionCellDidPressCancelBtn:(UIButton*)sender;

@end

@interface BookingCollectionViewCell : UICollectionViewCell

@property (nonatomic,weak)id<BookingCollectionCellDelegate> adelegate;

#pragma mark BookingCollectionCell0

@property (nonatomic) NSDictionary *flightBookingItem;
@property (nonatomic) UserBooking* flightBookingModel;
@property (weak, nonatomic) IBOutlet UILabel *flightHeaderLabel;
@property (weak, nonatomic) IBOutlet UILabel *flightDescLabelWhite;
@property (weak, nonatomic) IBOutlet UILabel *flightDescLabelGray;
@property (weak, nonatomic) IBOutlet UIButton *flightCancelButton;

- (IBAction)cancleBtnSelector:(id)sender;

#pragma mark BookingCollectionCell1

@property (nonatomic) NSDictionary *carBookingItem;
@property (nonatomic) UserBooking* carBookingModel;

@property (weak, nonatomic) IBOutlet UILabel *carHeaderLabel;
@property (weak, nonatomic) IBOutlet UILabel *carDescLabelWhite;
@property (weak, nonatomic) IBOutlet UILabel *carDescLabelGray;

@property (weak, nonatomic) IBOutlet UIButton *carCancelButton;

- (IBAction)carCancelBtnSelector:(id)sender;

#pragma mark BookingCollectionCell2

@property (nonatomic) NSDictionary *hotelBookingItem;
@property (nonatomic) UserBooking* hotelBookingModel;

@property (weak, nonatomic) IBOutlet UILabel *hotelHeaderLabel;

@property (weak, nonatomic) IBOutlet UILabel *hotelDescLabelWhite;
@property (weak, nonatomic) IBOutlet UILabel *hotelDescLabelGray;
@property (weak, nonatomic) IBOutlet UIButton *hotelCancelButton;

- (IBAction)hotelCancelBtnSelector:(id)sender;


#pragma mark BookingCollectionCell3

@property (nonatomic) UserBooking* umrahBookingModel;

@property (weak, nonatomic) IBOutlet UILabel *umrahHeaderLabel;
@property (weak, nonatomic) IBOutlet UILabel *umrahDescLabelWhite;
@property (weak, nonatomic) IBOutlet UILabel *umrahDescLabelGray;

@property (weak, nonatomic) IBOutlet UIButton *umrahCancelButton;

#pragma mark BookingCollectionCell4

@property (nonatomic) UserBooking* holidayBookingModel;

@property (weak, nonatomic) IBOutlet UILabel *holidayHeaderLabel;
@property (weak, nonatomic) IBOutlet UILabel *holidayDescLabelWhite;
@property (weak, nonatomic) IBOutlet UILabel *holidayDescLabelGray;

@property (weak, nonatomic) IBOutlet UIButton *holidayCancelButton;


#pragma mark BookingCollectionCell5

@property (nonatomic) NSDictionary *umrahBookingItem;

@property (weak, nonatomic) IBOutlet UILabel *umrahDetailLabel;
@property (weak, nonatomic) IBOutlet UILabel *umrahTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *umrahDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *umrahPassLabel;
@property (weak, nonatomic) IBOutlet UILabel *umrahPriceLabel;

- (IBAction)umrahCancelBtnSelector:(id)sender;

#pragma mark BookingCollectionCell6

@property (nonatomic) NSDictionary *holidayBookingItem;

@property (weak, nonatomic) IBOutlet UILabel *holidayDate;
@property (weak, nonatomic) IBOutlet UILabel *hollidayDestnLabel;
@property (weak, nonatomic) IBOutlet UILabel *holidayThemeLabel;
@property (weak, nonatomic) IBOutlet UILabel *holidayDurationLabel;
@property (weak, nonatomic) IBOutlet UILabel *holidayTravLabel;
@property (weak, nonatomic) IBOutlet UILabel *holidayPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *holidayNoOfPassLabel;

- (IBAction)holidayCancleBtnSelector:(id)sender;



@end
