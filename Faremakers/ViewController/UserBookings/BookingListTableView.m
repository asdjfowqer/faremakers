//
//  BookingListTableView.m
//  Faremakers
//
//  Created by Mac1 on 02/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "BookingListTableView.h"
#import "BookingListViewCell.h"
#import "BookingListViewCellSimple.h"

@interface BookingListTableView ()

@end

@implementation BookingListTableView

static NSString *kBookingViewCell = @"BookingListViewCell";

- (void)viewDidLoad
{
    [super viewDidLoad];
	
//	self.tableView.rowHeight = UITableViewAutomaticDimension;
//	[self.tableView registerNib:[UINib nibWithNibName:@"BookingListViewCellSimple" bundle:nil] forCellReuseIdentifier:@"BookingListViewCellSimple"];

    _listingsItems = @[@"Flights",@"Cars",@"Hotels",@"Umrah Packages",@"Holidays"];
//     Do any additional setup after loading the view.

	
}

-(void)viewDidAppear:(BOOL)animated
{
	[self reloadData];
}


-(void)reloadData
{
	DLog(@"reloadData %@ %d",_bookingItems , _bookingType);
	[self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return (self.view.frame.size.height);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//	return self.bookingItems.count;
	return 1;//self.listingsItems.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *reusedIdentifier = [kBookingViewCell stringByAppendingFormat:@"%ld",self.bookingType];
    BookingListViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reusedIdentifier];
//    [cell prepareCollectionView:(nil) withIdentifier:self.bookingType];
	[cell prepareCollectionView:_bookingItems withIdentifier:self.bookingType];
//	cell.dataSource = self;
    cell.travelTypeLabel.text = self.listingsItems[indexPath.row];
    return cell;
	
//	BookingListViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BookingListViewCellSimple"];
//	return cell;
}

//-(NSArray*)arrayToLoad
//{
//	DLog(@"arrayToLoad %@ %d",_bookingItems , _bookingType);
//
//	return _bookingItems;
//}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (IBAction)filterButtonPressed:(id)sender
{
    
}

@end
