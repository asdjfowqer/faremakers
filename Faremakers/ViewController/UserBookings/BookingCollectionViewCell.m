//
//  BookingCollectionViewCell.m
//  Faremakers
//
//  Created by Mac1 on 01/03/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "BookingCollectionViewCell.h"
#import "FMUser.h"

@implementation BookingCollectionViewCell

-(void)setCarBookingItem:(NSDictionary *)carBookingItem
{
    _carBookingItem = carBookingItem;
	DLog(@"carBookingItem %@",carBookingItem);

/* 
    self.carTravelDateLabel.text = @"";
    self.carCityLabel.text = @"";
    self.carMaxPassLabel.text = @"";
    self.carMaxLuggLabel.text = @"";
    self.carTypeLabel.text = @"";
    self.carOrigLabel.text = @"";
    self.carDestLabel.text = @"";
    self.carPassLabel.text = @"";
    self.carPriceLabel.text = @"";
  */
}

-(void)setCarBookingModel:(UserBooking *)carBookingModel
{
	_carBookingModel = carBookingModel;
	
	
	UserBooking* bookingModel = carBookingModel;
	
	NSString *textHeader = [NSString stringWithFormat:@"Orderid: %@",bookingModel.orderId];
	
	NSString *textWhite = [NSString stringWithFormat:@"Name : %@\nCard Number %@\nPNR/Bookid %@\nPrice: %@",bookingModel.userCompleteName,bookingModel.maskedCardNumber,bookingModel.bookId,bookingModel.price];
	
    NSString *textGray;
    if (bookingModel.transactionReference != nil && bookingModel.transactionReference.length > 0)
    {
        textGray = [NSString stringWithFormat:@"Payment Status : %@\nBooking Status: %@\nOrder Date: %@\nTransaction Id: %@",bookingModel.paymentStatus,bookingModel.bookStatus,[bookingModel dateString:bookingModel.transactionDate],bookingModel.transactionReference];

    }
    else
    {
        textGray = [NSString stringWithFormat:@"Payment Status : %@\nBooking Status: %@\nOrder Date: %@",bookingModel.paymentStatus,bookingModel.bookStatus,[bookingModel dateString:bookingModel.transactionDate]];
    }
		
		//	Cancel
	
	
	UIFont *font = [UIFont fontWithName:@"DIN-Regular" size:15.0f];
	UIColor *color = [UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:1.f];
	NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
	paragraphStyle.paragraphSpacing = 10.f;
	
	
	NSMutableAttributedString *attTextGray = [[NSMutableAttributedString alloc] initWithString:textGray attributes:@{																	NSFontAttributeName:font,
																																								NSForegroundColorAttributeName:color,
																																								NSParagraphStyleAttributeName:paragraphStyle,
																																								}];
	
	
	NSMutableAttributedString *attTextWhite = [[NSMutableAttributedString alloc] initWithString:textWhite attributes:@{																	NSFontAttributeName:font,
																																									
																																									NSForegroundColorAttributeName:color,
																																									NSParagraphStyleAttributeName:paragraphStyle,
																																									}];
	
	
	self.carDescLabelGray.attributedText = attTextGray;
	self.carDescLabelWhite.attributedText = attTextWhite;
	self.carHeaderLabel.text = textHeader;
	[self.carDescLabelGray setNumberOfLines:0];
	[self.carDescLabelWhite setNumberOfLines:0];

	if ([carBookingModel.cancelStatus isEqualToString:@"Active"])
	{
        [self.carCancelButton setEnabled:YES];
		[self.carCancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
		[self.carCancelButton setTitle:@"Cancel" forState:UIControlStateSelected];
		
		self.carCancelButton.tag = 2;
		[self.carCancelButton addTarget:self action:@selector(cancelUserBooking:) forControlEvents:UIControlEventTouchUpInside];

	}
	else
	{
		[self.carCancelButton setTitle:@"Pending" forState:UIControlStateNormal];
		[self.carCancelButton setTitle:@"Pending" forState:UIControlStateSelected];
        [self.carCancelButton setEnabled:NO];
	}
    if ([[FMUser defaultUser] isUserCorporate]) {
        [self.carCancelButton setHidden:true];
        [self.carCancelButton setEnabled:false];
    }
}


-(void)setFlightBookingItem:(NSDictionary *)flightBookingItem
{
    
    _flightBookingItem = flightBookingItem;
	
 /*
    self.travelLabel.text = @"";
    self.passengersLabel.text = @"";
    self.priceLabel.text = @"";
    self.depAbbLabel.text = @"";
    self.arrivalAbbLabel.text = @"";
    self.depLabel.text = @"";
    self.arrivalLabel.text = @"";
    self.depTime.text = @"";
    self.arrivalTime.text = @"";
    self.stopsLabel.text = @"";
     */
	
}

-(void)setFlightBookingModel:(UserBooking *)flightBookingModel
{
	_flightBookingModel = flightBookingModel;
	DLog(@"%@",flightBookingModel);
	
	UserBooking* bookingModel = flightBookingModel;
	
	NSString *textHeader = [NSString stringWithFormat:@"Orderid: %@",bookingModel.orderId];
	
	NSString *textWhite = [NSString stringWithFormat:@"Name : %@\nCard Number: %@\nPNR/Bookid: %@\nPrice: %@",bookingModel.userCompleteName,bookingModel.maskedCardNumber,bookingModel.bookId,bookingModel.price];
	
    NSString *textGray;
    if (bookingModel.transactionReference != nil && bookingModel.transactionReference.length > 0)
    {
        textGray = [NSString stringWithFormat:@"Payment Status : %@\nBooking Status: %@\nOrder Date: %@\nTransaction Id: %@",bookingModel.paymentStatus,bookingModel.bookStatus,[bookingModel dateString:bookingModel.transactionDate],bookingModel.transactionReference];
        
    }
    else
    {
        textGray = [NSString stringWithFormat:@"Payment Status : %@\nBooking Status: %@\nOrder Date: %@",bookingModel.paymentStatus,bookingModel.bookStatus,[bookingModel dateString:bookingModel.transactionDate]];
    }
    
	UIFont *font = [UIFont fontWithName:@"DIN-Regular" size:15.0f];
	UIColor *color = [UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:1.f];
	NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
	paragraphStyle.paragraphSpacing = 10.f;

	
	NSMutableAttributedString *attTextGray = [[NSMutableAttributedString alloc] initWithString:textGray attributes:@{																	NSFontAttributeName:font,
																NSForegroundColorAttributeName:color,
																NSParagraphStyleAttributeName:paragraphStyle,
																}];
	
	
	NSMutableAttributedString *attTextWhite = [[NSMutableAttributedString alloc] initWithString:textWhite attributes:@{																	NSFontAttributeName:font,

																	NSForegroundColorAttributeName:color,
																	NSParagraphStyleAttributeName:paragraphStyle,
																																							}];

	DLog(@"%@",attTextWhite);
	DLog(@"%@",attTextGray);
	
	
	self.flightDescLabelGray.attributedText = attTextGray;
	self.flightDescLabelWhite.attributedText = attTextWhite;
	self.flightHeaderLabel.text = textHeader;
	[self.flightDescLabelGray setNumberOfLines:0];
	[self.flightDescLabelWhite setNumberOfLines:0];
	
	if ([flightBookingModel.cancelStatus isEqualToString:@"Active"])
	{
        [self.flightCancelButton setEnabled:YES];
		[self.flightCancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
		[self.flightCancelButton setTitle:@"Cancel" forState:UIControlStateSelected];
		
		self.flightCancelButton.tag = 1;
		[self.flightCancelButton addTarget:self action:@selector(cancelUserBooking:) forControlEvents:UIControlEventTouchUpInside];
        
        if (flightBookingModel.IsRefundable == false) {
            [self.flightCancelButton setEnabled:false];
        }

	}
	else
	{
		[self.flightCancelButton setTitle:@"Pending" forState:UIControlStateNormal];
		[self.flightCancelButton setTitle:@"Pending" forState:UIControlStateSelected];
        [self.flightCancelButton setEnabled:NO];
	}
    if ([[FMUser defaultUser] isUserCorporate]) {
        [self.flightCancelButton setHidden:true];
        [self.flightCancelButton setEnabled:false];
    }

}

-(void)setHotelBookingItem:(NSDictionary *)hotelBookingItem
{
    _hotelBookingItem = hotelBookingItem;

    /*
    self.hotelDateLabel.text = @"";
    self.hotelNameLabel.text = @"";
    self.hotelLocationLabel.text = @"";
    self.hotelRoomsLabel.text = @"";
    self.hotelGuestLabel.text = @"";
    self.hotelGuestTitle.text = @"";
    self.hotelPriceLabel.text = @"";
     */
}

-(void)setHotelBookingModel:(UserBooking *)hotelBookingModel
{
	_hotelBookingModel = hotelBookingModel;
	
	UserBooking* bookingModel = hotelBookingModel;
	
	NSString *textHeader = [NSString stringWithFormat:@"Orderid: %@",bookingModel.orderId];
	
	NSString *textWhite = [NSString stringWithFormat:@"Name : %@\nCard Number %@\nPNR/Bookid %@\nPrice: %@",bookingModel.userCompleteName,bookingModel.maskedCardNumber,bookingModel.bookId,bookingModel.price];
	
    NSString *textGray;
    if (bookingModel.transactionReference != nil && bookingModel.transactionReference.length > 0)
    {
        textGray = [NSString stringWithFormat:@"Payment Status : %@\nBooking Status: %@\nOrder Date: %@\nTransaction Id: %@",bookingModel.paymentStatus,bookingModel.bookStatus,[bookingModel dateString:bookingModel.transactionDate],bookingModel.transactionReference];
        
    }
    else
    {
        textGray = [NSString stringWithFormat:@"Payment Status : %@\nBooking Status: %@\nOrder Date: %@",bookingModel.paymentStatus,bookingModel.bookStatus,[bookingModel dateString:bookingModel.transactionDate]];
    }
		//	Cancel
	
	
	UIFont *font = [UIFont fontWithName:@"DIN-Regular" size:15.0f];
	UIColor *color = [UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:1.f];
	NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
	paragraphStyle.paragraphSpacing = 10.f;
	
	
	NSMutableAttributedString *attTextGray = [[NSMutableAttributedString alloc] initWithString:textGray attributes:@{																	NSFontAttributeName:font,
																																								NSForegroundColorAttributeName:color,
																																								NSParagraphStyleAttributeName:paragraphStyle,
																																								}];
	
	
	NSMutableAttributedString *attTextWhite = [[NSMutableAttributedString alloc] initWithString:textWhite attributes:@{																	NSFontAttributeName:font,
																																									
																																									NSForegroundColorAttributeName:color,
																																									NSParagraphStyleAttributeName:paragraphStyle,
																																									}];
	
	
	self.hotelDescLabelGray.attributedText = attTextGray;
	self.hotelDescLabelWhite.attributedText = attTextWhite;
	self.hotelHeaderLabel.text = textHeader;
	[self.hotelDescLabelGray setNumberOfLines:0];
	[self.hotelDescLabelWhite setNumberOfLines:0];

	if ([hotelBookingModel.cancelStatus isEqualToString:@"Active"])
	{
        [self.hotelCancelButton setEnabled:YES];
		[self.hotelCancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
		[self.hotelCancelButton setTitle:@"Cancel" forState:UIControlStateSelected];
		self.hotelCancelButton.tag = 3;
		[self.hotelCancelButton addTarget:self action:@selector(cancelUserBooking:) forControlEvents:UIControlEventTouchUpInside];

	}
	else
	{
		[self.hotelCancelButton setTitle:@"Pending" forState:UIControlStateNormal];
		[self.hotelCancelButton setTitle:@"Pending" forState:UIControlStateSelected];
        [self.hotelCancelButton setEnabled:NO];
	}
    if ([[FMUser defaultUser] isUserCorporate]) {
        [self.hotelCancelButton setHidden:true];
        [self.hotelCancelButton setEnabled:false];
    }
}


-(void)setUmrahBookingItem:(NSDictionary *)umrahBookingItem
{
    _umrahBookingItem = umrahBookingItem;
	
	DLog(@"%@",umrahBookingItem);
    
    /*
     self.umrahDetailLabel.text =;
     self.umrahTitleLabel.text = ;
     self.umrahDateLabel.text = ;
     self.umrahPassLabel.text = ;
     self.umrahPriceLabel.text =;
     */
}


-(void)setUmrahBookingModel:(UserBooking *)umrahBookingModel
{
	_umrahBookingModel = umrahBookingModel;

    
    UserBooking* bookingModel = umrahBookingModel;
    
    NSString *textHeader = [NSString stringWithFormat:@"Orderid: %@",bookingModel.orderId];
    
    NSString *textWhite = [NSString stringWithFormat:@"Name : %@\nPrice: %@",bookingModel.packageName,bookingModel.packageAmount];
    
    NSString *textGray = [NSString stringWithFormat:@"Booking Status: %@\nOrder Date: %@",bookingModel.bookStatus,[bookingModel dateString:bookingModel.orderDate]];

    UIFont *font = [UIFont fontWithName:@"DIN-Regular" size:15.0f];
    UIColor *color = [UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:1.f];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.paragraphSpacing = 10.f;
    
    NSMutableAttributedString *attTextGray = [[NSMutableAttributedString alloc] initWithString:textGray attributes:@{																	NSFontAttributeName:font,
                                                                                                                                                                                        NSForegroundColorAttributeName:color,
                                                                                                                                                                                        NSParagraphStyleAttributeName:paragraphStyle,
                                                                                                                                                                                        }];
    NSMutableAttributedString *attTextWhite = [[NSMutableAttributedString alloc] initWithString:textWhite attributes:@{																	NSFontAttributeName:font,
                                                                                                                                                                                        
                                                                                                                                                                                        NSForegroundColorAttributeName:color,
                                                                                                                                                                                        NSParagraphStyleAttributeName:paragraphStyle,
                                                                                                                                                                                        }];
    
    self.umrahDescLabelGray.attributedText = attTextGray;
    self.umrahDescLabelWhite.attributedText = attTextWhite;
    self.umrahHeaderLabel.text = textHeader;
    [self.umrahDescLabelGray setNumberOfLines:0];
    [self.umrahDescLabelWhite setNumberOfLines:0];
    
    if ([umrahBookingModel.bookStatus isEqualToString:@"Active"])
    {
        [self.umrahCancelButton setEnabled:YES];
        [self.umrahCancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
        [self.umrahCancelButton setTitle:@"Cancel" forState:UIControlStateSelected];
        
        self.umrahCancelButton.tag = 4;
        [self.umrahCancelButton addTarget:self action:@selector(cancelUserBooking:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    else
    {
        [self.umrahCancelButton setTitle:@"Pending" forState:UIControlStateNormal];
        [self.umrahCancelButton setTitle:@"Pending" forState:UIControlStateSelected];
        [self.umrahCancelButton setEnabled:NO];
    }
    if ([[FMUser defaultUser] isUserCorporate]) {
        [self.umrahCancelButton setHidden:true];
        [self.umrahCancelButton setEnabled:false];
    }
	
}

-(void)setHolidayBookingItem:(NSDictionary *)holidayBookingItem
{
    _hotelBookingItem = holidayBookingItem;
	DLog(@"%@",holidayBookingItem);

}

-(void)setHolidayBookingModel:(UserBooking *)holidayBookingModel
{
	_holidayBookingModel = holidayBookingModel;

    
    UserBooking* bookingModel = holidayBookingModel;
    
    NSString *textHeader = [NSString stringWithFormat:@"Orderid: %@",bookingModel.orderId];
    
    NSString *textWhite = [NSString stringWithFormat:@"Name : %@\nPrice: %@",bookingModel.packageName,bookingModel.packageAmount];
    
    NSString *textGray = [NSString stringWithFormat:@"Booking Status: %@\nOrder Date: %@",bookingModel.bookStatus,[bookingModel dateString:bookingModel.orderDate]];

    UIFont *font = [UIFont fontWithName:@"DIN-Regular" size:15.0f];
    UIColor *color = [UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:1.f];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.paragraphSpacing = 10.f;
    
    NSMutableAttributedString *attTextWhite = [[NSMutableAttributedString alloc] initWithString:textWhite attributes:@{																	NSFontAttributeName:font,
                                                                                                                                                                                        
                                                                                                                                                                                        NSForegroundColorAttributeName:color,
                                                                                                                                                                                        NSParagraphStyleAttributeName:paragraphStyle,
                                                                                                                                                                                        }];
    
    NSMutableAttributedString *attTextGray = [[NSMutableAttributedString alloc] initWithString:textGray attributes:@{																	NSFontAttributeName:font,
                                                                                                                                                                                        NSForegroundColorAttributeName:color,
                                                                                                                                                                                        NSParagraphStyleAttributeName:paragraphStyle,
                                                                                                                                                                                        }];
    
    self.holidayDescLabelGray.attributedText = attTextGray;
    self.holidayDescLabelWhite.attributedText = attTextWhite;
    self.holidayHeaderLabel.text = textHeader;
    [self.holidayDescLabelGray setNumberOfLines:0];
    [self.holidayDescLabelWhite setNumberOfLines:0];
    
    if ([holidayBookingModel.bookStatus isEqualToString:@"Active"])
    {
        [self.holidayCancelButton setEnabled:YES];
        [self.holidayCancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
        [self.holidayCancelButton setTitle:@"Cancel" forState:UIControlStateSelected];
        
        self.holidayCancelButton.tag = 5;
        [self.holidayCancelButton addTarget:self action:@selector(cancelUserBooking:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    else// if ([holidayBookingModel.cancelStatus isEqualToString:@"Active"])
    {
        [self.holidayCancelButton setTitle:@"Pending" forState:UIControlStateNormal];
        [self.holidayCancelButton setTitle:@"Pending" forState:UIControlStateSelected];
        [self.holidayCancelButton setEnabled:NO];
    }

    if ([[FMUser defaultUser] isUserCorporate]) {
        [self.holidayCancelButton setHidden:true];
        [self.holidayCancelButton setEnabled:false];
    }
}

- (IBAction)cancleBtnSelector:(id)sender
{
    [self.adelegate bookingCollectionCellDidPressCancelBtn:sender];
}
- (IBAction)hotelCancelBtnSelector:(id)sender
{
    [self.adelegate bookingCollectionCellDidPressCancelBtn:sender];
}
- (IBAction)carCancelBtnSelector:(id)sender
{
    [self.adelegate bookingCollectionCellDidPressCancelBtn:sender];
}
- (IBAction)umrahCancelBtnSelector:(id)sender
{
    [self.adelegate bookingCollectionCellDidPressCancelBtn:sender];
}
- (IBAction)holidayCancleBtnSelector:(id)sender {
}

-(void)cancelUserBooking:(id)sender
{
	UIButton* btn = (UIButton*)sender;
	NSString* bookingId;
	NSString* cancelStatus;
    NSString* type;
	
	DLog(@"%@",_carBookingModel);
	DLog(@"%@",_hotelBookingModel);
	DLog(@"%@",_flightBookingModel);
	
	
	if (btn.tag == 1)
	{
		bookingId = [_flightBookingModel.orderId stringValue];
		cancelStatus = _flightBookingModel.cancelStatus;
	}
	else if (btn.tag == 2)
	{
		bookingId = [_carBookingModel.orderId stringValue];
		cancelStatus = _carBookingModel.cancelStatus;

	}
	else if (btn.tag == 3)
	{
		bookingId = [_hotelBookingModel.orderId stringValue];
		cancelStatus = _hotelBookingModel.cancelStatus;
	}
    else if (btn.tag == 4)
    {
        bookingId = [_umrahBookingModel.orderId stringValue];
        cancelStatus = _umrahBookingModel.bookStatus;
        type = @"Ummrahs";
    }
    else if (btn.tag == 5)
    {
        bookingId = [_holidayBookingModel.orderId stringValue];
        cancelStatus = _holidayBookingModel.bookStatus;
         type = @"Holidays";
    }
	if ([cancelStatus isEqualToString:@"Active"])
	{
		if (bookingId)
		{
            if (btn.tag!=4 && btn.tag!=5)
                [[NSNotificationCenter defaultCenter] postNotificationName:@"cancelBooking" object:bookingId];
            else
            {
                
                NSDictionary *Deleteobject = [[NSDictionary alloc] initWithObjectsAndKeys:bookingId,@"bookingId",type,@"type", nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"cancelPackageBooking" object:Deleteobject];
            }
		}
		else
		{
			UIAlertView* av = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Could not cancel booking" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
			[av show];
		}
	}
}

@end
