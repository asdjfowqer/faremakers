//
//  BookingListTableView.h
//  Faremakers
//
//  Created by Mac1 on 02/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BookingListViewCell.h"

typedef enum : NSUInteger {
    KFlightBooking,
    KCarBooking,
    KHotelBooking,
    KUmrahBooking,
    KHolidayBooking,
} BookingType;

@interface BookingListTableView : UIViewController<UITabBarDelegate,UITableViewDataSource>

@property (nonatomic) NSArray *listingsItems;
@property (nonatomic, strong) NSArray *bookingItems;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *noDataFoundView;
@property (weak, nonatomic) IBOutlet UIToolbar *toolBar;
@property (nonatomic) BookingType bookingType;


-(void)reloadData;

- (IBAction)filterButtonPressed:(id)sender;

@end
