//
//  PromotionListingsController.m
//  Faremakers
//
//  Created by Mac1 on 18/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "PromotionListingsController.h"
#import <SDWebImage/SDWebImagePrefetcher.h>
#import "PromotionDetailController.h"
#import "PromotionsListCell.h"
#import "Promotions.h"
#import "Constants.h"

@interface PromotionListingsController()<UITableViewDataSource,UITabBarControllerDelegate>

@end

@implementation PromotionListingsController


-(void) viewDidLoad
{
    [super viewDidLoad];
    
//    self.tableView.rowHeight = UITableViewAutomaticDimension;
//    self.tableView.estimatedRowHeight = 300;
    
    SDWebImagePrefetcher *prefetcher = [SDWebImagePrefetcher sharedImagePrefetcher];
    
    NSMutableArray *urls = [NSMutableArray array];
    for (int i =0; i < self.promotionData.count; i ++)
    {
        Promotions *promotionImage = self.promotionData[i];
        NSString *urlString = [[BASE_URL stringByDeletingLastPathComponent] stringByAppendingPathComponent:promotionImage.imageUrl];
        NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        [urls addObject:url];
    }
    [prefetcher prefetchURLs:urls];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}
-(void)animateTableViewToTop
{
    DLog(@"%.2f",self.view.frame.origin.y);
    [self.tableView setContentOffset:CGPointZero];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  self.promotionData.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
   return 230;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PromotionsListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PromotionsListCell"];
    cell.promotion = self.promotionData[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    PromotionDetailController *detailView = [self.storyboard instantiateViewControllerWithIdentifier:@"PromotionDetailController"];
    detailView.promotionData = self.promotionData[indexPath.row];
    [self.navigationController pushViewController:detailView animated:YES];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(nullable id)sender NS_AVAILABLE_IOS(5_0)
{
    PromotionDetailController *detailView = (PromotionDetailController*)segue.destinationViewController;
    NSIndexPath *index = [self.tableView indexPathForCell:(PromotionsListCell*)sender];
    detailView.promotionData = self.promotionData[index.row];
}
- (IBAction)filterButtonPressed:(id)sender
{
    
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
}


@end
