//
//  PromotionDetailController.m
//  Faremakers
//
//  Created by Mac1 on 18/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "PromotionDetailController.h"
#import <SDWebImage/SDWebImagePrefetcher.h>
#import "PromotionDetailHeader.h"
#import "PromotionDetailCell.h"
#import "NSString+Height.h"
#import "Promotions.h"
#import "Constants.h"

@interface PromotionDetailController()<UITableViewDataSource,UITabBarControllerDelegate>

@end

@implementation PromotionDetailController


-(void) viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Offer";
    [self loadHeaderView];
}
- (void)loadHeaderView
{
    [_tableHeaderView setPromotion:self.promotionData];
  //  _tableHeaderView = (PromotionDetailHeader *) self.tableView.tableHeaderView ;
//    tableHeaderView.item = self.item;
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0:{
            float height = 30;
            UIFont *font = [UIFont fontWithName:KCustomeFont size:14.0];
            CGFloat width = self.tableView.frame.size.width - 30;
            CGRect titleRect = [NSString
                                setAttributeWithString:self.promotionData.promDescription
                                withLineSpacing:kLineHeightSpacing
                                withSize:CGSizeMake(width, CGFLOAT_MAX)
                                withFont:font];
            
            CGFloat titleHeight = ceilf(titleRect.size.height);
            
            return height+titleHeight;
        }
            break;
        case 1:
        case 2:
        {
            return 44;
        }
        case 3:
        {
            float height = 33;

            UIFont *font = [UIFont fontWithName:KCustomeFont size:14.0];
            CGFloat width = self.tableView.frame.size.width - 30;
            CGRect titleRect = [NSString
                                setAttributeWithString:self.promotionData.termsAndConditions
                                withLineSpacing:kLineHeightSpacing
                                withSize:CGSizeMake(width, CGFLOAT_MAX)
                                withFont:font];
            
            CGFloat titleHeight = ceilf(titleRect.size.height);
            
            return height + titleHeight;
        }
            break;
        default:
            break;
    }
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;//self.promotionData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger index = indexPath.row;
    NSString *cellIdentifier = [@"PromotionDetailCell" stringByAppendingFormat:@"%ld",(long)index ];
    PromotionDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    cell.promotion = self.promotionData;
    [cell loadCellWithIdentifier:cellIdentifier];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (IBAction)filterButtonPressed:(id)sender
{
    
}

- (IBAction)bookNowButtonPressed:(id)sender
{
    NSInteger index = [self.promotionData getCurrentOfferMenuIndex];
    NSString *viewContrIdentifier;
        switch (index) {
            case 0:
                viewContrIdentifier = @"HotelSearchViewController";//hotels
                break;
            case 1:
                viewContrIdentifier = @"FlightSearchViewController";//Flihgts
                break;
            case 2:
                viewContrIdentifier = @"CarSearchViewController";//Cars
                break;
            case 3:
                viewContrIdentifier = @"HolidaysListingsViewController";//Holidays
                break;
            default:
                break;//
        }

    UINavigationController *navigationControlller = [[UINavigationController alloc] initWithNavigationBarClass:[FMNavigationBar class] toolbarClass:nil];
    [navigationControlller setViewControllers:@[[self.storyboard instantiateViewControllerWithIdentifier:viewContrIdentifier]]];

    [self.sideMenuViewController setContentViewController:navigationControlller
                                                 animated:YES];
    [self.sideMenuViewController hideMenuViewController];

}
@end
