//
//  PromotionsListCell.m
//  Faremakers
//
//  Created by Mac1 on 18/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "PromotionsListCell.h"
#import <UIImageView+WebCache.h>
#import "UILabel+FontName.h"
#import "BorderUIImageView.h"
#import "AirLinePricedItinerary.h"
#import "Promotions.h"
#import "Constants.h"

@implementation PromotionsListCell

- (void)awakeFromNib
{
    [self.titleLabel changeFontFamily];
    [self.descriptionLabel changeFontFamily];
}

- (NSAttributedString *)getPromotionNameAndCodeText:(Promotions *)promotion
{
    NSMutableAttributedString *nameAttribute = [[NSMutableAttributedString alloc] initWithString:promotion.promotionName];
    NSMutableParagraphStyle *nameStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    [nameStyle setAlignment:NSTextAlignmentLeft];

    return nameAttribute;
}

- (void)setPromotion:(Promotions *)promotion
{
    _promotion = promotion;
    //self.imageView

    self.titleLabel.attributedText = [self getPromotionNameAndCodeText:promotion];
    NSDateFormatter *dateFormater = [[NSDateFormatter alloc] init];
    [dateFormater setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    NSDate *date = [dateFormater dateFromString:promotion.validityP];
    [dateFormater setDateFormat:@"yyyy-MM-dd HH:mm"];

    self.descriptionLabel.text = [@"Validity " stringByAppendingFormat:@"%@", [dateFormater stringFromDate:date]];

    NSString *stringCode = [NSString stringWithFormat:@"code: %@", promotion.code];
    self.promoCodeLabel.text = stringCode;

    // UIImage *placeholderImage = [UIImage imageNamed:@"sample-Promotion.jpg"];
    [self.PromotionImageView sd_cancelCurrentImageLoad];
    [self.activityView startAnimating];
    if (_promotion)
    {
        [self.PromotionImageView sd_setImageWithURL:[NSURL URLWithString:[[BASE_URL stringByDeletingLastPathComponent] stringByAppendingPathComponent:_promotion.imageUrl]]
                                   placeholderImage:self.PromotionImageView.image
                                            options:SDWebImageRetryFailed
                                          completed: ^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
             [self.activityView stopAnimating];

             if (error)
             {
                 //self.PromotionImageView.image = placeholderImage;
             }
             else
             {
                 self.PromotionImageView.image = image;
             }
         }];
    }
    else
    {
        //self.PromotionImageView.image = placeholderImage;
    }
}

@end