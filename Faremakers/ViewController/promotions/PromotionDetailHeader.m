//
//  PromotionDetailHeader.m
//  Faremakers
//
//  Created by Mac1 on 19/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "PromotionDetailHeader.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "NSDate+DateComponents.h"
#import "Constants.h"
#import "Promotions.h"

@implementation PromotionDetailHeader

-(void)setPromotion:(Promotions *)promotion
{
    _promotion = promotion;
    //self.imageView
   
    self.promotionTypeLabel.text = promotion.promotionType;
    NSDateFormatter *dateFormater = [[NSDateFormatter alloc] init];
    [dateFormater setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    NSDate *date = [dateFormater dateFromString:promotion.validityP];
    [dateFormater setDateFormat:@"yyyy-MM-dd HH:mm"];
    
    self.validityLabel.text = [NSString stringWithFormat:@"%@ %@ %@ '%@",date.getDayOfTheWeek.capitalizedString,date.getDayOfTheMonth,date.getMonth,date.getYear];
//[dateFormater stringFromDate:date]];
    
    UIImage *placeholderImage = [UIImage imageNamed:@"preferredImage"];
    [self.headerImageView sd_cancelCurrentImageLoad];
    [self.activityView startAnimating];
    
    if (_promotion)
    {
        [self.headerImageView sd_setImageWithURL:[NSURL URLWithString:[[BASE_URL stringByDeletingLastPathComponent] stringByAppendingPathComponent:_promotion.imageUrl]]
                                   placeholderImage:placeholderImage
                                            options:SDWebImageRetryFailed
                                          completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
             [self.activityView stopAnimating];
             if(error == nil)
                 self.headerImageView.image = image;
             else
                 ;// self.headerImageView.image = [UIImage imageNamed:@"sample-Promotion.jpg"];
             
             
         }];
    }
    
}

@end
