//
//  PromotionsListCell.h
//  Faremakers
//
//  Created by Mac1 on 18/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//


#import <UIKit/UIKit.h>

@class Promotions;
@class BorderUIImageView;

@interface PromotionsListCell : UITableViewCell

@property (strong, nonatomic)Promotions *promotion;
@property (weak, nonatomic) IBOutlet BorderUIImageView *PromotionImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *promoCodeLabel;
@property (weak,nonatomic) IBOutlet UIActivityIndicatorView *activityView;

@end
