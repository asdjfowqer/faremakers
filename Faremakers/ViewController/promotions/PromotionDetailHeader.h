//
//  PromotionDetailHeader.h
//  Faremakers
//
//  Created by Mac1 on 19/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Promotions;

@interface PromotionDetailHeader : UIView

@property (strong, nonatomic)Promotions *promotion;
@property (weak,nonatomic) IBOutlet UIImageView *headerImageView;
@property (weak,nonatomic) IBOutlet UILabel *validityLabel;
@property (weak,nonatomic) IBOutlet UILabel *promotionTypeLabel;
@property (weak,nonatomic) IBOutlet UIActivityIndicatorView *activityView;


@end
