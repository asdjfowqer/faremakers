//
//  PromotionDetailController.h
//  Faremakers
//
//  Created by Mac1 on 18/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//


#import "RESideMenu.h"

@class PromotionDetailHeader;
@class Promotions;

@interface PromotionDetailController : UIViewController
@property (nonatomic)Promotions *promotionData;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet PromotionDetailHeader *tableHeaderView;
- (IBAction)bookNowButtonPressed:(id)sender;
@end
