//
//  PromotionListingsController.h
//  Faremakers
//
//  Created by Mac1 on 18/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PromotionListingsController : UIViewController
@property (nonatomic)NSArray *promotionData;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

-(void)animateTableViewToTop;

@end
