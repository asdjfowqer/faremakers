//
//  PromotionDetailCell.m
//  Faremakers
//
//  Created by Mac1 on 19/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "PromotionDetailCell.h"
#import "Promotions.h"
#import "UILabel+FontName.h"
#import <QuartzCore/QuartzCore.h>

@implementation PromotionDetailCell

-(void)awakeFromNib
{
    [self.termsLabel changeFontFamily];
    [self.codeLabel changeFontFamily];
    [self.titleLabel changeFontFamily];
    [self.descrLabel changeFontFamily];
}
- (void)loadCellWithIdentifier:(NSString *)cellIdentifier
{
    // Get Last componet from CellIdenfifier
    int value = [[cellIdentifier substringFromIndex: [cellIdentifier length] - 1] intValue];
    
    switch (value)
    {
        case 0:
            [self loadFirstCell];
            break;
        case 1:
            [self loadSecondCell];
            break;
        case 2:
            [self loadThirdCell];
            break;
        case 3:
            [self loadForthCell];
            break;

        default:
            break;
    }
}

-(void)loadFirstCell
{
    self.titleLabel.text = _promotion.promotionName;
    self.descrLabel.text = _promotion.promDescription;
}
-(void)loadSecondCell
{
    if (_promotion.code) {
        self.codeLabel.text = _promotion.code;
    }
    else
        self.codeLabel.text = @"Not Available";
}
-(void)loadThirdCell
{
    self.percentageLabel.text = _promotion.promotionPercentage.stringValue;
}
-(void)loadForthCell
{
    self.termsLabel.text = _promotion.termsAndConditions;
}
@end
