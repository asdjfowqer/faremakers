//
//  PromotionListingsController.m
//  Faremakers
//
//  Created by Mac1 on 18/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "PromotionHomeController.h"
#import "PromotionListingsController.h"
#import "PromotionDataApiManager.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "CarbonKit.h"

@interface PromotionHomeController () <CarbonTabSwipeNavigationDelegate,PromotionListingsDataDelegate>
{
    NSArray *items;
    NSArray *viewControllers;
    
    PromotionDataApiManager *promotionDataManager;
    CarbonTabSwipeNavigation *carbonTabSwipeNavigation;
}
@end

@implementation PromotionHomeController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getPromotionData];
    [self ShowActivityView];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (viewControllers)
    {
        for (PromotionListingsController *viewController in viewControllers)
        {
            [viewController animateTableViewToTop];
        }
    }
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)getPromotionData
{
    promotionDataManager = [[PromotionDataApiManager alloc] init];
    promotionDataManager.adelegate = self;
    [promotionDataManager getPromotionListingData];
}
//FlightDataManager Delegare
-(void) promotionDataManagerDidRecieveResponse:(id)object
{
    items = @[@"Flights",@"Hotels" /*,@"Cars"*/];
	
	
	DLog(@"%@",object);
	
    PromotionListingsController *viewController1 = [self.storyboard instantiateViewControllerWithIdentifier:@"PromotionListingsController"];
    viewController1.promotionData = [[NSArray alloc]initWithArray:[object objectForKey:@"Flight"]];
    PromotionListingsController *viewController2 = [self.storyboard instantiateViewControllerWithIdentifier:@"PromotionListingsController"];
    viewController2.promotionData = [[NSArray alloc]initWithArray:[object objectForKey:@"Hotel"]];
    PromotionListingsController *viewController3 = [self.storyboard instantiateViewControllerWithIdentifier:@"PromotionListingsController"];
    viewController3.promotionData = [[NSArray alloc]initWithArray:[object objectForKey:@"Car"]];

    viewControllers = @[viewController1,viewController2 /*,viewController3*/];
    
    carbonTabSwipeNavigation = [[CarbonTabSwipeNavigation alloc] initWithItems:items delegate:self];
    [carbonTabSwipeNavigation insertIntoRootViewController:self];
    [self style];
    [self ShowActivityView];

    
}
-(void) promotionDataManagerDidFail:(id)object
{
    [self ShowActivityView];
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"No Data Found!" preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [self.navigationController popViewControllerAnimated:YES];
    }]];
    [self.navigationController presentViewController:alertController animated:YES completion:nil];
}

- (void)style {
    
    UIColor *color = [UIColor colorWithRed:255.0/255 green:255.0/255 blue:255.0/255 alpha:1];
    carbonTabSwipeNavigation.toolbar.translucent = NO;
    [carbonTabSwipeNavigation setIndicatorColor:color];
    [carbonTabSwipeNavigation setTabExtraWidth:30];
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:self.view.frame.size.width/3.2 forSegmentAtIndex:0];
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:self.view.frame.size.width/3.2 forSegmentAtIndex:1];
    //[carbonTabSwipeNavigation.carbonSegmentedControl setWidth:self.view.frame.size.width/3.2 forSegmentAtIndex:2];
    [carbonTabSwipeNavigation.carbonTabSwipeScrollView setBackgroundColor:[UIColor colorWithRed:24.0/255 green:74.0/255 blue:150.0/255 alpha:1]];

    // Custimize segmented control
    [carbonTabSwipeNavigation setNormalColor:[UIColor colorWithRed:255.0/255 green:255.0/255 blue:255.0/255 alpha:0.5]
                                        font:[UIFont fontWithName:KCustomeFont size:14]];
    [carbonTabSwipeNavigation setSelectedColor:color
                                          font:[UIFont fontWithName:KCustomeFont size:14]];
}

# pragma mark - CarbonTabSwipeNavigation Delegate
// required
- (nonnull UIViewController *)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbontTabSwipeNavigation
                                 viewControllerAtIndex:(NSUInteger)index {
    return viewControllers[index];
}

// optional
- (void)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
                 willMoveAtIndex:(NSUInteger)index {
    
}

- (void)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
                  didMoveAtIndex:(NSUInteger)index {
    DLog(@"Did move at index: %ld", (unsigned long)index);
}

- (UIBarPosition)barPositionForCarbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation {
    return UIBarPositionTop; // default UIBarPositionTop
}
-(void)ShowActivityView
{
    UIView *view = [self.view viewWithTag:707];
    if (view)
    {
        [view removeFromSuperview];
        [[AppDelegate sharedDelegate] hideLoadingViewForView:self];
        
    }
    else
    {
        view = [[UIView alloc] initWithFrame:self.view.bounds];
        view.tag = 707;
        view.alpha = 0.5;
        
        [view setBackgroundColor:[UIColor whiteColor]];
        [self.view addSubview:view];
        [self.view bringSubviewToFront:view];
        [[AppDelegate sharedDelegate] showLoadingViewForView:self withText:@"Loading Promotions..."];
    }
}
-(void) viewWillDisappear:(BOOL)animated
{
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound)
    {
        [self CancleAllTask];
    }
    [super viewWillDisappear:animated];
}
-(void) CancleAllTask
{
    [promotionDataManager CancleAllTask];
}
@end
