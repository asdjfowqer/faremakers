//
//  PromotionDetailCell.h
//  Faremakers
//
//  Created by Mac1 on 19/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Promotions;

@interface PromotionDetailCell : UITableViewCell
@property (strong, nonatomic)Promotions *promotion;

#pragma mark PromotionDetailCell0
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *descrLabel;

#pragma mark PromotionDetailCell1
@property (weak, nonatomic) IBOutlet UIView *codeView;
@property (weak, nonatomic) IBOutlet UILabel *codeLabel;

#pragma mark PromotionDetailCell2
@property (weak, nonatomic) IBOutlet UILabel *percentageLabel;

#pragma mark PromotionDetailCell3
@property (weak, nonatomic) IBOutlet UILabel *termsLabel;

- (void)loadCellWithIdentifier:(NSString *)cellIdentifier;

@end
