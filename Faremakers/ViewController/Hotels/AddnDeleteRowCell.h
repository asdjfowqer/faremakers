//
//  AddnDeleteRowCell.h
//  Faremakers
//
//  Created by Mac1 on 12/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AddnDeleteRowCellDelegate <NSObject>

-(void) AddNewRowButtonDidSelected:(id)object;
-(void) DeleteRowButtonDidSelected:(id)object;

@end

@interface AddnDeleteRowCell : UITableViewCell

@property (nonatomic ,weak) id<AddnDeleteRowCellDelegate> adelegate;

#pragma mark AddNewRowCell0

- (IBAction)addNewRowBtnSelector:(id)sender;

#pragma mark AddNewRowCell1

- (IBAction)removeRowBtnSelector:(id)sender;
- (IBAction)addNewRowBtnSelector1:(id)sender;
@end
