//
//  CSStickyParallaxHeaderViewController.m
//  CSStickyHeaderFlowLayoutDemo
//
//  Created by James Tang on 6/4/14.
//  Copyright (c) 2014 Jamz Tang. All rights reserved.
//

#import "HotelDetailColletionController.h"
#import "DetailHotelCollectionViewCell.h"
#import "DetailMovieSectionHeader.h"
#import "DetailHotelColletionViewHeader.h"
#import "HotelRoomsViewController.h"
#import "RoomsTableViewContoller.h"
#import "CSStickyHeaderFlowLayout.h"
#import "DoubleDateCollectionCell.h"
#import "HotelBookingViewController.h"
#import "HotelDetailBottomView.h"
#import "HotelMapViewController.h"
#import "AminitiesView.h"
#import "Constants.h"
#import "HotelSearchItem.h"
#import "FMHotels.h"
#import "HotelRoomCategories.h"


static NSString *kDetailMovieTableViewCell = @"DetailHotelCollectionCell";

@interface HotelDetailColletionController ()<HotelDetailBottomViewDelegate,MWPhotoBrowserDelegate>
{
    BOOL isDismissing;
    BOOL isNewRelease;
    NSUInteger selectedShowTimeIndex;
    float headerHeight;
    BOOL shouldHideStatusBar;
    BOOL isBackgroundDark;
    BOOL isAnimating;
    NSMutableArray *photos;
}

@property (nonatomic, strong) NSArray *sections;
@property (nonatomic, strong) UINib *headerNib;

@end

@implementation HotelDetailColletionController


- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        self.headerNib = [UINib nibWithNibName:@"DetailHotelColletionViewHeader" bundle:nil];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self reloadLayout];
    
    shouldHideStatusBar = YES;
    // Also insets the scroll indicator so it appears below the search bar
    self.collectionView.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    
    [self.collectionView registerNib:self.headerNib
          forSupplementaryViewOfKind:CSStickyHeaderParallaxHeader
                 withReuseIdentifier:@"header"];
    
    //Double Date Cell Nib
    [self.collectionView registerNib:[UINib nibWithNibName:@"DoubleDateCollectionCell" bundle:nil]
          forCellWithReuseIdentifier:@"DoubleDateCollectionCell"];
    
    isBackgroundDark = YES;
    
    [self createBookHotelButtonView];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [self reloadLayout];
}


- (void)reloadLayout {
    
    CSStickyHeaderFlowLayout *layout = (id)self.collectionViewLayout;
    
    if ([layout isKindOfClass:[CSStickyHeaderFlowLayout class]])
    {
        layout.parallaxHeaderReferenceSize = CGSizeMake(self.view.frame.size.width, 320);
        layout.parallaxHeaderMinimumReferenceSize = CGSizeMake(self.view.frame.size.width, 10);
        layout.itemSize = CGSizeMake(self.view.frame.size.width, layout.itemSize.height);
        layout.parallaxHeaderAlwaysOnTop = NO;
        
        // If we want to disable the sticky header effect
        layout.disableStickyHeaders = YES;
    }
    
}

#pragma mark CSAlwayOnTopDelegate

- (void) getCollectionSizeOfImage:(CGSize)size atIndex:(NSInteger)index
{
    CSStickyHeaderFlowLayout *layout = (id)self.collectionViewLayout;
    headerHeight = size.height;
    
    if ([layout isKindOfClass:[CSStickyHeaderFlowLayout class]]) {
        layout.parallaxHeaderReferenceSize = CGSizeMake(self.view.frame.size.width, size.height);
        layout.parallaxHeaderMinimumReferenceSize = CGSizeMake(self.view.frame.size.width, (isNewRelease)?53:20);
        layout.itemSize = CGSizeMake(self.view.frame.size.width, layout.itemSize.height);
    }
    
}
-(void)showStatuBarOnTap:(BOOL)update{
    
    shouldHideStatusBar = !update;
    isAnimating = update;
    
    [self setNeedsStatusBarAppearanceUpdate];
    
}
- (void) collectionViewDidReceiveTap:(CGPoint)point{
    
}
- (void) getBrightnessFromImage:(BOOL)dark{
    isBackgroundDark = dark;
}


#pragma mark UICollectionViewDataSource

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    
    return  CGSizeMake(self.view.frame.size.width, 0.1);
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;//[self.sections count];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
//    return 7 + 1;//[self.sections[section] count];
	return 6 ;//[self.sections[section] count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSInteger index = indexPath.row;
    
    if (index == 3)
    {
        
        DoubleDateCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"DoubleDateCollectionCell" forIndexPath:indexPath];
        cell.dateItem = self.hotelSearchItem.doubleDate;
        [cell disableDateSelection:NO];
        return cell;
    }
    else
    {
        NSString *identifier = [kDetailMovieTableViewCell stringByAppendingFormat:@"%ld", (long)index];
        
        DetailHotelCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier
                                                                                        forIndexPath:indexPath];
        cell.hotelSearchItem = self.hotelSearchItem;
        [cell setHotelDetailItem:self.hotel withIdentifier:identifier];
        
        return cell;
    }
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CGFloat height = 66;
    if (indexPath.row == 3)
        height = 80;
    
    //Edit it when you have Real Data change Height for last Row to 44
    return CGSizeMake(self.view.frame.size.width, height) ;
    
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        
        UICollectionReusableView *cell = [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                                            withReuseIdentifier:@"sectionHeader"
                                                                                   forIndexPath:indexPath];
        return cell;
        
    }
    else if ([kind isEqualToString:CSStickyHeaderParallaxHeader])
    {
        DetailHotelColletionViewHeader *cell = [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                                                  withReuseIdentifier:@"header"
                                                                                         forIndexPath:indexPath];
        [cell loadCollectionViewWithArray:self.hotel.hotelImages];
        cell.adelegate = self;
        return cell;
    }
    return nil;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    
    switch (indexPath.row) {
        case 1:
        {
            [self openMapLocationWithPlace:@"Hafeez center lahore"];
        }
            break;
            
        case 2:
        {
            AminitiesView *aminityView =  [[AminitiesView alloc] init];
            [aminityView showPopupCentered:nil];
        }
            break;
        case 4:
        {
//            [self showRoomsViewController];
        }
            break;
        default:
            break;
    }
    
}
- (void)showRoomsViewController
{
    RoomsTableViewContoller *roomsTableView = [self.storyboard instantiateViewControllerWithIdentifier:@"RoomsTableViewContoller"];
    [self.navigationController pushViewController:roomsTableView animated:YES];
}

- (BOOL)prefersStatusBarHidden {
    return shouldHideStatusBar;
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return (isBackgroundDark)?UIStatusBarStyleDefault:UIStatusBarStyleLightContent;
}
#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self showStatuBarOnTap:NO];
    });
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if (!isAnimating) {
        
        shouldHideStatusBar = (scrollView.contentOffset.y - self.view.frame.origin.y + 20 < headerHeight);
        
        [self setNeedsStatusBarAppearanceUpdate];
    }
}

#pragma mark CSSHeaderSectionDelegate

- (void)backButtonPressed:(id)sender
{
    
}
- (void) collectionViewDidSelectItemAtIndexPath:(NSIndexPath*)indexPath
{
    [self showImageGallary:indexPath.row];
}

- (void) backButtonPressedFromTopHeader:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark bookHotelView

-(void)createBookHotelButtonView
{
    NSArray* nibViews = [[NSBundle mainBundle] loadNibNamed:@"HotelDetailBottomView" owner:self options:nil];
    
    HotelDetailBottomView *bottomView = [ nibViews objectAtIndex: 0];
    HotelRoomCategories *roomCategory = self.hotel.roomCategories[0];
    //   _priceLabel.text = [NSString stringWithFormat:@"%@ %@",[roomCategory calculatePricePerNight:self.hotel.baseFare.stringValue],self.hotel.currencyCode];
    bottomView.priceLabel.text = [NSString stringWithFormat:@"%@ %@ /night",[roomCategory calculatePricePerNight:self.hotel.baseFare.stringValue],self.hotel.currencyCode];//[NSString stringWithFormat:@"%@ %@ /night",self.hotel.baseFare,self.hotel.currencyCode];
    
    bottomView.adelegate = self;
    CGRect frame = bottomView.frame;
    frame.size.width = self.view.frame.size.width;
    frame.origin.y = self.view.frame.size.height - 44;
    
    bottomView.frame = frame;
    [self.view addSubview:bottomView];

    self.collectionView.contentInset = UIEdgeInsetsMake(0, 0, bottomView.frame.size.height, 0);
}
- (void) hotelDetailBottomViewRoomSelectoreClicked:(id)sender
{
    [self showRoomSelectionController:sender];
}
- (void) hotelDetailBottomViewPriceSelectoreClicked:(id)sender
{
    [self showRoomSelectionController:sender];
}
- (void)showRoomSelectionController:(id)sender
{
    HotelRoomsViewController *bookingController = [self.storyboard instantiateViewControllerWithIdentifier:@"HotelRoomsViewController"];
    bookingController.hotelItem = self.hotel;
    bookingController.searchItem = self.hotelSearchItem;
    [self.navigationController pushViewController:bookingController animated:YES];
}
- (void)bookHotelButtonSelector:(id)sender
{
    HotelBookingViewController *bookingController = [self.storyboard instantiateViewControllerWithIdentifier:@"HotelBookingViewController"];
    [self.navigationController pushViewController:bookingController animated:YES];
}
- (void)showImageGallary:(NSInteger)index
{
    if (self.hotel.hotelImages.count < 1) {
        return;
    }
    photos = [[NSMutableArray alloc] init];
    MWPhoto *photo;
    for (HotelImageItem *hotelImageItem in self.hotel.hotelImages)
    {
        photo = [MWPhoto photoWithURL:[NSURL URLWithString:hotelImageItem.imageUrl]];
        photo.caption = hotelImageItem.imageDescription;
        [photos addObject:photo];
        
    }
    if (photos.count)
    {
        // Create browser
        MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
        browser.displayActionButton = NO;
        browser.displayNavArrows = NO;
        browser.displaySelectionButtons = NO;
        browser.alwaysShowControls = NO;
        browser.zoomPhotosToFill = YES;
        browser.enableGrid = NO;
        browser.startOnGrid = NO;
        browser.enableSwipeToDismiss = NO;
        browser.autoPlayOnAppear = NO;
        [browser setCurrentPhotoIndex:index];
        {
            // Modal
            UINavigationController *navigationControlller = [[UINavigationController alloc] initWithNavigationBarClass:[FMNavigationBar class] toolbarClass:nil];
            [navigationControlller setViewControllers:@[browser]];
            navigationControlller.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            [self presentViewController:navigationControlller animated:YES completion:nil];
        }
    }
    
}
- (void)openMapLocationWithCoordinates:(CLLocationCoordinate2D )coordinates withPlace:(NSString *)place
{
    NSString *url = [@"http://maps.apple.com/?q=" stringByAppendingString:[place stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
    url = [url stringByAppendingString:[NSString stringWithFormat:@"&near=%f,%f",coordinates.latitude,coordinates.longitude]];
    //   [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}
- (void)openMapLocationWithPlace:(NSString *)place
{
    HotelMapViewController *mapView = [self.storyboard instantiateViewControllerWithIdentifier:@"HotelMapViewController"];
    mapView.mapType = SingleMAP;
    mapView.hotelsList = @[self.hotel];
    [self.navigationController pushViewController:mapView animated:YES];
    
}
#pragma mark - MWPhotoBrowserDelegate

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return photos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < photos.count)
        return [photos objectAtIndex:index];
    return nil;
}

- (void)photoBrowser:(MWPhotoBrowser *)photoBrowser didDisplayPhotoAtIndex:(NSUInteger)index {
    DLog(@"Did start viewing photo at index %lu", (unsigned long)index);
}

- (void)photoBrowserDidFinishModalPresentation:(MWPhotoBrowser *)photoBrowser {
    // If we subscribe to this method we must dismiss the view controller ourselves
    DLog(@"Did finish modal presentation");
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
