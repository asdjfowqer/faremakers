//
//  HotelTableViewCell.h
//  Faremakers
//
//  Created by Mac1 on 18/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HotelDealBannerView;
@class HotelPriceTagView;

@interface HotelDealBannerView : UIView

@property (weak, nonatomic) IBOutlet UILabel *dealLabel;

@end


@interface HotelPriceTagView : UIView

@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *discountLabel;
@property (weak, nonatomic) IBOutlet UILabel *oldPriceLabel;

@end


@class FMHotels;

@interface HotelTableViewCell : UITableViewCell

@property (strong, nonatomic) FMHotels *hotel;
@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *starImage;
@property (weak, nonatomic) IBOutlet UIImageView *hotelImageView;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *payAtHotelButton;
@property (weak, nonatomic) IBOutlet UILabel *hotelLocationLabel;
@property (weak, nonatomic) IBOutlet UILabel *hotelNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *reviewLabel;
@property (weak, nonatomic) IBOutlet UIButton *goRatingLabel;
@property (weak, nonatomic) IBOutlet UIButton *addToFavortiButton;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityView;

@property (weak, nonatomic) IBOutlet HotelDealBannerView *hotelDealView;
@property (weak, nonatomic) IBOutlet HotelPriceTagView *hotelPriceTagview;

- (IBAction)bannerInfoBtnSelector:(id)sender;
- (IBAction)PayAtHotelSelector:(id)sender;
- (IBAction)AddToFavourtySelector:(id)sender;
- (IBAction)FreeCancellationSelector:(id)sender;
@end
