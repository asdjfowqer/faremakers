//
//  HotelTableViewCell.m
//  Faremakers
//
//  Created by Mac1 on 18/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "HotelTableViewCell.h"
#import "HotelDataApiManger.h"
#import "UILabel+FontName.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "HotelRoomCategories.h"
#import "FMHotels.h"
#import "Constants.h"

@implementation HotelDealBannerView

@end

@implementation HotelPriceTagView


@synthesize oldPriceLabel;
@synthesize priceLabel;
@synthesize discountLabel;

- (void)awakeFromNib
{
    
}

@end

@implementation HotelTableViewCell

- (void)awakeFromNib
{
    [self.hotelLocationLabel changeFontFamily];
    [self.hotelNameLabel changeFontFamily];
    [self.reviewLabel changeFontFamily];
    [self.goRatingLabel.titleLabel changeFontFamily];
    //[self.priceLabel changeFontFamily];
}
-(void)setHotel:(FMHotels *)hotel
{
    _hotel = hotel;
    HotelRoomCategories *roomCategory = self.hotel.roomCategories[0];
    _priceLabel.text = [NSString stringWithFormat:@"%@ %@",[roomCategory calculatePricePerNight:self.hotel.baseFare.stringValue],self.hotel.currencyCode];
    _hotelNameLabel.text = self.hotel.hotelName;
    
    for (NSInteger i = 0 ; i < 5 ; i ++ )
    {
        UIImageView *starImageView = self.starImage[i];
        
        if (self.hotel.rating.integerValue <= i)
            starImageView.image = [UIImage imageNamed:@"star_empty"];
        else
            starImageView.image = [UIImage imageNamed:@"selected_star"];
    }
    
    [_goRatingLabel setTitle:[NSString stringWithFormat:@"%@/5",self.hotel.rating] forState:UIControlStateNormal];
    
    if (_hotel.locationTitle) {
        _hotelLocationLabel.text = _hotel.locationTitle;
        
    }
    else
    {
        NSDictionary *cities = self.hotel.city;
        _hotelLocationLabel.text = [NSString stringWithFormat:@"%@",[cities objectForKey:@"cityName"]];
        
        //        [_hotel getLocationWithsuccessBlock:^(id responseobject) {
        //            _hotelLocationLabel.text = _hotel.locationTitle;
        //
        //        } failureBlock:^(NSError *error) {
        //
        //        }];
    }
    
    UIImage *placeHolder = [UIImage imageNamed:@"placeholder"];

    [self.hotelImageView sd_setImageWithURL:[NSURL URLWithString:_hotel.hotelImageUrl]
                           placeholderImage:placeHolder
                                    options:SDWebImageRetryFailed
                                  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
     {
         [self.activityView stopAnimating];
         
         if(error == nil)
             self.hotelImageView.image = image;
         else
             self.hotelImageView.image = placeHolder;
         
     }];
}

- (void)setValuesForPriceTag:(NSDictionary *)item
{
    self.hotelPriceTagview.priceLabel.text = @"1000";
    self.hotelPriceTagview.oldPriceLabel.text = @"1300";
    self.hotelPriceTagview.discountLabel.text = @"30 %";
}
- (IBAction)bannerInfoBtnSelector:(id)sender
{
    
}

- (IBAction)PayAtHotelSelector:(id)sender
{
}

- (IBAction)AddToFavourtySelector:(id)sender
{
}
- (IBAction)FreeCancellationSelector:(id)sender
{
}
@end
