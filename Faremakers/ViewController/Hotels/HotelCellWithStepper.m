//
//  HotelCellWithStepper.m
//  Faremakers
//
//  Created by Mac1 on 17/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "HotelCellWithStepper.h"
#import "UILabel+FontName.h"
#import "Rooms.h"

@implementation HotelCellWithStepper

- (void)awakeFromNib {
    // Initialization code    
  //  [self.AdultLabel changeFontFamily];
}
-(void)loadRoomData:(Rooms*)room withIndexPath:(NSIndexPath*)indexPath
{
    _room = room;
    if (indexPath.row == 1)
    {
        self.AdultLabel.text = [NSString stringWithFormat:@"%ld Childrens",(long)room.numberOfChileds];
        self.travellerStepper.value = self.room.numberOfChileds;
        self.travellerStepper.minimumValue = 0;
        self.travellerStepper.maximumValue = 2;
        
        if (self.room.numberOfChileds == 0) {
            self.travellerStepper.value = 0;
        }

    }
    else if (indexPath.row == 0)
    {
        self.AdultLabel.text = [NSString stringWithFormat:@"%ld Adults",(long)room.numberOfAdults];
        self.travellerStepper.value = self.room.numberOfAdults;
        self.travellerStepper.minimumValue = 1;
        self.travellerStepper.maximumValue = 2;

    }
}

- (IBAction)stepperValueChanged:(id)sender
{
    UIStepper *stepper  = (UIStepper*)sender;
    NSIndexPath *indexPath = [(UITableView*)self.superview.superview indexPathForCell:(UITableViewCell *)stepper.superview.superview];
    
    NSInteger value = [stepper value];

    if (indexPath.row == 1)
    {
        if (value == 0 && self.room.numberOfChileds == 0) {//3rd room has child stepper to 1 by default
            return;
        }
        
        self.AdultLabel.text = [NSString stringWithFormat:@"%ld Childrens",(long)value];
        
        if (value == 1 && !self.room.numberOfChileds)
        {
            self.room.numberOfChileds = value;
            [self.aDelegate AddNewRowForChildAgeInSection:[NSIndexPath indexPathForItem:(indexPath.row+1) inSection:indexPath.section]];
        }
        else if(value == 0)
        {
            self.room.numberOfChileds = value;
            [self.aDelegate removeRowForChildAgeInSection:[NSIndexPath indexPathForItem:(indexPath.row+1) inSection:indexPath.section]];
        }
        else if(value == 2)
        {
            self.room.numberOfChileds = value;
            [self.aDelegate AddNewRowForChildAgeInSection:[NSIndexPath indexPathForItem:(indexPath.row+2) inSection:indexPath.section]];
        }
        else if(value == 1 && self.room.numberOfChileds == 2)
        {
            self.room.numberOfChileds = value;
            [self.aDelegate removeRowForChildAgeInSection:[NSIndexPath indexPathForItem:(indexPath.row+2) inSection:indexPath.section]];
        }
        else
        {
            self.room.numberOfChileds = value;
        }

    }
    else if (indexPath.row == 0)
    {
        self.AdultLabel.text = [NSString stringWithFormat:@"%ld Adults",(long)value];
        self.room.numberOfAdults = (value);

    }
    
}
@end
