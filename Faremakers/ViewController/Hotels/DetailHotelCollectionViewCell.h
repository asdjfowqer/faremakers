//
//  CSCell.h
//  CSStickyHeaderFlowLayoutDemo
//
//  Created by Jamz Tang on 8/1/14.
//  Copyright (c) 2014 Jamz Tang. All rights reserved.
//



#import <MapKit/MapKit.h>

@class FMHotels;
@class HotelSearchItem;

@interface DetailHotelCollectionViewCell : UICollectionViewCell< MKMapViewDelegate, UITextViewDelegate >

@property (nonatomic,strong) FMHotels *hotelDetailItem;
@property (nonatomic) HotelSearchItem *hotelSearchItem;

-(void)setHotelDetailItem:(id)hotelDetailItem withIdentifier:(NSString*)cellIdentifier;


#pragma mark - DetailHotelCollectionCell 0
@property (weak, nonatomic) IBOutlet UILabel *hotelTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *hotelDetailLabel;
@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *starsImageView;


#pragma mark - DetailHotelCollectionCell 1
@property (weak, nonatomic) IBOutlet UILabel *hotelAddressLabel;


#pragma mark - DetailHotelCollectionCell 2
@property (weak, nonatomic) IBOutlet UIButton *hotelAminityWifi;
@property (weak, nonatomic) IBOutlet UIButton *hotelAminityService;
@property (weak, nonatomic) IBOutlet UIButton *hotelAminityPool;
@property (weak, nonatomic) IBOutlet UIButton *hotelAminityRest;
@property (weak, nonatomic) IBOutlet UIButton *hotelAminityGym;


#pragma mark - DetailHotelCollectionCell 4
@property (weak, nonatomic) IBOutlet UILabel *numberOfRoomsLabel;
@property (weak, nonatomic) IBOutlet UILabel *roomDetailLabel;


#pragma mark - DetailHotelCollectionCell 5
@property (weak, nonatomic) IBOutlet UILabel *costTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *costPaymentLabel;


#pragma mark - DetailHotelCollectionCell 6

@property (weak, nonatomic) IBOutlet UILabel *hotelPolicyLabel;


@end
