//
//  HotelListingsViewController.h
//  Faremakers
//
//  Created by Mac1 on 18/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//


#import <YAScrollSegmentControl/YAScrollSegmentControl.h>

@protocol HotelListingsViewDelegate <NSObject>

-(void) hotelListingsViewDidSelectFilters:(id)item;

@end

@class HotelSearchItem;

@interface HotelListingsViewController : UIViewController <UITableViewDataSource,UITabBarDelegate,UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIToolbar *toolBar;
@property (weak, nonatomic) IBOutlet UITabBar *tabBarView;

@property (nonatomic) NSArray *hotelList;
@property (nonatomic) HotelSearchItem *searchItem;

- (IBAction)shortListButtonPressed:(id)sender;
- (IBAction)filterButtonPressed:(id)sender;

@end
