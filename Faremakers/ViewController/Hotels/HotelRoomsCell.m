//
//  HotelRoomsCell.m
//  Faremakers
//
//  Created by Mac1 on 10/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "HotelRoomsCell.h"
#import "HotelRoomCategories.h"
#import "UILabel+FontName.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "FMHotels.h"

@implementation HotelRoomsCell

- (void)awakeFromNib
{
    [self.roomDesLabel changeFontFamily];
    [self.roomNameLabel changeFontFamily];
    [self.priceLabel changeFontFamily];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setRoomCategories:(HotelRoomCategories *)roomCategories
{
    _roomCategories = roomCategories;
    
    [self.hotelImageView sd_setImageWithURL:[NSURL URLWithString:self.hotels.hotelImageUrl]
                           placeholderImage:self.hotelImageView.image
                                    options:SDWebImageRetryFailed
                                  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
     {
         [self.activityView stopAnimating];
         if(error == nil)
             self.hotelImageView.image = image;
         else
             ;//self.hotelImageView.image = [UIImage imageNamed:@"no-images.png"];
         
     }];
    self.roomNameLabel.text = self.roomCategories.hotelName;
    self.priceLabel.text = [NSString stringWithFormat:@"%@ %@",[self.roomCategories calculatePricePerNight:self.roomCategories.price],self.roomCategories.currency];
    self.roomDesLabel.text = self.roomCategories.roomDescription;
}
- (IBAction)selecRoomBtnPressed:(id)sender
{
    [self.adelegate HotelRoomsCellDidSelectRoom:sender];
}
@end
