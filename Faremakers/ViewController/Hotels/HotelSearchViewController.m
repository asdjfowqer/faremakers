//
//  HotelSearchViewController.m
//  Faremakers
//
//  Created by Mac1 on 16/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "HotelSearchViewController.h"
#import "CitySerachViewController.h"
#import "HotelCellWithStepper.h"
#import "DoubleDateTableCell.h"
#import "IQActionSheetPickerView.h"
#import "HotelListingsHomeViewController.h"
#import "AddnDeleteRowCell.h"
#import "HotelSearchItem.h"
#import "Rooms.h"
#import "Constants.h"
#import "Cities.h"

@interface HotelSearchViewController () <IQActionSheetPickerViewDelegate,AddnDeleteRowCellDelegate,CityNameSearchDelegate>
{
    NSIndexPath *selectedSection;
    CitySerachViewController *citySearchView;
}

@end

@implementation HotelSearchViewController


#define KRoomTableView 708
#define KTimmingTableView 707

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    citySearchView = [self.storyboard instantiateViewControllerWithIdentifier:@"CitySerachViewController"];
    citySearchView.adelegate = self;
    
    _item = [[HotelSearchItem alloc] init];
    Rooms *room = [[Rooms alloc] initWithNumberOfAdults:1];
    [_item.rooms addObject:room];
    
    [self.schedTableView registerNib:[UINib nibWithNibName:@"DoubleDateTableCell" bundle:nil] forCellReuseIdentifier:KDoubleDateTableCell];
    [self EnableScrollingForRoomsTable];
    [self adjustMarginsForLagerHeights];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([tableView tag] == KTimmingTableView)
    {
        return 1;
    }
    return _item.rooms.count + 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([tableView tag] == KTimmingTableView)
    {
        return 2;
    }
    if (section >= _item.rooms.count)
    {
        return 1;
    }
    Rooms *room = _item.rooms [section];
    
    return (room.numberOfChileds + 2);
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    if([view isKindOfClass:[UITableViewHeaderFooterView class]])
    {
        UITableViewHeaderFooterView *tableViewHeaderFooterView = (UITableViewHeaderFooterView *) view;
        tableViewHeaderFooterView.textLabel.textColor = [UIColor lightGrayColor];
        tableViewHeaderFooterView.textLabel.font = [UIFont fontWithName:KCustomeRagularFont size:14.0];
    }
}
- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if ([tableView tag] == KRoomTableView && section < _item.rooms.count)
    {
        return [NSString stringWithFormat:@"Room %ld",(long)(section+1)];
    }
    return NULL;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if ([tableView tag] == KRoomTableView && section < _item.rooms.count)
        return 18;
    return 0.1;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView tag] == KTimmingTableView){
        if (indexPath.row == 1)
            return 90;
    }
    return 60;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView tag] == KTimmingTableView)
    {
        return [self prepareTimingCellForTableView:tableView forIndexPath:indexPath];
    }
    else{
        return [self prepareRoomCellForTableView:tableView forIndexPath:indexPath];
    }
}
- (id)prepareTimingCellForTableView:(UITableView*)tableView forIndexPath:(NSIndexPath*)indexPath
{
    
    if (indexPath.row == 0)
    {
        UITableViewCell *cell = [self.schedTableView dequeueReusableCellWithIdentifier:@"SechudleTableCell" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.text = _item.searchTitle;
        return cell;
    }
    else
    {
        DoubleDateTableCell *cell = [self.schedTableView dequeueReusableCellWithIdentifier:KDoubleDateTableCell forIndexPath:indexPath];
        cell.departTitle.text = @"Check In";
        cell.returnTitle.text = @"Check Out";
        cell.doubleDateItem = _item.doubleDate;
        cell.searchType  = 1;//for Hotels Only
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    
}
- (id)prepareRoomCellForTableView:(UITableView*)tableView forIndexPath:(NSIndexPath*)indexPath
{
    if (indexPath.section >= _item.rooms.count)
    {
        NSInteger section = (indexPath.section > 1)?1:0;
        NSString *reuseableIdentifier = [NSString stringWithFormat:@"AddNewRowCell%ld",(long)(section)];
        AddnDeleteRowCell *cell = [self.roomsTableView dequeueReusableCellWithIdentifier:reuseableIdentifier forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.adelegate = self;
        return cell;
    }
    if (indexPath.row < 2)
    {
        HotelCellWithStepper *cell = [self.roomsTableView dequeueReusableCellWithIdentifier:@"HotelCellWithStepper" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell loadRoomData:_item.rooms[indexPath.section] withIndexPath:indexPath];
        cell.aDelegate = self;
        return cell;
    }
    else
    {
        Rooms *newRoom = _item.rooms[indexPath.section];
        
        UITableViewCell *cell = [self.roomsTableView dequeueReusableCellWithIdentifier:@"HotelChildAgeCell" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (indexPath.row  == 2)
        {
            cell.textLabel.text = [NSString stringWithFormat:@"1st Child' Age"];
            cell.detailTextLabel.text = [NSString stringWithFormat:@" %ld ' yrs",(long)newRoom._1stChildAge];
        }
        else{
            cell.textLabel.text = [NSString stringWithFormat:@"2nd Child' Age"];
            cell.detailTextLabel.text = [NSString stringWithFormat:@" %ld ' yrs",(long)newRoom._2ndChildAge];
        }
        
        return cell;
        
    }
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([tableView tag] == KRoomTableView && indexPath.row >= 2 && indexPath.section < (_item.rooms.count))
    {
        selectedSection = indexPath;
        [self showPickerView];
    }
    else  if([tableView tag] == KTimmingTableView && indexPath.row == 0)
    {
        [self PresentCitySearchViewController];
    }
    
}
#pragma mark CitySearchController

- (void)PresentCitySearchViewController
{
    [self.navigationController pushViewController:citySearchView animated:YES];
}

-(void) CitySearchViewController:(id)viewController didSelectCity:(id)city
{
    CitySerachViewController *Controller = (CitySerachViewController*)viewController;
    [_item setDestCity:city];
    _item.searchTitle = _item.destCity.text;
    [self.schedTableView reloadData];
    [Controller.navigationController popViewControllerAnimated:YES];
}

#pragma mark HotelCellWithStepperDelegate

- (void) AddNewRowForChildAgeInSection:(NSIndexPath*)indexPath
{
    Rooms *newRoom = _item.rooms[indexPath.section];
    
    if (indexPath.row == 2)
        newRoom._1stChildAge = 2;
    else
        newRoom._2ndChildAge = 2;
    
    
    NSIndexPath *newIndexPath = [NSIndexPath indexPathForItem:indexPath.row inSection:indexPath.section];
    [self.roomsTableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationBottom];
    
    //  [self.roomsTableView setScrollEnabled:YES];
}
- (void) removeRowForChildAgeInSection:(NSIndexPath*)indexPath // fix when Remove a Room and we have 3 rows
{
    Rooms *newRoom = _item.rooms[indexPath.section];
    newRoom._1stChildAge = 0;
    
    NSIndexPath *newIndexPath = [NSIndexPath indexPathForItem:indexPath.row inSection:indexPath.section];
    NSLog(@"%ld",(long)indexPath.row);
    [self.roomsTableView deleteRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationBottom];
    //[self EnableScrollingForRoomsTable];
}
-(void) AddNewRowButtonDidSelected:(id)object
{
    [self AddDeleteRoomSelector:object];
}
-(void) DeleteRowButtonDidSelected:(id)object
{
    [self AddDeleteRoomSelector:object];
}

- (IBAction)searchClicked:(id)sender
{
    if (![_item checkAllValues])
    {
        [self showHotelListingsView];
    }
    
    //
}
-(void)showHotelListingsView
{
    HotelListingsHomeViewController *viewContr = [self.storyboard instantiateViewControllerWithIdentifier:@"HotelListingsHomeViewController"];
    viewContr.searchItem = _item;
    [viewContr searchHotelDataWithDictionary:[_item preparePostData]];
    [self.navigationController pushViewController:viewContr animated:YES];
}
-(void)EnableScrollingForRoomsTable
{
    self.roomsTableView.scrollEnabled = true;//Modified these two lines
}
- (IBAction)AddDeleteRoomSelector:(id)sender
{
    if ([sender tag] == 1)
    {
        [self AddNewRoom];
    }
    else
    {
        [self DeleteRoom];
    }
    [self.roomsTableView reloadData];
    [self EnableScrollingForRoomsTable];
}
-(void)AddNewRoom
{
    Rooms *newRoom = [[Rooms alloc] initWithNumberOfAdults:1];
    [_item.rooms addObject:newRoom];
}
-(void)DeleteRoom
{
    if (_item.rooms.count == 1) {
        return;
    }
    [_item.rooms removeLastObject];
}

#pragma mark IQActionSheetPickerViewDelegate

- (void)showPickerView
{
    IQActionSheetPickerView *picker = [[IQActionSheetPickerView alloc] initWithTitle:@"Select Age" delegate:self];
    picker.titleFont = [UIFont fontWithName:KCustomeFont size:14];
    [picker setTag:1];
    [picker setTitlesForComponents:@[@[@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8",@"9"]]];
    [picker show];
}

-(void)actionSheetPickerView:(IQActionSheetPickerView *)pickerView didSelectTitles:(NSArray *)titles
{
    Rooms *newRoom = _item.rooms[selectedSection.section];
    
    if (selectedSection.row == 2)
    {
        
        newRoom._1stChildAge = [[titles firstObject] integerValue];
        UITableViewCell *cell = [self.roomsTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:2 inSection:selectedSection.section]];
        
        cell.textLabel.text = [NSString stringWithFormat:@"1st Child' Age"];
        cell.detailTextLabel.text = [NSString stringWithFormat:@" %ld ' yrs",(long)newRoom._1stChildAge];
    }
    else if (selectedSection.row == 3)
    {
        
        newRoom._2ndChildAge = [[titles firstObject] integerValue];
        UITableViewCell *cell = [self.roomsTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:3 inSection:selectedSection.section]];
        cell.textLabel.text = [NSString stringWithFormat:@"2nd Child' Age"];
        cell.detailTextLabel.text = [NSString stringWithFormat:@" %ld ' yrs",(long)newRoom._2ndChildAge];
    }
}
- (void) adjustMarginsForLagerHeights
{
    if ([UIScreen mainScreen].bounds.size.height == 480)
    {
        self.roomsTableTopMargin.constant = 20;
        self.searchBtnBottomMargin.constant = 0;
    }
    else if ([UIScreen mainScreen].bounds.size.height == 568)
        self.searchBtnBottomMargin.constant = 5;
}


@end
