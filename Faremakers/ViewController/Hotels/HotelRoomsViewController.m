//
//  HotelRoomsViewController.m
//  Faremakers
//
//  Created by Mac1 on 10/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "HotelRoomsViewController.h"
#import "HotelBookingViewController.h"
#import "HotelRoomsCell.h"
#import "FMHotels.h"

@interface HotelRoomsViewController ()<HotelRoomsCellDelegate>

@end

@implementation HotelRoomsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.hotelItem.roomCategories.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 250;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HotelRoomsCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"HotelRoomsCell"];
    cell.hotels = self.hotelItem;
    cell.roomCategories = self.hotelItem.roomCategories[indexPath.row];
    cell.adelegate = self;
    return cell;
    
}

-(void)HotelRoomsCellDidSelectRoom:(id)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    
    HotelBookingViewController *bookingController = [self.storyboard instantiateViewControllerWithIdentifier:@"HotelBookingViewController"];
    bookingController.hotelItem = self.hotelItem;
    bookingController.searchItem = self.searchItem;
    bookingController.hotelRoomCategory = self.hotelItem.roomCategories[indexPath.row];
    [self.navigationController pushViewController:bookingController animated:YES];
}
@end
