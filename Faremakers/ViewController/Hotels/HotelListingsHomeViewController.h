//
//  HotelListingsHomeViewController
//  Faremakers
//
//  Created by Mac1 on 30/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HotelSearchItem;

@interface HotelListingsHomeViewController : UIViewController

@property (strong, nonatomic) NSArray *flightSearchData;

@property (strong, nonatomic) HotelSearchItem *searchItem;

-(void)searchHotelDataWithDictionary:(NSDictionary*)dict;

@end

