//
//  AminitiesView.m
//  Faremakers
//
//  Created by Mac1 on 22/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "AminitiesView.h"
#import "CNPPopupController.h"
#import "Constants.h"


@interface AminitiesView () <CNPPopupControllerDelegate>

@property (nonatomic, strong) CNPPopupController *popupController;

@end

@implementation AminitiesView

-(id)init
{
    
    self = [super init];
    return self;
}

- (void)showPopupWithStyle:(CNPPopupStyle)popupStyle {
    
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSAttributedString *title = [[NSAttributedString alloc] initWithString:@"Amenities!" attributes:@{NSFontAttributeName : [UIFont fontWithName:KCustomeFont size:16], NSParagraphStyleAttributeName : paragraphStyle}];
    NSAttributedString *lineOne = [[NSAttributedString alloc] initWithString:@"You can add text and images" attributes:@{NSFontAttributeName : [UIFont fontWithName:KCustomeFont size:18], NSParagraphStyleAttributeName : paragraphStyle}];
    NSAttributedString *lineTwo = [[NSAttributedString alloc] initWithString:@"Available Amenities!" attributes:@{NSFontAttributeName : [UIFont fontWithName:KCustomeFont size:18], NSForegroundColorAttributeName : [UIColor colorWithRed:0.46 green:0.8 blue:1.0 alpha:1.0], NSParagraphStyleAttributeName : paragraphStyle}];
    
    CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 0, 100, 40)];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont fontWithName:KCustomeFont size:18];
    [button setTitle:@"Dissmiss" forState:UIControlStateNormal];
    button.backgroundColor = [UIColor colorWithRed:0.46 green:0.8 blue:1.0 alpha:1.0];
    button.layer.cornerRadius = 4;
    button.selectionHandler = ^(CNPPopupButton *button){
        [self.popupController dismissPopupControllerAnimated:YES];
        DLog(@"Block for button: %@", button.titleLabel.text);
    };
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.numberOfLines = 0;
    titleLabel.attributedText = title;
    
    UILabel *lineOneLabel = [[UILabel alloc] init];
    lineOneLabel.numberOfLines = 0;
    lineOneLabel.attributedText = lineOne;
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"hotelWifiIcn"]];
    UIImageView *imageView1 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"hotelServiceIcn"]];
    UIImageView *imageView2 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"hotelPoolIcn"]];
    UIImageView *imageView3 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"hotelResIcn"]];
    UIImageView *imageView4 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"gym"]];
    
    UILabel *lineTwoLabel = [[UILabel alloc] init];
    lineTwoLabel.numberOfLines = 0;
    lineTwoLabel.attributedText = lineTwo;
    
    
    UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(0, 40, 250, 30)];
    customView.backgroundColor = [UIColor clearColor];
    [customView addSubview:imageView];
    [customView addSubview:imageView1];
    [customView addSubview:imageView2];
    [customView addSubview:imageView3];
    [customView addSubview:imageView4];
    
    CGRect frame = CGRectMake(imageView1.frame.size.width, 0,imageView1.frame.size.width , imageView1.frame.size.height);
    imageView.frame = frame;
    frame.origin.x += (imageView1.frame.size.width + 20);
    imageView1.frame = frame;
    frame.origin.x += (imageView1.frame.size.width + 20);
    imageView2.frame = frame;
    frame.origin.x += (imageView1.frame.size.width + 20);
    imageView3.frame = frame;
    frame.origin.x += (imageView1.frame.size.width + 20);
    imageView4.frame = frame;
    
    
    self.popupController = [[CNPPopupController alloc] initWithContents:@[lineTwoLabel,customView, button]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = popupStyle;
    self.popupController.delegate = self;
    [self.popupController presentPopupControllerAnimated:YES];
}

#pragma mark - CNPPopupController Delegate

- (void)popupController:(CNPPopupController *)controller didDismissWithButtonTitle:(NSString *)title {
    DLog(@"Dismissed with button title: %@", title);
}

- (void)popupControllerDidPresent:(CNPPopupController *)controller {
    DLog(@"Popup controller presented.");
}

#pragma mark - event response

-(void)showPopupCentered:(id)sender {
    [self showPopupWithStyle:CNPPopupStyleCentered];
}

- (void)showPopupFormSheet:(id)sender {
    [self showPopupWithStyle:CNPPopupStyleActionSheet];
}

- (void)showPopupFullscreen:(id)sender {
    [self showPopupWithStyle:CNPPopupStyleFullscreen];
}

@end
