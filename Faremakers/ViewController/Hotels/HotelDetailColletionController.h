//
//  CSStickyParallaxHeaderViewController.h
//  CSStickyHeaderFlowLayoutDemo
//
//  Created by James Tang on 6/4/14.
//  Copyright (c) 2014 Jamz Tang. All rights reserved.
//


#import "DetailHotelColletionViewHeader.h"
#import "DetailMovieSectionHeader.h"
#import "MWPhotoBrowser.h"
#import "HotelImageItem.h"

@class FMHotels;
@class HotelSearchItem;

@interface HotelDetailColletionController : UICollectionViewController<CSAlwaysOnTopHeaderDelagate,CSSectionHeaderDelegate>

@property (strong, nonatomic) FMHotels *hotel;

@property (strong, nonatomic) HotelSearchItem *hotelSearchItem;

@end
