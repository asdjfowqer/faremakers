//
//  FlightFilterItem.m
//  Faremakers
//
//  Created by Mac1 on 20/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "HotelFilterItem.h"

@implementation HotelFilterItem

-(id)init
{
    self = [super init];
    _rating = nil;
    
    return self;
}

- (id)copyWithZone:(NSZone *)zone
{
    HotelFilterItem *copy = [[[self class] allocWithZone:zone] init];
    if (copy) {
        copy.location = [[NSArray alloc] initWithArray:self.location];
        copy.aminity = [[NSArray alloc] initWithArray:self.aminity];
        copy.hotelType = [[NSArray alloc] initWithArray:self.hotelType];
        copy.priceFilterRange = [[NSArray alloc] initWithArray:self.priceFilterRange];
        copy.rating = [self.rating copy];
        copy.price = [self.price copy];
        copy.minPrice = [self.minPrice copy];
        copy.maxprice = [self.maxprice copy];

    }
    return copy;
}
- (void)clearAllFilters
{
    self.location = nil;
    self.aminity = nil;
    self.hotelType = nil;
    self.rating = nil;
    self.price = nil;
}
- (void)setMinPrice:(NSNumber *)minPrice
{
    if (minPrice.integerValue > 1000)
    {
        NSInteger minimam = (minPrice.integerValue -((minPrice.integerValue % 1000) - 1000));
        _minPrice = [NSNumber numberWithInteger:minimam];
    }
    else
        _minPrice = minPrice;
}
-(void)setMaxprice:(NSNumber *)maxprice
{
    if (maxprice.integerValue > 1000)
    {
        NSInteger maximam = maxprice.integerValue;//(maxprice.integerValue - (maxprice.integerValue % 1000));
        _maxprice = [NSNumber numberWithInteger:maximam];
    }
    else
        _maxprice = maxprice;
    
}
- (void)preparePriceFilterRanges
{
    NSMutableArray *pricesRanger = [[NSMutableArray alloc] initWithCapacity:3];
    
    NSInteger maximam;
    if (self.maxprice.integerValue > 1000)
    {
        maximam = (self.maxprice.integerValue - (self.maxprice.integerValue % 1000));
    }
    else
        maximam = self.maxprice.integerValue;
    
    NSInteger median = (maximam - self.minPrice.integerValue)/3;
    
    [pricesRanger addObject:[NSNumber numberWithLong:self.minPrice.integerValue + median]];
    [pricesRanger addObject:[NSNumber numberWithLong:self.minPrice.integerValue + (median*2)]];
    [pricesRanger addObject:[NSNumber numberWithLong:self.minPrice.integerValue + (median*2)]];
    
    _priceFilterRange = [pricesRanger mutableCopy];
}
-(NSArray*)preparePriceRangeStrings
{
    NSMutableArray *pricesRanger = [[NSMutableArray alloc] initWithCapacity:3];
    
    NSNumber *number = self.priceFilterRange[0];
    [pricesRanger addObject:[NSString stringWithFormat:@"Below %ldK",number.integerValue/1000]];
    
    NSInteger starting = number.integerValue/1000;
    number = self.priceFilterRange[1];
    [pricesRanger addObject:[NSString stringWithFormat:@"%ldK - %ldK",starting ,number.integerValue/1000]];
    
    number = self.priceFilterRange[2];
    [pricesRanger addObject:[NSString stringWithFormat:@"Above %ldK",number.integerValue/1000]];
    
    return pricesRanger;
}

@end
