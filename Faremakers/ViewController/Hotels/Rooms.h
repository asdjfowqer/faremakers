//
//  Rooms.h
//  Faremakers
//
//  Created by Mac1 on 17/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Rooms : NSObject

@property (nonatomic)NSInteger numberOfAdults;
@property (nonatomic)NSInteger numberOfChileds;
@property (nonatomic)NSInteger _1stChildAge;
@property (nonatomic)NSInteger _2ndChildAge;


-(id)initWithNumberOfAdults:(NSInteger)adults;

@end
