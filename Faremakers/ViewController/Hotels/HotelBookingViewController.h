//
//  HotelBookingViewController.h
//  Faremakers
//
//  Created by Mac1 on 29/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//


@class FMHotels;
@class HotelSearchItem;
@class HotelRoomCategories;

@interface HotelBookingViewController : UIViewController <UITableViewDataSource , UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) FMHotels *hotelItem;
@property (nonatomic) HotelSearchItem *searchItem;
@property (nonatomic) HotelRoomCategories *hotelRoomCategory;

- (IBAction)continueBtnSelector:(id)sender;

@end
