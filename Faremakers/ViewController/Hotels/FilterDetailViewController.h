//
//  FilterDetailViewController.h
//  Faremakers
//
//  Created by Mac1 on 21/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <UIKit/UIKit.h>



typedef enum {
    Locations,
    Aminities,
    Chains
} FilterType;

@interface SelectionItem : NSObject

@property (nonatomic) NSString *item;
@property (nonatomic) BOOL isSelected;

@end

@class HotelFilterItem;

@interface FilterDetailViewController : UITableViewController

@property (nonatomic,strong)HotelFilterItem *item;

@property (nonatomic) FilterType filterType;


@end
