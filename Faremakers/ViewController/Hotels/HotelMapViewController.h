//
//  HotelMapViewController.h
//  Faremakers
//
//  Created by Mac1 on 12/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

typedef enum : NSUInteger {
    AllMAP,
    SingleMAP,
} MapType;

@class HotelSearchItem;

@interface HotelMapViewController : UIViewController<MKMapViewDelegate>
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (nonatomic) NSArray *hotelsList;
@property (nonatomic) HotelSearchItem *searchItem;
@property (nonatomic) MapType mapType;

@end
