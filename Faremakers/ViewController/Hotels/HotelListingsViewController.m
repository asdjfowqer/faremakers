//
//  HotelListingsViewController.m
//  Faremakers
//
//  Created by Mac1 on 18/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "HotelListingsViewController.h"
#import "HotelFilterViewController.h"
#import "HotelDetailColletionController.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import "HotelMapViewController.h"
#import "HotelTableViewCell.h"
#import "HotelFilterItem.h"
#import "Constants.h"
#import "FMHotels.h"


@interface HotelListingsViewController () <YAScrollSegmentControlDelegate, HotelFilterViewDelegate>
{
    HotelFilterItem *hotelfilterItem;
    NSInteger selectedSortIndex;
    NSArray *tmpHotelList;
    NSInteger selectedTab;
    NSArray *minMaxPrices;
    NSArray *sortKeys;
}

@end
@implementation HotelListingsViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    selectedTab = -1;
    selectedSortIndex = -1;
    
    UITabBarItem *item = self.tabBarView.items[0];
    [item setTitle:@"Sort By"];
    
    hotelfilterItem = [[HotelFilterItem alloc] init];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    sortKeys = @[@"Highly Rated", @"Lowest Price First", @"Most Expensive First"];
    
    //    self.segmentController.buttons = @[@"All", @"Budgeted"];
}

- (void)setHotelList:(NSArray *)hotelList
{
    _hotelList = [[NSArray alloc] initWithArray:hotelList];
    
    tmpHotelList = [self.hotelList mutableCopy];
    
    [self prepareFilterPriceRanges];
    
    [self.tableView reloadData];
}

- (void)prepareFilterPriceRanges
{
    minMaxPrices = [FMHotels findMinMaxPrices:tmpHotelList];
    hotelfilterItem.minPrice = minMaxPrices[0];
    hotelfilterItem.maxprice = minMaxPrices[1];
    [hotelfilterItem preparePriceFilterRanges];
}

- (void)getMinMaxHotelPrices
{
    minMaxPrices = [FMHotels findMinMaxPrices:tmpHotelList];
    DLog(@"minMaxPrices %@ , %@", minMaxPrices[0], minMaxPrices[1]);
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setTableviewContentView];
}

- (void)setTableviewContentView
{
    CGPoint contentOffset = self.tableView.contentOffset;
    contentOffset.y += CGRectGetHeight(self.tableView.tableHeaderView.frame);
    self.tableView.contentOffset = contentOffset;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return tmpHotelList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 260;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self prepareCellForTableView:tableView forIndexPath:indexPath];
}

- (id)prepareCellForTableView:(UITableView *)tableView forIndexPath:(NSIndexPath *)indexPath
{
    HotelTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"HotelTableViewCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.hotel = tmpHotelList[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    HotelDetailColletionController *detailView = [self.storyboard instantiateViewControllerWithIdentifier:@"CSStickyParallaxHeaderViewController"];
    detailView.hotelSearchItem  = self.searchItem;
    detailView.hotel = tmpHotelList[indexPath.row];
    [self.navigationController pushViewController:detailView animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController setToolbarHidden:YES];
}

- (IBAction)shortListButtonPressed:(id)sender
{
}

- (IBAction)filterButtonPressed:(id)sender
{
    HotelFilterViewController *filterView  = [self.storyboard instantiateViewControllerWithIdentifier:@"HotelFilterViewController"];
    filterView.filterItem = hotelfilterItem;
    filterView.adelegate = self;
    
    UINavigationController *navigationControlller = [[UINavigationController alloc] initWithNavigationBarClass:[FMNavigationBar class] toolbarClass:nil];
    [navigationControlller setViewControllers:@[filterView]];
    
    [self.navigationController presentViewController:navigationControlller animated:YES completion:nil];
}

- (void)hotelFilterViewDidDissmissWithFilterItem:(id)item
{
    hotelfilterItem = (HotelFilterItem *)item;
    [self perfomFilters];
}

- (void)hotelfilterViewDidSelectClrAllFilters:(BOOL)clear
{
    [hotelfilterItem clearAllFilters];
    
    [SVProgressHUD showWithStatus:@"Clearing Filters"];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        tmpHotelList = [self.hotelList mutableCopy];
        [self.tableView reloadData];
        [SVProgressHUD showSuccessWithStatus:@"Done"];
    });
}

- (void)didSelectItemAtIndex:(NSInteger)index
{
    DLog(@"Button selected at index: %lu", (long)index);
}

- (void)perfomFilters
{
    [SVProgressHUD showWithStatus:@"Applying Filters"];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        NSArray *SearchListCopy = [self.hotelList mutableCopy];
        NSMutableArray *array = [[NSMutableArray alloc] init];
        BOOL filterApplied = false;
        if (hotelfilterItem.rating)
        {
            [SearchListCopy enumerateObjectsUsingBlock: ^(id _Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop)
             {
                 FMHotels *hotelObj = (FMHotels *)obj;
                 if (hotelObj.rating.integerValue == hotelfilterItem.rating.integerValue)
                 {
                     [array addObject:hotelObj];
                 }
             }];
            
            
            tmpHotelList = [array mutableCopy];
            filterApplied = true;
        }
        if (hotelfilterItem.price)
        {
            NSNumber *startingPrice;
            NSNumber *endingPrice;
            switch ([[hotelfilterItem preparePriceRangeStrings] indexOfObject:hotelfilterItem.price])
            {
                case 0: {
                    startingPrice = 0;
                    endingPrice = hotelfilterItem.priceFilterRange[0];
                    DLog(@" case 0");
                }
                    break;
                    
                case 1:
                {
                    startingPrice = hotelfilterItem.priceFilterRange[0];
                    endingPrice = hotelfilterItem.priceFilterRange[1];
                    DLog(@" case 1");
                }
                    break;
                    
                case 2:
                {
                    startingPrice = hotelfilterItem.priceFilterRange[1];
                    endingPrice = hotelfilterItem.maxprice;
                    DLog(@" case 2");
                }
                    break;
                    
                default:
                    break;
            }
            [SearchListCopy enumerateObjectsUsingBlock: ^(id _Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop)
             {
                 FMHotels *hotelObj = (FMHotels *)obj;
                 
                 if (hotelObj.baseFare.integerValue > startingPrice.integerValue && hotelObj.baseFare.integerValue <= endingPrice.integerValue)
                 {
                     [array addObject:hotelObj];
                 }
             }];
            
            tmpHotelList = [array mutableCopy];
            filterApplied = true;
        }
        if (!filterApplied)
        {
            tmpHotelList = [self.hotelList mutableCopy];
        }
        [self.tableView reloadData];
        
        [SVProgressHUD showSuccessWithStatus:@"Done"];
    });
}

- (void)showMapViewcontroller:(id)sender
{
    HotelMapViewController *mapView = [self.storyboard instantiateViewControllerWithIdentifier:@"HotelMapViewController"];
    mapView.mapType = AllMAP;
    mapView.hotelsList = [self.hotelList mutableCopy];
    mapView.searchItem = self.searchItem;
    [self.navigationController pushViewController:mapView animated:NO];
    [UIView transitionWithView:self.navigationController.view duration:1 options:UIViewAnimationOptionTransitionFlipFromRight animations:nil completion:nil];
}

- (IBAction)SortButtonPressed:(id)sender
{
    HotelFilterViewController *filterView  = [self.storyboard instantiateViewControllerWithIdentifier:@"HotelFilterViewController"];
    filterView.filterItem = hotelfilterItem;
    filterView.adelegate = self;
    
    UINavigationController *navigationControlller = [[UINavigationController alloc] initWithNavigationBarClass:[FMNavigationBar class] toolbarClass:nil];
    [navigationControlller setViewControllers:@[filterView]];
    
    [self.navigationController presentViewController:navigationControlller animated:YES completion:nil];
}

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    NSInteger tag = [item tag];
    switch (tag)
    {
        case 0:
            [self showSortActionSheet];
            break;
            
        case 1:
        {
            [self showMapViewcontroller:nil];
            [tabBar setSelectedItem:nil];
            selectedTab = -1;
        }
            break;
            
        case 2:
            [self filterButtonPressed:nil];
            break;
            
        default:
            break;
    }
}

- (void)showSortActionSheet
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Sort By"
                                                                   message:@""
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *ratedAction = [UIAlertAction actionWithTitle:sortKeys[0] style:UIAlertActionStyleDefault
                                                        handler: ^(UIAlertAction *action) {
                                                            [self sortHotelsByKey:@"rating" assendingly:NO];
                                                            UITabBarItem *item = self.tabBarView.items[0];
                                                            [item setTitle:sortKeys[0]];
                                                        }];
    
    UIAlertAction *lowestPriceAction = [UIAlertAction actionWithTitle:sortKeys[1]
                                                                style:UIAlertActionStyleDefault handler: ^(UIAlertAction *action) {
                                                                    [self sortHotelsByKey:@"baseFare" assendingly:YES];
                                                                    UITabBarItem *item = self.tabBarView.items[0];
                                                                    [item setTitle:sortKeys[1]];
                                                                }];                                                             // 2
    UIAlertAction *mostExpensiveAction = [UIAlertAction actionWithTitle:sortKeys[2]
                                                                  style:UIAlertActionStyleDefault handler: ^(UIAlertAction *action) {
                                                                      [self sortHotelsByKey:@"baseFare" assendingly:NO];
                                                                      UITabBarItem *item = self.tabBarView.items[0];
                                                                      [item setTitle:sortKeys[2]];
                                                                  }];                                                               // 2
    UIAlertAction *cancleAction = [UIAlertAction actionWithTitle:@"Cancle"
                                                           style:UIAlertActionStyleCancel handler: ^(UIAlertAction *action) {
                                                               if (selectedSortIndex == -1)
                                                               {
                                                                   UITabBarItem *item = self.tabBarView.items[0];
                                                                   [item setTitle:@"Sort By"];
                                                                   [self.tabBarView setSelectedItem:nil];
                                                               }
                                                           }];                                                        // 3
    
    [alert addAction:ratedAction]; // 4
    [alert addAction:lowestPriceAction]; // 5
    [alert addAction:mostExpensiveAction];
    [alert addAction:cancleAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)sortHotelsByKey:(NSString *)key assendingly:(BOOL)assending
{
    [SVProgressHUD showWithStatus:@"Sorting"];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        if ([key isEqualToString:@"baseFare"] && assending)
        {
            NSArray *sortedArray;
            sortedArray = [tmpHotelList sortedArrayUsingComparator: ^NSComparisonResult (FMHotels *first, FMHotels *second)
                           {
                               return [[first valueForKey:key] compare:[second valueForKey:key]];
                           }];
            
            tmpHotelList = [sortedArray mutableCopy];
        }
        else if ([key isEqualToString:@"rating"] || [key isEqualToString:@"baseFare"])
        {
            NSArray *sortedArray;
            
            sortedArray = [tmpHotelList sortedArrayUsingComparator: ^NSComparisonResult (FMHotels *first, FMHotels *second)
                           {
                               return [[second valueForKey:key] compare:[first valueForKey:key]];
                           }];
            
            tmpHotelList = [sortedArray mutableCopy];
            
        }
        [self.tableView reloadData];
        
        [SVProgressHUD showSuccessWithStatus:@"Done"];
        
    });
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    if (searchBar.text.length > 0)
    {
        [self.searchBar resignFirstResponder];
        
        [SVProgressHUD showWithStatus:@"Searching"];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"SELF.hotelName contains[cd] %@", searchBar.text];
            tmpHotelList = [self.hotelList filteredArrayUsingPredicate:bPredicate];
            [self.tableView reloadData];
            [SVProgressHUD showSuccessWithStatus:@"Done"];
            [self setTableviewContentView];
        });
    }
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self.searchBar resignFirstResponder];
    [self setTableviewContentView];
}

@end