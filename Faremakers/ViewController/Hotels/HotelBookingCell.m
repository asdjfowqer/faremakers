//
//  HotelBookingCell.m
//  Faremakers
//
//  Created by Mac1 on 29/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "HotelBookingCell.h"
#import "UILabel+FontName.h"
#import "NSDate+DateComponents.h"
#import "HotelRoomCategories.h"
#import "FMHotels.h"
#import "HotelSearchItem.h"
#import "DoubleDateItem.h"

@implementation HotelBookingCell

- (void)awakeFromNib
{
    [self.detailTitle changeFontFamily];
    [self.detailDescription changeFontFamily];
    [self.policiesTitle changeFontFamily];
    [self.paymentDetail changeFontFamily];
    [self.Amount changeFontFamily];
}
-(void)setRoomCategory:(HotelRoomCategories *)roomCategory forIndex:(NSInteger)index withReuseIdentifier:(NSString*)identifier
{
    _roomCategory = roomCategory;
    int value = [[identifier substringFromIndex: [identifier length] - 1] intValue];
    
    switch (value)
    {
        case 0:
            [self loadFirstCellWithIndex:index];
            break;
        case 1:
            [self loadSecondCellWithIndex:index];
            break;
        case 2:
            [self loadSecondCellWithIndex:index];
            break;
        case 3:
            [self loadThirdCell:index];
            break;
        default:
            break;
    }
}

-(void)loadFirstCellWithIndex:(NSInteger)index
{
    switch (index)
    {
        case 0:
            self.detailDescription.text = [NSString stringWithFormat:@"%@ , %@",self.roomCategory.hotelName,self.roomCategory.roomDescription];
            break;
        case 1:
            self.detailDescription.text = [NSString stringWithFormat:@"%@ %@ %@ - %@ %@ %@",[self.searchItem.doubleDate.depDate getDayOfTheWeek],[self.searchItem.doubleDate.depDate getDayOfTheMonth],[self.searchItem.doubleDate.depDate getMonth],[self.searchItem.doubleDate.retDate getDayOfTheWeek],[self.searchItem.doubleDate.retDate getDayOfTheMonth],[self.searchItem.doubleDate.retDate getMonth]];
            break;
        case 2:
            self.detailDescription.text = [self.searchItem convertRoomIntoString];
            break;
        case 3:
            self.detailDescription.text = self.roomCategory.basisDescription;
            break;
        default:
            break;
    }
}
-(void)loadSecondCellWithIndex:(NSInteger)index
{
    switch (index)
    {
        case 0:
        case 1:
            self.Amount.text = @"N/A";
            break;
        case 2:
            self.Amount.text = [NSString stringWithFormat:@"%@ %@",self.hotel.maximumPrice,self.hotel.currencyCode];
            break;
        default:
            break;
    }
}
-(void)loadThirdCell:(NSInteger)index
{
    
}

@end
