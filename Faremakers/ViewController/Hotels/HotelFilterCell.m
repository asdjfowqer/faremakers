//
//  PromotionDetailCell.m
//  Faremakers
//
//  Created by Mac1 on 19/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "HotelFilterCell.h"
#import "HotelStarFilterColletionCell.h"
#import "PriceFilterColletionCell.h"
#import "UILabel+FontName.h"
#import "HotelFilterItem.h"
#import "Constants.h"

@implementation HotelFilterCell

#define KStarCollectionCell @"starCollectionCell"
#define KPriceCollectionCell @"priceCollectionCell"
#define KStarCollectionView 0
#define KPriceCollectionView 1

- (void)awakeFromNib
{
    [self.filterDetailLbl changeFontFamily];
    [self.filterTitlelbl changeFontFamily];
}
- (void)loadCellWithIdentifier:(NSString *)cellIdentifier ForIndexPath:(NSIndexPath*)index
{
    
    switch (index.section) {
        case 0:
            [self loadStarCollectionCell];
            break;
        case 1:
            [self loadPriceCollectionCell];
            break;
        case 2:
        {
            switch (index.row)
            {
                case 0:
                    [self loadOptionsFirstCell];
                    break;
                case 1:
                    [self loadOptionsSecondCell];
                    break;
                case 2:
                    [self loadThirdSecondCell];
                    break;
                default:
                    break;
            }
        }
            break;
            
        default:
            break;
    }
    
}

-(void)loadStarCollectionCell
{
    _starCollectionView.delegate = self;
    _starCollectionView.dataSource = self;
}
-(void)loadPriceCollectionCell
{
    _priceCollectionView.delegate = self;
    _priceCollectionView.dataSource = self;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if ([collectionView tag] == KStarCollectionView)
        return 5;
    return 3;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout  *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([collectionView tag] == KStarCollectionView)
    {
        return CGSizeMake(self.frame.size.width/4.95, 55);
    }
    else
        return CGSizeMake(self.frame.size.width/3, 55);

}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([collectionView tag] == KStarCollectionView)
    {
        HotelStarFilterColletionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:KStarCollectionCell forIndexPath:indexPath];
        [cell loadCellWithItem:self.item atIndexPath:indexPath ];
        return cell;
    }
    else
    {
        PriceFilterColletionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:KPriceCollectionCell forIndexPath:indexPath];
        [cell loadCellForPrice:self.item.price withAllPrices:[self.item preparePriceRangeStrings] atIndexPath:indexPath ];
        return cell;

    }
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([collectionView tag] == KStarCollectionView)
    {
        NSNumber *rating = [NSNumber numberWithInteger:(indexPath.row +1)];
        
        if (self.item.rating && [self.item.rating isEqualToNumber:rating]) {
            self.item.rating = nil;
        }
        else
            self.item.rating = [NSNumber numberWithInteger:(indexPath.row +1)];
        
    }
    else
    {
        
        NSString *price = [[self.item preparePriceRangeStrings] objectAtIndex:indexPath.row];
        
        if (self.item.price && [self.item.price isEqualToString:price]) {
            self.item.price = nil;
        }
        else
            self.item.price = price;

    }
    [collectionView reloadData];
    
}

-(void)loadOptionsFirstCell
{
    _filterTitlelbl.text = @"Locations";
    
    NSMutableString *locations = [[NSMutableString alloc] init];
    
    for (NSString *str in self.item.location) {
        [locations appendString:str];
        [locations appendString:@","];
    }
    _filterDetailLbl.text = (self.item.location.count)?locations:@"Choose Locations";
}
-(void)loadOptionsSecondCell
{
    _filterTitlelbl.text = @"Aminities";
    
    NSMutableString *locations = [[NSMutableString alloc] init];
    
    for (NSString *str in self.item.aminity) {
        [locations appendString:str];
        [locations appendString:@","];
    }
    _filterDetailLbl.text = (self.item.aminity.count)?locations:@"Choose Available Aminites";
}
-(void)loadThirdSecondCell
{
    _filterTitlelbl.text = @"Hotel Type";
    NSMutableString *locations = [[NSMutableString alloc] init];
    
    for (NSString *str in self.item.hotelType) {
        [locations appendString:str];
        [locations appendString:@","];
    }
    _filterDetailLbl.text = (self.item.hotelType.count)?locations:@"Select Hotel Type";
}

@end
