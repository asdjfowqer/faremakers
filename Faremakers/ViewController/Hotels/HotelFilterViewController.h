//
//  PromotionDetailController.h
//  Faremakers
//
//  Created by Mac1 on 18/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HotelFilterViewDelegate <NSObject>

- (void) hotelFilterViewDidDissmissWithFilterItem:(id)item;
- (void) hotelfilterViewDidSelectClrAllFilters:(BOOL)clear;

@end

@class HotelFilterItem;

@interface HotelFilterViewController : UIViewController

@property (weak, nonatomic) id<HotelFilterViewDelegate> adelegate;

@property (strong, nonatomic) NSArray *flightSearchData;

@property (copy, nonatomic) HotelFilterItem *filterItem;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)ApplyFilterButtonPressed:(id)sender;

@end
