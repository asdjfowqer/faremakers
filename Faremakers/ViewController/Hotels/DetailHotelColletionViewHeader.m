//
//  CSAlwaysOnTopHeader.m
//  CSStickyHeaderFlowLayoutDemo
//
//  Created by James Tang on 6/4/14.
//  Copyright (c) 2014 Jamz Tang. All rights reserved.
//

#import "DetailHotelColletionViewHeader.h"
#import "DetailHotelCollectionHeadercell.h"
#import "CSStickyHeaderFlowLayoutAttributes.h"
#import <SDWebImagePrefetcher.h>
#import "HotelImageItem.h"

@interface DetailHotelColletionViewHeader()
{
    NSUInteger indexNo;
    DetailHotelCollectionHeadercell *currentCell;
    float Cellheight;
}

@property (nonatomic, strong) NSArray *             images;
@property (nonatomic, strong) UICollectionView *    collectionV;
@property (nonatomic, assign) NSLayoutConstraint *  height;
@property (nonatomic, strong) NSMutableArray    *   cellSizes;

@property (nonatomic, strong) UIView *leftEdgeView;

@end

@implementation DetailHotelColletionViewHeader

- (void)applyLayoutAttributes:(CSStickyHeaderFlowLayoutAttributes *)layoutAttributes {
    
    if (!currentCell) {
        return;
    }
    if (layoutAttributes.progressiveness >= 0.95) {
        
    }
    else
    {
    }
}
- (void)loadCollectionViewWithArray:(NSArray *)allItems andCellHeight:(float)height
{
    [self loadCollectionViewWithArray:allItems];
    Cellheight = height;
}

- (void ) loadCollectionViewWithArray:(NSArray *)allItems
{
    Cellheight = 400;
    UINib *cellNib = [UINib nibWithNibName:@"DetailHotelCollectionHeadercell" bundle:nil];
    [self.customCollection registerNib:cellNib forCellWithReuseIdentifier:@"CELL"];
    
    
    if (!_images)
        _images = [[NSMutableArray alloc] init];
    
    _images  = [NSArray arrayWithArray:allItems];
        
    
    if (!self.cellSizes)
        self.cellSizes = [[NSMutableArray    alloc] init];
    
    
    SDWebImagePrefetcher *prefetcher = [SDWebImagePrefetcher sharedImagePrefetcher];

    NSMutableArray *urls = [NSMutableArray array];
    
    for (int i =0; i < self.images.count; i ++)
    {
        HotelImageItem *imageItem = self.images[i];
        NSURL *url = [NSURL URLWithString:[imageItem.imageUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        [urls addObject:url];
    }
    [prefetcher prefetchURLs:urls];
    
    [self.customCollection reloadData];

//    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapGesture:)];
//    tapGesture.numberOfTapsRequired = 1;
//    [self addGestureRecognizer:tapGesture];
   
    self.customCollection.delegate = self;
    self.customCollection.dataSource = self;
    [self.customCollection reloadData];

    
}
- (void)singleTapGesture:(UITapGestureRecognizer*)sender {
    
    if(sender.state == UIGestureRecognizerStateRecognized)
    {
        CGPoint point = [sender locationInView:sender.view];
        
        [_adelegate collectionViewDidReceiveTap:point];

    }
    
}
-(void)initializeCollectionView{

    UINib *cellNib = [UINib nibWithNibName:@"DetailHotelCollectionHeadercell" bundle:nil];
    [self.customCollection registerNib:cellNib forCellWithReuseIdentifier:@"CELL"];
    
    self.customCollection.delegate = self;
    self.customCollection.dataSource = self;
    [self.customCollection reloadData];
    
}

- (IBAction)shareButtonSelector:(id)sender
{
    
}

- (IBAction)shortListedSelector:(id)sender
{
    
}

- (IBAction)backButtonSelector:(id)sender
{
    [self.adelegate backButtonPressedFromTopHeader:sender];
}
- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self UpdateCurrentImageCount:(indexPath.row+1) :_images.count];
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{

    CGSize newSize = CGSizeMake(collectionView.frame.size.width, Cellheight);//[[self.cellSizes objectAtIndex:indexPath.row] floatValue]);
    return newSize;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _images.count;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (DetailHotelCollectionHeadercell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    DetailHotelCollectionHeadercell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CELL" forIndexPath:indexPath];
    HotelImageItem *imageItem = self.images[indexPath.row];
    
    [cell setHotelImage:imageItem.imageUrl];
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [_adelegate collectionViewDidSelectItemAtIndexPath:indexPath];

}
- (void) getSizeOfImage:(CGSize)size atIndex:(NSInteger)index
{
    
}
-(void)UpdateCurrentImageCount:(NSInteger)currentCount :(NSInteger)totalCount
{
    self.CurrentImageCount.text = [NSString stringWithFormat:@"%ld of %ld",(long)currentCount,(long)totalCount];
}
@end
