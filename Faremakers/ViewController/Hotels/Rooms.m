//
//  Rooms.m
//  Faremakers
//
//  Created by Mac1 on 17/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "Rooms.h"

@implementation Rooms

-(id)initWithNumberOfAdults:(NSInteger)adults
{
    if (self = [super init])
    {
        self.numberOfAdults = adults;
        self.numberOfChileds = 0;
        self._1stChildAge = 0;
        self._2ndChildAge = 0;
    }
    return self;
}
@end
