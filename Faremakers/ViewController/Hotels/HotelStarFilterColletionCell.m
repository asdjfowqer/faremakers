//
//  HotelStarFilterColletionCell.h
//  Faremakers
//
//  Created by Mac1 on 17/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "HotelStarFilterColletionCell.h"
#import "UILabel+FontName.h"
#import "HotelFilterItem.h"

@implementation HotelStarFilterColletionCell

- (void)awakeFromNib
{
    [self.starTitleLabel changeFontFamily];
}
-(void)loadCellWithItem:(HotelFilterItem*)item atIndexPath:(NSIndexPath*)indexPath
{
    _starTitleLabel.text = [NSString stringWithFormat:@"%ld",indexPath.row+1];
    
    if ((item.rating.intValue - 1) == indexPath.row)
    {
        _starTitleLabel.textColor = [UIColor blueColor];
        _starImageView.image =[UIImage imageNamed:@"blue_star"];
    }
    else
    {
        _starTitleLabel.textColor = [UIColor lightGrayColor];
        _starImageView.image =[UIImage imageNamed:@"outline_star"];
        
    }
    
    
}

-(void)loadStarCollectionCell
{
    
}
-(void)loadPriceCollectionCell
{
    
}

@end
