//
//  FlightFilterItem.h
//  Faremakers
//
//  Created by Mac1 on 20/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HotelFilterItem : NSObject <NSCopying>

@property (nonatomic) NSArray *location;
@property (nonatomic) NSArray *aminity;
@property (nonatomic) NSArray *hotelType;
@property (nonatomic) NSNumber *rating;
@property (nonatomic) NSString *price;
@property (nonatomic) NSNumber *minPrice;
@property (nonatomic) NSNumber *maxprice;
@property (nonatomic) NSArray *priceFilterRange;

- (void)preparePriceFilterRanges;
- (NSArray*)preparePriceRangeStrings;
- (void)clearAllFilters;

@end
