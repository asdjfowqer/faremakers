//
//  HotelSearchItem.h
//  Faremakers
//
//  Created by Mac1 on 29/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class DoubleDateItem;
@class Cities;

@interface HotelSearchItem : NSObject

@property (nonatomic , retain) NSString *searchTitle;
@property (nonatomic , retain) DoubleDateItem *doubleDate;
@property (nonatomic , retain) NSMutableArray *rooms;
@property (nonatomic) Cities *destCity;


- (BOOL)checkAllValues;
- (NSDictionary*) preparePostData;
- (NSString*) convertRoomIntoString;
-(NSInteger)childCount;
-(NSInteger)adultsCount;

@end
