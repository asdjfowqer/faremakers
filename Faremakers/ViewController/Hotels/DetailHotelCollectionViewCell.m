//
//  CSCell.m
//  CSStickyHeaderFlowLayoutDemo
//
//  Created by Jamz Tang on 8/1/14.
//  Copyright (c) 2014 Jamz Tang. All rights reserved.
//

#import "DetailHotelCollectionViewCell.h"
#import "NSDate+DateComponents.h"
#import "DoubleDateItem.h"
#import "HotelSearchItem.h"
#import "Rooms.h"
#import "FMHotels.h"

@interface DetailHotelCollectionViewCell()


@end

@implementation DetailHotelCollectionViewCell


- (void)awakeFromNib
{
    [self.hotelTitleLabel changeFontFamily];
    [self.hotelDetailLabel changeFontFamily];
    [self.hotelAddressLabel changeFontFamily];
    [self.numberOfRoomsLabel changeFontFamily];
    [self.roomDetailLabel changeFontFamily];
    [self.costTitleLabel changeFontFamily];
    [self.costPaymentLabel changeFontFamily];
    [self.hotelPolicyLabel changeFontFamily];
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)setHotelDetailItem:(id)hotelDetailItem withIdentifier:(NSString*)cellIdentifier
{
    _hotelDetailItem = (FMHotels*)hotelDetailItem;
    [self loadCellWithIdentifier:cellIdentifier];
}
#pragma mark - Private Method.

- (void)loadCellWithIdentifier:(NSString *)cellIdentifier
{
    // Get Last componet from CellIdenfifier
    int value = [[cellIdentifier substringFromIndex: [cellIdentifier length] - 1] intValue];
    
    switch (value)
    {
        case 0:
            [self loadFirstCollectionCell];
            break;
        case 1:
            [self loadSecondCollectionCell];
            break;
        case 2:
            [self loadThirdCollectionCell];
            break;
        case 4:
            [self loadFourthCollectionCell];
            break;
        case 5:
//            [self loadFifthCollectionCell];
		    [self loadSixthCollectionCell];
            break;
        case 6:
            [self loadSixthCollectionCell];
            break;
        default:
            break;
    }
    
}

- (void) loadFirstCollectionCell
{
    for (NSInteger i = 0 ; i < 5 ; i ++ )
    {
        UIImageView *starImageView = self.starsImageView[i];
        
        if (self.hotelDetailItem.rating.integerValue <= i)
            starImageView.image = [UIImage imageNamed:@"star_empty"];
        else
            starImageView.image = [UIImage imageNamed:@"selected_star"];
    }
    
    self.hotelTitleLabel.text = self.hotelDetailItem.hotelName;
    self.hotelDetailLabel.text = [NSString stringWithFormat:@"%@ To %@, %ld Room(s)",[self stringFromDate:self.hotelSearchItem.doubleDate.depDate],[self stringFromDate:self.hotelSearchItem.doubleDate.retDate],self.hotelSearchItem.rooms.count];
}

-(NSString*)stringFromDate:(NSDate*)date
{
    return [NSString stringWithFormat:@"%@ %@ ' %@", [date getDayOfTheMonth],[date getMonth],[date getYear]];
}
- (void) loadSecondCollectionCell
{
    NSDictionary *cities = self.hotelDetailItem.city;
    
    self.hotelAddressLabel.text = [NSString stringWithFormat:@"%@",[cities objectForKey:@"cityName"]];//self.hotelDetailItem.locationTitle;
}

- (void) loadThirdCollectionCell
{
    [self.hotelAminityWifi setEnabled:false];
    [self.hotelAminityService setEnabled:YES];
    [self.hotelAminityPool setEnabled:YES];
    [self.hotelAminityRest setEnabled:YES];
    [self.hotelAminityGym setEnabled:YES];
    
}

- (void) loadFourthCollectionCell
{
    self.numberOfRoomsLabel.text = [NSString stringWithFormat:@"%ld Room(s)",self.hotelSearchItem.rooms.count ];
    
    NSInteger adult = 0;
    NSInteger child = 0;
    for (Rooms *room in self.hotelSearchItem.rooms)
    {
        adult += room.numberOfAdults;
        child += room.numberOfChileds;
        
    }
    self.roomDetailLabel.text = [NSString stringWithFormat:@"%ld Adults %ld Children",adult,child];
}

- (void) loadFifthCollectionCell
{
    self.costPaymentLabel.text =   [NSString stringWithFormat:@"%@ %@",self.hotelDetailItem.baseFare,self.hotelDetailItem.currencyCode];
}

- (void) loadSixthCollectionCell
{
}


@end
