//
//  FilterDetailViewController.m
//  Faremakers
//
//  Created by Mac1 on 21/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "FilterDetailViewController.h"
#import "HotelFilterItem.h"


@implementation SelectionItem

-(id)init
{
    self = [super init];
    return self;
}
@end

@interface FilterDetailViewController ()
{
    NSMutableArray *dataList;
}

@end


@implementation FilterDetailViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.tableFooterView = [UIView new];

    dataList = [[NSMutableArray alloc] init];
   //  NSArray *titles = @[@"Parking Facilities Available",@"Lift / Elevator",@"Laundry Service Available",@"Front Desk",@"Internet Access - Surcharge",@"Room Service (24 Hours)",@"Power Backup",@"Airport Transfer Available / Surcharge",@"Business Center",@"Concierge",@"Doctor on Call",@"Dry Cleaning",@"Local Tour / Travel Desk",@"Valet service",@"Major Credit Cards Accepted",@"24 Hour Front Desk",@"Car Rental",@"Express Laundry",@"Taxi Services",@"Newspapers In Lobby",@"Internet Access",@"Wake-up Call / Service",@"Wheelchair Accessibility - Room",@"Lobby",@"Audio - Visual Equipment",@"Conference Facility",@"Board Room",@"Party hall",@"STD / ISD" ];
    NSArray *titles = @[];
    int i = 0;
    
    for (NSString *str in titles)
    {
        SelectionItem *sItem = [[SelectionItem alloc] init];
        sItem.item = str;
        sItem.isSelected = NO;
        [dataList addObject:sItem];
        i++;
    }

    UIBarButtonItem *doneButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneButtonClicked:)];
    self.navigationItem.rightBarButtonItem = doneButtonItem;

}

-(void)doneButtonClicked:(id)sender
{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    for (SelectionItem *sItem in dataList) {
        
        if (sItem.isSelected)
            [array addObject:sItem.item];
    }
    
    switch (self.filterType) {
        case Locations:
            self.item.location = [[NSArray alloc] initWithArray:array];
            break;
        case Aminities:
            self.item.aminity = [[NSArray alloc] initWithArray:array];
            break;
        case Chains:
            self.item.hotelType = [[NSArray alloc] initWithArray:array];
            break;
        default:
            break;
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    SelectionItem *sItem = [dataList objectAtIndex:indexPath.row];
    [cell.textLabel setText:sItem.item];
    
    if (sItem.isSelected) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
        cell.accessoryType = UITableViewCellAccessoryNone;

    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SelectionItem *sItem = dataList[indexPath.row];
    sItem.isSelected = !sItem.isSelected;
    [self.tableView reloadData];
    
}

@end
