//
//  HotelBookingViewController.m
//  Faremakers
//
//  Created by Mac1 on 29/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "HotelBookingViewController.h"
#import "TransitionDelegate.h"
#import "CheckOutRootController.h"
#import "BookingPromtView.h"
#import "HotelSearchItem.h"
#import "HotelBookingCell.h"
#import "CheckOutItems.h"
#import "Constants.h"

#import "FMHotels.h"
#import "DoubleDateItem.h"
#import "HotelRoomCategories.h"
#import "Cities.h"
#import "HotelSearchItem.h"
#import "Rooms.h"
#import "FMUser.h"

@interface HotelBookingViewController ()
{
    NSArray *SectionsData;
    NSArray *DataArray;
}
@property (nonatomic, strong) TransitionDelegate *transitionController;

@end

@implementation HotelBookingViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Review Booking";
    NSString *str = self.hotelItem.hotelName;
    // NSDate *date = [NSDate date];
    SectionsData = @[@"Booking Detail", @"Payment Detail"];
    DataArray = @[@[@"Hotel", @"Dates", @"Room(s)", @"Stay Includes"],
                  @[str, @"2nd Jan - 5th Jan (dummy)", @"2 Adults 1 child (dummy)"],
                  @[@"Premium room with Extra Service", @"Fee and Texs", @"Grand Total"],
                  @[@"$ 200", @"$ 35", @"$ 235"]];


    DLog(@"_hotelItem %@", _hotelItem);
    DLog(@"_searchItem %@", _searchItem);
    DLog(@"_hotelRoomCategory %@", _hotelRoomCategory);

    [self addHotelTransaction];
}

- (void)addHotelTransaction
{
    TransactionBookingPaidHotel *transaction = [TransactionBookingPaidHotel anInstance];
    transaction.hotelCode = _hotelItem.hotelCode;
    transaction.price = [NSNumber numberWithDouble:[_hotelItem.price doubleValue]];
    transaction.hotelName = _hotelItem.hotelName;
    transaction.country = _searchItem.destCity.countryName;
    transaction.currency = _hotelItem.currencyCode;
    transaction.amount = _hotelItem.maximumPrice;
    transaction.price = _hotelItem.maximumPrice;
    transaction.orderInfo = @"Book Hotel";
    transaction.orderName = @"Hotel";
    transaction.totalNights = [NSNumber numberWithInt:[_hotelRoomCategory.totalNights intValue]];
    transaction.checkInDate = _searchItem.doubleDate.depDate;
    transaction.checkOutDate = _searchItem.doubleDate.retDate;
    transaction.city = _searchItem.destCity.text;
    transaction.adults = [NSNumber numberWithInteger:[_searchItem adultsCount]];
    transaction.childs = [NSNumber numberWithInteger:[_searchItem childCount]];
    transaction.numberOfRooms = [NSNumber numberWithInteger:[_searchItem rooms].count];
    transaction.hotelId = [_hotelRoomCategory roomID];
    transaction.roomType = [_hotelRoomCategory roomType];
    transaction.firstName = [FMUser defaultUser].firstName;
    transaction.lastName = [FMUser defaultUser].lastName;
    transaction.phoneNumber = [FMUser defaultUser].phoneNumber;
    transaction.email = [FMUser defaultUser].email;
    [TransactionBookingManager shared].currentTransaction = transaction;

    DLog(@"%@", [_hotelRoomCategory roomID]);
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [SectionsData objectAtIndex:section];
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    if ([view isKindOfClass:[UITableViewHeaderFooterView class]])
    {
        UITableViewHeaderFooterView *tableViewHeaderFooterView = (UITableViewHeaderFooterView *)view;
        tableViewHeaderFooterView.textLabel.textColor = [UIColor lightGrayColor];
        tableViewHeaderFooterView.textLabel.font = [UIFont fontWithName:KCustomeRagularFont size:14.0];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
    {
        return 5;
    }
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self prepareCellForTableView:tableView forIndexPath:indexPath];
}

- (id)prepareCellForTableView:(UITableView *)tableView forIndexPath:(NSIndexPath *)indexPath
{
    HotelBookingCell *cell;
    NSString *reusIdentifier;
    if (indexPath.section == 0)
    {
        if (indexPath.row > 3)
        {
            reusIdentifier = @"HotelBookingCell1";
            cell = [self.tableView dequeueReusableCellWithIdentifier:reusIdentifier forIndexPath:indexPath];

            NSArray *array = [DataArray objectAtIndex:0];

            if (indexPath.row == array.count)
            {
                cell.policiesTitle.text = @"Cancel policy";
            }
            else
            {
                cell.policiesTitle.text = @"Room Detail";
            }

            return cell;
        }
        else
        {
            reusIdentifier = @"HotelBookingCell0";
            cell = [self.tableView dequeueReusableCellWithIdentifier:reusIdentifier forIndexPath:indexPath];
            cell.detailTitle.text = DataArray[indexPath.section][indexPath.row];
        }
    }
    else
    {
        reusIdentifier = @"HotelBookingCell2";
        cell = [self.tableView dequeueReusableCellWithIdentifier:reusIdentifier forIndexPath:indexPath];
        cell.paymentDetail.text = DataArray[indexPath.section + 1][indexPath.row];
        //        cell.Amount.text = DataArray [indexPath.section +2][indexPath.row];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.searchItem = self.searchItem;
    cell.hotel = self.hotelItem;
    [cell setRoomCategory:self.hotelRoomCategory forIndex:indexPath.row withReuseIdentifier:reusIdentifier];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

- (IBAction)continueBtnSelector:(id)sender
{
    if ([[FMUser defaultUser] isSessionAlive])
    {
        [self showBookingPromtViewController];
    }
    else {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Cannot perform action"
                                                                                 message:@"Please login to proceed."
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"Dismiss"
                                                            style:UIAlertActionStyleDefault
                                                          handler:nil]];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

- (void)showBookingPromtViewController
{
    CheckOutRootController *bookingController = [[CheckOutRootController alloc] init];
    [bookingController setCheckOutItem:[[CheckOutItems alloc] initWithType:KHotelItem withSearchItem:nil andSelectedItem:nil SubSelectedItem:nil andGrandTotal:[NSString stringWithFormat:@"%@ %@", self.hotelItem.maximumPrice, self.hotelItem.currencyCode]]];
    [bookingController pushProcesBookingViewController:self.navigationController];
    //  [self showFareRuleViewController];
}

@end