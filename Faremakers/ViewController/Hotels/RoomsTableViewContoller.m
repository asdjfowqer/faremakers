//
//  RoomsTableViewContoller.m
//  Faremakers
//
//  Created by Mac1 on 03/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "RoomsTableViewContoller.h"
#import "HotelCellWithStepper.h"
#import "DoubleDateTableCell.h"
#import "IQActionSheetPickerView.h"
#import "HotelListingsHomeViewController.h"
#import "Rooms.h"
#import "Constants.h"
#import "AddnDeleteRowCell.h"
#import "HotelSearchItem.h"


@interface RoomsTableViewContoller()<HotelCellWithStepperDelegate,IQActionSheetPickerViewDelegate,AddnDeleteRowCellDelegate>
{
    NSIndexPath *selectedSection;
    HotelSearchItem *item;
}

@end

@implementation RoomsTableViewContoller

- (void)viewDidLoad
{
    [super viewDidLoad];
    item = [[HotelSearchItem alloc] init];
    Rooms *room = [[Rooms alloc] initWithNumberOfAdults:1];
    [item.rooms addObject:room];
    
    UIBarButtonItem *doneButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneButtonClicked:)];
    self.navigationItem.rightBarButtonItem = doneButtonItem;
    
}

-(void)doneButtonClicked:(id)sender
{    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return item.rooms.count + 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section >= item.rooms.count)
    {
        return 1;
    }
    Rooms *room = item.rooms [section];
    
    return (room.numberOfChileds + 2);
}

- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if ( section < item.rooms.count)
    {
        return [NSString stringWithFormat:@"Room %ld",(long)(section+1)];
    }
    return NULL;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section < item.rooms.count)
        return 18;
    return 0.1;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return [self prepareRoomCellForTableView:tableView forIndexPath:indexPath];
}

- (id)prepareRoomCellForTableView:(UITableView*)tableView forIndexPath:(NSIndexPath*)indexPath
{
    if (indexPath.section >= item.rooms.count)
    {
        NSInteger section = (indexPath.section > 1)?1:0;
        NSString *reuseableIdentifier = [NSString stringWithFormat:@"AddNewRowCell%ld",(long)(section)];
        AddnDeleteRowCell *cell = [self.roomsTableView dequeueReusableCellWithIdentifier:reuseableIdentifier forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.adelegate = self;
        return cell;
    }
    if (indexPath.row < 2)
    {
        HotelCellWithStepper *cell = [self.roomsTableView dequeueReusableCellWithIdentifier:@"HotelCellWithStepper" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell loadRoomData:item.rooms[indexPath.section] withIndexPath:indexPath];
        cell.aDelegate = self;
        return cell;
    }
    else
    {
        Rooms *newRoom = item.rooms[indexPath.section];
        {
            UITableViewCell *cell = [self.roomsTableView dequeueReusableCellWithIdentifier:@"HotelChildAgeCell" forIndexPath:indexPath];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            if (indexPath.row  == 2)
            {
                cell.textLabel.text = [NSString stringWithFormat:@"1st Child' Age"];
                cell.detailTextLabel.text = [NSString stringWithFormat:@" %ld ' yrs",(long)newRoom._1stChildAge];
            }
            else{
                cell.textLabel.text = [NSString stringWithFormat:@"2nd Child' Age"];
                cell.detailTextLabel.text = [NSString stringWithFormat:@" %ld ' yrs",(long)newRoom._2ndChildAge];
            }
            
            return cell;
        }
        
    }
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if( indexPath.row >= 2 && indexPath.section < (item.rooms.count))
    {
        selectedSection = indexPath;
        [self showPickerView];
    }
    
}

#pragma mark HotelCellWithStepperDelegate

- (void) AddNewRowForChildAgeInSection:(NSIndexPath*)indexPath
{
    Rooms *newRoom = item.rooms[indexPath.section];
    
    if (indexPath.row == 2)
        newRoom._1stChildAge = 2;
    else
        newRoom._2ndChildAge = 2;
    
    
    NSIndexPath *newIndexPath = [NSIndexPath indexPathForItem:indexPath.row inSection:indexPath.section];
    [self.roomsTableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationBottom];
    
    //  [self.roomsTableView setScrollEnabled:YES];
}
- (void) removeRowForChildAgeInSection:(NSIndexPath*)indexPath // fix when Remove a Room and we have 3 rows
{
    Rooms *newRoom = item.rooms[indexPath.section];
    newRoom._1stChildAge = 0;
    
    NSIndexPath *newIndexPath = [NSIndexPath indexPathForItem:indexPath.row inSection:indexPath.section];
    [self.roomsTableView deleteRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationBottom];
    
    //[self EnableScrollingForRoomsTable];
}
-(void) AddNewRowButtonDidSelected:(id)object
{
    [self AddDeleteRoomSelector:object];
}
-(void) DeleteRowButtonDidSelected:(id)object
{
    [self AddDeleteRoomSelector:object];
}
- (IBAction)AddDeleteRoomSelector:(id)sender
{
    if ([sender tag] == 1)
    {
        [self AddNewRoom];
    }
    else
    {
        [self DeleteRoom];
    }
    [self.roomsTableView reloadData];
    //[self EnableScrollingForRoomsTable];
}
-(void)AddNewRoom
{
    Rooms *newRoom = [[Rooms alloc] initWithNumberOfAdults:1];
    [item.rooms addObject:newRoom];
}
-(void)DeleteRoom
{
    if (item.rooms.count == 1) {
        return;
    }
    [item.rooms removeLastObject];
}

#pragma mark IQActionSheetPickerViewDelegate

- (void)showPickerView
{
    IQActionSheetPickerView *picker = [[IQActionSheetPickerView alloc] initWithTitle:@"Select Age" delegate:self];
    picker.titleFont = [UIFont fontWithName:KCustomeFont size:14];
    [picker setTag:1];
    [picker setTitlesForComponents:@[@[@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8",@"9"]]];
    [picker show];
}

-(void)actionSheetPickerView:(IQActionSheetPickerView *)pickerView didSelectTitles:(NSArray *)titles
{
    Rooms *newRoom = item.rooms[selectedSection.section];
    
    if (selectedSection.row == 2)
    {
        
        newRoom._1stChildAge = [[titles firstObject] integerValue];
        UITableViewCell *cell = [self.roomsTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:2 inSection:selectedSection.section]];
        
        cell.textLabel.text = [NSString stringWithFormat:@"1st Child' Age"];
        cell.detailTextLabel.text = [NSString stringWithFormat:@" %ld ' yrs",(long)newRoom._1stChildAge];
    }
    else if (selectedSection.row == 3)
    {
        
        newRoom._2ndChildAge = [[titles firstObject] integerValue];
        UITableViewCell *cell = [self.roomsTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:3 inSection:selectedSection.section]];
        cell.textLabel.text = [NSString stringWithFormat:@"2nd Child' Age"];
        cell.detailTextLabel.text = [NSString stringWithFormat:@" %ld ' yrs",(long)newRoom._2ndChildAge];
    }
}

@end
