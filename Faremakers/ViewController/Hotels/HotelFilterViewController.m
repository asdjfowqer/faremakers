//
//  PromotionDetailController.m
//  Faremakers
//
//  Created by Mac1 on 18/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "HotelFilterViewController.h"
#import "IQActionSheetPickerView.h"
#import "HotelFilterCell.h"
#import "HotelFilterItem.h"
#import "FilterDetailViewController.h"

@interface HotelFilterViewController()<UITableViewDataSource,UITabBarControllerDelegate,IQActionSheetPickerViewDelegate>
{
    NSArray *headerTitles;
    NSInteger selectedIndex;
}

@end

@implementation HotelFilterViewController


-(void) viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *clrButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"Clear Filters" style:UIBarButtonItemStyleDone target:self action:@selector(clrFilterButtonClicked:)];
    self.navigationItem.leftBarButtonItem = clrButtonItem;

    headerTitles = @[@"Select Hotel Rating",@"Select Price Range"];//,@"Other Options"];
    self.navigationItem.title = @"Filter";
    UIBarButtonItem *doneButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleDone target:self action:@selector(doneButtonClicked:)];
    self.navigationItem.rightBarButtonItem = doneButtonItem;
    
}

-(void)doneButtonClicked:(id)sender
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}
-(void)clrFilterButtonClicked:(id)sender
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    [self.adelegate hotelfilterViewDidSelectClrAllFilters:YES];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 25;
}
- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return headerTitles[section];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 2)
    {
        return 3;
    }
    
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger index = indexPath.section;
    
    NSString *cellIdentifier = [@"HotelFilterCell" stringByAppendingFormat:@"%ld",(long)index ];
    HotelFilterCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    cell.item = self.filterItem;
    [cell loadCellWithIdentifier:cellIdentifier ForIndexPath:indexPath];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section > 1)
    {
        FilterDetailViewController *detailView = [[FilterDetailViewController alloc] initWithStyle:UITableViewStylePlain];
        detailView.item = self.filterItem;
        if (indexPath.row == 0)
            detailView.filterType = (int)indexPath.row;
        else if (indexPath.row == 1)
            detailView.filterType = Aminities;
        else
            detailView.filterType = Chains;
        
        [self.navigationController pushViewController:detailView animated:YES];
    }
    
}

- (IBAction)ApplyFilterButtonPressed:(id)sender;
{
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        [self.adelegate hotelFilterViewDidDissmissWithFilterItem:self.filterItem];
        
    }];
}

@end
