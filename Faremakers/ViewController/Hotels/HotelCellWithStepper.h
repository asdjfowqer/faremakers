//
//  HotelCellWithStepper.h
//  Faremakers
//
//  Created by Mac1 on 17/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//


#import "HotelCellWithStepperDelegate.h"

@class Rooms;

@interface HotelCellWithStepper : UITableViewCell

@property (nonatomic,strong) Rooms *room;

@property (nonatomic,weak) id<HotelCellWithStepperDelegate> aDelegate;


- (IBAction)stepperValueChanged:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *AdultLabel;

@property (weak, nonatomic) IBOutlet UIStepper *travellerStepper;

-(void)loadRoomData:(Rooms*)room withIndexPath:(NSIndexPath*)indexPath;

@end
