//
//  HotelSearchViewController.h
//  Faremakers
//
//  Created by Mac1 on 16/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//


#import "RESideMenu.h"
#import "HotelCellWithStepperDelegate.h"

@class HotelSearchItem;

@interface HotelSearchViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,HotelCellWithStepperDelegate>

@property (weak, nonatomic) IBOutlet UITableView *roomsTableView;
@property (weak, nonatomic) IBOutlet UITableView *schedTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *roomsTableTopMargin;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchBtnBottomMargin;
@property (strong, nonatomic) HotelSearchItem *item;


- (IBAction)searchClicked:(id)sender;

@end
