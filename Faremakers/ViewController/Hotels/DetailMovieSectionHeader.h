//
//  CSSectionHeader.h
//  CSStickyHeaderFlowLayoutDemo
//
//  Created by James Tang on 3/2/15.
//  Copyright (c) 2015 Jamz Tang. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CSSectionHeaderDelegate <NSObject>

- (void) backButtonPressed:(id)sender;

@end

@class Movie;

@interface DetailMovieSectionHeader : UICollectionReusableView

@property (nonatomic, assign) id<CSSectionHeaderDelegate>  theDelegate;
@property (nonatomic, strong) IBOutletCollection(UIImageView) NSArray *starImageViews;

- (IBAction)backPress:(id)sender;
@end
