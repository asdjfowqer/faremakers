//
//  MyCell.m
//  TestCollectionViewWithXIB
//
//  Created by Quy Sang Le on 2/3/13.
//  Copyright (c) 2013 Quy Sang Le. All rights reserved.
//

#import "DetailHotelCollectionHeadercell.h"
#import <UIImageView+WebCache.h>

@implementation DetailHotelCollectionHeadercell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void) setHotelImage:(NSString*)detailMovieImage
{
    
    UIImage *placeholderImage = [UIImage imageNamed:@"Stars"];
    [self.cellImage sd_cancelCurrentImageLoad];
    
    [_activityView setHidden:NO];
    
    
    {
        [self.cellImage sd_setImageWithURL:[NSURL URLWithString:detailMovieImage]
                          placeholderImage:self.cellImage.image
                                   options:SDWebImageRetryFailed
                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
             [self.activityView stopAnimating];
             if(error == nil)
                 self.cellImage.image = image;
             else
                 self.cellImage.image = [UIImage imageNamed:@"no-images.png"];
             
             //                 self.cellImage.image = [UIImage imageNamed:@"No-image-found.jpg"];
             
             
         }];
    }
}


@end
