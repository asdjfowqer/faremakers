//
//  HotelRoomsCell.h
//  Faremakers
//
//  Created by Mac1 on 10/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HotelRoomsCellDelegate <NSObject>

-(void) HotelRoomsCellDidSelectRoom:(id)sender;

@end

@class HotelRoomCategories;
@class FMHotels;

@interface HotelRoomsCell : UITableViewCell

@property (nonatomic) HotelRoomCategories *roomCategories;
@property (nonatomic) FMHotels *hotels;
@property (nonatomic,weak) id<HotelRoomsCellDelegate> adelegate;
@property (weak, nonatomic) IBOutlet UIImageView *hotelImageView;
@property (weak, nonatomic) IBOutlet UILabel *roomNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *roomDesLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityView;

- (IBAction)selecRoomBtnPressed:(id)sender;

@end
