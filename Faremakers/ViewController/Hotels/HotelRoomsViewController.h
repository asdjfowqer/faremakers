//
//  HotelRoomsViewController.h
//  Faremakers
//
//  Created by Mac1 on 10/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FMHotels;
@class HotelSearchItem;

@interface HotelRoomsViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) FMHotels *hotelItem;
@property (nonatomic) HotelSearchItem *searchItem;


@end
