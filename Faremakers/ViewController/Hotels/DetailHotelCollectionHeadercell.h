//
//  MyCell.h
//  TestCollectionViewWithXIB
//
//  Created by Quy Sang Le on 2/3/13.
//  Copyright (c) 2013 Quy Sang Le. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface DetailHotelCollectionHeadercell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *cellImage;

@property (strong, nonatomic)UIImageView *DetailHotelImage;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityView;

- (void) setHotelImage:(NSString*)detailMovieImage;

@end
