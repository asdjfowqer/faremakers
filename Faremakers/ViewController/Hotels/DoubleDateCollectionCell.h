//
//  DoubleDateCollectionCell.h
//  Faremakers
//
//  Created by Mac1 on 16/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IQActionSheetPickerView.h"

@class DoubleDateItem;

@interface DoubleDateCollectionCell : UICollectionViewCell <IQActionSheetPickerViewDelegate>
{
    NSInteger selectedCellTag;
}
@property (nonatomic) DoubleDateItem *dateItem;
@property (weak, nonatomic) IBOutlet UILabel *departTitle;
@property (weak, nonatomic) IBOutlet UILabel *returnTitle;
@property (weak, nonatomic) IBOutlet UILabel *departDay;
@property (weak, nonatomic) IBOutlet UILabel *departDate;
@property (weak, nonatomic) IBOutlet UILabel *departYear;
@property (weak, nonatomic) IBOutlet UIButton *deparBtn;

@property (weak, nonatomic) IBOutlet UILabel *returnDate;
@property (weak, nonatomic) IBOutlet UILabel *returnDay;
@property (weak, nonatomic) IBOutlet UILabel *returnYear;
@property (weak, nonatomic) IBOutlet UIButton *returnBtn;


- (IBAction)datePickerViewClicked:(UIButton *)sender;
-(void)disableDateSelection:(BOOL)disable;


@end
