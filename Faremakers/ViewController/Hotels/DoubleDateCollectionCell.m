//
//  DoubleDateCollectionCell.m
//  Faremakers
//
//  Created by Mac1 on 16/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "DoubleDateCollectionCell.h"
#import "IQActionSheetPickerView.h"
#import "NSDate+DateComponents.h"
#import "UILabel+FontName.h"
#import "DoubleDateItem.h"

@implementation DoubleDateCollectionCell

- (void)awakeFromNib
{
    [self.departTitle changeFontFamily];
    [self.returnTitle changeFontFamily];
    [self.departDay changeFontFamily];
    [self.departDate changeFontFamily];
    [self.departYear changeFontFamily];
    [self.returnDate changeFontFamily];
    [self.returnDay changeFontFamily];
    [self.returnYear changeFontFamily];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
-(void)disableDateSelection:(BOOL)disable
{
    [self.returnBtn setEnabled:disable];
    [self.deparBtn setEnabled:disable];
}
-(void) setDateItem:(DoubleDateItem *)dateItem
{
    _dateItem = dateItem;
    if (self.dateItem.depDate) {
        [self setDepartDateComponents:self.dateItem.depDate];
    }
    if (self.dateItem.retDate) {
        [self setReturnDateComponents:self.dateItem.retDate];
    }
}
- (IBAction)datePickerViewClicked:(UIButton *)sender
{
    selectedCellTag = [sender tag];
    
    IQActionSheetPickerView *picker = [[IQActionSheetPickerView alloc] initWithTitle:@"Date Picker" delegate:self];
    [picker setTag:6];
    [picker setActionSheetPickerStyle:IQActionSheetPickerStyleDatePicker];
    [picker show];
}

-(void)actionSheetPickerView:(IQActionSheetPickerView *)pickerView didSelectTitles:(NSArray *)titles
{
    DLog(@"%@",titles);
}

-(void)actionSheetPickerView:(IQActionSheetPickerView *)pickerView didSelectDate:(NSDate *)date
{
    switch (pickerView.tag)
    {
        case 6:
        {
            if (selectedCellTag == 0)
            {
                [self setDepartDateComponents:date];
            }
            else if (selectedCellTag == 1)
            {
                [self setReturnDateComponents:date];
            }
        }
            break;
        default:
            break;
    }
}

-(void)setDepartDateComponents:(NSDate*)date
{
    self.departYear.text = [NSString stringWithFormat:@"%@ ' %@", [date getMonth],[date getYear]];
    self.departDay.text = [date getDayOfTheWeek];
    self.departDate.text = [date getDayOfTheMonth];
}
-(void)setReturnDateComponents:(NSDate*)date
{
    self.returnYear.text = [NSString stringWithFormat:@"%@ ' %@", [date getMonth],[date getYear]];
    self.returnDay.text = [date getDayOfTheWeek];
    self.returnDate.text = [date getDayOfTheMonth];
}
@end
