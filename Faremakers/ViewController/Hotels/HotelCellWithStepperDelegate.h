//
//  HotelCellWithStepperDelegate.h
//  ElectionApp
//
//  Created by Mac1 on 09/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

//#ifndef HotelCellWithStepperDelegate_h
//#define HotelCellWithStepperDelegate_h

@protocol HotelCellWithStepperDelegate <NSObject>

- (void) AddNewRowForChildAgeInSection:(NSIndexPath*)indexPath;
- (void) removeRowForChildAgeInSection:(NSIndexPath*)indexPath;

@end

//#endif /* HotelCellWithStepperDelegate_h */
