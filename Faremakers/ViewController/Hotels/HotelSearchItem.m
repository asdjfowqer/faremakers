//
//  HotelSearchItem.m
//  Faremakers
//
//  Created by Mac1 on 29/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "HotelSearchItem.h"
#import "DoubleDateItem.h"
#import "Constants.h"
#import "Cities.h"
#import "FlightSegment.h"
#import "Rooms.h"

@implementation HotelSearchItem

-(id)init
{
    if (self = [super init])
    {
        self.searchTitle = @"DUBAI";
        self.doubleDate = [[DoubleDateItem alloc] initWithDoubleDate:[[NSDate date] dateByAddingTimeInterval:KTimeInterval*7] :[[NSDate date] dateByAddingTimeInterval:KTimeInterval*21]];
        self.rooms = [[NSMutableArray alloc] init];
        self.destCity = [Cities itemWithDictionary:[self prepareTempCityDictionary:1]];
    }
    return self;
}
-(NSDictionary*)prepareTempCityDictionary:(BOOL)isDestination
{
    if (isDestination)
    {
        return [NSDictionary dictionaryWithObjectsAndKeys:  @"DUBAI",@"airtportName",
                @"DXB", @"cityCode" ,
                @"UNITED ARAB EMI", @"countryName" ,
                @"AE", @"coutryCode",
                @"DUBAI,AE (DXB)", @"text", nil];
        
    }
    else
    {
        return [NSDictionary dictionaryWithObjectsAndKeys:  @"LAHORE",@"airtportName",
                @"LHE", @"cityCode" ,
                @"PAKISTAN", @"countryName" ,
                @"PK", @"coutryCode",
                @"LAHORE,PK (LHE)", @"text", nil];
        
    }
}
- (BOOL)checkAllValues
{
    BOOL error = NO;
    
    NSString *errorString = @"";
    
    if (!self.destCity)
    {
        errorString = @"Require Destination location";
        error = YES;
    }
    if (![self compareDeparDate:self.doubleDate.depDate withDate:self.doubleDate.retDate])
    {
        errorString = @"CheckOut Date is incorrect";
        error = YES;
    }
    if (error)
    {
        [[[UIAlertView alloc] initWithTitle:@"Error" message:errorString delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil] show];
    }
    return error;
}
-(BOOL)compareDeparDate:(NSDate*)date1 withDate:(NSDate*)date
{
    NSTimeInterval interval = [date timeIntervalSinceDate:date1];
    DLog(@"%f",interval);
    if (interval < 0)
        return false;
    return true;
}

-(NSString*)convertRoomIntoString
{
    NSInteger adults = 0;
    NSInteger childes = 0;
    
    for (Rooms *room in self.rooms)
    {
        adults += room.numberOfAdults;
        childes += room.numberOfChileds;
    }
    return [NSString stringWithFormat:@"Adults %ld childes %ld",adults,childes];
}


-(NSInteger)adultsCount
{
	NSInteger adults = 0;
	
	for (Rooms *room in self.rooms)
	{
		adults += room.numberOfAdults;
	}
	
	return adults;
}




-(NSInteger)childCount
{
	NSInteger childs = 0;
	
	for (Rooms *room in self.rooms)
	{
		childs += room.numberOfChileds;
	}
	
	return childs;
}



//
/*
 ============= Note this query is not working perfectly please check ===========
 
 Case # 1 when Adults Exceed to 3
 Case # 2 when Rooms Exceeds to 3
 Case # 3 when Childs Exceeds to 1
 Case # 4 API resturn Currency In PKR what ever you send
 
 */
-(NSDictionary*)preparePostData
{
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:[FlightSegment parseDateOnly:self.doubleDate.depDate] forKey:@"CheckInDate"];
    [dict setObject:[FlightSegment parseDateOnly:self.doubleDate.retDate] forKey:@"CheckOutDate"];
    [dict setObject:[NSString stringWithFormat:@"%d",(self.rooms.count<=3)?(int)self.rooms.count:3] forKey:@"NumberOfRooms"];
    [dict setObject:[NSString stringWithFormat:@"%ld",[self.doubleDate noOfDays]] forKey:@"TotalNights"];
    [dict setObject:self.destCity.text forKey:@"City"];
    [dict setObject:[[NSLocale currentLocale] objectForKey: NSLocaleCountryCode] forKey:@"Country"]; //@"PK"
    [dict setObject:@"1" forKey:@"Adults"];
    [dict setObject:@"0" forKey:@"Childs"];
    [dict setObject:@"0" forKey:@"FirstChild"];
    [dict setObject:@"0" forKey:@"SecondChild"];
    [dict setObject:[[NSLocale currentLocale] objectForKey: NSLocaleCurrencyCode] forKey:@"Currency"];//@"pkr"
  
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict
                                                       options:(NSJSONWritingOptions) NSJSONWritingPrettyPrinted
                                                         error:&error];
    
    if (! jsonData) {
        DLog(@"bv_jsonStringWithPrettyPrint: error: %@", error.localizedDescription);
    } else {
        DLog(@"%@", [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]);
    }
    return dict;
}
@end
