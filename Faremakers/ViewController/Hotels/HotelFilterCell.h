//
//  PromotionDetailCell.h
//  Faremakers
//
//  Created by Mac1 on 19/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HotelFilterItem;

@interface HotelFilterCell : UITableViewCell <UICollectionViewDataSource,UICollectionViewDelegate>

@property (strong, nonatomic)HotelFilterItem *item;

#pragma mark HotelFilterCell0
@property (weak, nonatomic) IBOutlet UICollectionView *starCollectionView;

#pragma mark HotelFilterCell0
@property (weak, nonatomic) IBOutlet UICollectionView *priceCollectionView;

#pragma mark HotelFilterCell2
@property (weak, nonatomic) IBOutlet UILabel *filterTitlelbl;
@property (weak, nonatomic) IBOutlet UILabel *filterDetailLbl;

- (void)loadCellWithIdentifier:(NSString *)cellIdentifier ForIndexPath:(NSIndexPath*)index;
@end
