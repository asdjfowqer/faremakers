//
//  HotelMapViewController.m
//  Faremakers
//
//  Created by Mac1 on 12/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "HotelMapViewController.h"
#import "HotelDetailColletionController.h"
#import "FMHotels.h"

@interface HotelMapViewController ()

@end

@implementation HotelMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.mapType == AllMAP)
        self.title = @"Hotels MapView";
    else
        self.title = @"Hotel MapView";

    [self addPlacesInMapView];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)addPlacesInMapView
{
    for (FMHotels *hotel in self.hotelsList)
    {
        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake([hotel.hotelLatitude doubleValue], [hotel.hotelLongitude doubleValue]);
        if (coordinate.latitude == 0 || coordinate.longitude == 0) {
            continue;
        }
        MKPointAnnotation *annotation = [[MKPointAnnotation alloc]init];
        annotation.coordinate = coordinate;
        annotation.title = hotel.hotelName;
        [self.mapView addAnnotation:annotation];
        
    }
    if(self.mapView.annotations.count)
        [self.mapView showAnnotations:self.mapView.annotations animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (nullable MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if(!annotation)
        return nil;
    NSString *reuseIdentifier = @"HotelMapPin";
    
    MKAnnotationView *annotationView = [mapView dequeueReusableAnnotationViewWithIdentifier:reuseIdentifier];
    
    if (!annotationView) {
        annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
        annotationView.canShowCallout = YES;
        annotationView.image = [UIImage imageNamed:@"marker_pin"];
    }
    UIButton *detailButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    if(self.mapType == AllMAP)
    annotationView.leftCalloutAccessoryView=detailButton;
    return annotationView;
    
}
- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    NSUInteger index = [mapView.annotations indexOfObject:view.annotation];
    [self showDetailViewForHotels:index];
}
- (void)showDetailViewForHotels:(NSInteger)index
{
    HotelDetailColletionController *detailView = [self.storyboard instantiateViewControllerWithIdentifier:@"CSStickyParallaxHeaderViewController"];
    detailView.hotelSearchItem  = self.searchItem;
    detailView.hotel = self.hotelsList[index];
    [self.navigationController pushViewController:detailView animated:YES];
}

@end
