//
//  AddnDeleteRowCell.m
//  Faremakers
//
//  Created by Mac1 on 12/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "AddnDeleteRowCell.h"

@implementation AddnDeleteRowCell

- (IBAction)addNewRowBtnSelector:(id)sender
{
    [self.adelegate AddNewRowButtonDidSelected:sender];
}

- (IBAction)removeRowBtnSelector:(id)sender
{
    [self.adelegate DeleteRowButtonDidSelected:sender];
}

- (IBAction)addNewRowBtnSelector1:(id)sender
{
    [self.adelegate AddNewRowButtonDidSelected:sender];
}
@end
