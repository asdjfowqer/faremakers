//
//  CSAlwaysOnTopHeader.h
//  CSStickyHeaderFlowLayoutDemo
//
//  Created by James Tang on 6/4/14.
//  Copyright (c) 2014 Jamz Tang. All rights reserved.
//

#import "DetailHotelCollectionViewCell.h"

@protocol CSAlwaysOnTopHeaderDelagate <NSObject>

- (void) getCollectionSizeOfImage:(CGSize)size atIndex:(NSInteger)index;
- (void) collectionViewDidReceiveTap:(CGPoint)size;
- (void) backButtonPressedFromTopHeader:(id)sender;
- (void) collectionViewDidSelectItemAtIndexPath:(NSIndexPath*)indexPath;

@end

@interface DetailHotelColletionViewHeader : DetailHotelCollectionViewCell< UICollectionViewDataSource, UICollectionViewDelegate >

@property (strong, nonatomic) IBOutlet UICollectionView *customCollection;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *imageCollectionHeight;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *collectionViewHeight;
@property (nonatomic, assign) id<CSAlwaysOnTopHeaderDelagate>  adelegate;
@property (weak, nonatomic) IBOutlet UILabel *CurrentImageCount;



-(void)initializeCollectionView;
- (IBAction)shareButtonSelector:(id)sender;
- (IBAction)shortListedSelector:(id)sender;
- (IBAction)backButtonSelector:(id)sender;
- (void)loadCollectionViewWithArray:(NSArray *)allItems;
- (void)loadCollectionViewWithArray:(NSArray *)allItems andCellHeight:(float)height;

@end
