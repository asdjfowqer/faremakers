//
//  HotelStarFilterColletionCell.h
//  Faremakers
//
//  Created by Mac1 on 17/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HotelStarFilterColletionCell : UICollectionViewCell

@property (nonatomic , weak) IBOutlet UILabel *starTitleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *starImageView;

-(void)loadCellWithItem:(id)item atIndexPath:(NSIndexPath*)indexPath;

@end
