//
//  HotelBookingCell.h
//  Faremakers
//
//  Created by Mac1 on 29/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FMHotels;
@class HotelRoomCategories;
@class HotelSearchItem;

@interface HotelBookingCell : UITableViewCell

@property (nonatomic) FMHotels *hotel;
@property (nonatomic) HotelRoomCategories *roomCategory;
@property (nonatomic) HotelSearchItem *searchItem;

#pragma HotelBookingCell0

@property (weak, nonatomic) IBOutlet UILabel *detailTitle;
@property (weak, nonatomic) IBOutlet UILabel *detailDescription;

#pragma HotelBookingCell1

@property (weak, nonatomic) IBOutlet UILabel *policiesTitle;

#pragma HotelBookingCell2
@property (weak, nonatomic) IBOutlet UILabel *paymentDetail;
@property (weak, nonatomic) IBOutlet UILabel *Amount;

-(void)setRoomCategory:(HotelRoomCategories *)roomCategory forIndex:(NSInteger)index withReuseIdentifier:(NSString*)identifier;

@end
