//
//  CSSectionHeader.m
//  CSStickyHeaderFlowLayoutDemo
//
//  Created by James Tang on 3/2/15.
//  Copyright (c) 2015 Jamz Tang. All rights reserved.
//

#import "DetailMovieSectionHeader.h"

@implementation DetailMovieSectionHeader


- (IBAction)backPress:(id)sender;
{
    [_theDelegate backButtonPressed:sender];
}

@end
