//
//  DoubleDateTableCell.h
//  Faremakers
//
//  Created by Mac1 on 16/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//


#import "IQActionSheetPickerView.h"
#import "DoubleDateCellViewDelegate.h"

@class FlightSearchItem, DoubleDateItem;

@interface DoubleDateTableCell : UITableViewCell <IQActionSheetPickerViewDelegate>
{
    NSInteger selectedCellTag;
}

@property (nonatomic , weak) id<DoubleDateCellViewDelegate> adelegate;

@property (nonatomic) NSInteger searchType;
@property (nonatomic , strong) FlightSearchItem *item;
@property (nonatomic , strong) NSDate *dateItem1;
@property (nonatomic , strong) NSDate *dateItem2;

@property (nonatomic , strong) DoubleDateItem *doubleDateItem;

@property (weak, nonatomic) IBOutlet UILabel *departTitle;
@property (weak, nonatomic) IBOutlet UILabel *returnTitle;
@property (weak, nonatomic) IBOutlet UILabel *departDay;
@property (weak, nonatomic) IBOutlet UILabel *departDate;
@property (weak, nonatomic) IBOutlet UILabel *departYear;

@property (weak, nonatomic) IBOutlet UILabel *returnDate;
@property (weak, nonatomic) IBOutlet UILabel *returnDay;
@property (weak, nonatomic) IBOutlet UILabel *returnYear;

@property (weak, nonatomic) IBOutlet UILabel *selectLabel;

- (IBAction)datePickerViewClicked:(UIButton *)sender;
- (void)setDoubleDateComponetsData:(NSDate *)date1 :(NSDate*)date2;
- (void)showRoundTripLables:(BOOL)show;
@end
