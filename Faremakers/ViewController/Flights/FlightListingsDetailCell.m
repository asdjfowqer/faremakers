//
//  FlightListingsDetailCell.m
//  Faremakers
//
//  Created by Mac1 on 23/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "FlightListingsDetailCell.h"
#import "FlightStopsCollectionCell.h"
#import "LayOverCollectionCell.h"
#import "AirLinePricedItinerary.h"
#import "AirItineraryPricingInfo.h"
#import "AirCustomeData.h"
#import "FlightSegment.h"

@interface FlightListingsDetailCell () <UICollectionViewDataSource,UICollectionViewDelegate>
{
    NSInteger totalStops;
    NSInteger totalOverLay;
    NSArray *flightSegmentsArr;
    OriginDestination *originDestinationItem;
}

@end

@implementation FlightListingsDetailCell

- (void)awakeFromNib
{
    [self.numberOfStopsLabel changeFontFamily];
    [self.totalTravellTimeLabel changeFontFamily];
    [self.originTitleLabel changeFontFamily];
    [self.destTitleLabel changeFontFamily];
}
- (void)setAirLineItem:(AirLinePricedItinerary *)airLineItem withTripIndex:(NSInteger)index
{
    AirCustomeData *customeData = airLineItem.customeArray[index];
    _numberOfStopsLabel.text = [NSString stringWithFormat:@"%@ Stops",customeData.totalStops];
    _totalTravellTimeLabel.text = [NSString stringWithFormat:@"Duration %@",customeData.duration];
    _originTitleLabel.text = customeData.DepartureAirport;
    _destTitleLabel.text = customeData.arrivalAirport;
    
    _airLineItem = airLineItem.airItinerary;
    
  //  flightSegmentsArr = [_airLineItem.originDestinationOptions objectAtIndex:index];
  //  totalStops = flightSegmentsArr.count;
    
    originDestinationItem = [_airLineItem.originDestinationOptions objectAtIndex:index];
    totalStops = originDestinationItem.stopQuantity.integerValue + 1;
    totalOverLay = totalStops - 1;

    [self.stopsCollectionView reloadData];
    
    AirItineraryPricingInfo *pricingInfo = airLineItem.airItineraryPricingInfo[0];
    self.refundableImageWidth.constant = ([pricingInfo checkNonRefunableFlight])?0:10;
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapGesture:)];
    tapGesture.numberOfTapsRequired = 1;
    [self addGestureRecognizer:tapGesture];
    
    
    
}
- (void)singleTapGesture:(UITapGestureRecognizer*)sender {
    
    if(sender.state == UIGestureRecognizerStateRecognized)
    {
        CGPoint point = [sender locationInView:sender.view];
        
        [self.aDelegate collectionViewDidSelectMenuItem:0];
        
    }
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return totalStops + totalOverLay;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger index = indexPath.row;
    
    float width = 0;
    
    if ((index +1) % 2 == 0)
    {
        width = 55;
    }
    else
    {
        width = 138;
    }
    return CGSizeMake(width,self.stopsCollectionView.frame.size.height);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger index = indexPath.row;
    
    if ((index +1) % 2 == 0)
    {
        LayOverCollectionCell *cell = [self.stopsCollectionView dequeueReusableCellWithReuseIdentifier:@"LayOverCollectionCell" forIndexPath:indexPath];
        index = (index) / 2;
        FlightSegment *flightSegment = originDestinationItem.flightSegments[index];
        cell.layOverTimeLabel.text = [NSString stringWithFormat:@"%@",[flightSegment stringFromTimeInterval:flightSegment.layOverTime]];
        
        return cell;
    }
    else
    {
        FlightStopsCollectionCell *cell = [self.stopsCollectionView dequeueReusableCellWithReuseIdentifier:@"FlightStopsCollectionCell" forIndexPath:indexPath];
        NSInteger index = (indexPath.row)?(indexPath.row/2):indexPath.row;
        cell.flightSegmentItem = originDestinationItem.flightSegments[index];
        return cell;
    }
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self.aDelegate collectionViewDidSelectMenuItem:indexPath.row];
    
}
@end
