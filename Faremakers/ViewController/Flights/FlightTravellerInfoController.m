//
//  ExamplesFormViewController.m
//  XLForm ( https://github.com/xmartlabs/XLForm )
//
//  Copyright (c) 2015 Xmartlabs ( http://xmartlabs.com )
//
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import "FlightTravellerInfoController.h"
#import "LogInWithEmailViewController.h"
#import "EditTravellerViewController.h"
#import "TPKeyboardAvoidingTableView.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "AirItineraryPricingInfo.h"
#import "AirLinePricedItinerary.h"
#import "FlightDataApiManager.h"
#import "NSString+PhoneNumber.h"
#import "FlightTravellerCell.h"
#import "FMWebViewController.h"
#import "TransitionDelegate.h"
#import "PaymentApiManager.h"
#import "BookingPromtView.h"
#import "FlightSearchItem.h"
#import "AirItinTotalFare.h"
#import "NSString+Email.h"
#import "AirBaseFare.h"
#import "Travellers.h"
#import "FMUser.h"



@interface FlightTravellerInfoController () <FMWebViewDelegate, FlightTravellerCellDelegate, UIAlertViewDelegate> {
    NSArray *profileDataArr;
    NSArray *listViewImageArr;
    NSArray *sectionHeaders; //111-007-008 / 03311-007-008
    CGFloat grandtotal;
    NSDictionary *emailResponse;
    int bankChargesAdded;
}

@property (nonatomic, strong) TransitionDelegate *transitionController;

@end

#define KProfileViewCell @"FlightTravellerCell"

@implementation FlightTravellerInfoController {
    NSInteger rowHeight;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Passenger Info";
    
    sectionHeaders = @[@"Personal Information", @"Contact Information"];
    profileDataArr  = @[@[@"First Name", @"Last Name", @"DOB"], @[@"Email", @"Contact Number"]];
    
    
    // TODO: Decorate User Interface
    [self DecorateUI];
    
    //    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Log In" style:UIBarButtonItemStylePlain target:self action:@selector(showUserLoginViewControllerCalled:)];

    rowHeight = (KMAXHEIGHT * 0.08)>50 ? (KMAXHEIGHT * 0.08): 50;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    if (self.flightSearchItem.travellers.passengersInfo.count < [self.flightSearchItem.travellers totalTravellers])
    {
        _flightSearchItem.travellers.passengersInfo = [[NSMutableArray alloc] initWithCapacity:[self.flightSearchItem.travellers totalTravellers]];
        
        int i = 0;
        while ([self.flightSearchItem.travellers totalTravellers] > i)
        {
            Passengers *pass = [[Passengers alloc] init];
            [self.flightSearchItem.travellers.passengersInfo addObject:pass];
            i++;
        }
        
        TransactionBookingPaidFlight *transaction = (TransactionBookingPaidFlight *)[TransactionBookingManager shared].currentTransaction;
        
        NSArray *passengers = self.flightSearchItem.travellers.passengersInfo;
        
        NSArray *adults = [passengers subarrayWithRange:NSMakeRange(0, self.flightSearchItem.travellers.Adults)];
        [transaction.adults addObjectsFromArray:adults];
        
        NSArray *childern = [passengers subarrayWithRange:NSMakeRange(self.flightSearchItem.travellers.Adults, self.flightSearchItem.travellers.Childs)];
        [transaction.children addObjectsFromArray:childern];
        
        NSArray *infants = [passengers subarrayWithRange:NSMakeRange(self.flightSearchItem.travellers.Adults + self.flightSearchItem.travellers.Childs, self.flightSearchItem.travellers.Infants)];
        [transaction.infants addObjectsFromArray:infants];
    }
    
    double totalamount = [self.airPricingInfoItem.itinTotalFare.totalFare.amount intValue];
    
    NSInteger addThreepercent = totalamount;
    
    addThreepercent = addThreepercent * 3/100;
    bankChargesAdded = addThreepercent + totalamount;
    
    NSLog(@"total fair nabeel bank charged: %d", bankChargesAdded);
    _totalCostLabel.text = [NSString stringWithFormat:@"%@ %d", KCurrencyCode, bankChargesAdded];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Decorate UI
-(void)DecorateUI {
    
    // TODO: Grand Total View.
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:self.vwGrandView.bounds];
    self.vwGrandView.layer.masksToBounds = NO;
    self.vwGrandView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.vwGrandView.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    self.vwGrandView.layer.shadowOpacity = 0.1f;
    self.vwGrandView.layer.shadowPath = shadowPath.CGPath;
    
    // TODO: Table View
    rowHeight = (KMAXHEIGHT * 0.08)>50 ? (KMAXHEIGHT * 0.08): 50;
    self.tableView.tableFooterView = [UIView new];
    self.tableView.alwaysBounceVertical = false;
    
}

- (void)userEnteredPromocode:(NSString *)promocode {
    
    [PaymentApiManager getPromoCode:promocode type:@"Flights" withSuccessBlock: ^(id responseobject) {
        if ([responseobject floatValue] == -1)
        {
            [self showErrorMessage:@"The promocode entered is not valid."];
        }
        else
        {
            [TransactionBookingManager shared].currentTransaction.promocodePercentage = [NSNumber numberWithFloat:[responseobject floatValue]];
            CGFloat amountNew = [[TransactionBookingManager shared].currentTransaction.amount floatValue];
            CGFloat discount = [responseobject floatValue];
            
            amountNew = amountNew - (amountNew * (discount / 100));
            
            self.totalCostLabel.text = [NSString stringWithFormat:@"%.02f %@", amountNew, [TransactionBookingManager shared].currentTransaction.currency];
            
            [TransactionBookingManager shared].currentTransaction.promocodePercentage = [NSNumber numberWithFloat:discount];
            [TransactionBookingManager shared].currentTransaction.promocode = promocode;
            
            grandtotal = amountNew;
        }
    }
                       failureBlock: ^(NSError *error) {
                           [self showErrorMessage:@"An error occured while trying to verify promocode."];
                       }];
}

#pragma mark - Reload TableView
-(void)ReloadTableView {
    
}

#pragma mark - UITableView Datasource & Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return sectionHeaders.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //    return (section == 0)?[self.flightSearchItem.travellers totalTravellers]: 2;//array.count;
    return (section == 0) ? [self.flightSearchItem.travellers totalTravellers] : 3;//array.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *reuseIdentifier = [KProfileViewCell stringByAppendingFormat:@"%ld", (long)indexPath.section];
    
    FlightTravellerCell *cell = [self.tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    cell.flightSearchItem = self.flightSearchItem;
    [cell loadCellWithIdentifier:reuseIdentifier forRowCell:indexPath.row];
    cell.delegate = self;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return rowHeight;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (!indexPath.section)
    {
        EditTravellerViewController *editTravellerView = [self.storyboard instantiateViewControllerWithIdentifier:@"EditTravellerViewController"];
        editTravellerView.passenger = self.flightSearchItem.travellers.passengersInfo[indexPath.row];
        editTravellerView.person = [self getPersonType:indexPath.row];
        [self.navigationController pushViewController:editTravellerView animated:YES];
    }
}

- (UINavigationController *)setUpNavigationControllerWithRootView:(id)rootView
{
    UINavigationController *navigationControlller = [[UINavigationController alloc] initWithNavigationBarClass:[FMNavigationBar class] toolbarClass:nil];
    [navigationControlller setViewControllers:@[rootView]];
    
    return navigationControlller;
}

- (void)DoneButtonPressed:(id)sender
{
}

- (void)showUserLoginViewControllerCalled:(id)sender
{
    LogInWithEmailViewController *loginView = [self.storyboard instantiateViewControllerWithIdentifier:@"LogInWithEmailViewController"];
    [loginView setUpRightNavigationItem];
    [self.navigationController presentViewController:[self setUpNavigationControllerWithRootView:loginView] animated:YES completion:nil];
}

- (IBAction)saveProfileBtnSelector:(id)sender
{
    [self.tableView endEditing:YES];
}

- (void)showErrorMessage:(NSString *)message
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:message preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler: ^(UIAlertAction *_Nonnull action) {
        DLog(@"cancled");
    }]];
    [self.navigationController presentViewController:alertController animated:YES completion:nil];
}

- (Person)getPersonType:(NSInteger)row
{
    if (row < self.flightSearchItem.travellers.Adults)
    {
        return 0;
    }
    else if (row < (self.flightSearchItem.travellers.Childs + self.flightSearchItem.travellers.Adults))
    {
        return 1;
    }
    else
    {
        return 2;
    }
}

- (IBAction)proceedButtonSelector:(id)sender
{
    [self.tableView endEditing:YES];

    TransactionBookingPaidFlight *transaction = (TransactionBookingPaidFlight *)[TransactionBookingManager shared].currentTransaction;
    
    DLog(@"%@ ", transaction.adults);
    DLog(@"%@ ", transaction.children);
    DLog(@"%@ ", transaction.infants);
    
    [self validateData];
    
    //    if ([FMUser defaultUser].authToken)
    //    {
    //        [self validateData];
    //    }
    //    else
    //    {
    //        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Error" message:@"You must be logged in to continue." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    //        [av show];
    //    }
}

- (void)showBookingPromtViewController
{
    BookingPromtView *rulesView = [self.storyboard instantiateViewControllerWithIdentifier:@"BookingPromtView"];
    rulesView.passenger  = self.flightSearchItem.travellers.passengersInfo[0];
    rulesView.view.backgroundColor = [UIColor clearColor];
    [rulesView setTransitioningDelegate:_transitionController];
    rulesView.modalPresentationStyle = UIModalPresentationCustom;
    [self.navigationController presentViewController:rulesView animated:NO completion:nil];//:rulesView animated:YES];
}

- (void)showBookingWebView:(NSString *)html
{
    FMWebViewController *webVC = [[FMWebViewController alloc] initWithNibName:@"FMWebViewController" bundle:nil];
    webVC.html = html;
    webVC.adelegate = self;
    [self.navigationController presentViewController:webVC animated:YES completion:nil];
}
- (void)postBookingDataWithUser:(BOOL)isloggedIn
{
    [SVProgressHUD show];
    
    TransactionBookingPaid *transaction = [TransactionBookingManager shared].currentTransaction;
    
    transaction.firstName = [FMUser defaultUser].firstName;
    transaction.lastName = [FMUser defaultUser].phoneNumber;
    transaction.phoneNumber = [FMUser defaultUser].lastName;
    
    NSDictionary *postData ;
    if (!isloggedIn) {
        
        if (![(emailResponse[@"isEmailExists"]) boolValue])
        {
            Passengers *pass = self.flightSearchItem.travellers.passengersInfo[0];
            
            transaction.firstName = pass.fName;
            transaction.lastName = pass.lName;
            transaction.phoneNumber = [FMUser defaultUser].phoneNumber;
        }
        else
        {
            transaction.phoneNumber = emailResponse[@"data"][@"phone"];
            transaction.firstName = emailResponse[@"data"][@"firstName"];
            transaction.lastName = emailResponse[@"data"][@"lastName"];

        }
        
        postData = [transaction prePaymentDictionaryWithNewUser:emailResponse];
    }
    else
        postData = [transaction prePaymentDictionary];
    
    
    DLog(@"%@", [transaction prePaymentDictionary]);
    
    [PaymentApiManager uploadBookingDetailData:postData success: ^(id responseobject) {
        [SVProgressHUD dismiss];
        
        DLog(@"%@", responseobject);
        
        if (responseobject)
        {
            DLog(@"%@", responseobject[@"data"][@"html"]);
            
            if ([(responseobject[@"data"][@"corporateUser"]) boolValue])
            {
                [self postDataAfterPayment:YES];
            }
            else
            {
                [[TransactionBookingManager shared].currentTransaction addPaymentInformationFromDictionary:responseobject];
                
                if (responseobject[@"data"][@"authToken"]) {
                    [FMUser defaultUser].authToken = responseobject[@"data"][@"authToken"];
                }
                [self showBookingWebView:responseobject[@"data"][@"html"]];
            }
        }
    }
                                  failureBlock: ^(NSError *error) {
                                      [SVProgressHUD dismiss];
                                      
                                      UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Booking Failed" message:@"An error occured. Please try again." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                      [av show];
                                  }];
    
}

- (NSDictionary *)preparePostDictionaty
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    [dict setObject:[FMUser defaultUser].phoneNumber forKey:@"phoneNumber"];
    [dict setObject:[FMUser defaultUser].email forKey:@"Email"];
    
    NSMutableArray *passInfo = [[NSMutableArray alloc] init];
    
    for (Passengers *pass in self.flightSearchItem.travellers.passengersInfo)
    {
        [passInfo addObject:[pass getPassengersDetail]];
    }
    
    [dict setObject:passInfo forKey:@"person"];
    
    return dict;
}

- (void)validateData
{
    if (![self validatePassengersInfo])
    {
        [self showErrorMessage:[NSString stringWithFormat:@"Please Enter %@", self.title]];
    }
    else if (![[FMUser defaultUser].phoneNumber isValidPhoneNumber])
    {
        [self showErrorMessage:@"Please Enter valid Phone number"];
    }
    else if (![[FMUser defaultUser].email isValidEmail])
    {
        [self showErrorMessage:@"Please Enter valid Email"];
    }
    else
    {
        if ([[FMUser defaultUser] isSessionAlive])//([FMUser defaultUser].authToken)
        {
            [self postBookingDataWithUser:YES];
        }
        else
        {
            [self showAlertView];
        }
    }
}

- (BOOL)validatePassengersInfo
{
    if (self.flightSearchItem.travellers.passengersInfo.count == 0)
    {
        return false;
    }
    
    for (Passengers *pass in self.flightSearchItem.travellers.passengersInfo)
    {
        if (![pass isPassengerInfoAvaialble])
        {
            return false;
        }
    }
    return true;
}

- (void)postDataAfterPayment:(BOOL)isCorporateUser
{
    [SVProgressHUD show];
    
    TransactionBookingPaidFlight *transaction1 =  (TransactionBookingPaidFlight *)[TransactionBookingManager shared].currentTransaction;
    
    DLog(@"%@", transaction1.adults);
    DLog(@"%@", transaction1.children);
    DLog(@"%@", transaction1.infants);
    
    
    TransactionBookingPaid *transaction =  [TransactionBookingManager shared].currentTransaction;
    
    
    NSDictionary *dictionary;//TransactionBookingPaidFlight
    
    if (isCorporateUser)
    {
        DLog(@"%@", [transaction postPaymentDictionaryCorporate]);
        dictionary = [transaction postPaymentDictionaryCorporate];
    }
    else
    {
        DLog(@"%@", [transaction postPaymentDictionary]);
        dictionary = [transaction postPaymentDictionary];
    }
    
    [FlightDataApiManager uploadPaidBookingDetail:dictionary success: ^(NSDictionary *responseobject) {
        DLog(@"completed flight data upload");
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
            
            NSString *velocityCheckMessage = [[responseobject objectForKey:@"data"] valueForKey:@"velocityCheckMessage"];
            NSString *errorMessage = [[responseobject objectForKey:@"data"] valueForKey:@"errorMessage"];
            NSString *bookingID = [[responseobject objectForKey:@"data"] valueForKey:@"bookingID"];
            NSString *bookingStatus = [[responseobject objectForKey:@"data"] valueForKey:@"bookingStatus"];
            NSString *transactionId = [[responseobject objectForKey:@"data"] valueForKey:@"paymentTransactionId"];
            NSString *transactionStatus = [[responseobject objectForKey:@"data"] valueForKey:@"paymentTransactionStatus"];
            
            NSString *messageTitle = @"Booking Failed";
            NSString *messageDetail = @"Please try again later.";
            
            id alertDelegate = nil;
            
            if (errorMessage)
            {
                messageTitle = @"Booking Failed";
                messageDetail = errorMessage;
            }
            else if (velocityCheckMessage)
            {
                messageTitle = @"Booking Failed";
                messageDetail = velocityCheckMessage;
            }
            else
            {
                messageTitle = @"Booking Status";
                messageDetail = [NSString stringWithFormat:@"Booking Status: %@\nBooking ID: %@\nTransaction Status: %@\nTransaction ID: %@", bookingStatus, bookingID, transactionStatus, transactionId ? : @"Corporate User"];
                
                //[self.navigationController popToRootViewControllerAnimated:YES];
                alertDelegate = self;
            }
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:messageTitle
                                                                message:messageDetail
                                                               delegate:alertDelegate
                                                      cancelButtonTitle:@"Dismiss"
                                                      otherButtonTitles:nil];
            alertView.tag = 707;
            
            [alertView show];
        });
    }
                                     failureBlock: ^(NSError *error) {
                                         [SVProgressHUD dismiss];
                                         
                                         UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Booking" message:@"Booking failed" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                         [av show];
                                     }];
}

- (void)fmWebViewDidRedirectedToPaymentURL
{
    DLog(@"fmWebViewDidRedirectedToPaymentURL");
    [self postDataAfterPayment:NO];
}

#pragma mark - NewUSerBooking

-(void)userEnteredEmailAddress:(NSString *)email
{
    if ([[FMUser defaultUser] isSessionAlive])//([FMUser defaultUser].authToken)
        return;

    emailResponse = nil;
    [self checkUserExistance];
}
- (void)checkUserExistance
{
    if (emailResponse) {
        [self showAlertView];
    }
    else{
        [SVProgressHUD show];
        
        DLog(@"%@", [transaction prePaymentDictionary]);
        
        [PaymentApiManager prepareNewUSerForBooking:[FMUser defaultUser].email success: ^(id responseobject) {
            [SVProgressHUD dismiss];
            
            DLog(@"%@", responseobject);
            
            if (responseobject)
            {
                emailResponse = responseobject[@"data"];
                
                if ([(responseobject[@"data"][@"isEmailExists"]) boolValue])
                {
                    [FMUser defaultUser].phoneNumber = responseobject[@"data"][@"data"][@"phone"];
                    [self.tableView reloadData];
                }
                else
                {
                    
                }
            }
        }
                                       failureBlock: ^(NSError *error) {
                                           [SVProgressHUD dismiss];
                                           
                                       }];
    }
}

-(void)showAlertView
{
    if (!emailResponse) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Opps" message:@"Something went Wrong please try later!" preferredStyle:UIAlertControllerStyleAlert];
        
        [alertController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            //textField.text = @"";
        }]];
    }
    else{
        NSString *message ;
        if ([(emailResponse[@"isEmailExists"]) boolValue])
        {
            message  = @"Your email Exists! you will be proceed to payment by using existing registed Data against this information";
        }
        else
        {
            message  = @"Your account will be created by provided information";
        }
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Alert" message:message preferredStyle:UIAlertControllerStyleAlert];
        
        [alertController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [self postBookingDataWithUser:false];
        }]];
        [self.navigationController presentViewController:alertController animated:YES completion:nil];
    }
}
#pragma AlertViewDelegate

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag == 707){
        [self showBookingsViewOnSuccess];
    }
}

#pragma BookingView

- (void)showBookingsViewOnSuccess
{    
    UINavigationController *navigationControlller = [[UINavigationController alloc] initWithNavigationBarClass:[FMNavigationBar class] toolbarClass:nil];
    [navigationControlller setViewControllers:@[[self.storyboard instantiateViewControllerWithIdentifier:@"BookingListingsViewController"]]];
    [self.sideMenuViewController setContentViewController:navigationControlller
                                                 animated:YES];
    [self.sideMenuViewController hideMenuViewController];
    
}

@end
