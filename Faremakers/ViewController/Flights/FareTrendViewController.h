//
//  FareTrendViewController.h
//  Chart
//
//  Created by Hipolito Arias on 1/6/15.
//  Copyright (c) 2015 Hipolito Arias. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HACBarChart.h"

@protocol FareTrendViewControllerDelegate <NSObject>

-(void) fareTrendViewControllerDidDissmissed:(id)sender;
-(void) fareTrendViewControllerDidDissmissedWithDate:(NSDate*)date;

@end

@class FlightSearchItem;
@class AirLinePricedItinerary;

@interface FareTrendViewController : UIViewController <UICollectionViewDataSource,UICollectionViewDelegate>

@property (strong, nonatomic) AirLinePricedItinerary *airLineItem;
@property (weak, nonatomic) IBOutlet UIView *dateView;
@property (weak, nonatomic) IBOutlet UIView *toolbarHeader;
@property (weak, nonatomic) IBOutlet UILabel *travellLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityView;
@property (weak, nonatomic)id <FareTrendViewControllerDelegate> adelegate;
@property (strong, nonatomic) FlightSearchItem *searchItem;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *flexibleBarItem;


- (IBAction)DoneButtonPressed:(id)sender;


@end

