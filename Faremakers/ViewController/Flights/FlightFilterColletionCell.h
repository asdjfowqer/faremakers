//
//  FlightFilterColletionCell.h
//  Faremakers
//
//  Created by Mac1 on 17/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FlightFilterItem;

@interface FlightFilterColletionCell : UICollectionViewCell

@property (nonatomic) FlightFilterItem *item;
@property (nonatomic , weak) IBOutlet UILabel *TitleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *ImageView;

-(void)loadCellWithItem:(id)item atIndexPath:(NSIndexPath*)indexPath;

@end
