//
//  FlightSearchViewController.h
//  Faremakers
//
//  Created by Mac1 on 16/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BorderButton;

@interface FlightSearchViewController : UIViewController <UITableViewDataSource,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)searchButtonSelector:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *originLabel;
@property (weak, nonatomic) IBOutlet UILabel *destLabel;
@property (weak, nonatomic) IBOutlet BorderButton *originButton;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentController;
@property (weak, nonatomic) IBOutlet BorderButton *destAbbrButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint* searchBtnBottomMargin;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *originBtnTopMargin;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableTopMargin;

- (IBAction)departureBtnSelector:(id)sender;
- (IBAction)arrivalBtnSelector:(id)sender;
- (IBAction)tripSegmentValueChange:(id)sender;

@end
