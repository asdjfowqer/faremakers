//
//  FlightFilterColletionCell.h
//  Faremakers
//
//  Created by Mac1 on 17/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "FlightFilterColletionCell.h"
#import "FlightFilterItem.h"

@implementation FlightFilterColletionCell

- (void)awakeFromNib
{
    [self.TitleLabel changeFontFamily];
}
-(void)loadCellWithItem:(FlightFilterItem*)item atIndexPath:(NSIndexPath*)indexPath
{
    NSArray *images = @[@"morning_icn",@"afternoon_icn", @"evening_icn", @"night_icn"];
    NSArray *sImages = @[@"morning_icn",@"morning_icon", @"evening_icn", @"night_icn"];
    NSArray *arrayContent = @[@"Before 11AM",@"11AM-5PM", @"5PM-9PM", @"After 9PM"];
    
    _TitleLabel.text = arrayContent[indexPath.row];
    
    if (item.departureTime == indexPath.row)
    {
        _TitleLabel.textColor = [UIColor blueColor];
        _ImageView.image =[UIImage imageNamed:sImages[indexPath.row]];
    }
    else
    {
        _TitleLabel.textColor = [UIColor lightGrayColor];
        _ImageView.image =[UIImage imageNamed:images[indexPath.row]];
        
    }
    
    
}

-(void)loadStarCollectionCell
{
    
}
-(void)loadPriceCollectionCell
{
    
}

@end
