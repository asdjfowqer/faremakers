//
//  AirRulesViewController.m
//  Faremakers
//
//  Created by Mac1 on 03/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "AirRulesViewController.h"
#import "FlightRulesApiManger.h"
#import "AirRulesCell.h"
#import "AirLineRules.h"
#import "Constants.h"

@interface AirRulesViewController ()<FlightRulesApiMangerDelegate>
{
    FlightRulesApiManger *flightDataManager;
    NSMutableArray *requestArray;
    NSMutableArray *tmprequestArray;

}

@end

@implementation AirRulesViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.rulesList = [[NSMutableArray alloc] init];
    self.title = @"Fare Rules";
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 160.0;
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.rulesList.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 15;
}
- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSDictionary *dit = requestArray[section];
    NSString *str = [NSString stringWithFormat:@"Fare Rules %@ - %@",[dit objectForKey:@"OriginLocation"],[dit objectForKey:@"DestinationLocation"]];
    return str;
}
- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    if([view isKindOfClass:[UITableViewHeaderFooterView class]]){
        
        UITableViewHeaderFooterView *tableViewHeaderFooterView = (UITableViewHeaderFooterView *) view;
        tableViewHeaderFooterView.textLabel.textColor = [UIColor colorWithRed:24.0/255 green:74.0/255 blue:150.0/255 alpha:1];
        tableViewHeaderFooterView.textLabel.font = [UIFont fontWithName:KCustomeRagularFont size:12.0];
        CGRect frame = tableViewHeaderFooterView.textLabel.frame;
        frame = CGRectOffset( frame, -100,0);
        tableViewHeaderFooterView.textLabel.frame = frame;
    }
}
- (IBAction)crossButtonPressed:(id)sender
{
    [self.adelegate airRulesViewControllerDidDissmissed:sender];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)searchFlightDataWithDictionary:(NSDictionary*)dict
{
    
}
- (void)searchFlightDataWithArray:(NSMutableArray*)array
{
    requestArray = [array mutableCopy];
    flightDataManager = [[FlightRulesApiManger alloc] init];
    flightDataManager.adelegate = self;
    [flightDataManager getFlightRulesDataWithRequestData:[array firstObject]];
    [array removeObject:[array firstObject]];
    tmprequestArray = [array mutableCopy];
}

-(void) checkForMoreRules
{
    if (tmprequestArray.count)
    {
        [flightDataManager getFlightRulesDataWithRequestData:[tmprequestArray firstObject]];
        [tmprequestArray removeObject:[tmprequestArray firstObject]];
    }
    else
    {
        [self.activityView stopAnimating];
        if (!self.rulesList.count)
        {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"No Rules Found!" preferredStyle:UIAlertControllerStyleAlert];
            
            [alertController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                [self crossButtonPressed:nil];
            }]];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }
}
-(void) flightRulesApiMangerRecieveResponse:(id)object
{
    [self.rulesList addObject:[[NSArray alloc] initWithArray:object]];
    [self.tableView reloadData];
    [self checkForMoreRules];
}
-(void) flightRulesApiMangerDidFail:(id)object
{
    [self checkForMoreRules];

}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *array = self.rulesList[section];
    return array.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return [self prepareCellForTableView:tableView forIndexPath:indexPath];
}
- (id)prepareCellForTableView:(UITableView*)tableView forIndexPath:(NSIndexPath*)indexPath
{
    AirRulesCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AirRulesCell"];
    NSArray *array = self.rulesList[indexPath.section];
    AirLineRules *rules = array[indexPath.row];
    cell.rulesTitleLabel.text = rules.ruleTitle;
    cell.rulesDetailLabel.text = rules.ruleText;
   // [cell setBackgroundColor:[UIColor clearColor]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (IBAction)continueBtnSelector:(id)sender
{
}

@end
