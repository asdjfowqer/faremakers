//
//  FlightListingsDetailCell.h
//  Faremakers
//
//  Created by Mac1 on 23/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainColloctionViewDelegate.h"

@class AirItinerary, AirLinePricedItinerary;

@interface FlightListingsDetailCell : UITableViewCell

@property (strong, nonatomic) AirItinerary *airLineItem;
@property (weak, nonatomic) IBOutlet UILabel *originTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *destTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalTravellTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *numberOfStopsLabel;
@property (nonatomic,weak)id<MainColloctionViewDelegate> aDelegate;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *refundableImageWidth;

@property (weak, nonatomic) IBOutlet UICollectionView *stopsCollectionView;

- (void)setAirLineItem:(AirLinePricedItinerary *)airLineItem withTripIndex:(NSInteger)index;

@end
