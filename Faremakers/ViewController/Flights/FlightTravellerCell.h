//
//  FlightTravellerCell.h
//  Faremakers
//
//  Created by Mac1 on 30/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//



@class FlightSearchItem;

@protocol FlightTravellerCellDelegate <NSObject>

-(void)userEnteredPromocode:(NSString*)promocode;
-(void)userEnteredEmailAddress:(NSString*)email;

@end


@interface FlightTravellerCell : UITableViewCell <UITextFieldDelegate>

@property (nonatomic) FlightSearchItem *flightSearchItem;

#pragma mark FlightTravellerCell0
@property (weak, nonatomic) IBOutlet UIImageView *ImageView;
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;

#pragma mark FlightTravellerCell1
@property (weak, nonatomic) IBOutlet UIImageView *itemImageView;
@property (weak, nonatomic) IBOutlet UITextField *detailText;

@property (nonatomic,weak) id <FlightTravellerCellDelegate> delegate;

- (void)loadCellWithIdentifier:(NSString *)cellIdentifier forRowCell:(NSInteger)row;

@end
