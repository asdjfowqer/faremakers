//
//  BaggageDetailViewCell.h
//  Faremakers
//
//  Created by Mac1 on 06/10/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaggageDetailViewCell: UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *flightTitle;
@property (strong, nonatomic) IBOutlet UILabel *checkInBaggage;
@property (strong, nonatomic) IBOutlet UILabel *cabinBaggage;

@end
