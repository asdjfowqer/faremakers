//
//  BaggageDetailViewController.h
//  Faremakers
//
//  Created by Mac1 on 06/10/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AirItinerary;
@class OriginDestination;

@interface BaggageDetailViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (retain, nonatomic) OriginDestination *airLineItem;

@end
