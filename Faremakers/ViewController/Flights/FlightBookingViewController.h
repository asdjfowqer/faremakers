//
//  FlightBookingViewController.h
//  Faremakers
//
//  Created by Mac1 on 29/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AirItinerary;
@class AirItineraryPricingInfo;
@class FlightSearchItem;
@class AirLinePricedItinerary;


@interface FlightBookingViewController : UIViewController <UITableViewDataSource , UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (copy, nonatomic) AirLinePricedItinerary *airPricedItinerar;
@property (copy, nonatomic) AirItinerary *airLineItem;
@property (copy, nonatomic) AirItineraryPricingInfo *airPricingInfoItem;
@property (weak, nonatomic) FlightSearchItem *flightSearchItem;

- (IBAction)continueBtnSelector:(id)sender;


@end
