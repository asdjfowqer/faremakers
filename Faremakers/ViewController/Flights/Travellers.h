//
//  Travellers.h
//  Faremakers
//
//  Created by Mac1 on 29/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Passengers : NSObject

@property (nonatomic) NSString* title;
@property (nonatomic) NSString* fName;
@property (nonatomic) NSString* lName;
@property (nonatomic) NSString* dob;
@property (nonatomic) NSString* type;

@property (nonatomic,strong) NSDate* dobUser;

-(BOOL)isPassengerInfoAvaialble;
-(NSString*) convertIntoString;
-(NSDictionary*)getPassengersDetail;
-(NSDictionary*)getPassengersDetailForFlightTransaction;

@end

@interface Travellers : NSObject

@property (nonatomic) NSInteger Adults;
@property (nonatomic) NSInteger Childs;
@property (nonatomic) NSInteger Infants;
@property (nonatomic) NSMutableArray *passengersInfo;

-(id)initWithNumberOfTravellers:(NSInteger)adults;

-(NSInteger)totalTravellers;
- (NSArray *)getPassengersWithType:(NSString *)type;

@end
