//
//  LayOverCollectionCell.h
//  Faremakers
//
//  Created by Mac1 on 23/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LayOverCollectionCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *layOverTimeLabel;

@end
