//
//  AirRulesViewController.h
//  Faremakers
//
//  Created by Mac1 on 03/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AirRulesViewControllerDelegate <NSObject>

-(void) airRulesViewControllerDidDissmissed:(id)sender;

@end


@class BorderButton;

@interface AirRulesViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic)  NSMutableArray *rulesList;

@property (weak, nonatomic)id <AirRulesViewControllerDelegate> adelegate;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityView;

@property (weak, nonatomic) IBOutlet BorderButton *crossButton;

- (IBAction)crossButtonPressed:(id)sender;
- (void)searchFlightDataWithDictionary:(NSDictionary*)dict;
- (void)searchFlightDataWithArray:(NSMutableArray*)array;

@end
