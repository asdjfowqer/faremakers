//
//  FlightBookingCell.h
//  Faremakers
//
//  Created by Mac1 on 30/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//



@class AirItineraryPricingInfo;

@interface FlightBookingCell : UITableViewCell

@property (weak, nonatomic) AirItineraryPricingInfo *pricingItem;
@property (nonatomic) NSInteger TripType;

#pragma mark FlightBookingCell0
@property (weak, nonatomic) IBOutlet UILabel *PoliciesLabel;


#pragma mark FlightBookingCell1
@property (weak, nonatomic) IBOutlet UILabel *chargesDetailLabel;
@property (weak, nonatomic) IBOutlet UILabel *chargesCostLabel;

#pragma  mark myIdentifier

@property (weak, nonatomic) IBOutlet UILabel *grandTotalLabel;
@property (weak, nonatomic) IBOutlet UILabel *grandTotalPrice;

- (void)setPricingItem:(AirItineraryPricingInfo *)pricingItem withIdentifier:(NSString*)cellIdentifier forRowCell:(NSInteger)row;
@end
