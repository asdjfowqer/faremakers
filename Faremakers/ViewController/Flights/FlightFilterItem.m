//
//  FlightFilterItem.m
//  Faremakers
//
//  Created by Mac1 on 20/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "FlightFilterItem.h"
#import "Constants.h"

@implementation FlightFilterItem

-(id)init
{
    self = [super init];
    _airLines = [[NSMutableArray alloc] init];
    _totalStops = nil;
    _departureTime = KNon;
    _flightDuration = 0;
    _layoverDuration = 0;
    
    return self;
}

- (id)copyWithZone:(NSZone *)zone
{
    FlightFilterItem *copy = [[[self class] allocWithZone:zone] init];
    if (copy) {
        copy.airLines = [[NSMutableArray alloc] initWithArray:self.airLines];
        copy.totalStops = [self.totalStops copy];
        copy.nonRefunded = self.nonRefunded;
        copy.nonStop = self.nonStop;
        copy.departureTime = self.departureTime;
        copy.flightDuration = self.flightDuration;
        copy.layoverDuration = self.layoverDuration;
    }
    return copy;
}
- (NSArray*)converDepartureTimeToDates
{
//    NSArray *dates ;
//    NSDate *today = [NSDate date];
//    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
//    [calendar setLocale:[NSLocale currentLocale]];
//    //[calendar setTimeZone:[NSTimeZone timeZoneWithName:@"US/Central"]];
//    
//    NSDateComponents *nowComponents = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:today];
//    today = [calendar dateFromComponents:nowComponents];
//    
//    NSDictionary *dictonary = [[NSDictionary alloc] initWithObjectsAndKeys:KDayTimingIntervals, nil];
//    NSString *str = [dictonary objectForKey:timings[0]];
//    NSTimeInterval startInterval = str.doubleValue;
//    str = [dictonary objectForKey:timings[1]];
//    
//    NSTimeInterval endInterval = [dictonary objectForKey:timings[1]];
    
    
    
    if (self.departureTime == KMorning)
    {
        return [[NSArray alloc] initWithObjects:KDayTimingIntervals[0],KDayTimingIntervals[1], nil];
    }
    else if (self.departureTime == KAfterNoon)
    {
        return [[NSArray alloc] initWithObjects:KDayTimingIntervals[1],KDayTimingIntervals[2], nil];
    }
    else if (self.departureTime == KEvening)
    {
        return [[NSArray alloc] initWithObjects:KDayTimingIntervals[2],KDayTimingIntervals[3], nil];
    }
    else if (self.departureTime == KNight)
    {
        return [[NSArray alloc] initWithObjects:KDayTimingIntervals[3],KDayTimingIntervals[4], nil];
    }

    return nil;
}
@end
