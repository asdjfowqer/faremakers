//
//  HomeViewController.m
//  Faremakers
//
//  Created by Mac1 on 18/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "FlightDataApiManager.h"
#import <ASProgressPopUpView/ASProgressPopUpView.h>
#import "FlightListingsViewController.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "FlightBookingViewController.h"
#import "FlightListingsViewDelegate.h"
#import "AirItineraryPricingInfo.h"
#import <MRProgress/MRProgress.h>
#import "NSDate+DateComponents.h"
#import "ListingsSegmentHeaders.h"
#import "AirLinePricedItinerary.h"
#import "HomeViewController.h"
#import "FlightSearchItem.h"
#import "FlightFilterItem.h"
#import "AirItinTotalFare.h"
#import "AirBaseFare.h"
#import "AirCustomeData.h"
#import "UIView+LayerShot.h"
#import "DoubleDateItem.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "AirLines.h"
#import "CarbonKit.h"

@interface HomeViewController () <CarbonTabSwipeNavigationDelegate,SearchFlightDataDelegate,FlightListingsViewDelegate>
{
    NSArray *items;
    NSArray *viewControllers;
    FlightDataApiManager *flightDataManager;
    CarbonTabSwipeNavigation *carbonTabSwipeNavigation;
    UIActivityIndicatorView *activityIndicator;
    float progress;
}
@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Flights";
    
    self.progressView1.popUpViewAnimatedColors = @[[UIColor redColor], [UIColor orangeColor], [UIColor greenColor]];
    
    [self showSegmentBarHeaderView];
    
    if (_searchItem.is_roundTrip) {
        
        FlightListingsViewController *viewController1 = [self.storyboard instantiateViewControllerWithIdentifier:@"FlightListingsViewController1"];
        viewController1.OneWayIndex = 0;
        viewController1.adelegate = self;
        viewController1.searchItem = self.searchItem;
        FlightListingsViewController *viewController2 = [self.storyboard instantiateViewControllerWithIdentifier:@"FlightListingsViewController1"];
        viewController2.OneWayIndex = 1;
        viewController2.adelegate = self;
        viewController2.searchItem = self.searchItem;
        
        
        viewControllers = @[viewController1,viewController2];
    }
    else
    {
        FlightListingsViewController *viewController1 = [self.storyboard instantiateViewControllerWithIdentifier:@"FlightListingsViewController1"];
        viewController1.OneWayIndex = 0;
        viewController1.adelegate = self;
        viewController1.searchItem = self.searchItem;
        viewControllers = @[viewController1];
    }
    carbonTabSwipeNavigation = [[CarbonTabSwipeNavigation alloc] initWithItems:items delegate:self];
    [carbonTabSwipeNavigation insertIntoRootViewController:self];
    
   // [[MRNavigationBarProgressView progressViewForNavigationController:self.navigationController] setProgress:0.01 animated:YES];
    
    [self style];
    [self ShowActivityView];
    [self.view bringSubviewToFront:self.progressView1];
    [self updateNavigationBarTitle];
}
-(void)updateNavigationBarTitle
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    label.backgroundColor = [UIColor clearColor];
    label.numberOfLines = 2;
    label.font = [UIFont fontWithName:KCustomeRagularFont size:14.0];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor blackColor];
    label.text = [NSString stringWithFormat:@"%@ %@ %@ \n %@ - %@",[self.searchItem.doubleDate.depDate getDayOfTheWeek],[self.searchItem.doubleDate.depDate getDayOfTheMonth],[self.searchItem.doubleDate.depDate getMonth],self.searchItem.origin ,self.searchItem.Destination];
    [label sizeToFit];
    self.navigationItem.titleView = label;
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    progress = 0.0;
}
-(void)searchFlightDataWithDictionary:(NSDictionary*)dict
{
    flightDataManager = [[FlightDataApiManager alloc] init];
    flightDataManager.adelegate = self;
    [flightDataManager getFlightListingDataWithRequestData:dict withProgressView:self.progressView1];
}
-(void)getAirLineDataFromAPI
{
    [flightDataManager getAirLineDataFromAPI];
}

-(void)updateProgressBar
{
    //[self.progressView1 setProgress:progress animated:NO];
}
#pragma mark FlightDataManager Delegate

-(void) flightDataManagerStartProgressing:(float)prog;
{
    progress = (prog > progress)?prog:progress;
    
    dispatch_async(dispatch_get_main_queue(), ^(void){
      //  [[MRNavigationBarProgressView progressViewForNavigationController:self.navigationController] setProgress:progress animated:YES];
    });
    
}

-(void) flightDataManagerDidRecieveResponse:(id)object
{
    [self.progressView1 setHidden:YES];
	
	DLog(@"%@",object);
	
    _flightSearchData = object;
    
    FlightListingsViewController *viewController1 = viewControllers[0];
    viewController1.flightSearchData = _flightSearchData;
    
    if (_searchItem.is_roundTrip)
    {
        FlightListingsViewController *viewController2 = viewControllers[1];
        viewController2.flightSearchData = _flightSearchData;
    }
    [self ShowActivityView];
    
}
-(void) flightDataManagerDidFail:(id)object
{
    [self.progressView1 setHidden:YES];
    [self ShowActivityView];

    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"No Data Found!" preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [self.navigationController popViewControllerAnimated:YES];
    }]];
    [self.navigationController presentViewController:alertController animated:YES completion:nil];
    
}

- (void)style {
    
    UIColor *color = [UIColor colorWithRed:255.0/255 green:255.0/255 blue:255.0/255 alpha:1];
    carbonTabSwipeNavigation.toolbar.translucent = NO;
    [carbonTabSwipeNavigation setIndicatorColor:color];
    [carbonTabSwipeNavigation setTabExtraWidth:30];
    
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:151.f forSegmentAtIndex:0];
    if (items.count >1)
        [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:151.f forSegmentAtIndex:1];
    [carbonTabSwipeNavigation.carbonTabSwipeScrollView setBackgroundColor:[UIColor colorWithRed:24.0/255 green:74.0/255 blue:150.0/255 alpha:1]];
    
    // Custimize segmented control
    [carbonTabSwipeNavigation setNormalColor:[UIColor colorWithRed:255.0/255 green:255.0/255 blue:255.0/255 alpha:0.5]
                                        font:[UIFont fontWithName:KCustomeFont size:14]];
    [carbonTabSwipeNavigation setSelectedColor:color
                                          font:[UIFont fontWithName:KCustomeFont size:14]];
    [carbonTabSwipeNavigation.carbonTabSwipeScrollView setScrollEnabled:false];
    
    
}

# pragma mark - CarbonTabSwipeNavigation Delegate
// required
- (nonnull UIViewController *)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbontTabSwipeNavigation
                                 viewControllerAtIndex:(NSUInteger)index {
    return viewControllers[index];
}

// optional
- (void)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
                 willMoveAtIndex:(NSUInteger)index {
    
}

- (void)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
                  didMoveAtIndex:(NSUInteger)index {
    DLog(@"Did move at index: %ld", (unsigned long)index);
}

- (UIBarPosition)barPositionForCarbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation {
    return UIBarPositionTop; // default UIBarPositionTop
}
-(void)ShowActivityView
{
    UIView *view = [self.view viewWithTag:707];
    if (view)
    {
        [view removeFromSuperview];
        [activityIndicator stopAnimating];
        [[AppDelegate sharedDelegate] hideLoadingViewForView:self];

    }
    else
    {
        view = [[UIView alloc] initWithFrame:self.view.bounds];
        view.tag = 707;
        view.alpha = 0.5;

        [view setBackgroundColor:[UIColor whiteColor]];
        [self.view addSubview:view];
        [self.view bringSubviewToFront:view];
        [[AppDelegate sharedDelegate] showLoadingViewForView:self withText:@"Searching Flights..."];
    }
}
- (void)barButtonWithActivityIndicator
{
    activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    [activityIndicator hidesWhenStopped];
    UIBarButtonItem *activityItem = [[UIBarButtonItem alloc] initWithCustomView:activityIndicator];
    self.navigationItem.rightBarButtonItem = activityItem;
}
-(void)showSegmentBarHeaderView
{
    ListingsSegmentHeaders *goingHeaderView = [[[NSBundle mainBundle] loadNibNamed:@"ListingsSegmentHeaders" owner:self options:nil] lastObject];
    
    [goingHeaderView setDepartureDate:self.searchItem.doubleDate.depDate];
    
    UIImage *goingimage = goingHeaderView.imageFromLayer;
    
    if (self.searchItem.is_roundTrip) {
        
        ListingsSegmentHeaders *returnHeaderView = [[[NSBundle mainBundle] loadNibNamed:@"ListingsSegmentHeaders" owner:self options:nil] lastObject];
        [returnHeaderView setDepartureDate:self.searchItem.doubleDate.retDate];
        [returnHeaderView changeDepartureTitle:YES];
        UIImage *returnimage = returnHeaderView.imageFromLayer;
        
        items = @[goingimage,returnimage];
        
    }
    else
    {
        items = @[goingimage];
        
    }
    
}

#pragma mark FLightFiltringSorting Deletgates

- (void) flightPageRemoveAppliedSortingWithRoundTrip:(NSInteger)roundTrip
{
    if (_searchItem.is_roundTrip)
    {
        FlightListingsViewController *viewController2 = viewControllers[1];
        viewController2.flightSearchData = _flightSearchData;
    }
    else
    {
        FlightListingsViewController *viewController1 = viewControllers[0];
        viewController1.flightSearchData = _flightSearchData;
    }
}
- (void) FlightPageSortFlightDataCalledWithSortKey:(NSString*)key isRoundTrip:(NSInteger)roundTrip
{
    NSArray *sortedArray;
    if ([key isEqualToString:@"departureDateTime"])
    {
        
        sortedArray = [_flightSearchData sortedArrayUsingComparator:^NSComparisonResult(AirLinePricedItinerary *first, AirLinePricedItinerary *second)
                       {
                           // ...
                           AirCustomeData *data1 = first.customeArray[roundTrip];
                           AirCustomeData *data2 = second.customeArray[roundTrip];
                           
                           return [[data1 valueForKey:key] compare:[data2 valueForKey:key] ];
                       }];
    }
    else  if ([key isEqualToString:@"duration"])
    {
        
        
        sortedArray = [_flightSearchData sortedArrayUsingComparator:^NSComparisonResult(AirLinePricedItinerary *first, AirLinePricedItinerary *second)
                       {
                           // ...
                           AirCustomeData *data1 = first.customeArray[roundTrip];
                           AirCustomeData *data2 = second.customeArray[roundTrip];
                           
                           NSTimeInterval interval1 = [data1 converDurationToInterval];
                           NSTimeInterval interval2 = [data2 converDurationToInterval];
                           
                           if ( interval1 < interval2) {
                               
                               return (NSComparisonResult)NSOrderedAscending;
                               
                           }
                           else if (interval1 > interval2) {
                               
                               return (NSComparisonResult)NSOrderedDescending;
                               
                           }
                           return (NSComparisonResult)NSOrderedSame;
                           
                           //return [[data1 valueForKey:key] compare:[data2 valueForKey:key] ];
                       }];
    }
    else if ([key isEqualToString:@"price"])
    {
        
        sortedArray = [_flightSearchData sortedArrayUsingComparator:^NSComparisonResult(AirLinePricedItinerary *first, AirLinePricedItinerary *second)
                       {
                           
                           AirItineraryPricingInfo *pricingInfo1 = [first.airItineraryPricingInfo objectAtIndex:0];
                           AirItineraryPricingInfo *pricingInfo2 = [second.airItineraryPricingInfo objectAtIndex:0];
                           
                           return [pricingInfo1.itinTotalFare.totalFare.amount compare:pricingInfo2.itinTotalFare.totalFare.amount ];
                       }];
    }
    
    if (_searchItem.is_roundTrip)
    {
        FlightListingsViewController *viewController2 = viewControllers[1];
        viewController2.flightSearchData = [[NSArray alloc] initWithArray:sortedArray];
    }
    else
    {
        FlightListingsViewController *viewController1 = viewControllers[0];
        viewController1.flightSearchData = [[NSArray alloc] initWithArray:sortedArray];
    }
}
- (void) filterFlightDataWithFilterItem:(id)filterItem isRoundTrip:(NSInteger)roundTrip;
{
    
    [SVProgressHUD showWithStatus:@"Applying Filters"];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        FlightFilterItem *filter = (FlightFilterItem*)filterItem;
        NSMutableArray *array = [[NSMutableArray alloc] init];
        BOOL filterApplied = false;
        
        if (filter.nonStop)
        {
            [_flightSearchData enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop)
             {
                 AirLinePricedItinerary *airlinePriced = (AirLinePricedItinerary*)obj;
                 AirCustomeData *customAirData = airlinePriced.customeArray[roundTrip];
                 if (customAirData.totalStops.integerValue == 0)
                 {
                     [array addObject:airlinePriced];
                 }
             }];
            filterApplied = true;
        }
        if (filter.nonRefunded)
        {
            array = [[NSMutableArray alloc] initWithArray:[AirLinePricedItinerary filterNonRefundableFlight:(filterApplied)?array:[_flightSearchData mutableCopy]]];
            filterApplied = true;
            
        }
        if (filter.airLines.count > 0) {
            
            array = [[NSMutableArray alloc] initWithArray:[AirLinePricedItinerary filterFlightData:(filterApplied)?array:[_flightSearchData mutableCopy] forItem:filter.airLines withKey:@"airLine" andIndex:roundTrip]];
            filterApplied = true;
            
        }
        if (filter.departureTime != KNon ) {
            
            NSArray *filteredIntervArray = [filterItem converDepartureTimeToDates];
            
            if (filteredIntervArray.count == 2)
            {
                NSTimeInterval startInterval = [[filteredIntervArray objectAtIndex:0] doubleValue];
                NSTimeInterval endInterval = [[filteredIntervArray objectAtIndex:1] doubleValue];
                
                array = [[NSMutableArray alloc] initWithArray:[AirLinePricedItinerary FilterFlightsArray:(filterApplied)?array:[_flightSearchData mutableCopy] forDayTimingsWithStartInterv:startInterval andEndInterv:endInterval withTripIndex:roundTrip]];
                filterApplied = true;
                
            }
            
        }
        if (filter.flightDuration)//Do this
        {
            array = [[NSMutableArray alloc] initWithArray:[AirLinePricedItinerary filterFlightForDuration:(filterApplied)?array:[_flightSearchData mutableCopy] withDuration:filter.flightDuration withTripIndex:roundTrip]];
            filterApplied = true;
            
        }
        if (filter.layoverDuration)//Do this
        {
            array = [[NSMutableArray alloc] initWithArray:[AirLinePricedItinerary filterFlightForLayoverDuration:(filterApplied)?array:[_flightSearchData mutableCopy] withLayoverDuration:filter.layoverDuration withTripIndex:roundTrip]];
            filterApplied = true;
            
        }
        FlightListingsViewController *viewController1 = viewControllers[roundTrip];
        
        if (filterApplied)
        {
            viewController1.flightSearchData = [[NSArray alloc] initWithArray:array];
        }
        else
            viewController1.flightSearchData = [[NSArray alloc] initWithArray:_flightSearchData];
        
        
        [SVProgressHUD showSuccessWithStatus:@"Done"];
    });
}
- (void) flightPageDidChangeDepartureDate:(NSDate*)departureDate
{
    [self ShowActivityView];
    [[MRNavigationBarProgressView progressViewForNavigationController:self.navigationController] setProgress:0.01 animated:YES];

    [flightDataManager getFlightListingDataWithRequestData:[self.searchItem preparePOstDAtaWithNewDeparDate:departureDate] withProgressView:self.progressView1];
}
- (void)flightPageDidSelectClearAllFilters:(NSInteger)isRound
{
    [SVProgressHUD showWithStatus:@"Clearing All Filters"];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        if (isRound)
        {
            FlightListingsViewController *viewController2 = viewControllers[1];
            viewController2.flightSearchData = _flightSearchData;
        }
        else
        {
            FlightListingsViewController *viewController1 = viewControllers[0];
            viewController1.flightSearchData = _flightSearchData;
        }
        [SVProgressHUD showSuccessWithStatus:@"Done"];

    });
}
-(void)flightlistingsViewController:(id)viewController DidSelectFlight:(id)flightItem forIndexPath:(NSIndexPath *)indexPath
{
    FlightListingsViewController *viewContr = (FlightListingsViewController*)viewController;
    
    if (viewContr.OneWayIndex == 0 && self.searchItem.is_roundTrip)
    {
        FlightListingsViewController *viewController1 = viewControllers[1];
        [viewController1 showDetailRowForRoundTrip:[NSIndexPath indexPathForItem:indexPath.row inSection:indexPath.section]];
        carbonTabSwipeNavigation.carbonSegmentedControl.selectedSegmentIndex = 1;
        [carbonTabSwipeNavigation.carbonSegmentedControl sendActionsForControlEvents:UIControlEventValueChanged];
        [viewController1 scrollTableViewToIndex:indexPath];        
    }
    else
    {
        FlightBookingViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"FlightBookingViewController"];
	    DLog(@"%@",flightItem);
	    DLog(@"%@",self.searchItem);
	    
	    
        viewController.airLineItem = flightItem;
        viewController.flightSearchItem = self.searchItem;
        [viewContr.navigationController pushViewController:viewController animated:YES];
    }
}

-(void) viewWillDisappear:(BOOL)animated
{
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound)
    {
        [self CancleAllTask];
    }
    [super viewWillDisappear:animated];
}
-(void) CancleAllTask
{
    [flightDataManager CancleAllTask];
}
- (NSArray*) getFlightSearchData
{
    return [[NSArray alloc] initWithArray:_flightSearchData];
}
@end
