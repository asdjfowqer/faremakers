//
//  FlightFilterItem.h
//  Faremakers
//
//  Created by Mac1 on 20/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    KMorning,
    KAfterNoon,
    KEvening,
    KNight,
    KNon,
} DayIntervals;

@interface FlightFilterItem : NSObject <NSCopying>

@property (nonatomic) NSMutableArray *airLines;
@property (nonatomic) NSNumber *totalStops;
@property (nonatomic) DayIntervals departureTime;
@property (nonatomic) BOOL nonStop;
@property (nonatomic) BOOL nonRefunded;
@property (nonatomic) NSInteger flightDuration;
@property (nonatomic) NSInteger layoverDuration;


- (NSArray*)converDepartureTimeToDates;

@end
