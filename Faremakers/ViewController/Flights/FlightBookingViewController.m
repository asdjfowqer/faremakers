//
//  FlightBookingViewController.m
//  Faremakers
//
//  Created by Mac1 on 29/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "FlightBookingViewController.h"
#import "BaggageDetailViewController.h"
#import "FlightBookingDetailCell.h"
#import "AirLinePricedItinerary.h"
#import "ListingHeaderView.h"
#import "FlightBookingCell.h"
#import "FlightFilterItem.h"
#import "FlightTravellerInfoController.h"
#import "AirRulesViewController.h"
#import "TransitionDelegate.h"
#import "AirItineraryPricingInfo.h"
#import "AirFareBreakDown.h"
#import "FlightSegment.h"
#import "AirCustomeData.h"

#import "AirItineraryPricingInfo.h"
#import "AirItinTotalFare.h"
#import "AirBaseFare.h"
#import "AirFareTaxes.h"
#import "AirFareBreakDown.h"
#import "FlightSearchItem.h"
#import "DoubleDateItem.h"

@interface FlightBookingViewController () <AirRulesViewControllerDelegate>
{
    NSArray *SectionsData;
    NSArray *DataArray;
    NSArray *flightsDataArray;
    NSInteger noOfSegments;
    NSArray *airLineDataBase;
    
}

@property (nonatomic, strong) TransitionDelegate *transitionController;

@end

@implementation FlightBookingViewController


- (void)setAirLineItem:(AirLinePricedItinerary *)airLineItem
{
    _airPricedItinerar = airLineItem;
    _airLineItem = airLineItem.airItinerary;
    _airPricingInfoItem = [airLineItem.airItineraryPricingInfo objectAtIndex:0];
}

- (void)viewDidLoad
{
    [super viewDidLoad];


    self.title = @"Review Booking";
    SectionsData = @[@"Fare Details"];//,@"Fare Details"];
    flightsDataArray = @[@"a", @"b"];
    [self.tableView registerNib:[UINib nibWithNibName:@"ListingsHeaderView" bundle:nil] forHeaderFooterViewReuseIdentifier:@"ListingHeaderView"];
    DataArray = @[@[@"Baggage", @"Fare Rules"],
                  @[@"Base Fare", @"Surcharges", @"Fee and Texs", @"Grand Total"],
                  @[@"$ 200", @"$ 20", @"$ 35", @"$ 235"]];

}

- (void)addFlightTransaction
{
    TransactionBookingPaidFlight *transaction = [TransactionBookingPaidFlight anInstance];
    transaction.orderInfo = @"Book Flight";
    transaction.orderName = @"Flight";
    transaction.firstName = [FMUser defaultUser].firstName;
    transaction.lastName = [FMUser defaultUser].lastName;
    transaction.phoneNumber = [FMUser defaultUser].phoneNumber;
    transaction.email = [FMUser defaultUser].email;
    
    
    NSNumber *myFirstInteger = [NSNumber numberWithDouble:[_airPricingInfoItem.itinTotalFare.totalFare.amount doubleValue]];
    transaction.currency = _airPricingInfoItem.itinTotalFare.totalFare.currencyCode;
    
    int addThreepercent = [myFirstInteger intValue];
    addThreepercent = addThreepercent * 3/100;
    int bankChargesAdded = addThreepercent + [myFirstInteger intValue];
    
    NSLog(@"total fair nabeel: %d", addThreepercent);
    NSLog(@"total fair nabeel bank charged: %d", bankChargesAdded);
    
    NSNumber *grandTotal;
    grandTotal = [NSNumber numberWithInt:bankChargesAdded];
    
    transaction.amount = grandTotal;
    transaction.currency = _airPricingInfoItem.itinTotalFare.totalFare.currencyCode;

    transaction.commission = _airPricingInfoItem.itinTotalFare.commission.amount;
    transaction.originDestinationOptions = [_airPricedItinerar.airItinerary.originDestinationOptions firstObject].flightSegments;
   
    AirItineraryPricingInfo *pricingInfo = _airPricedItinerar.airItineraryPricingInfo[0];
    transaction.IsRefundable = ![pricingInfo checkNonRefunableFlight];
    NSLog(@"%d",transaction.IsRefundable);
    [TransactionBookingManager shared].currentTransaction = transaction;

    NSLog(@"commission: %@", transaction.commission);
    NSLog(@"phone: %@", transaction.phoneNumber);
    NSLog(@"amount: %@", transaction.amount);
    NSLog(@"total fair: %@", _airPricingInfoItem.itinTotalFare.totalFare.amount);
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _airLineItem.originDestinationOptions.count + SectionsData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section <= _airLineItem.originDestinationOptions.count)
    {
        return 44;
    }
    else
    {
        return 30;
    }
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section < _airLineItem.originDestinationOptions.count)
    {
        ListingHeaderView *header = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"ListingHeaderView"];

        switch (section)
        {
            case 0:
            {
                header.goingLabel.text = @"Going";
            }
                break;

            case 1:
            {
                header.goingLabel.text = @"Return";
            }
                break;

            default:
                break;
        }
        [header showSectionLabel:NO];

        AirCustomeData *customeData = [_airPricedItinerar.customeArray objectAtIndex:section];
        [header setDepartureDate:customeData.departureDateTime];
        header.iconImageView.image = [UIImage imageNamed:@"booking_past_flight"];
        [header setrefundedView:[self.airPricingInfoItem checkNonRefunableFlight]];

        return header;
    }
    else
    {
        ListingHeaderView *header = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"ListingHeaderView"];
        [header showSectionLabel:YES];
        [header.sectionTitleLabel setText:SectionsData[section - _airLineItem.originDestinationOptions.count]];
        return header;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section < _airLineItem.originDestinationOptions.count)
    {
        OriginDestination *originDestinationItem = [_airLineItem.originDestinationOptions objectAtIndex:section];
        return originDestinationItem.flightSegments.count + 4;
    }
    return 3;//base fare,taxes,totalfare
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section < _airLineItem.originDestinationOptions.count)
    {
        OriginDestination *originDetationItem = [_airLineItem.originDestinationOptions objectAtIndex:indexPath.section];

        if (originDetationItem.flightSegments.count > indexPath.row + 1)
        {
            return 250;
        }
        else if (originDetationItem.flightSegments.count == indexPath.row + 1)
        {
            return 220;
        }
        else
        {
            return 55;
        }
    }
    return 55;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self prepareCellForTableView:tableView forIndexPath:indexPath];
}

- (id)prepareCellForTableView:(UITableView *)tableView forIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section < _airLineItem.originDestinationOptions.count)
    {
        OriginDestination *originDetationItem = [_airLineItem.originDestinationOptions objectAtIndex:indexPath.section];


        if (originDetationItem.flightSegments.count > indexPath.row)
        {
            FlightBookingDetailCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"FlightBookingDetailCell" forIndexPath:indexPath];
            [cell setFlightSegmentsArray:_airLineItem.originDestinationOptions[indexPath.section] forRowCell:indexPath.row];
            [cell setCabinData:_airPricingInfoItem];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
        else if (indexPath.row < (originDetationItem.flightSegments.count + 2))
        {
            FlightBookingCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"FlightBookingCell1" forIndexPath:indexPath];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.TripType = indexPath.section;
            [cell setPricingItem:_airPricingInfoItem withIdentifier:@"FlightBookingCell0" forRowCell:(indexPath.row - originDetationItem.flightSegments.count)];
            return cell;
        }
        else
        {
            FlightBookingCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"FlightBookingCell0" forIndexPath:indexPath];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.TripType = indexPath.section;
            [cell setPricingItem:_airPricingInfoItem withIdentifier:@"FlightBookingCell0" forRowCell:(indexPath.row - originDetationItem.flightSegments.count)];
            return cell;
        }
    }
    else
    {
        FlightBookingCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"FlightBookingCell1" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell setPricingItem:_airPricingInfoItem withIdentifier:@"FlightBookingCell1" forRowCell:indexPath.row];
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    if (indexPath.section < _airLineItem.originDestinationOptions.count){
        
        OriginDestination *originDetationItem = [_airLineItem.originDestinationOptions objectAtIndex:indexPath.section];
        
        NSInteger index = originDetationItem.flightSegments.count + 3;
        
        if (indexPath.row == index)
        {
            [self showFareRuleViewController:[self prepareAirRulesPostData:indexPath]];//[self prepareAirRulesPostData:indexPath];
        }
        else if ((indexPath.row + 1) == index){
            [self showBaggageDetailView:indexPath];
        }
    }
    
}
- (void)showBaggageDetailView:(NSIndexPath *)indexPath
{
    BaggageDetailViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BaggageDetailViewController"];
    viewController.airLineItem = _airLineItem.originDestinationOptions[indexPath.section];
    [self.navigationController pushViewController:viewController animated:true];
}
- (void)showFareRuleViewController:(NSMutableArray *)postArray
{
    AirRulesViewController *rulesView = [self.storyboard instantiateViewControllerWithIdentifier:@"AirRulesViewController"];
    rulesView.view.backgroundColor = [UIColor clearColor];
    rulesView.adelegate = self;
    [rulesView setTransitioningDelegate:_transitionController];
    rulesView.modalPresentationStyle = UIModalPresentationCustom;
    [rulesView searchFlightDataWithArray:postArray];
    [self.navigationController presentViewController:rulesView animated:nil completion:nil];//:rulesView animated:YES];
    self.view.alpha = 0.9;
}

- (NSMutableArray *)prepareAirRulesPostData:(NSIndexPath *)indexPath
{
    OriginDestination *originDetationItem = [_airLineItem.originDestinationOptions objectAtIndex:indexPath.section];
    AirFareBreakDown *fareBreakDown = _airPricingInfoItem.ptC_FareBreakdown[0];
    NSMutableArray *postArray = [[NSMutableArray alloc] init];

    int i = 0;

    for (FareBasisCodes *basicCodes in fareBreakDown.fareBasisCodes)
    {
        if (originDetationItem.flightSegments.count <= i)
        {
            break;
        }
        FlightSegment *flightSegment = originDetationItem.flightSegments[i++];
        NSDictionary *dict = [basicCodes prepareDictForPostDataWithDate:[FlightSegment parseDateOnly:flightSegment.departureDateTime] andAirlineCode:flightSegment.operatingAirline.code];
        [postArray addObject:dict];
    }
    if (postArray.count > 1)
    {
        AirCustomeData *airdata = [self.airPricedItinerar.customeArray objectAtIndex:indexPath.section];
        FareBasisCodes *basicCodes = fareBreakDown.fareBasisCodes[fareBreakDown.fareBasisCodes.count - 1];
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:[basicCodes prepareDictForPostDataWithDate:[FlightSegment parseDateOnly:airdata.departureDateTime] andAirlineCode:airdata.airLine]];
        [dict setObject:airdata.DepartureAirport forKey:@"OriginLocation"];
        [postArray addObject:dict];
    }

    return postArray;
}

#pragma mark

- (void)airRulesViewControllerDidDissmissed:(id)sender
{
    self.view.alpha = 1.0;
}

- (IBAction)continueBtnSelector:(id)sender
{
    [self addFlightTransaction];

    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];

    FlightTravellerInfoController *example = [self.storyboard instantiateViewControllerWithIdentifier:@"FlightTravellerInfoController"];
    example.flightSearchItem = self.flightSearchItem;
    example.airLineItem = self.airLineItem;
    example.airPricingInfoItem = self.airPricingInfoItem;
    [self.navigationController pushViewController:example animated:YES];
}

@end
