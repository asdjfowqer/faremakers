//
//  AirRulesCell.h
//  Faremakers
//
//  Created by Mac1 on 03/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AirRulesCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *rulesTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *rulesDetailLabel;

@end
