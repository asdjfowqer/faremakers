//
//  FlightSearchItem.m
//  Faremakers
//
//  Created by Mac1 on 29/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "FlightSearchItem.h"
#import "DoubleDateItem.h"
#import "Travellers.h"
#import "Constants.h"
#import "Cities.h"


@implementation FlightSearchItem

- (id)init
{
    if (self = [super init])
    {
        self.classType = @"Economic";
        self.doubleDate = [[DoubleDateItem alloc] initWithDoubleDate:[NSDate date]:nil];
        self.is_roundTrip = NO;
        self.origin = @"Origin";
        self.Destination = @"Destination";
        self.travellers = [[Travellers alloc] initWithNumberOfTravellers:1];
        self.originAbr = @"";
        self.DestinationAbr = @"";
    }
    return self;
}

- (id)initWithTempData
{
    if (self = [super init])
    {
        self.classType = @"Economic";
        self.doubleDate = [[DoubleDateItem alloc] initWithDoubleDate:[[NSDate date]dateByAddingTimeInterval:KTimeInterval*7]:[[NSDate date] dateByAddingTimeInterval:KTimeInterval * 15]];
        self.is_roundTrip = NO;
        self.originCity = [Cities itemWithDictionary:[self prepareTempCityDictionary:0]];
        self.departCity = [Cities itemWithDictionary:[self prepareTempCityDictionary:1]];
        self.origin = @"LAHORE";
        self.Destination = @"DUBAI";
        self.travellers = [[Travellers alloc] initWithNumberOfTravellers:1];
        self.originAbr = @"LHE";
        self.DestinationAbr = @"DXB";
    }
    return self;
}

- (NSDictionary *)prepareTempCityDictionary:(BOOL)isDestination
{
    if (isDestination)
    {
        return [NSDictionary dictionaryWithObjectsAndKeys:@"LAHORE", @"airtportName",
                @"DXB", @"cityCode",
                @"DUBAI", @"countryName",
                @"AE", @"coutryCode",
                @"DUBAI", @"text", nil];
    }
    else
    {
        return [NSDictionary dictionaryWithObjectsAndKeys:@"LAHORE", @"airtportName",
                @"LHE", @"cityCode",
                @"PAKISTAN", @"countryName",
                @"PK", @"coutryCode",
                @"LAHORE,PK (LHE)", @"text", nil];
    }
}

- (void)updateAllValues
{
    [self.doubleDate updateAllDates];
}

- (NSString *)dateFormater:(NSDate *)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm"];
    return [formatter stringFromDate:date];
}

- (NSDictionary *)preparePOstDAtaWithNewDeparDate:(NSDate *)departureDate
{
    self.doubleDate.depDate = departureDate;
    return [self prepareDictForPostData];
}

- (NSDictionary *)preparePostDataForFareCalender
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:self.originAbr forKey:@"origin"];
    [dict setObject:self.DestinationAbr forKey:@"destination"];
    [dict setObject:@"5" forKey:@"lengthofstay"];
    [dict setObject:[self dateFormater:self.doubleDate.depDate] forKey:@"DepartureDate"];
    [dict setObject:[NSNumber numberWithInt:0] forKey:@"minfare"];
    [dict setObject:[NSNumber numberWithInt:0] forKey:@"maxfare"];
    [dict setObject:[[NSLocale currentLocale] objectForKey:NSLocaleCountryCode] forKey:@"pointofsalecountry"]; //@"US"

    return dict;
}

- (NSArray *)prepareLocationsArray:(NSMutableDictionary *)dic
{
    NSMutableArray *array = [[NSMutableArray alloc] init];

    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:_DestinationAbr forKey:@"TravelTo"];
    [dict setObject:_originAbr forKey:@"TravelFrom"];
    [dict setObject:[self dateFormater:_doubleDate.depDate] forKey:@"DepartureDateTime"];
    [dict setObject:[NSNull null] forKey:@"TravelFromText"];
    [dict setObject:[NSNull null] forKey:@"TravelToText"];
    [dict setObject:[NSNull null] forKey:@"MulticityFromText"];
    [dict setObject:[NSNull null] forKey:@"MulticityToText"];

    [array addObject:dict];

    if (_is_roundTrip)
    {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        [dict setObject:_DestinationAbr forKey:@"TravelFrom"];
        [dict setObject:_originAbr forKey:@"TravelTo"];
        [dict setObject:[self dateFormater:_doubleDate.retDate] forKey:@"DepartureDateTime"];
        [dict setObject:[NSNull null] forKey:@"TravelFromText"];
        [dict setObject:[NSNull null] forKey:@"TravelToText"];
        [dict setObject:[NSNull null] forKey:@"MulticityFromText"];
        [dict setObject:[NSNull null] forKey:@"MulticityToText"];
        [array addObject:dict];
    }


    return array;
}

- (NSDictionary *)prepareDictForPostData
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:[NSNumber numberWithBool:true] forKey:@"IsLocal"];
    [dict setObject:[self dateFormater:_doubleDate.depDate] forKey:@"DepartureDateTime"];
    [dict setObject:[NSString stringWithFormat:@"%ld", (long)_travellers.Adults] forKey:@"TotalAdults"];
    [dict setObject:[NSString stringWithFormat:@"%ld", (long)_travellers.Childs] forKey:@"TotalChildren"];
    [dict setObject:[NSString stringWithFormat:@"%ld", (long)_travellers.Infants] forKey:@"TotalInfants"];
    [dict setObject:[[NSLocale currentLocale] objectForKey:NSLocaleCurrencyCode] forKey:@"CurrencyCode"];
    if (_is_roundTrip)
    {
        [dict setObject:@"RoundTrip" forKey:@"FlightSearchType"];
    }
    else
    {
        [dict setObject:@"Oneway" forKey:@"FlightSearchType"];
    }

    [dict setObject:[_classType stringByReplacingOccurrencesOfString:@" " withString:@""] forKey:@"PreferredCabin"];

    [dict setObject:[self prepareLocationsArray:dict] forKey:@"Locations"];
    [dict setObject:[NSNull null] forKey:@"TravelFromLocation"];
    [dict setObject:[NSNull null] forKey:@"TravelToLocation"];
    [dict setObject:[NSNull null] forKey:@"ReturnDateTime"];

    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict
                                                       options:(NSJSONWritingOptions)NSJSONWritingPrettyPrinted
                                                         error:&error];
    if (!jsonData)
    {
        DLog(@"bv_jsonStringWithPrettyPrint: error: %@", error.localizedDescription);
    }
    else
    {
        DLog(@"%@", [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]);
    }
    return dict;
}

- (BOOL)checkAllValues
{
    BOOL error = NO;
    NSString *errorString = @"";
    if (_originAbr.length < 1)
    {
        errorString = @"Require Origin location";
        error = YES;
    }
    else if (_DestinationAbr.length < 1)
    {
        errorString = @"Require Destination location";
        error = YES;
    }

    if (error)
    {
        [[[UIAlertView alloc] initWithTitle:@"Error" message:errorString delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil] show];
    }
    return error;
}

@end