//
//  FlightBookingDetailCell.m
//  Faremakers
//
//  Created by Mac1 on 01/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "FlightBookingDetailCell.h"
#import "AirLinePricedItinerary.h"
#import "FlightSegment.h"
#import "AirPort.h"
#import "AirItineraryPricingInfo.h"
#import "AirLinesNameData.h"

@implementation FlightBookingDetailCell

- (void)awakeFromNib
{
    [self.flightTitleLabel changeFontFamily];
    [self.flightClassLabel changeFontFamily];
    [self.departAbrLabel changeFontFamily];
    [self.deparTimeLabel changeFontFamily];
    [self.deparDateLabel changeFontFamily];
    [self.deparAirportLabel changeFontFamily];
    [self.flightTravellTimeLabel changeFontFamily];
    [self.destAbrLabel changeFontFamily];
    [self.destTimeLabel changeFontFamily];
    [self.destDateLabel changeFontFamily];
    [self.destAirportLabel changeFontFamily];
    [self.layoverTimeLabel changeFontFamily];
}
- (void)setFlightSegmentsArray:(OriginDestination *)originDestinationItem forRowCell:(NSInteger)row
{
    _originDestinationItem = originDestinationItem;
    [self populateSegment:_originDestinationItem.flightSegments[row]];
}

- (void)populateSegment:(FlightSegment*)flightSegment
{
    _layoverTimeLabel.text = [NSString stringWithFormat:@"%@ Overlay",[flightSegment stringFromTimeInterval:flightSegment.layOverTime]];
    _flightLogoImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@.gif",flightSegment.operatingAirline.code]];
    
    _flightTitleLabel.text = [NSString stringWithFormat:@"%@ %@ %@",[[AirLinesNameData defaultAirLinesData] getAirlineNameByCode:flightSegment.operatingAirline.code], flightSegment.operatingAirline.code,flightSegment.operatingAirline.flightNumber];
    
    //_flightClassLabel.text = @"Economic";
    _deparTimeLabel.text = [FlightSegment getTimeStringFromDate:flightSegment.departureDateTime];
    _deparDateLabel.text = [flightSegment getDateStringFromDate:flightSegment.departureDateTime];;

    _flightTravellTimeLabel.text = [flightSegment stringFromTimeInterval:[flightSegment.arrivalDateTime timeIntervalSinceDate:flightSegment.departureDateTime]] ;
    
    _destTimeLabel.text = [FlightSegment getTimeStringFromDate:flightSegment.arrivalDateTime];
    _destDateLabel.text = [flightSegment getDateStringFromDate:flightSegment.arrivalDateTime];;
    
    
    AirPort *originAirport = flightSegment.departureAirport;
    _deparAirportLabel.text = [NSString stringWithFormat:@"%@, %@",originAirport.locationCode,[[AirLinesNameData defaultAirLinesData] getAirPortNameByCode:originAirport.locationCode]];//originAirport.airPortName;
    _departAbrLabel.text = [NSString stringWithFormat:@"%@",originAirport.locationCode];

    AirPort *destAirport = flightSegment.arrivalAirport;
    _destAirportLabel.text = [NSString stringWithFormat:@"%@, %@",destAirport.locationCode,[[AirLinesNameData defaultAirLinesData] getAirPortNameByCode:destAirport.locationCode]];//destAirport.airPortName;
    _destAbrLabel.text = [NSString stringWithFormat:@"%@",destAirport.locationCode];
    
}
- (void) setCabinData:(AirItineraryPricingInfo*)pricingInfo
{
    FareInfos *fareInfo =  pricingInfo.fareInfos[0];
    _flightClassLabel.text = fareInfo.cabin;
}

@end
