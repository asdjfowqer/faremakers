//
//  FlightTravellerCell.m
//  Faremakers
//
//  Created by Mac1 on 30/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "FlightTravellerCell.h"
#import "FlightSearchItem.h"
#import "UILabel+FontName.h"
#import "NSString+Email.h"
#import "Travellers.h"
#import "Constants.h"
#import "FMUser.h"

@implementation FlightTravellerCell

- (void)awakeFromNib
{
    [self.detailLabel changeFontFamily];
    [self.detailText setFont:[UIFont fontWithName:KCustomeFont size:self.detailText.font.pointSize]];
    self.detailText.delegate = self;
}
- (void)loadCellWithIdentifier:(NSString *)cellIdentifier forRowCell:(NSInteger)row
{
    // Get Last componet from CellIdenfifier
    int value = [[cellIdentifier substringFromIndex: [cellIdentifier length] - 1] intValue];
    
    switch (value)
    {
        case 0:
        {
            int rowSelection = 2;
            if (row < self.flightSearchItem.travellers.Adults)
                rowSelection = 0;
            else if (row < (self.flightSearchItem.travellers.Childs + self.flightSearchItem.travellers.Adults))
                rowSelection = 1;
            
            switch (rowSelection) {
                case 0:
                    [self loadAdultCellWithIndex:row];
                    break;
                case 1:
                    [self loadChildCellWithIndex:row];
                    break;
                case 2:
                    [self loadInfantsCellWithIndex:row];
                    break;
                default:
                    break;
            }
        }
            break;
        case 1:
        {
            switch (row) {
                case 1:
                    [self loadPhoneNumberCell];
                    break;
                case 0:
                    [self loadEmailCell];
                    break;
			  case 2:
				  [self loadPromocodeCell];
				  break;

				  
                default:
                    break;
            }
        }
            break;
        case 2:
            break;
        default:
            break;
    }
}

- (void) loadAdultCellWithIndex:(NSInteger)index
{
    self.ImageView.image = [UIImage imageNamed:@"traveller_icn"];
    
    Passengers *newPassenger = [self.flightSearchItem.travellers.passengersInfo objectAtIndex:index];
    if ([newPassenger isPassengerInfoAvaialble])
    {
        self.detailLabel.text = [newPassenger convertIntoString];
    }
    else
        self.detailLabel.text = [NSString stringWithFormat:@"Adult %ld",index+1];
}
- (void) loadChildCellWithIndex:(NSInteger)index
{
    self.ImageView.image = [UIImage imageNamed:@"traveller_icn"];
    Passengers *newPassenger = [self.flightSearchItem.travellers.passengersInfo objectAtIndex:index];
    if ([newPassenger isPassengerInfoAvaialble])
    {
        self.detailLabel.text = [newPassenger convertIntoString];
    }
    else
        self.detailLabel.text = [NSString stringWithFormat:@"Child %ld",(index+1 -  self.flightSearchItem.travellers.Adults)];
}
- (void) loadInfantsCellWithIndex:(NSInteger)index
{
    self.ImageView.image = [UIImage imageNamed:@"traveller_icn"];
    Passengers *newPassenger = [self.flightSearchItem.travellers.passengersInfo objectAtIndex:index];
    if ([newPassenger isPassengerInfoAvaialble])
    {
        self.detailLabel.text = [newPassenger convertIntoString];
    }
    else
        self.detailLabel.text = [NSString stringWithFormat:@"Infant %ld",(index+1 -  self.flightSearchItem.travellers.Adults - self.flightSearchItem.travellers.Childs)];
}
- (void) loadEmailCell
{
    self.itemImageView.image = [UIImage imageNamed:@"email_icn"];
    self.detailText.placeholder = @"Email";
    self.detailText.tag = 1;
    if ([FMUser defaultUser].email)
        self.detailText.text = [FMUser defaultUser].email;

}

- (void) loadPromocodeCell
{
	self.itemImageView.image = [UIImage imageNamed:@"mobile_icn"];
	self.itemImageView.hidden = YES;
	self.detailText.placeholder = @"Enter promo code";
	self.detailText.tag = 3;
	
}

- (void) loadPhoneNumberCell
{
    self.itemImageView.image = [UIImage imageNamed:@"mobile_icn"];
    self.detailText.placeholder = @"Phone";
    self.detailText.tag = 2;
    if ([FMUser defaultUser].phoneNumber)
        self.detailText.text = [FMUser defaultUser].phoneNumber;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField.text.length > 0)
    {
        if (textField.tag ==1 && ![textField.text isValidEmail])
        {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"Invalid Email Address" preferredStyle:UIAlertControllerStyleAlert];
                
                [alertController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                    textField.text = @"";
                }]];
        }
        else
            [self saveUserInfoText:textField.text forIndex:textField.tag];

    }
}
- (void)saveUserInfoText:(NSString*)text forIndex:(NSInteger)tag
{
	if (tag ==1)
	{
		[FMUser defaultUser].email = text;
        if (_delegate)
        {
            if ([_delegate respondsToSelector:@selector(userEnteredEmailAddress:)])
            {
                [_delegate userEnteredEmailAddress:text];
            }
        }
	}
	else if (tag ==2)
	{
		[FMUser defaultUser].phoneNumber = text;
	}
	else if (tag ==3)
	{
		if (_delegate)
		{
			if ([_delegate respondsToSelector:@selector(userEnteredPromocode:)])
			{
				[_delegate userEnteredPromocode:text];
			}
		}
	}
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

@end
