//
//  FlightListingsCell.h
//  Faremakers
//
//  Created by Mac1 on 21/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AirLinePricedItinerary;

@interface FlightListingsCell : UITableViewCell

@property (strong, nonatomic) AirLinePricedItinerary *airLineItem;

@property (weak, nonatomic) IBOutlet UILabel *flightTime;
@property (weak, nonatomic) IBOutlet UILabel *flightTravelTime;
@property (weak, nonatomic) IBOutlet UILabel *flightCost;
@property (weak, nonatomic) IBOutlet UILabel *airlineTittle;
@property (weak, nonatomic) IBOutlet UIImageView *airLineImageView;

- (void)setAirLineItem:(AirLinePricedItinerary *)airLineItem withTripIndex:(NSInteger)index;

@end
