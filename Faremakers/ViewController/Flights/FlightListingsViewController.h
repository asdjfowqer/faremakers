//
//  FlightListingsViewController.h
//  Faremakers
//
//  Created by Mac1 on 21/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FlightListingsViewDelegate.h"

@class FlightSearchItem;

@interface FlightListingsViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,UITabBarDelegate>

@property (weak, nonatomic) IBOutlet UIToolbar *toolBar;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *flightSearchData;
@property ( nonatomic) NSInteger OneWayIndex;
@property (weak , nonatomic) id <FlightListingsViewDelegate> adelegate;
@property (weak, nonatomic) IBOutlet UIView *notFoundView;
@property (strong, nonatomic) FlightSearchItem *searchItem;
@property (weak, nonatomic) IBOutlet UITabBar *tabBarView;


- (IBAction)filterButtonPressed:(id)sender;
- (void)rePositionDetailCell:(NSIndexPath*)indexPath;
- (void)scrollTableViewToIndex:(NSIndexPath*)index;
- (void)showDetailRowForRoundTrip:(NSIndexPath*)indexPath;

@end
