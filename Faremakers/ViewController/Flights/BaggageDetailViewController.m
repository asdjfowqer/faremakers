//
//  BaggageDetailViewController.m
//  Faremakers
//
//  Created by Mac1 on 06/10/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "BaggageDetailViewController.h"
#import "AirLinePricedItinerary.h"
#import "BaggageDetailViewCell.h"
#import "FlightSegment.h"
#import "AirPort.h"

@interface BaggageDetailViewController ()

@end

@implementation BaggageDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Baggage Info";

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger index = 0;
    
    index += _airLineItem.flightSegments.count;

    if (index != 0) {
        return index + 1;
    }
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self prepareCellForTableView:tableView forIndexPath:indexPath];
}

- (id)prepareCellForTableView:(UITableView *)tableView forIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row == 0)
    {
        BaggageDetailViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"BaggageDetailViewCell0" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else
    {
        BaggageDetailViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"BaggageDetailViewCell1" forIndexPath:indexPath];
        
        FlightSegment *segment = _airLineItem.flightSegments[indexPath.row - 1];
        
        cell.flightTitle.text = [NSString stringWithFormat:@"%@ to %@",segment.departureAirport.locationCode,segment.arrivalAirport.locationCode];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
}

@end
