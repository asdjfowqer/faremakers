//
//  FlightBookingCell.m
//  Faremakers
//
//  Created by Mac1 on 30/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "FlightBookingCell.h"
#import "AirItineraryPricingInfo.h"
#import "AirItinTotalFare.h"
#import "AirBaseFare.h"
#import "AirFareTaxes.h"
#import "AirFareBreakDown.h"

@implementation FlightBookingCell

- (void)awakeFromNib
{
    [self.PoliciesLabel changeFontFamily];
    [self.chargesCostLabel changeFontFamily];
    [self.chargesDetailLabel changeFontFamily];
}
- (void)setPricingItem:(AirItineraryPricingInfo *)pricingItem withIdentifier:(NSString*)cellIdentifier forRowCell:(NSInteger)row
{
    _pricingItem = pricingItem;
    [self loadCellWithIdentifier:cellIdentifier forRowCell:row];
}
#pragma mark - Private Method.

- (void)loadCellWithIdentifier:(NSString *)cellIdentifier forRowCell:(NSInteger)row
{
    // Get Last componet from CellIdenfifier
    int value = [[cellIdentifier substringFromIndex: [cellIdentifier length] - 1] intValue];
    
    switch (value)
    {
        case 0:
        {
            switch (row) {
                case 0:
                    [self loadDetailFirstCell];
                    break;
                case 1:
                    [self loadDetailSecondCell];
                    break;
                case 2:
                    [self loadDetailThirdCell];
                    break;
                case 3:
                    [self loadDetailForthCell];
                    break;
                default:
                    break;
            }
        }
            break;
        case 1:
        {
            switch (row) {
                case 0:
                    [self loadPriceFirstCell];
                    break;
                case 1:
                    [self loadPriceSecondCell];
                    break;
                case 2:
                    [self loadPriceForthCell];
                    break;
                case 3:
                    [self loadPriceForthCell];
                    break;
                default:
                    break;
            }
        }
            break;
        case 2:
            break;
        default:
            break;
    }
}
- (void) loadPriceFirstCell
{
    _chargesDetailLabel.text = @"Base Fare";
    _chargesCostLabel.text = [NSString stringWithFormat:@"%@ %@",_pricingItem.itinTotalFare.equivFare.amount,_pricingItem.itinTotalFare.equivFare.currencyCode];
}
- (void) loadPriceSecondCell
{
    _chargesDetailLabel.text = @"Total Tax";
    AirFareTaxes *taxes= [_pricingItem.itinTotalFare.taxes objectAtIndex:0];
    _chargesCostLabel.text = [NSString stringWithFormat:@"%.1f %@",(taxes.amount.doubleValue + _pricingItem.itinTotalFare.commission.amount.doubleValue),taxes.currencyCode];

}
- (void) loadPriceThirdCell
{
    _chargesDetailLabel.text = @"Commission";
    _chargesCostLabel.text = [NSString stringWithFormat:@"%@ %@",_pricingItem.itinTotalFare.commission.amount,_pricingItem.itinTotalFare.equivFare.currencyCode];
    
}
- (void) loadPriceForthCell
{
    _chargesDetailLabel.text = @"Total Fare";
    _chargesCostLabel.text = [NSString stringWithFormat:@"%@ %@",_pricingItem.itinTotalFare.totalFare.amount,_pricingItem.itinTotalFare.totalFare.currencyCode];

}

- (void) loadDetailFirstCell
{
    FareInfos *fareInfos = [_pricingItem.fareInfos objectAtIndex:_TripType];

    _chargesDetailLabel.text = @"Seat Left";
    _chargesCostLabel.text = [NSString stringWithFormat:@"%@",fareInfos.seatsRemaining];
}
- (void) loadDetailSecondCell
{
    _chargesDetailLabel.text = @"Travellers";

    NSArray *fareBreakDownArray = _pricingItem.ptC_FareBreakdown;
    
    int i = 0;
    NSString *adults = @"0";
    NSString *childes = @"0";
    NSString *infants = @"0";
    
    for (AirFareBreakDown *farebreakDown in fareBreakDownArray)
    {
        if (i == 0)
            adults = farebreakDown.passengerTypeQuantity.quantity;
        else if (i == 1)
            childes = farebreakDown.passengerTypeQuantity.quantity;
        else
            infants = farebreakDown.passengerTypeQuantity.quantity;
        i++;
    }
    _chargesCostLabel.text = [NSString stringWithFormat:@"Adults %@ childes %@ Infants %@",adults,childes,infants];
}
- (void) loadDetailThirdCell
{
    _PoliciesLabel.text = @"Baggage Info";
}
- (void) loadDetailForthCell
{
    _PoliciesLabel.text = @"Rules";
}
@end
