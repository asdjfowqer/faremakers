//
//  PromotionDetailCell.h
//  Faremakers
//
//  Created by Mac1 on 19/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FlightFilterItem;

@interface FlightFilterCell : UITableViewCell<UICollectionViewDelegate,UICollectionViewDataSource>

@property (strong, nonatomic)NSArray *customArray;
@property (nonatomic)FlightFilterItem *filterItem;

#pragma mark FlightFilterCell0
@property (weak, nonatomic) IBOutlet UIImageView *checkedImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

#pragma mark FlightFilterCell1
@property (weak, nonatomic) IBOutlet UISlider *durationSlider;
@property (weak, nonatomic) IBOutlet UILabel *sliderDurationLabel;
- (IBAction)sliderValueChanged:(id)sender;

#pragma mark FlightFilterCell2
@property (weak, nonatomic) IBOutlet UISlider *layoverDurationSlider;
@property (weak, nonatomic) IBOutlet UILabel *layoverSliderLabel;
- (IBAction)layoverSliderValueChanged:(id)sender;

#pragma mark FlightFilterCell3
@property (weak, nonatomic) IBOutlet UICollectionView *timingCollectionView;

#pragma mark FlightFilterCell4
@property (weak, nonatomic) IBOutlet UIImageView *airLineImageView;
@property (weak, nonatomic) IBOutlet UILabel *airLineTitle;
@property (weak, nonatomic) IBOutlet UILabel *airLineCode;


- (void)loadCellWithIdentifier:(NSString *)cellIdentifier ForIndexPath:(NSIndexPath*)index;

@end
