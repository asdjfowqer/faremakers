//
//  HotelCellWithStepperDelegate.h
//  ElectionApp
//
//  Created by Mac1 on 09/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

//#ifndef HotelCellWithStepperDelegate_h
//#define HotelCellWithStepperDelegate_h

@protocol FlightListingsViewDelegate <NSObject>

- (void) FlightPageSortFlightDataCalledWithSortKey:(NSString*)key isRoundTrip:(NSInteger)roundTrip;
- (void) filterFlightDataWithFilterItem:(id)filterItem isRoundTrip:(NSInteger)roundTrip;
- (void) flightPageRemoveAppliedSortingWithRoundTrip:(NSInteger)roundTrip;
- (void) flightPageDidChangeDepartureDate:(NSDate*)departureDate;
- (void) flightPageDidSelectClearAllFilters:(NSInteger)isRound;
- (void) flightlistingsViewController:(id)viewController DidSelectFlight:(id)flightItem forIndexPath:(NSIndexPath*)indexPath;

- (NSArray*) getFlightSearchData;

@end

//#endif /* HotelCellWithStepperDelegate_h */
