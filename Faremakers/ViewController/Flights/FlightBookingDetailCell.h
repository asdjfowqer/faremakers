//
//  FlightBookingDetailCell.h
//  Faremakers
//
//  Created by Mac1 on 01/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <UIKit/UIKit.h>


@class AirItineraryPricingInfo;
@class OriginDestination;

@interface FlightBookingDetailCell : UITableViewCell

@property (weak, nonatomic) OriginDestination *originDestinationItem;
@property (weak, nonatomic) IBOutlet UIImageView *flightLogoImageView;
@property (weak, nonatomic) IBOutlet UILabel *flightTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *flightClassLabel;
@property (weak, nonatomic) IBOutlet UILabel *departAbrLabel;
@property (weak, nonatomic) IBOutlet UILabel *deparTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *deparDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *deparAirportLabel;
@property (weak, nonatomic) IBOutlet UILabel *flightTravellTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *destAbrLabel;
@property (weak, nonatomic) IBOutlet UILabel *destTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *destDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *destAirportLabel;
@property (weak, nonatomic) IBOutlet UILabel *layoverTimeLabel;

- (void)setFlightSegmentsArray:(OriginDestination *)flightSegmentsArray forRowCell:(NSInteger)row;
- (void) setCabinData:(AirItineraryPricingInfo*)pricingInfo;

@end
