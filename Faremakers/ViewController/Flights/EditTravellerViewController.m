//
//  EditTravellerViewController.m
//  Faremakers
//
//  Created by Mac1 on 29/01/2016.
//  Copyright © 2015 Self. All rights reserved.
//

#import "XLForm.h"
#import "DateAndTimeValueTrasformer.h"
#import "EditTravellerViewController.h"
#import <DateTools/DateTools.h>
#import "Constants.h"
#import "Travellers.h"

NSString *const KTitlePicker = @"titlePicker";
NSString *const KFNameField = @"fNameField";
NSString *const KMNameField = @"mNameField";
NSString *const KLNameField = @"lNameField";
NSString *const KDatePicker = @"dobPicker";

@interface EditTravellerViewController ()

@end

@implementation EditTravellerViewController

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self)
    {
        //[self initializeForm];
    }
    return self;
}

- (instancetype)init
{
    self = [super init];
    if (self)
    {
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
    }
    return self;
}

- (void)initializeForm
{
    XLFormDescriptor *form;
    XLFormSectionDescriptor *section;
    XLFormRowDescriptor *row;

    form = [XLFormDescriptor formDescriptorWithTitle:@"Passenger"];
    section = [XLFormSectionDescriptor formSection];
    [form addFormSection:section];

    //  Title
    row = [XLFormRowDescriptor formRowDescriptorWithTag:KTitlePicker rowType:XLFormRowDescriptorTypeSelectorPickerView title:@"Title"];

    if (self.person == KAdult)
    {
        row.selectorOptions = @[@"Mr", @"Mrs", @"Ms"];
    }
    else
    {
        row.selectorOptions = @[@"Mstr", @"Miss"];
    }

    row.value = (self.passenger.title) ? self.passenger.title : [row.selectorOptions firstObject];
    row.required = YES;
    [section addFormRow:row];

    // First Name
    row = [XLFormRowDescriptor formRowDescriptorWithTag:KFNameField rowType:XLFormRowDescriptorTypeText];
    [row.cellConfigAtConfigure setObject:@"First Name" forKey:@"textField.placeholder"];
    row.value = self.passenger.fName;
    row.required = YES;
    [section addFormRow:row];

    // Last Name
    row = [XLFormRowDescriptor formRowDescriptorWithTag:KLNameField rowType:XLFormRowDescriptorTypeText];
    [row.cellConfigAtConfigure setObject:@"Last Name" forKey:@"textField.placeholder"];
    row.value = self.passenger.lName;
    row.required = YES;
    [section addFormRow:row];

    // dob
    row = [XLFormRowDescriptor formRowDescriptorWithTag:KDatePicker rowType:XLFormRowDescriptorTypeDateInline title:@"DOB"];
    
    if (self.person == KAdult) {
        [row.cellConfigAtConfigure setObject:[NSDate dateWithYear:1940 month:1 day:1] forKey:@"minimumDate"];
        NSDate *date = [[NSDate date] dateBySubtractingMonths:(23 + (11*12))];
        date = [date dateBySubtractingDays:3];
        [row.cellConfigAtConfigure setObject:date forKey:@"maximumDate"];
    }
    else if (self.person == KChild)
    {
        NSDate *date = [[NSDate date] dateBySubtractingMonths:23];
        date = [date dateBySubtractingDays:2];
        [row.cellConfigAtConfigure setObject:[date dateBySubtractingMonths:(11*12)] forKey:@"minimumDate"];
        [row.cellConfigAtConfigure setObject:date forKey:@"maximumDate"];
    }
    else if (self.person == KInfant)
    {
        NSDate *date = [[NSDate date] dateBySubtractingDays:1];
        [row.cellConfigAtConfigure setObject:[date dateBySubtractingMonths:23] forKey:@"minimumDate"];
        [row.cellConfigAtConfigure setObject:date forKey:@"maximumDate"];
    }
    row.required = YES;
    
    if(self.passenger.dob)
        row.value = self.passenger.dob;
    else if (self.person == KAdult)
        row.value = [[NSDate date] dateBySubtractingYears:25];
    else if (self.person == KChild)
        row.value = [[NSDate date] dateBySubtractingYears:8];
    else if (self.person == KInfant)
        row.value = [[NSDate date] dateBySubtractingYears:1];
    
    [section addFormRow:row];

    self.form = form;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initializeForm];

    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(savePressed:)];
    self.title = @"Personal Info";
}

#pragma mark - XLFormDescriptorDelegate

- (void)formRowDescriptorValueHasChanged:(XLFormRowDescriptor *)rowDescriptor oldValue:(id)oldValue newValue:(id)newValue
{
    [super formRowDescriptorValueHasChanged:rowDescriptor oldValue:oldValue newValue:newValue];
    if ([rowDescriptor.tag isEqualToString:@"alert"])
    {
        if ([[rowDescriptor.value valueData] isEqualToNumber:@(0)] == NO && [[oldValue valueData] isEqualToNumber:@(0)])
        {
            XLFormRowDescriptor *newRow = [rowDescriptor copy];
            newRow.tag = @"secondAlert";
            newRow.title = @"Second Alert";
            [self.form addFormRow:newRow afterRow:rowDescriptor];
        }
        else if ([[oldValue valueData] isEqualToNumber:@(0)] == NO && [[newValue valueData] isEqualToNumber:@(0)])
        {
            [self.form removeFormRowWithTag:@"secondAlert"];
        }
    }
    else if ([rowDescriptor.tag isEqualToString:@"all-day"])
    {
        XLFormRowDescriptor *startDateDescriptor = [self.form formRowWithTag:@"starts"];
        XLFormRowDescriptor *endDateDescriptor = [self.form formRowWithTag:@"ends"];
        XLFormDateCell *dateStartCell = (XLFormDateCell *)[[self.form formRowWithTag:@"starts"] cellForFormController:self];
        XLFormDateCell *dateEndCell = (XLFormDateCell *)[[self.form formRowWithTag:@"ends"] cellForFormController:self];
        if ([[rowDescriptor.value valueData] boolValue] == YES)
        {
            startDateDescriptor.valueTransformer = [DateValueTrasformer class];
            endDateDescriptor.valueTransformer = [DateValueTrasformer class];
            [dateStartCell setFormDatePickerMode:XLFormDateDatePickerModeDate];
            [dateEndCell setFormDatePickerMode:XLFormDateDatePickerModeDate];
        }
        else
        {
            startDateDescriptor.valueTransformer = [DateTimeValueTrasformer class];
            endDateDescriptor.valueTransformer = [DateTimeValueTrasformer class];
            [dateStartCell setFormDatePickerMode:XLFormDateDatePickerModeDateTime];
            [dateEndCell setFormDatePickerMode:XLFormDateDatePickerModeDateTime];
        }
        [self updateFormRow:startDateDescriptor];
        [self updateFormRow:endDateDescriptor];
    }
    else if ([rowDescriptor.tag isEqualToString:@"starts"])
    {
        XLFormRowDescriptor *startDateDescriptor = [self.form formRowWithTag:@"starts"];
        XLFormRowDescriptor *endDateDescriptor = [self.form formRowWithTag:@"ends"];
        if ([startDateDescriptor.value compare:endDateDescriptor.value] == NSOrderedDescending)
        {
            // startDateDescriptor is later than endDateDescriptor
            endDateDescriptor.value =  [[NSDate alloc] initWithTimeInterval:(60 * 60 * 24) sinceDate:startDateDescriptor.value];
            [endDateDescriptor.cellConfig removeObjectForKey:@"detailTextLabel.attributedText"];
            [self updateFormRow:endDateDescriptor];
        }
    }
    else if ([rowDescriptor.tag isEqualToString:@"ends"])
    {
        XLFormRowDescriptor *startDateDescriptor = [self.form formRowWithTag:@"starts"];
        XLFormRowDescriptor *endDateDescriptor = [self.form formRowWithTag:@"ends"];
        XLFormDateCell *dateEndCell = (XLFormDateCell *)[endDateDescriptor cellForFormController:self];
        if ([startDateDescriptor.value compare:endDateDescriptor.value] == NSOrderedDescending)
        {
            // startDateDescriptor is later than endDateDescriptor
            [dateEndCell update]; // force detailTextLabel update
            NSDictionary *strikeThroughAttribute = [NSDictionary dictionaryWithObject:@1
                                                                               forKey:NSStrikethroughStyleAttributeName];
            NSAttributedString *strikeThroughText = [[NSAttributedString alloc] initWithString:dateEndCell.detailTextLabel.text attributes:strikeThroughAttribute];
            [endDateDescriptor.cellConfig setObject:strikeThroughText forKey:@"detailTextLabel.attributedText"];
            [self updateFormRow:endDateDescriptor];
        }
        else
        {
            [endDateDescriptor.cellConfig removeObjectForKey:@"detailTextLabel.attributedText"];
            [self updateFormRow:endDateDescriptor];
        }
    }
}

- (void)cancelPressed:(UIBarButtonItem *__unused)button
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)savePressed:(UIBarButtonItem *__unused)button
{
    NSArray *validationErrors = [self formValidationErrors];
    if (validationErrors.count > 0)
    {
        [self showFormValidationError:[validationErrors firstObject]];
        return;
    }
    XLFormRowDescriptor *row = [self.form formRowWithTag:KTitlePicker];
    self.passenger.title = row.value;

    row = [self.form formRowWithTag:KFNameField];
    self.passenger.fName = row.value;

    row = [self.form formRowWithTag:KLNameField];
    self.passenger.lName = row.value;

    row = [self.form formRowWithTag:KDatePicker];
    self.passenger.dob = row.value;

    DLog(@"%@ %@", row.value, [row.value class]);
    self.passenger.dobUser = row.value;

    [self.tableView endEditing:YES];

    [self.navigationController popViewControllerAnimated:YES];
}

@end