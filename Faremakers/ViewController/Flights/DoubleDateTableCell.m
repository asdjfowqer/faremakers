//
//  DoubleDateTableCell.m
//  Faremakers
//
//  Created by Mac1 on 16/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "DoubleDateTableCell.h"
#import "IQActionSheetPickerView.h"
#import "NSDate+DateComponents.h"
#import "FlightSearchItem.h"
#import "DoubleDateItem.h"
#import "UILabel+FontName.h"
#import "Constants.h"

@implementation DoubleDateTableCell

- (void)awakeFromNib
{
    [self.departTitle changeFontFamily];
    [self.returnTitle changeFontFamily];
    [self.departDate changeFontFamily];
    [self.departYear changeFontFamily];
    [self.returnDate changeFontFamily];
    [self.returnDay changeFontFamily];
    [self.departDay changeFontFamily];
    [self.returnYear changeFontFamily];
    [self.selectLabel changeFontFamily];
    selectedCellTag = -1;
    _searchType = 0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
- (IBAction)datePickerViewClicked:(UIButton *)sender
{
    selectedCellTag = [sender tag];
    
    IQActionSheetPickerView *picker = [[IQActionSheetPickerView alloc] initWithTitle:@"Date Picker" delegate:self];
    [picker setTag:6];
    [picker setDate:([sender tag] == 0)?self.doubleDateItem.depDate:self.doubleDateItem.retDate];
    [picker setActionSheetPickerStyle:IQActionSheetPickerStyleDatePicker];
    
    if ([sender tag] == 0)
        [picker setMinimumDate:[NSDate date]];
    else
        [picker setMinimumDate:[[NSDate date] dateByAddingTimeInterval:KTimeInterval]];
    
    if (self.searchType)
    {
        if ([sender tag] == 0)
            [picker setMinimumDate:[[NSDate date] dateByAddingTimeInterval:KTimeInterval*7]];
        else
            [picker setMinimumDate:[[NSDate date] dateByAddingTimeInterval:KTimeInterval*8]];
    }
    [picker setMaximumDate:[[NSDate date] dateByAddingTimeInterval:KTimeInterval * 365]];
    [picker show];
}
- (void)setDoubleDateItem:(DoubleDateItem *)doubleDateItem
{
    _doubleDateItem = doubleDateItem;
    [self setDepartDateComponent:self.doubleDateItem.depDate];
    [self setReturnDateComponent:self.doubleDateItem.retDate];
    
}
- (void)setDoubleDateComponetsData:(NSDate *)date1 :(NSDate*)date2{
    
    _dateItem1 = date1;
    _dateItem2 = date2;
    
    [self setDepartDateComponent:_dateItem1];
    [self setReturnDateComponent:_dateItem2];
    
}

-(void)actionSheetPickerView:(IQActionSheetPickerView *)pickerView didSelectTitles:(NSArray *)titles
{
    DLog(@"%@",titles);
}

-(void)actionSheetPickerView:(IQActionSheetPickerView *)pickerView didSelectDate:(NSDate *)date
{
    switch (pickerView.tag)
    {
        case 6:
        {
//            if (![self compareDates:date])
//                return;
            
            if (selectedCellTag == 0)
            {
                
                [self setDepartDateComponent:date];
                _dateItem1  = date;
                self.doubleDateItem.depDate = date;
                
                if ([date compare:self.doubleDateItem.retDate] == NSOrderedDescending)
                {
                    NSDate *returnDate  = [date dateByAddingTimeInterval:KTimeInterval];
                    [self setReturnDateComponent:returnDate];
                    self.doubleDateItem.retDate = returnDate;
                }
            }
            else if (selectedCellTag == 1)
            {
                if (self.searchType && ![self compareHotelCheckOutDate:date])
                    return;
                
                [self showRoundTripLables:YES];
                [self setReturnDateComponent:date];
                self.doubleDateItem.retDate = date;
                if ([self.adelegate respondsToSelector:@selector(doubleDateReturnComponentDidSelected:)])
                    [self.adelegate doubleDateReturnComponentDidSelected:nil];
            }
        }
            break;
        default:
            break;
    }
}
-(void)setDepartDateComponent:(NSDate*)date
{
    if(!date)
        return;
    self.departYear.text = [NSString stringWithFormat:@"%@'%@", [date getMonth],[date getYear]];
    self.departDay.text = [date getDayOfTheWeek];
    self.departDate.text = [date getDayOfTheMonth];
    
}
-(void)setReturnDateComponent:(NSDate*)date
{
    if(!date)
        return;
    self.returnYear.text = [NSString stringWithFormat:@"%@'%@", [date getMonth],[date getYear]];
    self.returnDay.text = [date getDayOfTheWeek];
    self.returnDate.text = [date getDayOfTheMonth];
}
- (void)showRoundTripLables:(BOOL)show
{
    // show = YES;
    if (show) {
        if (self.doubleDateItem.retDate == nil)
            self.doubleDateItem.retDate = [NSDate date];
        [self setReturnDateComponent:self.doubleDateItem.retDate];
    }
    [self.selectLabel setHidden:show];
    [self.returnDate setHidden:!show];
    [self.returnDay setHidden:!show];
    [self.returnYear setHidden:!show];
}

-(BOOL)checkFutureDate:(NSDate*)date
{
    NSTimeInterval interval = [date timeIntervalSinceDate:[NSDate date]];
    NSTimeInterval difference = -300;
    if (interval < difference)
        return false;
    return true;
}
-(BOOL)compareDates:(NSDate*)date
{
    BOOL isDateOld = [self checkFutureDate:date];
    if (!isDateOld)
    {
        if (self.searchType)
            [[[UIAlertView alloc] initWithTitle:@"Error" message:@"Please Select future Date from 1 weak now" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
        else
            [[[UIAlertView alloc] initWithTitle:@"Error" message:@"Please Select Future Date" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
    }
    return isDateOld;
}
- (BOOL)compareHotelCheckOutDate:(NSDate*)date
{
    NSTimeInterval interval = [date timeIntervalSinceDate:self.doubleDateItem.depDate];
    
    if (interval <  (-500)) {
        [[[UIAlertView alloc] initWithTitle:@"Error" message:@"Please Select future Date from checkIn Date" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
        return false;
    }
    return true;
    
}
@end
