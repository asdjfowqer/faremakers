//
//  FlightSearchViewController.m
//  Faremakers
//
//  Created by Mac1 on 16/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "FlightSearchViewController.h"
#import "FlightTravellerInfoController.h"
#import "IQActionSheetPickerView.h"
#import "CitySerachViewController.h"
#import "AirFareRulesView.h"
#import "HomeViewController.h"
#import "DoubleDateTableCell.h"
#import "UIView+LayerShot.h"
#import "FlightSearchItem.h"
#import "Travellers.h"
#import "BorderButton.h"
#import "Constants.h"
#import "Cities.h"

@interface FlightSearchViewController ()<IQActionSheetPickerViewDelegate,CityNameSearchDelegate,DoubleDateCellViewDelegate>
{
    NSUInteger _selectedSection;
    NSArray *flightClassData;
    FlightSearchItem *item;
    NSInteger selectedCityIndex;
    CitySerachViewController *citySearchView;
}

@end

@implementation FlightSearchViewController


#define KFlightDetailCell @"FlightDetailCell"

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Menu_icon"] style:UIBarButtonItemStylePlain target:self action:@selector(presentLeftMenuViewController:)];
    
    flightClassData = @[ @"Economy", @"Business", @"Premium Economy", @"Premium First"];
    [self.tableView registerNib:[UINib nibWithNibName:@"DoubleDateTableCell" bundle:nil] forCellReuseIdentifier:KDoubleDateTableCell];
    item = [[FlightSearchItem alloc] initWithTempData];
    citySearchView = [self.storyboard instantiateViewControllerWithIdentifier:@"CitySerachViewController"];
    citySearchView.adelegate = self;
   // item = [[FlightSearchItem alloc] init];
    [self adjustMarginsForLagerHeights];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [item updateAllValues];
    [self updateLocations];
    [self.tableView reloadData];
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section)
        return 2;
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section)
        return 55;
    return 90;
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section) {
        
        UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:KFlightDetailCell];
        switch (indexPath.row) {
            case 0:{
                cell.textLabel.text = @"Travellers";
                cell.detailTextLabel.text = [NSString stringWithFormat:@"%ld Adults, %ld Childs, %ld Infants",(long)item.travellers.Adults,(long)item.travellers.Childs,(long)item.travellers.Infants];
            }
                break;
            case 1:{
                cell.textLabel.text = @"Class";
                cell.detailTextLabel.text = item.classType;
            }
                break;
                
            default:
                break;
        }
        return cell;
    }
    else
    {
        DoubleDateTableCell *cell = [self.tableView dequeueReusableCellWithIdentifier:KDoubleDateTableCell];
        cell.doubleDateItem = item.doubleDate;
        cell.adelegate = self;
        [cell showRoundTripLables:item.is_roundTrip];
        return cell;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _selectedSection = indexPath.row;
    if (_selectedSection == 0)
    {
        [self showPickerViewForTravellers];
    }
    else{
        [self showPickerViewFlightClass];
    }
}
- (IBAction)searchButtonSelector:(id)sender
{
    NSLog(@"%@",sender);
    if (![item checkAllValues])
    {
        HomeViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
        viewController.searchItem = item;
        
        
        
        [viewController searchFlightDataWithDictionary:[item prepareDictForPostData]];
        [self.navigationController pushViewController:viewController animated:YES];
    }
}
- (IBAction)departureBtnSelector:(id)sender
{
    [self PresentCitySearchViewController];
    selectedCityIndex = 0;
}
- (IBAction)arrivalBtnSelector:(id)sender
{
    [self PresentCitySearchViewController];
    selectedCityIndex = 1;
}
- (IBAction)tripSegmentValueChange:(id)sender
{
    if ([sender selectedSegmentIndex])
    {
        DoubleDateTableCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
        [cell showRoundTripLables:YES];
        item.is_roundTrip = YES;

    }
    else
    {
        DoubleDateTableCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
        [cell showRoundTripLables:NO];
        item.is_roundTrip = NO;

    }
}

#pragma mark CitySearchController

- (void)PresentCitySearchViewController
{
    [self.navigationController pushViewController:citySearchView animated:YES];
}

-(void) CitySearchViewController:(id)viewController didSelectCity:(id)city
{
    CitySerachViewController *Controller = (CitySerachViewController*)viewController;
    [self populateCiteData:city];
    [Controller.navigationController popViewControllerAnimated:YES];
}

- (void) populateCiteData:(id)city
{
    Cities *newcity = (Cities*)city;
    switch (selectedCityIndex) {
        case 0:
        {
            self.originLabel.text = [Cities stringWithCityNameOnly:newcity.text];
            [self.originButton setTitle:newcity.cityCode forState:UIControlStateNormal];
            item.originCity = newcity;
            item.originAbr = newcity.cityCode;
            item.origin = [[newcity.text componentsSeparatedByString:@","] firstObject];
        }
            break;
        case 1:
        {
            self.destLabel.text = [Cities stringWithCityNameOnly:newcity.text];
            [self.destAbbrButton setTitle:newcity.cityCode forState:UIControlStateNormal];
            item.departCity = newcity;
            item.DestinationAbr = newcity.cityCode;
            item.Destination = [[newcity.text componentsSeparatedByString:@","] firstObject];

        }
            break;
        default:
            break;
    }
}
- (void) updateLocations
{
    if (item.originCity)
    {
        self.originLabel.text = [Cities stringWithCityNameOnly:item.originCity.text];
        [self.originButton setTitle:item.originCity.cityCode forState:UIControlStateNormal];
    }
    if (item.departCity)
    {
        self.destLabel.text = [Cities stringWithCityNameOnly:item.departCity.text];
        [self.destAbbrButton setTitle:item.departCity.cityCode forState:UIControlStateNormal];
    }
    if(item.is_roundTrip)
        self.segmentController.selectedSegmentIndex = 1;
}

#pragma mark DoubleDateCellViewDelegate

-(void) doubleDateReturnComponentDidSelected:(id)sender
{
    item.is_roundTrip = YES;
    self.segmentController.selectedSegmentIndex = 1;
    
}

#pragma mark IQActionSheet

- (void)showPickerViewFlightClass
{
    IQActionSheetPickerView *picker = [[IQActionSheetPickerView alloc] initWithTitle:@"Flight Class" delegate:self];
    picker.titleFont = [UIFont fontWithName:KCustomeFont size:14];
    [picker setTag:1];
    [picker setTitlesForComponents:@[flightClassData]];
    [picker show];
}
- (void)showPickerViewForTravellers
{
    IQActionSheetPickerView *picker = [[IQActionSheetPickerView alloc] initWithTitle:@"Select No Of Travellers" delegate:self withMultiPickerTitle:@[@"Adults",@"Childes",@"Infants"]];
    
    picker.titleFont = [UIFont fontWithName:KCustomeFont size:14];
    [picker setTag:3];
    
    [picker setTitlesForComponents:@[@[@"1", @"2", @"3", @"4", @"5", @"6"],
                                     @[@"0",@"1", @"2", @"3", @"4", @"5", @"6"],
                                     @[@"0",@"1", @"2", @"3", @"4", @"5", @"6"]]];
    
    [picker selectPickerRowIndexes:@[[NSNumber numberWithInteger:item.travellers.Adults],[NSNumber numberWithInteger:item.travellers.Childs],[NSNumber numberWithInteger:item.travellers.Infants]] Animated:NO];
    [picker show];
}

-(void)actionSheetPickerView:(IQActionSheetPickerView *)pickerView didSelectTitles:(NSArray *)titles
{
    if (pickerView.tag == 1)
    {
        item.classType = [titles objectAtIndex:0];
    }
    else  if (pickerView.tag == 3)
    {
        NSInteger adults = [[titles objectAtIndex:0] integerValue];
        NSInteger Childs = [[titles objectAtIndex:1] integerValue];
        NSInteger Infants = [[titles objectAtIndex:2] integerValue];
        
        if ((adults+Childs+Infants) > 9)
        {
            [[[UIAlertView alloc] initWithTitle:@"Error" message:@"Maximum of 9 travellers allowed" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil] show];
        }
        else
        {
            item.travellers.Adults = adults;
            item.travellers.Childs = Childs;
            item.travellers.Infants = Infants;
        }
    }
    [self.tableView reloadData];
}
- (void) adjustMarginsForLagerHeights
{
    if ([UIScreen mainScreen].bounds.size.height == 568)
    {
        self.searchBtnBottomMargin.constant = 2;
    }
    else if ([UIScreen mainScreen].bounds.size.height == 480)
    {
        self.originBtnTopMargin.constant = 10;
        self.tableTopMargin.constant = 5;
        self.searchBtnBottomMargin.constant = 0;
        
    }
}

@end
