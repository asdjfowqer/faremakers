//
//  FlightListingsViewController.m
//  Faremakers
//
//  Created by Mac1 on 21/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "FlightListingsViewController.h"
#import "MainColloctionViewDelegate.h"
#import "FlightBookingViewController.h"
#import "FlightDataApiManager.h"
#import "FlightListingsCell.h"
#import "FlightListingsDetailCell.h"
#import "FlightFilterViewController.h"
#import "FlightFilterItem.h"
#import "FareTrendViewController.h"
#import "TransitionDelegate.h"

@interface FlightListingsViewController()<MainColloctionViewDelegate,FlightFilterViewDelegate,FareTrendViewControllerDelegate>
{
    NSArray *filtersItem;
    NSIndexPath *previousIndexPath;
    NSIndexPath *newIndexPath;
    BOOL isEditing ;
    NSInteger total;
    FlightFilterItem *filterItem;
    NSInteger selectedTab;
}
@property (nonatomic, strong) TransitionDelegate *transitionController;

@end

@implementation FlightListingsViewController

-(void)awakeFromNib
{
    total = 0;
}
-(void) viewDidLoad
{
    [super viewDidLoad];
    selectedTab = -1;
    filterItem = [[FlightFilterItem alloc] init];
    [self generateIndexesForDetailCell];
    isEditing  = false;
    filtersItem = @[@"price",@"departureDateTime",@"duration"];//@[@"totalStops",@"airLine"];//@"From",@"To",@"Refunded",@"Non-Refunded"];//,@"Baggage",@"Aminities",@"Short Time", @"Seat Map", @"Seat Left"];
    
}
-(void)generateIndexesForDetailCell
{
    previousIndexPath = [NSIndexPath indexPathForItem:1 inSection:0];
    newIndexPath = [NSIndexPath indexPathForItem:1 inSection:0];
}
- (void)setFlightSearchData:(NSArray *)flightSearchData
{
    _flightSearchData = flightSearchData;
    total = _flightSearchData.count;
    if (total > 0) {
        total++;
        [self generateIndexesForDetailCell];
        [self.tableView reloadData];
        [self.notFoundView setHidden:YES];
    }
    else
    {
        total = 0;
        [self.notFoundView setHidden:NO];
        [self.tableView reloadData];
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return total;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == newIndexPath.row)
        return 150;
    return 45;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == newIndexPath.row)
    {
        FlightListingsDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FlightListingsDetailCell1"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        NSInteger Index = indexPath.row - 1;
        [cell setAirLineItem:[_flightSearchData objectAtIndex:Index] withTripIndex:_OneWayIndex];
        cell.aDelegate = self;
        return cell;
    }
    else{
        
        FlightListingsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FlightListingsCell1"];
        NSInteger Index = indexPath.row;
        if (Index > newIndexPath.row)
            Index--;
        if (Index >= _flightSearchData.count)
        {
            DLog(@"Repeated Data leakage %ld , %ld",total,Index);
        }
        [cell setAirLineItem:[_flightSearchData objectAtIndex:Index] withTripIndex:_OneWayIndex];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self prePareCellForRepositioning:indexPath];
}
- (void)prePareCellForRepositioning:(NSIndexPath*)index
{
    if(previousIndexPath.row == index.row || previousIndexPath.row == index.row +1)
        return;
    [self rePositionDetailCell:index];
}
- (void)rePositionDetailCell:(NSIndexPath*)indexPath
{
    isEditing = true;
    total = total - 1;
    [self DeleteRow:previousIndexPath];
    
    total = total+1;
    NSInteger addition = 0;
    if (indexPath.row < previousIndexPath.row)
    {
        addition++;
    }
    newIndexPath = [NSIndexPath indexPathForItem:(indexPath.row + addition) inSection:indexPath.section];
    previousIndexPath = newIndexPath;
    [self insertNewRow:newIndexPath];
    isEditing = false;
}
- (void)showDetailRowForRoundTrip:(NSIndexPath*)indexPath
{
    total--;
    [self DeleteRow:previousIndexPath];
    
    total++;
    newIndexPath = [NSIndexPath indexPathForItem:(indexPath.row) inSection:indexPath.section];
    previousIndexPath = newIndexPath;
    [self insertNewRow:newIndexPath];
}
-(void)scrollTableViewToIndex:(NSIndexPath*)index
{
    [self.tableView scrollToRowAtIndexPath:index atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}
- (void)DeleteRow:(NSIndexPath*)indexPath
{
    [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
}
- (void)insertNewRow:(NSIndexPath*)indexPath
{
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationBottom];
}

- (void) callApplyFilterWithTag:(NSInteger)tag
{
    if (tag >= 0  && tag < filtersItem.count)
        [self.adelegate FlightPageSortFlightDataCalledWithSortKey:filtersItem[tag] isRoundTrip:_OneWayIndex ];
    DLog(@"%@", filtersItem[tag]);
}
- (void) removeAppliedFilter
{
    [self.adelegate flightPageRemoveAppliedSortingWithRoundTrip:_OneWayIndex ];
}
- (IBAction)filterButtonPressed:(id)sender
{
    FlightFilterViewController *filterView = [self.storyboard instantiateViewControllerWithIdentifier:@"FlightFilterViewController"];
    filterView.flightSearchData = [self.adelegate getFlightSearchData];
    filterView.filterItem = filterItem;
    filterView.adelegate = self;

    UINavigationController *navigationController = [[UINavigationController alloc] initWithNavigationBarClass:[FMNavigationBar class] toolbarClass:nil];
    [navigationController setViewControllers:@[filterView]];

    [self.navigationController presentViewController:navigationController animated:YES completion:nil];
}
- (void) collectionViewDidSelectMenuItem:(NSInteger)index
{
	DLog(@"%@",_flightSearchData[newIndexPath.row-1]);
	
    [self.adelegate flightlistingsViewController:self DidSelectFlight:_flightSearchData[newIndexPath.row-1] forIndexPath:newIndexPath];
}
-(void)FlightFilterViewDidDissmissWithFilterItem:(id)item
{
    filterItem = (FlightFilterItem*)item;
    [self.adelegate filterFlightDataWithFilterItem:item isRoundTrip:_OneWayIndex];
}
-(void)flightFilterViewClearAllFilters:(BOOL)clear
{
    filterItem = [[FlightFilterItem alloc] init];
    [self.adelegate flightPageDidSelectClearAllFilters:self.OneWayIndex];
}
-(void)showFlightFareTrendController
{
    FareTrendViewController *fareTrendView = [[FareTrendViewController alloc] initWithNibName:@"FareTrendViewController" bundle:nil];
    fareTrendView.searchItem = self.searchItem;
    //  fareTrendView.changePrice = self.flightSearchData[0];
    fareTrendView.view.backgroundColor = [UIColor clearColor];
    [fareTrendView setTransitioningDelegate:_transitionController];
    fareTrendView.modalPresentationStyle= UIModalPresentationCustom;
    [self.navigationController presentViewController:fareTrendView animated:nil completion:nil];//:rulesView animated:YES];
    fareTrendView.adelegate = self;
    self.view.alpha = 0.5;
}

#pragma mark

-(void)fareTrendViewControllerDidDissmissed:(id)sender
{
    self.view.alpha = 1.0;
}
-(void)fareTrendViewControllerDidDissmissedWithDate:(NSDate *)date
{
    self.view.alpha = 1.0;
    [self.adelegate flightPageDidChangeDepartureDate:date];
}
- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    NSInteger tag = [item tag];
    
    switch (tag) {
        case 0:
        case 1:
        case 2:
            [self callApplyFilterWithTag:tag];
            break;
        case 3:
            [self showFlightFareTrendController];
            break;
        case 4:
            [self filterButtonPressed:nil];
            break;
        default:
            break;
    }
}
@end
