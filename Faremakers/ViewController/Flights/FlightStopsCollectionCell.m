//
//  FlightStopsCollectionCell.m
//  Faremakers
//
//  Created by Mac1 on 23/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "FlightStopsCollectionCell.h"
#import "AirLinePricedItinerary.h"
#import "AirPort.h"
#import "FlightSegment.h"

@implementation FlightStopsCollectionCell

- (void)awakeFromNib
{
    [self.airLineTitleLabel changeFontFamily];
    [self.travellTimeLabel changeFontFamily];
    [self.originTitleLabel changeFontFamily];
    [self.destTitleLabel changeFontFamily];
    [self.deparTimeLabel changeFontFamily];
    [self.arivalTimeLabel changeFontFamily];
}
- (void)setFlightSegmentItem:(FlightSegment *)flightSegmentItem
{
    _flightSegmentItem = flightSegmentItem;
    [self populateSegment];
}
- (void)populateSegment
{
    OperatingAirline *operatingAirline = _flightSegmentItem.operatingAirline;
    _airLineTitleLabel.text = [NSString stringWithFormat:@"%@-%@",operatingAirline.code,operatingAirline.flightNumber];
    _travellTimeLabel.text = [_flightSegmentItem stringFromTimeInterval:_flightSegmentItem.elapsedTime.floatValue * 60];//[_flightSegmentItem stringFromTimeInterval:[_flightSegmentItem.arrivalDateTime timeIntervalSinceDate:_flightSegmentItem.departureDateTime]];//
    AirPort *depAirPort = _flightSegmentItem.departureAirport;
    _originTitleLabel.text = depAirPort.locationCode;
    _deparTimeLabel.text = [FlightSegment getTimeStringFromDate:_flightSegmentItem.departureDateTime];
    AirPort *airAirPort = _flightSegmentItem.arrivalAirport;
    _destTitleLabel.text = airAirPort.locationCode;
    _arivalTimeLabel.text = [FlightSegment getTimeStringFromDate:_flightSegmentItem.arrivalDateTime];
}
@end
