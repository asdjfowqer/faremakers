//
//  FareTrendViewController.m
//  Chart
//
//  Created by Hipolito Arias on 1/6/15.
//  Copyright (c) 2015 Hipolito Arias. All rights reserved.
//

#import "FareTrendViewController.h"
#import "FlightFareCalendar.h"
#import "FareTrendCollectionCell.h"
#import "NSDate+DateComponents.h"
#import "FlightDataApiManager.h"
#import "FlightSearchItem.h"
#import "AirLinePricedItinerary.h"
#import "AirItineraryPricingInfo.h"
#import "AirItinTotalFare.h"
#import "AirBaseFare.h"
#import "Constants.h"

@interface FareTrendViewController (){
    NSArray *data;
    NSArray *data3;
    NSInteger SelectedIndex;
    __weak IBOutlet UICollectionView *dateCollectionView;
    __weak IBOutlet UIScrollView *scrollView;
    __weak IBOutlet UIBarButtonItem *fareTrendBarItem;
    __weak IBOutlet UIBarButtonItem *doneBarItem;
}
@property ( nonatomic) IBOutlet HACBarChart *chart2;

@end

@implementation FareTrendViewController

#define iphone6PlusWidth 500.0

- (IBAction)DoneButtonPressed:(id)sender
{
    [self.adelegate fareTrendViewControllerDidDissmissed:sender];
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.travellLabel.text = [NSString stringWithFormat:@"%@ - %@",self.searchItem.originAbr , self.searchItem.DestinationAbr];
    [dateCollectionView registerNib:[UINib nibWithNibName:@"FareTrendDateCell" bundle:nil]
         forCellWithReuseIdentifier:@"FareTrendDateCell"];
    //  [dateCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"dateCollectionCell"];
    
    [self updateTitleBarItem];
    [self positionAllViewToBottom];
    SelectedIndex = -1;
    [self getPromotionDataFromLocalFile];
   
    DLog(@"doneBarItem.width %f",doneBarItem.width);
    //Center Align 'Fare Trend' title bar item
    self.flexibleBarItem.width = self.flexibleBarItem.width * ([UIScreen mainScreen].bounds.size.width)/iphone6PlusWidth;
    CGRect frame =  self.activityView.frame ;
    frame.origin.x = ([UIScreen mainScreen].bounds.size.width)/2;
    self.activityView.frame = frame;
    //
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}
- (void)positionAllViewToBottom
{
    DLog(@"%f",[UIScreen mainScreen].bounds.size.height);
    CGFloat scrollViewY = [UIScreen mainScreen].bounds.size.height - scrollView.frame.size.height;
    [scrollView setFrame:CGRectMake(scrollView.frame.origin.x,scrollViewY ,scrollView.frame.size.width, scrollView.frame.size.height)];
    [self.toolbarHeader setFrame:CGRectMake(self.toolbarHeader.frame.origin.x,scrollViewY - (self.toolbarHeader.frame.size.height) ,self.toolbarHeader.frame.size.width, self.toolbarHeader.frame.size.height)];
    
}
- (void)layoutEver:(CGSize)size
{
    [dateCollectionView setHidden:NO];
    UICollectionViewFlowLayout *flowLayout = (id)dateCollectionView.collectionViewLayout;
    
    float MaxWidht = (size.width < self.view.frame.size.width)?self.view.frame.size.width:size.width;
    dateCollectionView.frame = CGRectMake(2,dateCollectionView.frame.origin.y ,MaxWidht, dateCollectionView.frame.size.height);
    self.dateView.frame = CGRectMake(self.dateView.frame.origin.x,self.dateView.frame.origin.y ,MaxWidht, dateCollectionView.frame.size.height);
    
    _chart2 = [[HACBarChart alloc] initWithFrame:CGRectMake(0,12 ,size.width,size.height - 12)];
    [scrollView addSubview:_chart2];
    [scrollView sendSubviewToBack:_chart2];
    
    CGFloat scrollViewY = self.view.frame.size.height - scrollView.frame.size.height;
    [scrollView setFrame:CGRectMake(scrollView.frame.origin.x,scrollViewY ,self.view.frame.size.width, scrollView.frame.size.height)];
    
    [self.toolbarHeader setFrame:CGRectMake(self.toolbarHeader.frame.origin.x,scrollViewY - (self.toolbarHeader.frame.size.height) ,self.toolbarHeader.frame.size.width, self.toolbarHeader.frame.size.height)];
    
    [flowLayout invalidateLayout]; //force the elements to get laid out again with the new size
    
 }

#pragma mark dateCollectionView

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(KFareCalanderBarWidth, 20);
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (data3.count >= 30) {
        return 30;
    }
    return data3.count;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"FareTrendDateCell" forIndexPath:indexPath];
    UILabel *label = [cell viewWithTag:707];
    if (label) {
        [label setText:[NSString stringWithFormat:@"%ld Feb",(indexPath.row+1)]];
    }
    FlightFareCalendar *airCalendar = data3[indexPath.row];
    [label setText:[NSString stringWithFormat:@"%@ %@",[airCalendar.DepartureDateTime getDayOfTheMonth],[airCalendar.DepartureDateTime getMonth]]];
    // [label setText:airCalendar.DepartureDateTime];
    return cell;
}

- (void)updateTitleBarItem {
    
    fareTrendBarItem.enabled=NO;
    
    [fareTrendBarItem setTitleTextAttributes:
     [NSDictionary dictionaryWithObject:[UIColor whiteColor]
                                 forKey:NSForegroundColorAttributeName]
                                    forState:UIControlStateDisabled];
}

- (void)reloadDataGraph
{
    [self.activityView stopAnimating];
    
    NSMutableArray *dataCharts = [[NSMutableArray alloc] init];
    
    for (int i=0; i<data3.count; i++) {
        
        if (i == 30) {
            break;
        }
        FlightFareCalendar *fareCalendar = data3[i];
        LowestFare *lowestFare =fareCalendar.lowestFare;
        NSString *lable = [NSString stringWithFormat:@"%@ %@",lowestFare.Fare,fareCalendar.CurrencyCode];
        NSNumber *calFare = [NSNumber numberWithLong:lowestFare.Fare.integerValue];// * 100];
        if (i%2 == 0)
            [dataCharts addObject:@{kHACPercentage:calFare, kHACColor  : [UIColor colorWithRed:0.44 green:0.66 blue:0.86 alpha:.5], kHACCustomText : lable}];
        else
            [dataCharts addObject:@{kHACPercentage:calFare,  kHACColor  : [UIColor colorWithRed:0.43 green:0.62 blue:0.92 alpha:1.0], kHACCustomText : lable}];
        
        
    }
    
    data = [[NSArray alloc] initWithArray:dataCharts];
    
    CGSize size  = scrollView.contentSize;
    size.width = (data.count*(KFareCalanderBarWidth));
    size.height = scrollView.frame.size.height;
    [scrollView setContentSize:size];
    
    [self layoutEver:size];
    
    
    _chart2.showAxis                 = NO;   // Show axis line
    _chart2.showProgressLabel        = YES;   // Show text for bar
    _chart2.vertical                 = YES;   // Orientation chart
    _chart2.reverse                  = NO;   // Orientation chart
    _chart2.showDataValue            = YES;   // Show value contains _data, or real percent value
    _chart2.showCustomText           = YES;   // Show custom text, in _data with key kHACCustomText
    _chart2.barsMargin               = 2;     // Margin between bars
    _chart2.sizeLabelProgress        = KFareCalanderBarWidth;    // Width of label progress text
    _chart2.numberDividersAxisY      = 8;
    _chart2.animationDuration        = 2;
    //   _chart.axisMaxValue              = 2000;    // If no define maxValue, get maxium of _data
    _chart2.progressTextColor        = [UIColor darkGrayColor];
    _chart2.axisYTextColor           = [UIColor colorWithRed:0.80 green:0.80 blue:0.80 alpha:1.0];
    _chart2.progressTextFont         = [UIFont fontWithName:@"DINCondensed-Bold" size:12];
    _chart2.typeBar                  = HACBarType2;
    _chart2.dashedLineColor          = [UIColor colorWithRed:0.44 green:0.66 blue:0.86 alpha:.3];
    _chart2.axisXColor               = [UIColor colorWithRed:0.44 green:0.66 blue:0.86 alpha:1.0];
    _chart2.axisYColor               = [UIColor colorWithRed:0.44 green:0.66 blue:0.86 alpha:1.0];
    _chart2.data = data;
    
    ////// CHART SET DATA
    [_chart2 draw];
    [dateCollectionView reloadData];
    _chart2.exclusiveTouch = YES;
    _chart2.multipleTouchEnabled = YES;
    _chart2.userInteractionEnabled = YES;
    
    scrollView.delaysContentTouches = NO;
    [scrollView setDelaysContentTouches:NO];
    
    UITapGestureRecognizer *singlTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(methodName:)];
    singlTap.numberOfTapsRequired = 1; //Default value for cancelsTouchesInView is YES, which will prevent buttons to be clicked
    [scrollView addGestureRecognizer:singlTap];
    
}
-(void)methodName:(UITapGestureRecognizer *)gesture
{
    UIView* view = gesture.view;
    CGPoint loc = [gesture locationInView:view];
    UIView* subview = [view hitTest:loc withEvent:nil];
    if ([view isKindOfClass:[UIScrollView class]] ) {
        
        NSInteger index = floor(loc.x/KFareCalanderBarWidth);
        [self userDidSelectFareItem:index];
        DLog(@"%ld",index);
        
    }
}
- (void)userDidSelectFareItem:(NSInteger)index
{
    if (index >= 0 && index < data3.count) {
        
        SelectedIndex = index;
        [self showDateChangeAlert];
    }
}

-(void)callChangeSelectedDate
{
    [self dismissViewControllerAnimated:YES completion:^{
        FlightFareCalendar *fareItem = data3[SelectedIndex];
        [self.adelegate fareTrendViewControllerDidDissmissedWithDate:fareItem.DepartureDateTime];
        
    }];
}
- (void)getPromotionDataFromLocalFile
{
    NSDictionary *dict = [self.searchItem preparePostDataForFareCalender];
    [FlightDataApiManager getFareTrendForFlights:dict withBaseCurrency:@"" success:^(id responseobject) {

        data3 = [FlightFareCalendar getFareCalendarArray:responseobject forCurrentDate:[dict objectForKey:@"DepartureDate"]];
        //data3 = [[NSArray alloc]initWithArray:responseobject];
        [self reloadDataGraph];
        
    } failureBlock:^(NSError *error) {
        [self showFareNotFoundError];
    }];
}
-(void)showDateChangeAlert
{
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Change Date"
                                                                   message:@"Are you sure you want to change the selected Date"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* changeAction = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action) {
                                                             [self callChangeSelectedDate];
                                                         }];
    
    UIAlertAction *noAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                       style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                           
                                                           
                                                       }]; // 2
    [alert addAction:changeAction];
    [alert addAction:noAction];
    [self presentViewController:alert animated:YES completion:nil];
}
-(NSString*)getBasePrice
{
    AirItineraryPricingInfo *pricingInfo = self.airLineItem.airItineraryPricingInfo[0];
    return pricingInfo.itinTotalFare.baseFare.currencyCode;
}
- (void)showFareNotFoundError
{
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"Fare Trends"
                                                                              message:@"No Data Found!"
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self DoneButtonPressed:nil];
        
    } ]];
    [self presentViewController:alertController animated:YES completion:nil];
    
}
@end
