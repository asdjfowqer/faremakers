//
//  PromotionDetailController.m
//  Faremakers
//
//  Created by Mac1 on 18/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "FlightFilterViewController.h"
#import "IQActionSheetPickerView.h"
#import "AirCustomeData.h"
#import "AirLinePricedItinerary.h"
#import "FlightFilterCell.h"
#import "FlightFilterItem.h"

@interface FlightFilterViewController()<UITableViewDataSource,UITabBarControllerDelegate,IQActionSheetPickerViewDelegate>
{
    NSArray *uniqueAirLines;
    NSInteger selectedIndex;
    NSInteger noOfSections;
}

@end

@implementation FlightFilterViewController


-(void) viewDidLoad
{
    [super viewDidLoad];
    
    noOfSections = 5;
    
    self.navigationItem.title = @"Filter";
    
    UIBarButtonItem *clrButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"Clear Filters" style:UIBarButtonItemStyleDone target:self action:@selector(clrFilterButtonClicked:)];
    self.navigationItem.leftBarButtonItem = clrButtonItem;
    
    
    UIBarButtonItem *doneButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleDone target:self action:@selector(doneButtonClicked:)];
    self.navigationItem.rightBarButtonItem = doneButtonItem;
    
    NSMutableArray *airLinesArray = [[NSMutableArray alloc] init];
    
    for (AirLinePricedItinerary *airObject in self.flightSearchData) {
        
        for (AirCustomeData *airCustomeObject in airObject.customeArray) {
            
            [airLinesArray addObjectsFromArray:airCustomeObject.allAirLines];
        }
    }
    
    selectedIndex = -1;
    uniqueAirLines = [airLinesArray valueForKeyPath:@"@distinctUnionOfObjects.airLine"];
    
}

-(void)doneButtonClicked:(id)sender
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}
-(void)clrFilterButtonClicked:(id)sender
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    [self.adelegate flightFilterViewClearAllFilters:YES];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    switch (section) {
//        case 0:
//            return 0;
//            break;
//        case 1:
//            return 88;
//            break;
//        case 2:
//            return 66;
//            break;
//        default:
//            break;
//    }
//    return 0;
//}
- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return nil;
            break;
        case 1:
            return @"Flight Duration";
            break;
        case 2:
            return @"Flight Layover Duration";
            break;
        case 3:
            return @"Departure";
            break;
        case 4:
            return @"Airlines";
            break;
        default:
            break;
    }
    return nil;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return noOfSections;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:
        case 1:
        case 2:
            return 55;
            break;
        case 3:
            return 88;
            break;
        case 4:
            return 66;
            break;
        default:
            break;
    }
    return 55;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return 2;
            break;
        case 1:
        case 2:
        case 3:
            return 1;
            break;
        case 4:
            return uniqueAirLines.count;
            break;
        default:
            break;
    }
    return 0;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger index = indexPath.section;
    
    NSString *cellIdentifier = [@"FlightFilterCell" stringByAppendingFormat:@"%ld",(long)index ];
    FlightFilterCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    cell.customArray = uniqueAirLines;
    cell.filterItem = self.filterItem;
    
    [cell loadCellWithIdentifier:cellIdentifier ForIndexPath:indexPath];
    
    if (indexPath.section == (noOfSections - 1))//1 is flight section
    {
        if ([self.filterItem.airLines containsObject:uniqueAirLines[indexPath.row]])
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        else
            cell.accessoryType = UITableViewCellAccessoryNone;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0)
    {
        switch (indexPath.row) {
            case 0:{
                self.filterItem.nonRefunded = !self.filterItem.nonRefunded;
            }
                break;
            case 1:
            {
                self.filterItem.nonStop = !self.filterItem.nonStop;
            }
                break;
                
            default:
                break;
        }
        [self.tableView reloadData];
        //[self showPickerView];
    }
    else if (indexPath.section == (noOfSections - 1))
    {
        selectedIndex = indexPath.row;
        
        if ([self.filterItem.airLines containsObject:uniqueAirLines[indexPath.row]])
        {
            [self.filterItem.airLines removeObject:uniqueAirLines[indexPath.row]];
        }
        else
            [self.filterItem.airLines  addObject:uniqueAirLines[indexPath.row]];
        
        [self.tableView reloadData];
    }
    
}
- (void)showPickerView
{
    IQActionSheetPickerView *picker = [[IQActionSheetPickerView alloc] initWithTitle:@"Select No Of Travellers" delegate:self];
    
    picker.titleFont = [UIFont fontWithName:KCustomeFont size:14];
    [picker setTag:3];
    
    [picker setTitlesForComponents:@[@[@"0",@"1", @"2", @"3"]]];
    
    [picker show];
}
-(void)actionSheetPickerView:(IQActionSheetPickerView *)pickerView didSelectTitles:(NSArray *)titles
{
    if (pickerView.tag == 3)
    {
        NSString *str = titles[0];
        self.filterItem.totalStops = [NSNumber numberWithInteger:str.integerValue];
    }
    [self.tableView reloadData];
}
- (void)actionSheetPickerViewDidCancel:(IQActionSheetPickerView *)pickerView
{
    self.filterItem.totalStops = nil;
    [self.tableView reloadData];
    
}

- (IBAction)ApplyFilterButtonPressed:(id)sender;
{
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        [self.adelegate FlightFilterViewDidDissmissWithFilterItem:self.filterItem];
        
    }];
}

@end
