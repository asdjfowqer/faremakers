//
//  PromotionDetailCell.m
//  Faremakers
//
//  Created by Mac1 on 19/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "FlightFilterCell.h"
#import "AirCustomeData.h"
#import "AirLinesNameData.h"
#import "FlightFilterItem.h"
#import "FlightFilterColletionCell.h"

@implementation FlightFilterCell

- (void)awakeFromNib
{
    [self.airLineCode changeFontFamily];
    [self.airLineTitle changeFontFamily];
    [self.titleLabel changeFontFamily];
}
- (IBAction)sliderValueChanged:(id)sender { //Flight Duration
    
    UISlider *slider = (UISlider*)sender;
    int newStep = roundf(slider.value);
    
    // Convert "steps" back to the context of the sliders values.
    slider.value = newStep;
    self.sliderDurationLabel.text = [NSString stringWithFormat:@"Duration: (%d hours)",newStep];
    self.filterItem.flightDuration = newStep;
}

- (IBAction)layoverSliderValueChanged:(UISlider *)sender { //Flight Layover Duration
    
    int newStep = roundf(sender.value);
    sender.value = newStep;
    self.layoverSliderLabel.text = [NSString stringWithFormat:@"Duration: (%d hours)",newStep];
    self.filterItem.layoverDuration = newStep;
}

- (void)loadCellWithIdentifier:(NSString *)cellIdentifier ForIndexPath:(NSIndexPath*)index
{
    // Get Last componet from CellIdenfifier
    int value = [[cellIdentifier substringFromIndex: [cellIdentifier length] - 1] intValue];
    
    switch (value)
    {
        case 0:
            [self loadFirstCellWithIndex:index];
            break;
        case 1:
            [self loadSecondCellWithIndex:index];
            break;
        case 2:
            [self loadThirdCellWithIndex:index];
            break;
        case 3:
            [self loadForthCellWithIndex:index];
            break;
        case 4:
            [self loadFifthCellWithIndex:index];
            break;
            
        default:
            break;
    }
}

-(void)loadFirstCellWithIndex:(NSIndexPath*)index
{
    switch (index.row)
    {
        case 0:
        {
            self.titleLabel.text = @"Non Refunded";
            self.checkedImageView.image = (self.filterItem.nonRefunded)?[UIImage imageNamed:@"checked_icn"]:[UIImage imageNamed:@"uncheck_icn"];
            
        }
            break;
        case 1:
        {
            self.titleLabel.text = @"Non Stop";
            self.checkedImageView.image = (self.filterItem.nonStop)?[UIImage imageNamed:@"checked_icn"]:[UIImage imageNamed:@"uncheck_icn"];
            
        }
            break;
        default:
            break;
    }
}
-(void)loadSecondCellWithIndex:(NSIndexPath*)index
{
    self.durationSlider.value = self.filterItem.flightDuration;
    self.sliderDurationLabel.text = [NSString stringWithFormat:@"Duration: (%ld hours)",(long)self.filterItem.flightDuration];

}
-(void)loadThirdCellWithIndex:(NSIndexPath*)index
{
    self.layoverDurationSlider.value = self.filterItem.layoverDuration;
    self.layoverSliderLabel.text = [NSString stringWithFormat:@"Duration: (%ld hours)",(long)self.filterItem.layoverDuration];

}
-(void)loadForthCellWithIndex:(NSIndexPath*)index
{
    [self loadTimmingCollectionCell];
}
-(void)loadFifthCellWithIndex:(NSIndexPath*)index
{
    NSString *str = self.customArray[index.row];
    _airLineImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@.gif",str]];
    
    _airLineTitle.text = [[AirLinesNameData defaultAirLinesData] getAirlineNameByCode:str];
    _airLineCode.text = str;
    
}

-(void)loadTimmingCollectionCell
{
    _timingCollectionView.delegate = self;
    _timingCollectionView.dataSource = self;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 4;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout  *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.frame.size.width/4, 87);
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    FlightFilterColletionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"FlightFilterColletionCell" forIndexPath:indexPath];
    [cell loadCellWithItem:self.filterItem atIndexPath:indexPath ];
    return cell;
    
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    {
        
        NSInteger time = indexPath.row;
        
        if (self.filterItem.departureTime == time) {
            self.filterItem.departureTime = -1;
        }
        else
            self.filterItem.departureTime = indexPath.row;
        
    }
    
    [collectionView reloadData];
    
}

@end
