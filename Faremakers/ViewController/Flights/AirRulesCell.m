//
//  AirRulesCell.m
//  Faremakers
//
//  Created by Mac1 on 03/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "AirRulesCell.h"

@implementation AirRulesCell

- (void)awakeFromNib
{
    [self.rulesDetailLabel changeFontFamily];
    [self.rulesTitleLabel changeFontFamily];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
