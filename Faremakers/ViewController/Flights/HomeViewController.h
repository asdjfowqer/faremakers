//
//  HomeViewController.h
//  Faremakers
//
//  Created by Mac1 on 18/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

@class FlightSearchItem;
@class ASProgressPopUpView;

@interface HomeViewController : UIViewController

@property (strong, nonatomic) NSArray *flightSearchData;

@property (strong, nonatomic) FlightSearchItem *searchItem;

@property (weak, nonatomic) IBOutlet ASProgressPopUpView *progressView1;

@property (weak, nonatomic) IBOutlet UIView *progressBarView;


-(void)searchFlightDataWithDictionary:(NSDictionary*)dict;

@end

