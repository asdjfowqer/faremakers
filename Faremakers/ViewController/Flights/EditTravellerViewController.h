//
//  EditTravellerViewController.h
//  Faremakers
//
//  Created by Mac1 on 29/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "XLFormViewController.h"

@class Passengers;

typedef enum : NSUInteger
{
    KAdult,
    KChild,
    KInfant,
} Person;

@interface EditTravellerViewController : XLFormViewController

@property (nonatomic) Person person;

@property (nonatomic, strong) Passengers *passenger;

@end