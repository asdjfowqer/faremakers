//
//  PromotionDetailController.h
//  Faremakers
//
//  Created by Mac1 on 18/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//


#import "RESideMenu.h"

@protocol FlightFilterViewDelegate <NSObject>

- (void) FlightFilterViewDidDissmissWithFilterItem:(id)item;
- (void) flightFilterViewClearAllFilters:(BOOL)clear;


@end

@class PromotionDetailHeader;
@class Promotions;
@class FlightFilterItem;

@interface FlightFilterViewController : UIViewController

@property (weak, nonatomic) id<FlightFilterViewDelegate> adelegate;

@property (strong, nonatomic) NSArray *flightSearchData;

@property (copy, nonatomic) FlightFilterItem *filterItem;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)ApplyFilterButtonPressed:(id)sender;

@end
