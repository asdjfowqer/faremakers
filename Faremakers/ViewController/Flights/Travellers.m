//
//  Travellers.m
//  Faremakers
//
//  Created by Mac1 on 29/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "Travellers.h"
#import "NSDate+DateComponents.h"
#import "NSDate+DateTools.h"

@implementation Passengers

- (id)init
{
    if (self = [super init])
    {
        self.title = nil;
        self.fName = nil;
        self.lName = nil;
        self.dob = nil;
    }
    return self;
}

- (BOOL)isPassengerInfoAvaialble
{
    if (!self.fName || self.fName.length < 1)
    {
        return false;
    }
    return true;
}

- (NSString *)convertIntoString
{
    if (self.title)
    {
        return [NSString stringWithFormat:@"%@ %@ %@", self.title, self.fName, self.lName];
    }
    if (self.dob)
    {
        return [NSString stringWithFormat:@"%@ %@ %@", self.fName, self.lName, self.dob];
    }
    return @"";
}

- (NSDictionary *)getPassengersDetail
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:self.title forKey:@"title"];
    [dict setObject:self.fName forKey:@"firstName"];
    [dict setObject:self.lName forKey:@"lastName"];
    [dict setObject:self.dob forKey:@"dob"];

    return dict;
}

- (NSDictionary *)getPassengersDetailForFlightTransaction
{
    DLog(@"_dob %@", [_dob class]);

    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:self.title forKey:@"Title"];
    [dict setObject:self.fName forKey:@"FirstName"];
    [dict setObject:self.lName forKey:@"LastName"];
    [dict setObject:[_dobUser getDayOfTheMonth] forKey:@"Day"];
    [dict setObject:[NSString stringWithFormat:@"%d",[_dobUser month]] forKey:@"Month"];
    [dict setObject:[_dobUser getFourDigitYear] forKey:@"Year"];

    return dict;
}

@end

@implementation Travellers

- (id)initWithNumberOfTravellers:(NSInteger)adults
{
    if (self = [super init])
    {
        self.Adults = adults;
        self.Childs = 0;
        self.Infants = 0;
    }
    return self;
}

- (NSInteger)totalTravellers
{
    return (self.Adults + self.Childs + self.Infants);
}

- (NSArray *)getPassengersWithType:(NSString *)type
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"type = %@", type];
    return [self.passengersInfo filteredArrayUsingPredicate:predicate];
}

@end
