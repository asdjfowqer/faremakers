//
//  FlightStopsCollectionCell.h
//  Faremakers
//
//  Created by Mac1 on 23/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//


#import <UIKit/UIKit.h>

@class FlightSegment;

@interface FlightStopsCollectionCell : UICollectionViewCell

@property (strong, nonatomic) FlightSegment *flightSegmentItem;
@property (weak, nonatomic) IBOutlet UILabel *airLineTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *travellTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *originTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *deparTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *destTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *arivalTimeLabel;

@end
