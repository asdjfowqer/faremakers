//
//  FlightSearchItem.h
//  Faremakers
//
//  Created by Mac1 on 29/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@class Travellers, DoubleDateItem, Cities;

@interface FlightSearchItem : NSObject

@property (nonatomic , retain) NSString *classType;
@property (nonatomic , retain) DoubleDateItem *doubleDate;
@property (nonatomic , retain) NSString *origin;
@property (nonatomic , retain) NSString *Destination;
@property (nonatomic , retain) NSString *originAbr;
@property (nonatomic , retain) NSString *DestinationAbr;
@property (nonatomic , retain) Travellers *travellers;
@property (nonatomic , readwrite) BOOL is_roundTrip;
@property (nonatomic , retain) Cities *originCity;
@property (nonatomic , retain) Cities *departCity;


- (NSDictionary*)prepareDictForPostData;
- (NSDictionary*)preparePostDataForFareCalender;
- (NSDictionary*)preparePOstDAtaWithNewDeparDate:(NSDate*)departureDate;
- (BOOL)checkAllValues;
- (id)initWithTempData;
- (void)updateAllValues;

@end
