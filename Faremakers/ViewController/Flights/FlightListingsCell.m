//
//  FlightListingsCell.m
//  Faremakers
//
//  Created by Mac1 on 21/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "FlightListingsCell.h"
#import "AirLinePricedItinerary.h"
#import "AirItineraryPricingInfo.h"
#import "AirItinTotalFare.h"
#import "AirCustomeData.h"
#import "AirBaseFare.h"
#import "AirLinesNameData.h"

@implementation FlightListingsCell

- (void)awakeFromNib
{
    [self.flightCost changeFontFamily];
    [self.flightTime changeFontFamily];
    [self.flightTravelTime changeFontFamily];
    [self.airlineTittle changeFontFamily];
}
- (void)setAirLineItem:(AirLinePricedItinerary *)airLineItem withTripIndex:(NSInteger)index
{
    _airLineItem = airLineItem;
    [self populateSegment:index];

}

- (void)setAirLineItem:(AirLinePricedItinerary *)airLineItem 
{
    _airLineItem = airLineItem;
    //[self populateSegment];
}
- (NSString*)getTimeStringFromDate:(NSDate*)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    return [dateFormatter stringFromDate:date];
}
- (void)populateSegment:(NSInteger)index
{
    AirCustomeData *customeData = _airLineItem.customeArray[index];
    
    _flightTime.text = [customeData getTimeOnlyFromDate:customeData.departureDateTime];
    _flightTravelTime.text = [customeData getTimeOnlyFromDate:customeData.arrivalDateTime];

    AirItineraryPricingInfo *pricingInfo = [_airLineItem.airItineraryPricingInfo objectAtIndex:0];
    
    _flightCost.text = [NSString stringWithFormat:@"%@ %@", KCurrencyCode, pricingInfo.itinTotalFare.totalFare.amount];
    _airlineTittle.text = [[AirLinesNameData defaultAirLinesData] getAirlineNameByCode:customeData.airLine];
    _airLineImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@.gif",customeData.airLine]];
}
@end
