//
//  MainScreenViewController.h
//  GoibiboStoryBoarding
//
//  Created by Mac1 on 15/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//


#import "RESideMenu.h"
#import "MainMenuColletionView.h"

@interface MainScreenViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet MainMenuColletionView *menuCollection;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint* collectionViewTopMargin;

@end
