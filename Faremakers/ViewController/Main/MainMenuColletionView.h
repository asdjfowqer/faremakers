//
//  MainMenuColletionView.h
//  Faremakers
//
//  Created by Mac1 on 17/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//


#import "MainColloctionViewDelegate.h"

@interface MainMenuColletionView : UICollectionView < UICollectionViewDataSource, UICollectionViewDelegate >

@property (nonatomic,weak)id<MainColloctionViewDelegate> aDelegate;

-(void)LoadMenuItemsForColltionView:(UICollectionView*)collectionView;

@end
