//
//  MainScreenViewController.m
//  Faremakers
//
//  Created by Mac1 on 16/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "MainMenuViewController.h"
#import "UIViewController+RESideMenu.h"
#import "LogInWithEmailViewController.h"
#import <UIImageView+Letters.h>
#import "ShareActivityView.h"
#import "UILabel+FontName.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "FMUser.h"

@interface MainMenuViewController ()
{
    ShareActivityView *shareActivity;
    NSDictionary *sectionContents;
    NSArray *sectionTitles;
    
}
@property (strong, readwrite, nonatomic) UITableView *tableView;

@end

@implementation MainMenuViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    sectionTitles = @[@"Booking",@"Others"];
	
	if ([[FMUser defaultUser] user])
	{
//		sectionContents = [[NSDictionary alloc] initWithObjectsAndKeys:@[@"Home", @"Hotels", @"Flights", @"Cars",@"Holidays",@"Umrah Packages"],[sectionTitles objectAtIndex:0],@[@"Profile", @"Bookings",@"Promotions", @"Notifications", @"Log Out"],[sectionTitles objectAtIndex:1], nil];
		sectionContents = [[NSDictionary alloc] initWithObjectsAndKeys:@[@"Home",@"Flights",@"Hotels",@"Umrah Packages",@"Holidays",@"Cars"],[sectionTitles objectAtIndex:0],@[@"Bookings",@"Promotions", @"Log Out"],[sectionTitles objectAtIndex:1], nil];

	}
	else
	{
//		sectionContents = [[NSDictionary alloc] initWithObjectsAndKeys:@[@"Home", @"Hotels", @"Flights", @"Cars",@"Holidays",@"Umrah Packages"],[sectionTitles objectAtIndex:0],@[@"Profile", @"Bookings",@"Promotions", @"Notifications"],[sectionTitles objectAtIndex:1], nil];
		sectionContents = [[NSDictionary alloc] initWithObjectsAndKeys:@[@"Home",@"Flights",@"Hotels",@"Umrah Packages",@"Holidays",@"Cars"],[sectionTitles objectAtIndex:0],@[@"Promotions"],[sectionTitles objectAtIndex:1], nil];

	}
	
	
    if ([FMUser defaultUser].displayName !=nil)
    {
        self.nameLabel.text = [FMUser defaultUser].displayName;
        [self.nameImageView setImageWithString:self.nameLabel.text color:nil circular:YES fontName:KCustomeFont];
    }
    else
    {
        self.nameLabel.text = @"Guest User";
        [self.nameImageView setImageWithString:self.nameLabel.text color:nil circular:YES fontName:KCustomeFont];
    }
    [self.nameLabel changeFontFamily];
    
    self.tableView = ({
        UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, (80), self.view.frame.size.width, self.view.frame.size.height-(120)) style:UITableViewStyleGrouped];
        tableView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth;
        tableView.delegate = self;
        tableView.dataSource = self;
        tableView.opaque = NO;
        tableView.backgroundColor = [UIColor clearColor];
        tableView.backgroundView = nil;
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        //     tableView.bounces = NO;
        tableView.scrollsToTop = NO;
        tableView.showsVerticalScrollIndicator = NO;
        tableView;
    });
    [self.view addSubview:self.tableView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(UpdateUserNameLabel:) name:@"UpdateUserNameLabel" object:nil];
    
    self.nameImageView.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showUserProfileView:)];
    tapGesture.numberOfTapsRequired = 1;
    [self.nameImageView addGestureRecognizer:tapGesture];
    
}

- (void) showUserProfileView:(id)gesture
{
    if ([FMUser defaultUser].user)
    {
        NSString * viewContrIdentifier = @"UserManagementController";
        if (viewContrIdentifier!=nil)
        {
            UINavigationController *navigationControlller = [[UINavigationController alloc] initWithNavigationBarClass:[FMNavigationBar class] toolbarClass:nil];
            [navigationControlller setViewControllers:@[[self.storyboard instantiateViewControllerWithIdentifier:viewContrIdentifier]]];
            
            [self.sideMenuViewController setContentViewController:navigationControlller
                                                         animated:YES];
            [self.sideMenuViewController hideMenuViewController];
        }
    }
    else
    {
        LogInWithEmailViewController *loginView = [self.storyboard instantiateViewControllerWithIdentifier:@"LogInWithEmailViewController"];
        
        UINavigationController *navigationControlller = [[UINavigationController alloc] initWithNavigationBarClass:[FMNavigationBar class] toolbarClass:nil];
        [navigationControlller setViewControllers:@[loginView]];
        [self.sideMenuViewController setContentViewController:navigationControlller
                                                     animated:YES];
        [loginView setUpRightNavigationItem];
        loginView.isHomePageNeed = true;
        [self.sideMenuViewController hideMenuViewController];
    }
    
}
- (void)UpdateUserNameLabel:(NSNotification*)notifi
{
    self.nameLabel.text = [FMUser defaultUser].displayName;
    [self.nameImageView setImageWithString:self.nameLabel.text color:nil circular:YES fontName:KCustomeFont];
	
	
	if ([[FMUser defaultUser] user])
	{

//        sectionContents = [[NSDictionary alloc] initWithObjectsAndKeys:@[@"Home",@"Flights",@"Hotels",@"Umrah Packages",@"Holidays",@"Cars"],[sectionTitles objectAtIndex:0],@[@"Bookings",@"Promotions", @"Log Out"],[sectionTitles objectAtIndex:1], nil];
        sectionContents = [[NSDictionary alloc] initWithObjectsAndKeys:@[@"Home",@"Flights",@"Hotels",@"Umrah Packages",@"Holidays"],[sectionTitles objectAtIndex:0],@[@"Bookings",@"Promotions", @"Log Out"],[sectionTitles objectAtIndex:1], nil];

	}
	else
	{

//		sectionContents = [[NSDictionary alloc] initWithObjectsAndKeys:@[@"Home",@"Flights",@"Hotels",@"Umrah Packages",@"Holidays",@"Cars"],[sectionTitles objectAtIndex:0],@[@"Promotions"],[sectionTitles objectAtIndex:1], nil];
        sectionContents = [[NSDictionary alloc] initWithObjectsAndKeys:@[@"Home",@"Flights",@"Hotels",@"Umrah Packages",@"Holidays"],[sectionTitles objectAtIndex:0],@[@"Promotions"],[sectionTitles objectAtIndex:1], nil];

	}
	
	[self.tableView reloadData];

}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}


#pragma mark -
#pragma mark UITableView Datasource

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView* headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, [self.tableView sectionHeaderHeight])];
    
    UILabel* headerLabel = [[UILabel alloc] init];
    headerLabel.frame = CGRectMake(10, 0, 300, 40);
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.textColor = [UIColor colorWithRed:0.80f green:0.80f blue:0.80f alpha:1.0];
    //headerLabel.font = [UIFont boldSystemFontOfSize:15];
    [headerLabel changeFontFamily];
    headerLabel.text = sectionTitles[section];
    
    [headerView addSubview:headerLabel];
    
    return headerView;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 40;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1;
}
- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return sectionTitles[section];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 66;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return sectionTitles.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    NSArray *arrayContent = [sectionContents objectForKey:[sectionTitles objectAtIndex:sectionIndex]];
    
    return arrayContent.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        [cell.textLabel changeFontFamily:KCustomeFont WithSize:18.0];
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.textLabel.highlightedTextColor = [UIColor lightGrayColor];
        cell.selectedBackgroundView = [[UIView alloc] init];
    }
	
	
	NSArray* images;
	
	if ([[FMUser defaultUser] user])
	{
		images = @[@[@"Menu_Home",@"Menu_Flights",@"Menu_Hotel",@"Menu_Umrah",@"Menu_Holiday",@"Menu_Cars"], @[@"Menu_Account",@"Menu_Booking",@"Menu_Promotions",@"Menu_Notification",@"Menu_Logout"]];
	}
	else
	{
		images = @[@[@"Menu_Home",@"Menu_Flights",@"Menu_Hotel",@"Menu_Umrah",@"Menu_Holiday",@"Menu_Cars"], @[@"Menu_Account",@"Menu_Booking",@"Menu_Promotions",@"Menu_Notification"]];
	}

    NSArray *arrayContent = [sectionContents objectForKey:[sectionTitles objectAtIndex:indexPath.section]];
    
    cell.textLabel.text = arrayContent[indexPath.row];
    cell.imageView.image = [UIImage imageNamed:images[indexPath.section][indexPath.row]];
    return cell;
}

- (IBAction)shareButtonPressed:(id)sender
{
    NSURL *myWebsite = [NSURL URLWithString:@"https://www.faremakers.com/"];
    shareActivity = [[ShareActivityView alloc] initWithActivityItems:@[myWebsite] applicationActivities:nil];
    [shareActivity showItSelf];
}
- (IBAction)alertButtonPressed:(id)sender
{
    
}
#pragma mark -

#pragma mark UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 2)
    {
        switch (indexPath.row) {
            case 0:
                [self settingsButtonPressed:nil];
                break;
            default:
                break;
        }
        
    }
    else
    {
        [self showViewControllerForIndexPath:indexPath];
    }
}
-(void)showViewControllerForIndexPath:(NSIndexPath*)indexPath
{
    NSString *viewContrIdentifier;
    
    if (indexPath.section == 0) {
        
        switch (indexPath.row) {
            case 0:
                viewContrIdentifier = @"MainScreenViewController";
                break;
            case 2:
                viewContrIdentifier = @"HotelSearchViewController";
                break;
            case 1:
                viewContrIdentifier = @"FlightSearchViewController";
                break;
            case 5:
                viewContrIdentifier = @"CarSearchViewController";
                break;
            case 4:
                viewContrIdentifier = @"HolidaysListingsViewController";
                break;
            case 3:
                viewContrIdentifier = @"UmrahPackageViewController";
                break;
            default:
                break;//
        }
    }
    else
    {
//        switch (indexPath.row) {
//            case 0:
//                viewContrIdentifier = @"UserManagementController";
//                break;
//            case 1:
//                viewContrIdentifier = @"BookingListingsViewController";
//                break;
//            case 2:
//                viewContrIdentifier = @"PromotionHomeController";
//                break;
//            case 3:
//                viewContrIdentifier = @"NotificationsViewController";
//                break;
//            case 4:
//                viewContrIdentifier = nil;
//                [self settingsButtonPressed:nil];
//                break;
//            default:
//                break;
//        }
	    
	    if ([FMUser defaultUser].user)
	    {
		    switch (indexPath.row) {
			    case 0:
				    viewContrIdentifier = @"BookingListingsViewController";
				    break;
			    case 1:
				    viewContrIdentifier = @"PromotionHomeController";
				    break;
			    case 2:
				    viewContrIdentifier = nil;
				    [self settingsButtonPressed:nil];
			    default:
				    break;
		    }
	    }
	    else
	    {
		    switch (indexPath.row) {
			    case 0:
				    viewContrIdentifier = @"PromotionHomeController";
				    break;
			    default:
				    break;
		    }
	    }
	    
    }
    if (viewContrIdentifier!=nil)
    {
        UINavigationController *navigationControlller = [[UINavigationController alloc] initWithNavigationBarClass:[FMNavigationBar class] toolbarClass:nil];
        [navigationControlller setViewControllers:@[[self.storyboard instantiateViewControllerWithIdentifier:viewContrIdentifier]]];

        [self.sideMenuViewController setContentViewController:navigationControlller
                                                     animated:YES];
        [self.sideMenuViewController hideMenuViewController];
    }
}

- (IBAction)settingsButtonPressed:(id)sender
{
    [[FMUser defaultUser] logOutUser];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}
@end
