//
//  MainScreenViewController.h
//  Faremakers
//
//  Created by Mac1 on 16/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//


#import "RESideMenu.h"

@interface MainMenuViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, RESideMenuDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *nameImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

- (IBAction)settingsButtonPressed:(id)sender;
- (IBAction)shareButtonPressed:(id)sender;
- (IBAction)alertButtonPressed:(id)sender;
-(void)showViewControllerForIndexPath:(NSIndexPath*)indexPath;

@end
