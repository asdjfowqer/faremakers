//
//  MainScreenViewController.m
//  GoibiboStoryBoarding
//
//  Created by Mac1 on 15/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "MainScreenViewController.h"
#import "LogInWithEmailViewController.h"
#import "MainColloctionViewDelegate.h"
#import "MainMenuColletionView.h"
#import "FlightDataApiManager.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "FMWebViewController.h"
#import "TransitionDelegate.h"
#import "AppDelegate.h"
#import "FMUser.h"

@interface MainScreenViewController () <MainColloctionViewDelegate, FMWebViewDelegate, loginVCDidSuccessLogin>
{
    NSArray *tableViewData;
}

@end

@implementation MainScreenViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    tableViewData = @[@"You", @"Bookings"];
    [self.menuCollection LoadMenuItemsForColltionView:self.menuCollection];
    self.menuCollection.aDelegate = self;
    [self adjustMarginsForLagerHeights];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[FMUser defaultUser] isSessionAlive] ? 2 : 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"KProfileViewCell"];

    if (indexPath.row == 0)
    {
        cell.textLabel.text = [tableViewData objectAtIndex:(indexPath.section + indexPath.row)];

        if ([FMUser defaultUser].displayName != nil)
        {
            cell.detailTextLabel.text = [FMUser defaultUser].displayName;
        }
        else
        {
            cell.detailTextLabel.text = @"Sign In / Sign Up";
        }
    }
    else
    {
        cell.textLabel.text = [tableViewData objectAtIndex:(indexPath.section + indexPath.row)];
        cell.detailTextLabel.text = @"";
    }
    cell.imageView.image = [UIImage imageNamed:@"first"];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    if (indexPath.row == 0 && ![[FMUser defaultUser] isSessionAlive])
    {
        LogInWithEmailViewController *loginView = [self.storyboard instantiateViewControllerWithIdentifier:@"LogInWithEmailViewController"];

        UINavigationController *navigationControlller = [[UINavigationController alloc] initWithNavigationBarClass:[FMNavigationBar class] toolbarClass:nil];
        [navigationControlller setViewControllers:@[loginView]];
        [loginView setUpRightNavigationItem];
        loginView.isHomePageNeed = true;
        loginView.loginDelegate = self;
        [self.navigationController presentViewController:navigationControlller animated:YES completion:nil];
    }
    else
    {
        [self showUserProfileView:indexPath.row];
    }
}

#pragma mark MaincolloctionViewDelegate
- (void)collectionViewDidSelectMenuItem:(NSInteger)index
{
    //    [self postBookingData];
    //    return;
    ////    FMWebViewController *webVC = [[FMWebViewController alloc] initWithNibName:@"FMWebViewController" bundle:nil];
    ////    [self.navigationController presentViewController:webVC animated:YES completion:nil];
    ////    return;
    NSString *viewContrIdentifier = nil;

    switch (index)
    {
        case 1:
            viewContrIdentifier = @"HotelSearchViewController";
            break;

        case 0:
            viewContrIdentifier = @"FlightSearchViewController";
            break;

        case 4:
            viewContrIdentifier = @"CarSearchViewController";
            break;

        case 3:
            viewContrIdentifier = @"HolidaysListingsViewController";
            break;

        case 2:
            viewContrIdentifier = @"UmrahPackageViewController";
            break;

        default:
            break;//
    }
    if (viewContrIdentifier != nil)
    {
        [self presentViewControllerWithIdentifier:viewContrIdentifier];
    }
}

- (void)postBookingData
{
    [SVProgressHUD show];
    [FlightDataApiManager uploadBookingDetailData:nil success: ^(id responseobject)
     {
         [SVProgressHUD dismiss];

         [AppDelegate sharedDelegate].transactionId =  [[responseobject objectForKey:@"data"] objectForKey:@"transactionId"];
         FMWebViewController *webVC = [[FMWebViewController alloc] initWithNibName:@"FMWebViewController" bundle:nil];
         webVC.html = [[responseobject objectForKey:@"data"] objectForKey:@"html"];
         webVC.adelegate = self;
         [self.navigationController presentViewController:webVC animated:YES completion:nil];
     }
                                     failureBlock: ^(NSError *error)
     {
         [SVProgressHUD dismiss];
     }];
}

- (void)showUserProfileView:(NSInteger)index
{
    switch (index)
    {
        case 0:
        {
            UINavigationController *navigationControlller = [[UINavigationController alloc] initWithNavigationBarClass:[FMNavigationBar class] toolbarClass:nil];
            [navigationControlller setViewControllers:@[[self.storyboard instantiateViewControllerWithIdentifier:@"UserManagementController"]]];

            [self.sideMenuViewController setContentViewController:navigationControlller
                                                         animated:YES];
            [self.sideMenuViewController hideMenuViewController];
        }
            break;

        case 1:
        {
            UINavigationController *navigationControlller = [[UINavigationController alloc] initWithNavigationBarClass:[FMNavigationBar class] toolbarClass:nil];
            [navigationControlller setViewControllers:@[[self.storyboard instantiateViewControllerWithIdentifier:@"BookingListingsViewController"]]];
            [self.sideMenuViewController setContentViewController:navigationControlller
                                                         animated:YES];
            [self.sideMenuViewController hideMenuViewController];
        }
            break;

        default:
            break;
    }
}

- (void)adjustMarginsForLagerHeights
{
    self.collectionViewTopMargin.constant = self.collectionViewTopMargin.constant * ([UIScreen mainScreen].bounds.size.height / 480);
}

- (void)callPaidBookingPayment
{
    [SVProgressHUD show];
    [FlightDataApiManager uploadPaidBookingDetail:[NSDictionary dictionaryWithObjectsAndKeys:[AppDelegate sharedDelegate].transactionId, @"transactionId", nil] success: ^(id responseobject)
     {
         [SVProgressHUD dismiss];
         DLog(@"%@", responseobject);
     }
                                     failureBlock: ^(NSError *error)
     {
         [SVProgressHUD dismiss];
     }];
}

#pragma FMWebViewDelegate

- (void)fmWebViewDidRedirectedToPaymentURL
{
    [self callPaidBookingPayment];
}

#pragma LoginViewDelegate

-(void)LogInWithEmailViewControllerDidLoggedInSuccussfully:(LogInWithEmailViewController *)ViewController{
    [self showFlightSearchViewController];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [ViewController dismissViewControllerAnimated:YES completion:nil];
    });
}

#pragma LoginViewDelegate

-(void)showFlightSearchViewController
{
    NSString *viewContrIdentifier = @"FlightSearchViewController";
    [self presentViewControllerWithIdentifier:viewContrIdentifier];
}
-(void)presentViewControllerWithIdentifier:(NSString*)viewContrIdentifier{
    
    if (viewContrIdentifier!=nil)
    {
        UINavigationController *navigationControlller = [[UINavigationController alloc] initWithNavigationBarClass:[FMNavigationBar class] toolbarClass:nil];
        [navigationControlller setViewControllers:@[[self.storyboard instantiateViewControllerWithIdentifier:viewContrIdentifier]]];
        
        [self.sideMenuViewController setContentViewController:navigationControlller
                                                     animated:YES];
        [self.sideMenuViewController hideMenuViewController];
    }
}
@end
