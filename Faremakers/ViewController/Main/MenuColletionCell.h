//
//  MenuColletionCell.h
//  Faremakers
//
//  Created by Mac1 on 17/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//


#import <UIKit/UIKit.h>

@interface MenuColletionCell : UICollectionViewCell

@property (nonatomic , weak) IBOutlet UILabel *MenutitleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end
