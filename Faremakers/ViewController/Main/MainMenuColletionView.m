//
//  MainMenuColletionView.m
//  Faremakers
//
//  Created by Mac1 on 17/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "MainMenuColletionView.h"
#import "MenuColletionCell.h"

@implementation MainMenuColletionView

#define KMainViewCollectionCell @"MenuColletionCell"


-(void)LoadMenuItemsForColltionView:(UICollectionView*)collectionView
{
    
    collectionView.dataSource = self;
    collectionView.delegate = self;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 4;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MenuColletionCell *cell = [self dequeueReusableCellWithReuseIdentifier:KMainViewCollectionCell forIndexPath:indexPath];
    NSArray *items = @[@"Flights",@"Hotels",@"Umrah Packages",@"Holidays",@"Cars",@"Notification"];
    NSArray *imageItems = @[@"Flight_icon",@"Hotels_icon",@"Umrah_icon",@"icon_holiday",@"Cars_icon",@"Credit"];

    cell.MenutitleLabel.text = items[indexPath.row];
    cell.imageView.image = [UIImage imageNamed:imageItems[indexPath.row]];
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self.aDelegate collectionViewDidSelectMenuItem:indexPath.row];
    
}
@end
