//
//  HolidayFilterViewController.h
//  Faremakers
//
//  Created by Mac1 on 18/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "HolidayFilterViewController.h"
#import "IQActionSheetPickerView.h"
#import "FilterDetailViewController.h"
#import "HolidayFilterDetailVC.h"
#import "HolidayFilterItem.h"
#import "HolidayFilterCell.h"

@interface HolidayFilterViewController()<UITableViewDataSource,UITabBarControllerDelegate,IQActionSheetPickerViewDelegate>
{
    NSArray *headerTitles;
    NSInteger selectedIndex;
}

@end

@implementation HolidayFilterViewController


-(void) viewDidLoad
{
    [super viewDidLoad];
    
    headerTitles = @[@"Select Price Range"];//@[@"Select Price Range",@"Select Location",@"Select Categories"];
    self.navigationItem.title = @"Filter";
    
    UIBarButtonItem *doneButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleDone target:self action:@selector(doneButtonClicked:)];
    self.navigationItem.rightBarButtonItem = doneButtonItem;
    
    UIBarButtonItem *clrButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"Clear Filters" style:UIBarButtonItemStyleDone target:self action:@selector(clrFilterButtonClicked:)];
    self.navigationItem.leftBarButtonItem = clrButtonItem;

}

-(void)doneButtonClicked:(id)sender
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return headerTitles.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
        return 55;
    return 45;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 25;
}
- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
   return headerTitles[section];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger index = indexPath.section;
    
    index = index?1:0;
    
    NSString *cellIdentifier = [@"HolidayFilterCell" stringByAppendingFormat:@"%ld",(long)index ];
    HolidayFilterCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    cell.item = self.filterItem;
    [cell loadCellWithIdentifier:cellIdentifier ForIndexPath:indexPath];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section > 0)
    {
        HolidayFilterDetailVC *detailView = [[HolidayFilterDetailVC alloc] initWithStyle:UITableViewStylePlain];
        detailView.item = self.filterItem;
        detailView.filterType = (int)(indexPath.section -1);
      
        [self.navigationController pushViewController:detailView animated:YES];
    }
}

- (IBAction)ApplyFilterButtonPressed:(id)sender;
{
    [self.adelegate holidaysfilterViewWillDismiss];
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        [self.adelegate holidaysFilterViewDidDissmissWithFilterItem:self.filterItem];

    }];
}
-(void)clrFilterButtonClicked:(id)sender
{
    [self.adelegate holidaysfilterViewWillDismiss];
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        [self.adelegate holidaysfilterViewDidSelectClrAllFilters:YES];
    }];
    
}
@end
