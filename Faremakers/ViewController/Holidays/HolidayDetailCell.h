//
//  HolidayDetailCollectionCellCollectionViewCell.h
//  Faremakers
//
//  Created by Mac1 on 04/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HolidayItem;

@interface HolidayDetailCell : UICollectionViewCell

@property (nonatomic,strong) HolidayItem *holidayItem;

#pragma mark HolidayDetailCell0

@property (weak, nonatomic) IBOutlet UILabel *holidayTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *holidayDetailLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

#pragma mark HolidayDetailCell1

@property (weak, nonatomic) IBOutlet UILabel *holidayAccTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *holidayAccDetailLabel;

#pragma mark HolidayDetailCell2
@property (weak, nonatomic) IBOutlet UILabel *holidayPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *holidayPriceNote;

#pragma mark HolidayDetailCell3
@property (weak, nonatomic) IBOutlet UILabel *holidayVisaLabel;
@property (weak, nonatomic) IBOutlet UILabel *holidayVisaNoteLabel;

#pragma mark HolidayDetailCell4

@property (weak, nonatomic) IBOutlet UILabel *holidayTourTitle;
@property (weak, nonatomic) IBOutlet UILabel *holidayTourDetail;
@property (weak, nonatomic) IBOutlet UILabel *holidayTourExtras;

-(void)setHolidayItem:(HolidayItem *)holidayItem withIdentifier:(NSString*)cellIdentifier;

@end
