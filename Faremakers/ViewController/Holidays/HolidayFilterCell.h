//
//  HolidayFilterCell.h
//  Faremakers
//
//  Created by Mac1 on 02/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <UIKit/UIKit.h>


@class HolidayFilterItem;
@interface HolidayFilterCell : UITableViewCell <UICollectionViewDataSource,UICollectionViewDelegate>

@property (nonatomic) HolidayFilterItem *item;

#pragma mark HolidayFilterItem 0
@property (weak, nonatomic) IBOutlet UICollectionView *priceCollectionView;

#pragma mark HolidayFilterItem 1
@property (weak, nonatomic) IBOutlet UILabel *locationsLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationTitleLabel;

- (void)loadCellWithIdentifier:(NSString *)cellIdentifier ForIndexPath:(NSIndexPath*)index;

@end
