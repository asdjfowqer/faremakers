//
//  FlightBookingViewController.m
//  Faremakers
//
//  Created by Mac1 on 29/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "HolidayBookingViewController.h"
#import "HolidayBookingCell.h"
#import "TransitionDelegate.h"
#import "BookingPromtView.h"

@interface HolidayBookingViewController ()
{
    NSArray *SectionsData;
    NSArray *DataArray;
    NSArray *flightsDataArray;

}

@property (nonatomic, strong) TransitionDelegate *transitionController;

@end

@implementation HolidayBookingViewController

//layover_img@2x,devider@3x
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Review Booking";
    SectionsData = @[@"Booking Detail",@"Fare Details"];
    flightsDataArray = @[@"a"];
    DataArray = @[@[@"Baggage",@"Fare Rules"],
                  @[@"Base Fare", @"Surcharges",@"Fee and Texs",@"Grand Total"],
                  @[@"$ 200",@"$ 20",@"$ 35",@"$ 235"]];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [SectionsData objectAtIndex:section];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
      return  flightsDataArray.count;
    NSArray *array = [DataArray objectAtIndex:section];
    return array.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 )
    {
        return  120;
    }
    return 55;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return [self prepareCellForTableView:tableView forIndexPath:indexPath];
}
- (id)prepareCellForTableView:(UITableView*)tableView forIndexPath:(NSIndexPath*)indexPath
{
    if (indexPath.section == 0)
    {
        
            HolidayBookingCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"HolidayBookingCell0" forIndexPath:indexPath];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
            return cell;
    }
    else
    {
        HolidayBookingCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"HolidayBookingCell1" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.chargesDetailLabel.text = DataArray [indexPath.section][indexPath.row];
        cell.chargesCostLabel.text = DataArray [indexPath.section + 1][indexPath.row];
        
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (IBAction)continueBtnSelector:(id)sender
{
    [self showBookingPromtViewController];
}
-(void)showBookingPromtViewController
{
    BookingPromtView *rulesView = [self.storyboard instantiateViewControllerWithIdentifier:@"BookingPromtView"];
    rulesView.view.backgroundColor = [UIColor clearColor];
    [rulesView setTransitioningDelegate:_transitionController];
    rulesView.modalPresentationStyle= UIModalPresentationCustom;
    [self.navigationController presentViewController:rulesView animated:nil completion:nil];//:rulesView animated:YES];
    
}
@end
