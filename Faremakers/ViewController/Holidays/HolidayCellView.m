//
//  HolidayViewCell.m
//  Faremakers
//
//  Created by Mac1 on 23/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "HolidayCellView.h"
#import "IQActionSheetPickerView.h"
#import "NSDate+DateComponents.h"
#import "UILabel+FontName.h"

@interface HolidayCellView()<IQActionSheetPickerViewDelegate>

@end

@implementation HolidayCellView


- (void)awakeFromNib
{
    [self.dateLabel changeFontFamily];
    [self.destinationTitle changeFontFamily];
    [self.durationLabel changeFontFamily];
    
    // Initialization code
}

-(void)showPickerViewForIndex:(NSIndexPath*)indexPath
{
 
    [self showTotalDayPickerView];
}
-(void)showDatePickerViewForIndex:(NSIndexPath*)indexPath
{
    [self showDatePicker];
}


- (void)showDatePicker
{
    IQActionSheetPickerView *picker = [[IQActionSheetPickerView alloc] initWithTitle:@"Select Date" delegate:self];
    [picker setTag:6];
    [picker setActionSheetPickerStyle:IQActionSheetPickerStyleDatePicker];
    [picker show];
}

-(void)actionSheetPickerView:(IQActionSheetPickerView *)pickerView didSelectTitles:(NSArray *)titles
{
    self.durationLabel.text = [NSString stringWithFormat:@"%@", [titles objectAtIndex:0]];
}

-(void)actionSheetPickerView:(IQActionSheetPickerView *)pickerView didSelectDate:(NSDate *)date
{
    switch (pickerView.tag)
    {
        case 6:
        {
            self.dateLabel.text = [NSString stringWithFormat:@"%@ %@ ' %@", [date getDayOfTheMonth],[date getMonth],[date getYear]];
        }
            break;
        default:
            break;
    }
}
- (void)showTotalDayPickerView
{
    IQActionSheetPickerView *picker = [[IQActionSheetPickerView alloc] initWithTitle:@"Select Duration" delegate:self];
    picker.titleFont = [UIFont fontWithName:KCustomeFont size:14];
    [picker setTag:1];
    [picker setTitlesForComponents:@[@[@"Any", @"1-3 Nights", @"4-7 Nights",@"+7 Nights"]]];
    [picker show];
}
@end
