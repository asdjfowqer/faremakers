//
//  HolidayFilterDetailVC.h
//  Faremakers
//
//  Created by Mac1 on 21/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    Location,
    Themes
} HolidayFilterType;


@class HolidayFilterItem;

@interface HolidayFilterDetailVC : UITableViewController

@property (nonatomic,strong)HolidayFilterItem *item;

@property (nonatomic) HolidayFilterType filterType;


@end
