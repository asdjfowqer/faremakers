//
//  HolidayListingsTabController
//  Faremakers
//
//  Created by Mac1 on 30/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "HolidayListingsTabController.h"
#import "HolidaysListingsViewController.h"
#import <ASProgressPopUpView/ASProgressPopUpView.h>
#import <MRProgress/MRProgress.h>
#import "HotelDataApiManger.h"
#import "CarbonKit.h"
#import "Constants.h"


@interface HolidayListingsTabController () <CarbonTabSwipeNavigationDelegate,HotelDataApiSearchDelegate>
{
    NSArray *items;
    NSArray *viewControllers;
    CarbonTabSwipeNavigation *carbonTabSwipeNavigation;
    UIActivityIndicatorView *activityIndicator;
    HotelDataApiManger *hotelApiManger;
    float progress;
}
@end

@implementation HolidayListingsTabController

-(void)loadView
{
    [super loadView];
    
    UIView *view = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [view setBackgroundColor:[UIColor whiteColor]];
    
    self.view = view;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Holidays";
    
    items = @[@"All", @"Top Rated"];
    
    HolidaysListingsViewController *viewController1 = [self instantiateViewControllerWithIdentifier:@"HolidaysListingsViewController"];
    HolidaysListingsViewController *viewController2 = [self instantiateViewControllerWithIdentifier:@"HolidaysListingsViewController"];
    viewControllers = @[viewController1,viewController2];
    
    carbonTabSwipeNavigation = [[CarbonTabSwipeNavigation alloc] initWithItems:items delegate:self];
    [carbonTabSwipeNavigation insertIntoRootViewController:self];
    
    //[[MRNavigationBarProgressView progressViewForNavigationController:self.navigationController] setProgress:0.005 animated:YES];
    
    [self style];
    // [self ShowActivityView];
//    [viewController1 getHolidayData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    
}
-(void)searchHotelDataWithDictionary:(NSDictionary*)dict
{
    hotelApiManger = [[HotelDataApiManger alloc] init];
    hotelApiManger.adelegate = self;
    [hotelApiManger searchHotelDataForItem:dict];
}

#pragma mark HotelApiManager Delegate

-(void) HotelDataManagerStartProgressing:(float)progres;
{
    progress = progres;
    dispatch_async(dispatch_get_main_queue(), ^(void){
        [[MRNavigationBarProgressView progressViewForNavigationController:self.navigationController] setProgress:progress animated:YES];
    });
    
}

-(void) HotelDataManagerDidRecieveResponse:(id)object
{
    //   _flightSearchData = object;
    [self ShowActivityView];
    
}
-(void) HotelDataManagerDidFail:(id)object
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"No Data Found!" preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action)
                                {
                                    // [self.navigationController popViewControllerAnimated:YES];
                                    [self ShowActivityView];
                                    
                                }]];
    [self.navigationController presentViewController:alertController animated:YES completion:nil];
    
}

- (void)style {
    
    UIColor *color = [UIColor colorWithRed:255.0/255 green:255.0/255 blue:255.0/255 alpha:1];
    carbonTabSwipeNavigation.toolbar.translucent = NO;
    [carbonTabSwipeNavigation setIndicatorColor:color];
    [carbonTabSwipeNavigation setTabExtraWidth:30];
    
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:[UIScreen mainScreen].bounds.size.width/2.74 forSegmentAtIndex:0];
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:[UIScreen mainScreen].bounds.size.width/2.74 forSegmentAtIndex:1];
    [carbonTabSwipeNavigation.carbonTabSwipeScrollView setBackgroundColor:[UIColor colorWithRed:24.0/255 green:74.0/255 blue:150.0/255 alpha:1]];
    
    // Custimize segmented control
    [carbonTabSwipeNavigation setNormalColor:[UIColor colorWithRed:255.0/255 green:255.0/255 blue:255.0/255 alpha:0.5]
                                        font:[UIFont fontWithName:KCustomeFont size:14]];
    [carbonTabSwipeNavigation setSelectedColor:color
                                          font:[UIFont fontWithName:KCustomeFont size:14]];
    [carbonTabSwipeNavigation.carbonTabSwipeScrollView setScrollEnabled:false];
    
    
}

# pragma mark - CarbonTabSwipeNavigation Delegate
// required
- (nonnull UIViewController *)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbontTabSwipeNavigation
                                 viewControllerAtIndex:(NSUInteger)index {
    return viewControllers[index];
}

// optional
- (void)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
                 willMoveAtIndex:(NSUInteger)index {
    
}

- (void)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
                  didMoveAtIndex:(NSUInteger)index {
    DLog(@"Did move at index: %ld", (unsigned long)index);
}

- (UIBarPosition)barPositionForCarbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation {
    return UIBarPositionTop; // default UIBarPositionTop
}
-(void)ShowActivityView
{
    UIView *view = [self.view viewWithTag:707];
    if (view)
    {
        [view removeFromSuperview];
        [activityIndicator stopAnimating];
    }
    else
    {
        view = [[UIView alloc] initWithFrame:self.view.bounds];
        view.tag = 707;
        [view setBackgroundColor:[UIColor whiteColor]];
        [self.view addSubview:view];
        [self.view bringSubviewToFront:view];
        [self barButtonWithActivityIndicator];
        [activityIndicator startAnimating];
    }
}
- (void)barButtonWithActivityIndicator
{
    activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    [activityIndicator hidesWhenStopped];
    UIBarButtonItem *activityItem = [[UIBarButtonItem alloc] initWithCustomView:activityIndicator];
    self.navigationItem.rightBarButtonItem = activityItem;
}
- (HolidaysListingsViewController*)instantiateViewControllerWithIdentifier:(NSString*)identifier
{
    UIStoryboard *mystoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    return [mystoryboard instantiateViewControllerWithIdentifier:identifier];
}

-(void) viewWillDisappear:(BOOL)animated
{
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound)
    {
        //[self CancleAllTask];
    }
    [super viewWillDisappear:animated];
}

@end
