//
//  CSStickyParallaxHeaderViewController.m
//  CSStickyHeaderFlowLayoutDemo
//
//  Created by James Tang on 6/4/14.
//  Copyright (c) 2014 Jamz Tang. All rights reserved.
//

#import "HolidaysDetailColletionController.h"
#import <MWPhotoBrowser/MWPhotoBrowser.h>
#import "HolidayBookingViewController.h"
#import "TransactionBookingPaidHolidays.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "CSStickyHeaderFlowLayout.h"
#import "CheckOutRootController.h"
#import "TransitionDelegate.h"
#import "BookingApiManager.h"
#import "HolidayDetailCell.h"
#import "BookingPromtView.h"
#import "NSString+Height.h"
#import "AminitiesView.h"
#import "CheckOutItems.h"
#import "HolidayItem.h"
#import "Constants.h"
#import "FMUser.h"

static NSString *kDetailMovieTableViewCell = @"HolidayDetailCell";

@interface HolidaysDetailColletionController () <MWPhotoBrowserDelegate>
{
    NSMutableArray *photos;
}

@property (nonatomic, strong) TransitionDelegate *transitionController;
@property (nonatomic, strong) NSArray *sections;
@property (nonatomic, strong) UINib *headerNib;

@end

@implementation HolidaysDetailColletionController

#define KStickHeaderHeight 250.f

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.headerNib = [UINib nibWithNibName:@"HolidayDetailHeaderView" bundle:nil];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self reloadLayout];

    // self.holidayItem = [HolidaysItem itemWithDictionary:[[NSDictionary alloc] init]];

    // Also insets the scroll indicator so it appears below the search bar
    self.collectionView.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, 0, 0);

    [self.collectionView registerNib:self.headerNib
          forSupplementaryViewOfKind:CSStickyHeaderParallaxHeader
                 withReuseIdentifier:@"header"];
    [self createBookHotelButtonView];
    [self updateNavigationBarTitle];
}

- (void)updateNavigationBarTitle
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 5, self.view.frame.size.width, 44)];
    label.backgroundColor = [UIColor clearColor];
    label.numberOfLines = 2;
    label.font = [UIFont fontWithName:KCustomeRagularFont size:16.0];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor blackColor];
    label.text = @"Holiday Package";
    [label sizeToFit];
    self.navigationItem.titleView = label;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    //[self.navigationController setNavigationBarHidden:NO animated:NO];
}

//-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
//    currentOffset = scrollView.contentOffset.y;
//}
//
//-(void)scrollViewDidScroll:(UIScrollView *)scrollView
//{
//    DLog(@"%f",scrollView.contentOffset.y);
//    CGFloat scrollPos = scrollView.contentOffset.y + self.navigationController.navigationBar.frame.size.height;
//
//    if(scrollPos >= KStickHeaderHeight){
//            [self.navigationController setNavigationBarHidden:NO animated:NO];
//    } else {
//        [self.navigationController setNavigationBarHidden:YES animated:NO];
//    }
//}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //  [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [self reloadLayout];
}

- (void)reloadLayout
{
    CSStickyHeaderFlowLayout *layout = (id)self.collectionViewLayout;

    if ([layout isKindOfClass:[CSStickyHeaderFlowLayout class]])
    {
        layout.parallaxHeaderReferenceSize = CGSizeMake(self.view.frame.size.width, KStickHeaderHeight);
        layout.parallaxHeaderMinimumReferenceSize = CGSizeMake(self.view.frame.size.width, KStickHeaderHeight);
        layout.itemSize = CGSizeMake(self.view.frame.size.width, layout.itemSize.height);
        layout.parallaxHeaderAlwaysOnTop = NO;

        // If we want to disable the sticky header effect
        layout.disableStickyHeaders = YES;
    }
}

#pragma mark UICollectionViewDataSource

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    return CGSizeMake(self.view.frame.size.width, 0.1);
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;//[self.sections count];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (self.holidayItem)
    {
        return 5 + self.holidayItem.holidaysTourList.count;//[self.sections[section] count];
    }
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger index = indexPath.row;
    if ((index - 4) >= 0)
    {
        if ((index - 4) >= self.holidayItem.holidaysTourList.count)
        {
            index = 5;
        }
        else
        {
            index = 4;
        }
    }
    NSString *identifier = [kDetailMovieTableViewCell stringByAppendingFormat:@"%ld", (long)index];
    HolidayDetailCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier
                                                                        forIndexPath:indexPath];

    //   cell.holidayItem = self.holidayItem;
    [cell setHolidayItem:self.holidayItem withIdentifier:identifier];

    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height = 20;
    switch (indexPath.row)
    {
        case 0: {
            UIFont *font = [UIFont fontWithName:KCustomeFont size:15.0];

            CGFloat width = collectionView.frame.size.width - 115;

            CGRect titleRect = [NSString
                                setAttributeWithString:self.holidayItem.holidayPackage
                                withLineSpacing:kLineHeightSpacing
                                withSize:CGSizeMake(width, CGFLOAT_MAX)
                                withFont:font];

            CGFloat titleHeight = ceilf(titleRect.size.height);

            height = height + titleHeight + 30;
        }
            break;

        case 1: {
            UIFont *font = [UIFont fontWithName:KCustomeFont size:14.0];

            CGFloat width = collectionView.frame.size.width - 20;

            CGRect titleRect = [NSString
                                setAttributeWithString:self.holidayItem.holidayAccomodDetail
                                withLineSpacing:kLineHeightSpacing
                                withSize:CGSizeMake(width, CGFLOAT_MAX)
                                withFont:font];

            CGFloat titleHeight = ceilf(titleRect.size.height);

            height = height + titleHeight + 20;
        }
            break;

        case 2: {
            UIFont *font = [UIFont fontWithName:KCustomeFont size:14.0];

            CGFloat width = collectionView.frame.size.width - 20;

            CGRect titleRect = [NSString
                                setAttributeWithString:self.holidayItem.holidayPriceDetail
                                withLineSpacing:kLineHeightSpacing
                                withSize:CGSizeMake(width, CGFLOAT_MAX)
                                withFont:font];

            CGFloat titleHeight = ceilf(titleRect.size.height);


            height = height + titleHeight + 20;
        }
            break;

        case 3: {
            UIFont *font = [UIFont fontWithName:KCustomeFont size:14.0];

            CGFloat width = collectionView.frame.size.width - 20;

            CGRect titleRect = [NSString
                                setAttributeWithString:self.holidayItem.holidayVisaDetail
                                withLineSpacing:kLineHeightSpacing
                                withSize:CGSizeMake(width, CGFLOAT_MAX)
                                withFont:font];

            CGFloat titleHeight = ceilf(titleRect.size.height);

            CGRect noteRect = [NSString
                               setAttributeWithString:self.holidayItem.holidayTicketNote
                               withLineSpacing:kLineHeightSpacing
                               withSize:CGSizeMake(width, CGFLOAT_MAX)
                               withFont:font];

            CGFloat noteHeight = ceilf(noteRect.size.height);

            height = height + titleHeight + noteHeight + 50;
        }
            break;

        default:
        {
            NSInteger index = indexPath.row - 4; //self.holidayItem.holidaysTourList.count;
            if (index < self.holidayItem.holidaysTourList.count)
            {
                HolidayTours *holidayTour = self.holidayItem.holidaysTourList[index];

                UIFont *font = [UIFont fontWithName:KCustomeFont size:14.0];

                CGFloat width = collectionView.frame.size.width - 20;

                CGRect titleRect = [NSString
                                    setAttributeWithString:holidayTour.tourTitle
                                    withLineSpacing:kLineHeightSpacing
                                    withSize:CGSizeMake(width, CGFLOAT_MAX)
                                    withFont:font];

                CGFloat titleHeight = ceilf(titleRect.size.height);

                CGRect noteRect = [NSString
                                   setAttributeWithString:holidayTour.tourIncludes
                                   withLineSpacing:kLineHeightSpacing
                                   withSize:CGSizeMake(width, CGFLOAT_MAX)
                                   withFont:font];

                CGFloat noteHeight = ceilf(noteRect.size.height);

                CGRect detailRect = [NSString
                                     setAttributeWithString:holidayTour.tourDetails
                                     withLineSpacing:kLineHeightSpacing
                                     withSize:CGSizeMake(width, CGFLOAT_MAX)
                                     withFont:font];

                CGFloat detailHeight = ceilf(detailRect.size.height);


                height = height + titleHeight + noteHeight + detailHeight + 35;
            }
            else
            {
                height += 30;
            }
        }
            break;
    }

    return CGSizeMake(self.view.frame.size.width, height);
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if ([kind isEqualToString:UICollectionElementKindSectionHeader])
    {
        UICollectionReusableView *cell = [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                                            withReuseIdentifier:@"sectionHeader"
                                                                                   forIndexPath:indexPath];
        return cell;
    }
    else if ([kind isEqualToString:CSStickyHeaderParallaxHeader])
    {
        HolidayDetailHeaderView *cell = [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                                           withReuseIdentifier:@"header"
                                                                                  forIndexPath:indexPath];

        [cell loadHeaderViewWithImageUrl:self.holidayItem.holidayImageUrl];
        cell.adelegate = self;

        return cell;
    }
    return nil;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
}

#pragma mark CSSHeaderSectionDelegate

- (void)backButtonPressed:(id)sender
{
}

- (void)collectionViewDidReceiveTap:(CGPoint)point
{
    [self showImageGallary:0];
}

- (void)collectionViewDidSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self showImageGallary:indexPath.row];
}

- (void)backButtonPressedFromTopHeader:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)showImageGallary:(NSInteger)index
{
    if (!self.holidayItem.holidayImageUrl)
    {
        return;
    }
    photos = [[NSMutableArray alloc] init];
    MWPhoto *photo;
    //  for (NSString *imageLink in self.umrahpackage.imageUrls)
    {
        NSString *urlString = [[BASE_URL stringByDeletingLastPathComponent] stringByAppendingPathComponent:self.holidayItem.holidayImageUrl];
        photo = [MWPhoto photoWithURL:[NSURL URLWithString:urlString]];
        [photos addObject:photo];
    }
    if (photos.count)
    {
        // Create browser
        MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
        browser.displayActionButton = NO;
        browser.displayNavArrows = NO;
        browser.displaySelectionButtons = NO;
        browser.alwaysShowControls = NO;
        browser.zoomPhotosToFill = YES;
        browser.enableGrid = NO;
        browser.startOnGrid = NO;
        browser.enableSwipeToDismiss = NO;
        browser.autoPlayOnAppear = NO;
        [browser setCurrentPhotoIndex:index];
        {
            // Modal
            UINavigationController *navigationControlller = [[UINavigationController alloc] initWithNavigationBarClass:[FMNavigationBar class] toolbarClass:nil];
            [navigationControlller setViewControllers:@[browser]];
            navigationControlller.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            [self presentViewController:navigationControlller animated:YES completion:nil];
        }
    }
}

#pragma mark bookHotelView

- (void)createBookHotelButtonView
{
    UIView *Bookview = [[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height - (CUSTOM_NAVIGATION_HEIGHT + 20) - 44, self.view.frame.size.width, 44)];
    [Bookview setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:Bookview];
    UIButton *bookButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [bookButton setTitle:@"Book Now" forState:UIControlStateNormal];
    [bookButton setFrame:CGRectMake(20, 7, Bookview.frame.size.width - 40, 30)];
    [bookButton setBackgroundImage:[UIImage imageNamed:@"blueButton"] forState:UIControlStateNormal];
    [bookButton addTarget:self action:@selector(continueBtnSelector:) forControlEvents:UIControlEventTouchUpInside];
    [Bookview addSubview:bookButton];

    [self.view bringSubviewToFront:Bookview];
}

- (void)continueBtnSelector:(id)sender
{
    if ([[FMUser defaultUser] isSessionAlive]) {
        [self showBookingPromtViewController];
    }
    else{
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Login" message:@"Please login to book" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alertView show];
    }
}

- (void)showBookingPromtViewController
{
    [SVProgressHUD show];

    TransactionBookingPaidHolidays *transaction = [[TransactionBookingPaidHolidays alloc] init];
    transaction.packageId = self.holidayItem.packageID;

    [TransactionBookingManager shared].currentTransaction = transaction;

    [BookingApiManager postPackageBooking:transaction
                         withSuccessBlock: ^(NSDictionary *responseobject) {
                             [SVProgressHUD dismiss];

                             NSNumber *statusCode = [[responseobject objectForKey:@"data"] objectForKey:@"statusCode"];
                             if ([statusCode integerValue] == 200)
                             {
                                 [self showAlertControllerWithMessage:[[responseobject objectForKey:@"data"] objectForKey:@"serviceResponse"]];
                                 [self.navigationController popToRootViewControllerAnimated:YES];
                             }
                             else
                             {
                                 [self showErrorAlertControllerWithMessage:[[responseobject objectForKey:@"data"] objectForKey:@"serviceResponse"]];
                             }
                         }
                             failureBlock: ^(NSError *error) {
                                 [SVProgressHUD dismiss];
                                 [self showErrorAlertControllerWithMessage:error.localizedRecoverySuggestion];
                             }];
}

- (void)bookHotelButtonSelector:(id)sender
{
    HolidayBookingViewController *holidayBookingView = [self.storyboard instantiateViewControllerWithIdentifier:@"HolidayBookingViewController"];
    [self.navigationController pushViewController:holidayBookingView animated:YES];
}

- (void)showAlertControllerWithMessage:(NSString *)message
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Success" message:message delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil];
    [alertView show];
}

- (void)showErrorAlertControllerWithMessage:(NSString *)message
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:message delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil];
    [alertView show];
}

#pragma mark - MWPhotoBrowserDelegate

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser
{
    return photos.count;
}

- (id <MWPhoto> )photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index
{
    if (index < photos.count)
    {
        return [photos objectAtIndex:index];
    }
    return nil;
}

- (void)photoBrowser:(MWPhotoBrowser *)photoBrowser didDisplayPhotoAtIndex:(NSUInteger)index
{
    DLog(@"Did start viewing photo at index %lu", (unsigned long)index);
}

- (void)photoBrowserDidFinishModalPresentation:(MWPhotoBrowser *)photoBrowser
{
    // If we subscribe to this method we must dismiss the view controller ourselves
    DLog(@"Did finish modal presentation");
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end