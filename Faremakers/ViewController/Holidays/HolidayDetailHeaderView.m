//
//   HolidayDetailHeaderView.m
//  CSStickyHeaderFlowLayoutDemo
//
//  Created by James Tang on 6/4/14.
//  Copyright (c) 2014 Jamz Tang. All rights reserved.
//

#import "HolidayDetailHeaderView.h"
#import "CSStickyHeaderFlowLayoutAttributes.h"
#import <UIImageView+WebCache.h>
#import "HolidayItem.h"
#import "Constants.h"

@interface HolidayDetailHeaderView()
{
    NSUInteger indexNo;
    float Cellheight;
}

@end

@implementation HolidayDetailHeaderView

- (void)applyLayoutAttributes:(CSStickyHeaderFlowLayoutAttributes *)layoutAttributes {
    
    if (layoutAttributes.progressiveness >= 0.95)
    {
       // DLog(@"header always Top");
    }
    else
    {
        //DLog(@"header On Top");
    }
}

- (void)loadHeaderViewWithImageUrl:(NSString *)imageUrl
{    
    if (true)
    {
        UIImage *placeHolder = [UIImage imageNamed:@"placeholder"];
        
        NSString *urlString = [[BASE_URL stringByDeletingLastPathComponent] stringByAppendingPathComponent:imageUrl];

        [self.holidayImageView sd_setImageWithURL:[NSURL URLWithString:urlString]
                                 placeholderImage:placeHolder
                                          options:SDWebImageRetryFailed
                                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
             if(error == nil)
                 self.holidayImageView.image = image;
             else
                 self.holidayImageView.image = placeHolder;//[UIImage imageNamed:@"no-images.png"];
             
         }];
    }
    
      UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapGesture:)];
      tapGesture.numberOfTapsRequired = 1;
      [self addGestureRecognizer:tapGesture];
}
- (void)singleTapGesture:(UITapGestureRecognizer*)sender {
    
    if(sender.state == UIGestureRecognizerStateRecognized)
    {
        CGPoint point = [sender locationInView:sender.view];
        
        [_adelegate collectionViewDidReceiveTap:point];
        
    }
    
}

- (IBAction)shareButtonSelector:(id)sender
{
    
}

- (IBAction)shortListedSelector:(id)sender
{
    
}

- (IBAction)backButtonSelector:(id)sender
{
    [self.adelegate backButtonPressedFromTopHeader:sender];
}

@end
