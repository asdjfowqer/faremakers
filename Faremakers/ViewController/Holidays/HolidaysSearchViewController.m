//
//  HolidaysSearchViewController.m
//  Faremakers
//
//  Created by Mac1 on 16/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "HolidaysSearchViewController.h"
#import "HolidaysListingsViewController.h"
#import "CitySerachViewController.h"
#import "HolidayListingsTabController.h"
#import "CheckOutRootController.h"
#import "CheckOutItems.h"
#import "HolidayCellView.h"

@implementation HolidaysSearchViewController

#define KFlightDetailCell @"HolidayCellView"

-(void)viewDidLoad
{
    [super viewDidLoad];
    [self adjustMarginsForLagerHeights];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return 90;
    }
    if ([UIScreen mainScreen].bounds.size.height == 480)
        return 60;
    return 74;
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger index = indexPath.row;
    NSString *identifier = [KFlightDetailCell stringByAppendingFormat:@"%ld", (long)index];
    
    HolidayCellView *cell = [self.tableView dequeueReusableCellWithIdentifier:identifier];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    switch (indexPath.row) {
        case 0:{
            CitySerachViewController *searchView =  [self.storyboard instantiateViewControllerWithIdentifier:@"CitySerachViewController"];
            searchView.title = @"Select Destination";
            [self.navigationController pushViewController:searchView animated:YES];
        }
            break;
        case 1:{
            
            HolidayCellView *cell = [self.tableView cellForRowAtIndexPath:indexPath];
            [cell showPickerViewForIndex:indexPath];
        }
            break;
        case 2:{
            
            HolidayCellView *cell = [self.tableView cellForRowAtIndexPath:indexPath];
            [cell showDatePickerViewForIndex:indexPath];
        }
            break;
        case 3:{
            
            CitySerachViewController *searchView =  [self.storyboard instantiateViewControllerWithIdentifier:@"CitySerachViewController"];
            searchView.title = @"Select Theme";
            [self.navigationController pushViewController:searchView animated:YES];
        }
            break;
        default:
            break;
    }
    
}

- (IBAction)searchButtonSelector:(id)sender
{
    HolidayListingsTabController *viewContr = [[HolidayListingsTabController alloc] init];
    //viewContr.searchItem = item;
   // [viewContr searchHotelDataWithDictionary:[item preparePostData]];
    [self.navigationController pushViewController:viewContr animated:YES];
}
- (void) adjustMarginsForLagerHeights
{
    if ([UIScreen mainScreen].bounds.size.height <= 568) {
        
        self.searchBtnBottomMargin.constant = 50;
    }
}
@end
