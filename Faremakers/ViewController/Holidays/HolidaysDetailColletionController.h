//
//  CSStickyParallaxHeaderViewController.h
//  CSStickyHeaderFlowLayoutDemo
//
//  Created by James Tang on 6/4/14.
//  Copyright (c) 2014 Jamz Tang. All rights reserved.
//


#import "HolidayDetailHeaderView.h"
#import "HolidayItem.h"

@interface HolidaysDetailColletionController : UICollectionViewController<CSAlwaysOnTopHeaderDelagate>

@property (nonatomic)HolidayItem *holidayItem;

@end
