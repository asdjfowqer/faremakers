//
//  FlightBookingCell.h
//  Faremakers
//
//  Created by Mac1 on 30/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HolidayBookingCell : UITableViewCell

#pragma mark HolidayBookingCell0
@property (weak, nonatomic) IBOutlet UILabel *flightTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *deparTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *deparDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *deparFlightDetail;


#pragma mark HolidayBookingCell1

@property (weak, nonatomic) IBOutlet UILabel *chargesDetailLabel;
@property (weak, nonatomic) IBOutlet UILabel *chargesCostLabel;

@end
