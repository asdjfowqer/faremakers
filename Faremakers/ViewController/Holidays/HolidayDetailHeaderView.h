//
//  CSAlwaysOnTopHeader.h
//  CSStickyHeaderFlowLayoutDemo
//
//  Created by James Tang on 6/4/14.
//  Copyright (c) 2014 Jamz Tang. All rights reserved.
//

#import "DetailHotelCollectionViewCell.h"

@protocol CSAlwaysOnTopHeaderDelagate <NSObject>

- (void) getCollectionSizeOfImage:(CGSize)size atIndex:(NSInteger)index;
- (void) collectionViewDidReceiveTap:(CGPoint)size;
- (void) backButtonPressedFromTopHeader:(id)sender;
- (void) collectionViewDidSelectItemAtIndexPath:(NSIndexPath*)indexPath;

@end

@interface HolidayDetailHeaderView : DetailHotelCollectionViewCell

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *imageCollectionHeight;
@property (weak, nonatomic) IBOutlet UIImageView *holidayImageView;

@property (nonatomic, assign) id<CSAlwaysOnTopHeaderDelagate>  adelegate;


- (IBAction)shareButtonSelector:(id)sender;
- (IBAction)shortListedSelector:(id)sender;
- (IBAction)backButtonSelector:(id)sender;
- (void)loadHeaderViewWithImageUrl:(NSString *)imageUrl;
@end
