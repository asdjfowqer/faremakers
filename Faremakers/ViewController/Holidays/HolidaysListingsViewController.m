//
//  HolidaysListingsViewController.m
//  Faremakers
//
//  Created by Mac1 on 23/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "HolidaysListingsViewController.h"
#import "HolidaysDetailColletionController.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "HolidayFilterViewController.h"
#import "HolidayDataApiManager.h"
#import "HolidaysListViewCell.h"
#import "HolidayFilterItem.h"
#import "HolidayItem.h"
#import "AppDelegate.h"

@interface HolidaysListingsViewController()<HolidaysFilterViewDelegate>
{
    NSArray *tmpHolidayList;
    NSArray *minMaxPrices;
}

@end

@implementation HolidaysListingsViewController


- (void)viewDidLoad
{
    [super viewDidLoad];

    _filterItem = [[HolidayFilterItem alloc] init];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 310;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

    [self ShowActivityView];
    [self getHolidayData];
}
- (void)getHolidayData
{
    [HolidayDataApiManager getHolidayListWithSuccessBlock:^(id response)
     {
         _holidayList = [response mutableCopy];
         tmpHolidayList = [self.holidayList mutableCopy];
        
         if (!tmpHolidayList.count)
         {
             [self ShowActivityView];
             [self showErrorAlert];
         }
         else
         {
             [self prepareFilterPriceRanges];
             [self.tableView reloadData];
             [self ShowActivityView];
         }
        // [self setTableviewContentView];
         
     } faliureBlock:^(NSError *error) {
         [self ShowActivityView];
         [self showErrorAlert];

     }];
}
-(void)prepareFilterPriceRanges
{
    minMaxPrices = [HolidayItem findMinMaxPrices:tmpHolidayList];
    _filterItem.minPrice = minMaxPrices[0];
    _filterItem.maxprice = minMaxPrices[1];
    [_filterItem preparePriceFilterRanges];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return tmpHolidayList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HolidaysListViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"HolidaysListViewCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.item = tmpHolidayList[indexPath.row];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    HolidaysDetailColletionController *detailView = [self.storyboard instantiateViewControllerWithIdentifier:@"HolidaysDetailColletionController"];
    //detailView.hotelSearchItem  = self.searchItem;
    detailView.holidayItem = tmpHolidayList[indexPath.row];
    [self.navigationController pushViewController:detailView animated:YES];

}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController setToolbarHidden:YES];
}
-(void)ShowActivityView
{
    UIView *view = [self.view viewWithTag:707];
    if (view)
    {
        [view removeFromSuperview];
        [[AppDelegate sharedDelegate] hideLoadingViewForView:self];
    }
    else
    {
        view = [[UIView alloc] initWithFrame:self.view.bounds];
        view.tag = 707;
        view.alpha = 0.5;
        
        [view setBackgroundColor:[UIColor whiteColor]];
        [self.view addSubview:view];
        [self.view bringSubviewToFront:view];
        [[AppDelegate sharedDelegate] showLoadingViewForView:self withText:@"Searching Holidays..."];
    }
}

- (void)filterButtonSelector:(id)sender
{
    HolidayFilterViewController *filterView = [self.storyboard instantiateViewControllerWithIdentifier:@"HolidayFilterViewController"];
    filterView.filterItem = self.filterItem;
    filterView.adelegate = self;

    UINavigationController *navigationControlller = [[UINavigationController alloc] initWithNavigationBarClass:[FMNavigationBar class] toolbarClass:nil];
    [navigationControlller setViewControllers:@[filterView]];

    [self.navigationController presentViewController:navigationControlller animated:YES completion:nil];
}

#pragma mark filterViewDelegate

- (void)holidaysFilterViewDidDissmissWithFilterItem:(id)item
{
    _filterItem = (HolidayFilterItem*)item;
    [self perfomFilters];
}

- (void) holidaysfilterViewDidSelectClrAllFilters:(BOOL)clear
{
    [_filterItem clearAllFilters];
    
    [SVProgressHUD showWithStatus:@"Applying Filters"];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        tmpHolidayList = [self.holidayList mutableCopy];
        [self.tableView reloadData];
        [SVProgressHUD showSuccessWithStatus:@"Done"];
    });
}
-(void)holidaysfilterViewWillDismiss
{
    [self setTableviewContentView];
}
- (void) perfomFilters
{
  //  [self setTableviewContentView];
    [SVProgressHUD showWithStatus:@"Applying Filters"];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        NSArray *SearchListCopy = [self.holidayList mutableCopy];
        NSMutableArray *array = [[NSMutableArray alloc] init];
        BOOL filterApplied = false;
        
        if(_filterItem.price)
        {
            NSNumber *startingPrice;
            NSNumber *endingPrice ;
            switch ([[_filterItem preparePriceRangeStrings] indexOfObject:_filterItem.price]) {
                case 0:{
                    startingPrice = [NSNumber numberWithInteger:0];
                    endingPrice = _filterItem.priceFilterRange[0];
                    DLog(@" case 0");
                }
                    break;
                case 1:
                {
                    startingPrice = _filterItem.priceFilterRange[0];
                    endingPrice = _filterItem.priceFilterRange[1];
                    DLog(@" case 1");
                }
                    break;
                case 2:
                {
                    startingPrice = _filterItem.priceFilterRange[1];
                    endingPrice = _filterItem.maxprice;
                    DLog(@" case 2");
                }
                    break;
                default:
                    break;
            }
            [SearchListCopy enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop)
             {
                 HolidayItem *Obj = (HolidayItem*)obj;
                 if (Obj.packagePrice.integerValue > startingPrice.integerValue && Obj.packagePrice.integerValue <= endingPrice.integerValue) {
                     [array addObject:Obj];
                 }
                 
             }];
            
            tmpHolidayList = [array mutableCopy];
            filterApplied = true;
            
        }
        if (!filterApplied)
        {
            tmpHolidayList = [self.holidayList mutableCopy];
        }
        [self.tableView reloadData];
        [self setTableviewContentView];
        [SVProgressHUD showSuccessWithStatus:@"Done"];
    });
    
}
-(void)sortHolidaysPackages
{
    NSArray *sortedArray;
    
    sortedArray = [self.holidayList sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        
        HolidayItem *package1 = obj1;
        HolidayItem *package2 = obj2;
        
        return [package1.packagePrice compare:package2.packagePrice];
        
    }];
    tmpHolidayList = [sortedArray mutableCopy];
    [self.tableView reloadData];
}
- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    if (_holidayList.count < 1) {
        return;
    }
    NSInteger tag = [item tag];
    
    switch (tag) {
        case 0:
             [self sortHolidaysPackages];
            break;
        case 1:
        {
            // [self showMapViewcontroller:nil];
            [tabBar setSelectedItem:nil];
            //selectedTab = -1;
        }
            break;
        case 2:
            [self filterButtonSelector:nil];
            break;
        default:
            break;
    }
}
-(void)setTableviewContentView
{
    CGPoint contentOffset = self.tableView.contentOffset;
    contentOffset.y = 0;//contentOffset.y += CGRectGetHeight(self.tableView.tableHeaderView.frame);
//    self.tableView.contentOffset = contentOffset;
    [self.tableView setContentOffset:contentOffset animated:YES];
    
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    DLog(@"%f",self.tableView.contentOffset.y);
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    if (searchBar.text.length > 0) {
        
        [self.searchBar resignFirstResponder];
        
        [SVProgressHUD showWithStatus:@"Searching"];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"SELF.holidayPackage contains[cd] %@",searchBar.text];
            tmpHolidayList = [self.holidayList filteredArrayUsingPredicate:bPredicate];
            [self.tableView reloadData];
            [SVProgressHUD showSuccessWithStatus:@"Done"];
            
        });
    }
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self.searchBar resignFirstResponder];
    if (searchBar.text.length == 0) {
        tmpHolidayList = [self.holidayList mutableCopy];
        [self.tableView reloadData];
    }
   // [self setTableviewContentView];
}
-(void) showErrorAlert
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"No Data Found!" preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action)
                                {
                                    //[self.navigationController popViewControllerAnimated:YES];
                                    //[self ShowActivityView];
                                    
                                }]];
    [self.navigationController presentViewController:alertController animated:YES completion:nil];
    
}

@end
