//
//  HolidayFilterCell.m
//  Faremakers
//
//  Created by Mac1 on 02/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "HolidayFilterCell.h"
#import "HolidayFilterItem.h"
#import "PriceFilterColletionCell.h"
#import "UILabel+FontName.h"
#import "Constants.h"


@implementation HolidayFilterCell

- (void)awakeFromNib
{
    [self.locationsLabel changeFontFamily];
    [self.locationTitleLabel changeFontFamily];
    // Initialization code
}
- (void)loadCellWithIdentifier:(NSString *)cellIdentifier ForIndexPath:(NSIndexPath*)index
{
    switch (index.section) {
        case 0:
            [self loadPriceCollectionCell];
            break;
        case 1:
            [self loadOptionsFirstCell];
            break;
        case 2:
            [self loadOptionsSecondCell];
            break;
        default:
            break;
    }
}

-(void)loadPriceCollectionCell
{
    _priceCollectionView.delegate = self;
    _priceCollectionView.dataSource = self;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 3;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout  *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.frame.size.width/3, 55);
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PriceFilterColletionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HolidayPriceFilterColletionCell" forIndexPath:indexPath];
    [cell loadCellForPrice:self.item.price withAllPrices:[self.item preparePriceRangeStrings] atIndexPath:indexPath ];
    return cell;
    
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    {
        self.item.price = [[self.item preparePriceRangeStrings] objectAtIndex:indexPath.row];//HolidayFilterPriceStrings[indexPath.row];
    }
    [collectionView reloadData];
    
}

-(void)loadOptionsFirstCell
{
    _locationTitleLabel.text = @"Locations";
    
    NSMutableString *locations = [[NSMutableString alloc] init];
    
    for (NSString *str in self.item.locations) {
        [locations appendString:str];
        [locations appendString:@","];
    }
    _locationsLabel.text = (self.item.locations.count)?locations:@"Select Locations";
}
-(void)loadOptionsSecondCell
{
    _locationTitleLabel.text = @"Category/Theme";
    
    NSMutableString *locations = [[NSMutableString alloc] init];
    
    for (NSString *str in self.item.themes) {
        [locations appendString:str];
        [locations appendString:@","];
    }
    _locationsLabel.text = (self.item.themes.count)?locations:@"Select Category/Theme";
}

@end
