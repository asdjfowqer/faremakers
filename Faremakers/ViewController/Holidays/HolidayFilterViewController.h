//
//  HolidayFilterViewController.h
//  Faremakers
//
//  Created by Mac1 on 18/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol HolidaysFilterViewDelegate <NSObject>

- (void) holidaysFilterViewDidDissmissWithFilterItem:(id)item;
- (void) holidaysfilterViewDidSelectClrAllFilters:(BOOL)clear;
- (void) holidaysfilterViewWillDismiss;


@end

@class HolidayFilterItem;

@interface HolidayFilterViewController : UIViewController

@property (weak, nonatomic) id<HolidaysFilterViewDelegate> adelegate;

@property (strong, nonatomic) NSArray *carSearchData;

@property (copy, nonatomic) HolidayFilterItem *filterItem;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)ApplyFilterButtonPressed:(id)sender;

@end
