//
//  HolidaysListingsViewController.h
//  Faremakers
//
//  Created by Mac1 on 23/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HolidayFilterItem;

@interface HolidaysListingsViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,UITabBarDelegate,UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITabBar *tabBarView;
@property (strong, nonatomic) HolidayFilterItem *filterItem;
@property (strong, nonatomic) NSArray *holidayList;


- (IBAction)filterButtonSelector:(id)sender;
- (void)getHolidayData;

@end
