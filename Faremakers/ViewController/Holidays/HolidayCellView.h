//
//  HolidayViewCell.h
//  Faremakers
//
//  Created by Mac1 on 23/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HolidayCellView : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *destinationTitle;
@property (weak, nonatomic) IBOutlet UILabel *durationLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

-(void)showPickerViewForIndex:(NSIndexPath*)indexPath;
-(void)showDatePickerViewForIndex:(NSIndexPath*)indexPath;

@end
