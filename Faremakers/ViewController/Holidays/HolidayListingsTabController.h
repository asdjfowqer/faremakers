//
//  HolidayListingsTabController.h
//  Faremakers
//
//  Created by Mac1 on 18/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HotelSearchItem;

@interface HolidayListingsTabController : UIViewController <UITabBarDelegate>

@property (strong, nonatomic) NSArray *flightSearchData;

@property (strong, nonatomic) HotelSearchItem *searchItem;

-(void)searchHotelDataWithDictionary:(NSDictionary*)dict;

@end
