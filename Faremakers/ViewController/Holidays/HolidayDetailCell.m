//
//  HolidayDetailCollectionCellCollectionViewCell.m
//  Faremakers
//
//  Created by Mac1 on 04/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "HolidayDetailCell.h"
#import "HolidayItem.h"

@implementation HolidayDetailCell

-(void)setHolidayItem:(HolidayItem *)holidayItem withIdentifier:(NSString*)cellIdentifier
{
    _holidayItem = (HolidayItem*)holidayItem;
    [self loadCellWithIdentifier:cellIdentifier];
}
#pragma mark - Private Method.

- (void)loadCellWithIdentifier:(NSString *)cellIdentifier
{
    // Get Last componet from CellIdenfifier
    int value = [[cellIdentifier substringFromIndex: [cellIdentifier length] - 1] intValue];
    
    switch (value)
    {
        case 0:
            [self loadFirstCollectionCell];
            break;
        case 1:
            [self loadSecondCollectionCell];
            break;
        case 2:
            [self loadThirdCollectionCell];
            break;
        case 3:
            [self loadFourthCollectionCell];
            break;
        case 4:
        default:
            [self loadFifthCollectionCellWithIndex:(value - 4)];
            break;
    }
    
}

- (void) loadFirstCollectionCell
{
    self.holidayTitleLabel.text = self.holidayItem.holidayPackage;
    self.holidayDetailLabel.text = self.holidayItem.holidayDetail;
    self.priceLabel.text = [NSString stringWithFormat:@"%@ %@",self.holidayItem.packagePrice,self.holidayItem.currencyCode];//[NSString stringWithFormat:@"Price: %@ PKR",self.holidayItem.packagePrice];
}

- (void) loadSecondCollectionCell
{
    self.holidayAccTitleLabel.text = @"Package Accomodation Detail:";
    self.holidayAccDetailLabel.text = self.holidayItem.holidayAccomodDetail;
}

- (void) loadThirdCollectionCell
{
   // self.holidayPriceLabel.text = self.holidayItem.holidayPriceDetail;
    self.holidayPriceLabel.text = self.holidayItem.holidayPriceDetail;
}

- (void) loadFourthCollectionCell
{
    self.holidayVisaLabel.text = self.holidayItem.holidayVisaDetail;
    self.holidayVisaNoteLabel.text = self.holidayItem.holidayTicketNote;
}

- (void) loadFifthCollectionCellWithIndex:(NSInteger)index
{
    NSInteger count = self.holidayItem.holidaysTourList.count;
    
    if (count > 0 && index < count)
    {
        HolidayTours* tour = self.holidayItem.holidaysTourList[index];
        self.holidayTourTitle.text = tour.tourTitle;
        self.holidayTourDetail.text = tour.tourDetails;
        self.holidayTourExtras.text = tour.tourIncludes;
    }
   
}

- (void) loadSixthCollectionCell
{
}


@end
