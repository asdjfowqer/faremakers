//
//  HolidaysListViewCell.m
//  Faremakers
//
//  Created by Mac1 on 23/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "HolidaysListViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/SDWebImagePrefetcher.h>
#import "UILabel+FontName.h"
#import "HolidayItem.h"
#import "Constants.h"

@implementation HolidaysListViewCell

- (void)awakeFromNib
{
    [self.descriptionLabel changeFontFamily];
    [self.destinationLabel changeFontFamily];
    [self.durationLabel changeFontFamily];
    
    // Initialization code
}

-(void)setItem:(HolidayItem *)item
{
    _item = item;
    self.descriptionLabel.text = self.item.holidayPackage;
    self.destinationLabel.text = [NSString stringWithFormat:@"%@ %@",self.item.noOfRooms,self.item.roomType];
    self.durationLabel.text = [NSString stringWithFormat:@"%@ Nights",self.item.totalNightsStay];
    self.priceLabel.text = [NSString stringWithFormat:@"%@ %@",self.item.packagePrice.stringValue,self.item.currencyCode];
    [self loadHolidayImage];
}

- (void)loadHolidayImage
{
    [self.activityView startAnimating];
    if (self.item.holidayImageUrl)
    {
        SDWebImagePrefetcher *prefetcher = [SDWebImagePrefetcher sharedImagePrefetcher];
        NSString *urlString = [[BASE_URL stringByDeletingLastPathComponent] stringByAppendingPathComponent:self.item.holidayImageUrl];
        NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        [prefetcher prefetchURLs:@[url]];
        
        UIImage *placeHolder = [UIImage imageNamed:@"placeholder"];

        [self.ImageView sd_setImageWithURL:[NSURL URLWithString:urlString]
                          placeholderImage:placeHolder
                                   options:SDWebImageRetryFailed
                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
             
             [self.activityView stopAnimating];
             
             if (error)
                 self.ImageView.image = placeHolder;
             else
                 self.ImageView.image = image;

             
             
         }];
    }
}
@end
