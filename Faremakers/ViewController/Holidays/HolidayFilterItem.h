//
//  HolidayFilterItem.h
//  Faremakers
//
//  Created by Mac1 on 02/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HolidayFilterItem : NSObject

@property (nonatomic) NSString *price;
@property (nonatomic) NSArray *locations;
@property (nonatomic) NSArray *themes;
@property (nonatomic) NSNumber *minPrice;
@property (nonatomic) NSNumber *maxprice;
@property (nonatomic) NSArray *priceFilterRange;

- (void)preparePriceFilterRanges;
- (NSArray*)preparePriceRangeStrings;
- (void)clearAllFilters;

@end
