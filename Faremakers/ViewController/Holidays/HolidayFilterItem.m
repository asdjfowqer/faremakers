//
//  HolidayFilterItem.m
//  Faremakers
//
//  Created by Mac1 on 02/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "HolidayFilterItem.h"

@implementation HolidayFilterItem

-(id)init
{
    self = [super init];    
    return self;
}

- (id)copyWithZone:(NSZone *)zone
{
    HolidayFilterItem *copy = [[[self class] allocWithZone:zone] init];
    if (copy) {
        copy.locations = [[NSArray alloc] initWithArray:self.locations];
        copy.themes = [[NSArray alloc] initWithArray:self.themes];
        copy.price = [self.price copy];
        copy.minPrice = [self.minPrice copy];
        copy.maxprice = [self.maxprice copy];
        copy.priceFilterRange = [[NSArray alloc] initWithArray:self.priceFilterRange];
        
    }
    return copy;
}
- (void)clearAllFilters
{
    self.locations = nil;
    self.themes = nil;
    self.price = nil;
}
- (void)setMinPrice:(NSNumber *)minPrice
{
    if (minPrice.integerValue > 1000)
    {
        NSInteger minimam = (minPrice.integerValue -((minPrice.integerValue % 1000) - 1000));
        _minPrice = [NSNumber numberWithInteger:minimam];
    }
    else
        _minPrice = minPrice;
}
-(void)setMaxprice:(NSNumber *)maxprice
{
    if (maxprice.integerValue > 1000)
    {
        NSInteger maximam = (maxprice.integerValue - (maxprice.integerValue % 1000));
        _maxprice = [NSNumber numberWithInteger:maximam];
    }
    else
        _maxprice = maxprice;

}
- (void)preparePriceFilterRanges
{
    NSMutableArray *pricesRanger = [[NSMutableArray alloc] initWithCapacity:3];
    NSInteger median = (self.maxprice.integerValue - self.minPrice.integerValue)/3;
    
    [pricesRanger addObject:[NSNumber numberWithLong:self.minPrice.integerValue + median]];
    [pricesRanger addObject:[NSNumber numberWithLong:self.minPrice.integerValue + (median*2)]];
    [pricesRanger addObject:[NSNumber numberWithLong:self.minPrice.integerValue + (median*2)]];

    _priceFilterRange = [pricesRanger mutableCopy];
}
-(NSArray*)preparePriceRangeStrings
{
    NSMutableArray *pricesRanger = [[NSMutableArray alloc] initWithCapacity:3];
    
    NSNumber *number = self.priceFilterRange[0];
    [pricesRanger addObject:[NSString stringWithFormat:@"Below %ldK",number.integerValue/1000]];
    
    NSInteger starting = number.integerValue/1000;
    number = self.priceFilterRange[1];
    [pricesRanger addObject:[NSString stringWithFormat:@"%ldK - %ldK",starting ,number.integerValue/1000]];
    
    number = self.priceFilterRange[2];
    [pricesRanger addObject:[NSString stringWithFormat:@"Above %ldK",number.integerValue/1000]];
    
    return pricesRanger;
}
@end
