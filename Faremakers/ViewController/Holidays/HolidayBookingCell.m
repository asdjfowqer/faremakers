//
//  FlightBookingCell.m
//  Faremakers
//
//  Created by Mac1 on 30/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "HolidayBookingCell.h"
#import "UILabel+FontName.h"

@implementation HolidayBookingCell

- (void)awakeFromNib
{
    [self.flightTitleLabel changeFontFamily];
    [self.deparTimeLabel changeFontFamily];
    [self.deparDateLabel changeFontFamily];
    [self.deparFlightDetail changeFontFamily];
    [self.chargesDetailLabel changeFontFamily];
    [self.chargesCostLabel changeFontFamily];
}

@end
