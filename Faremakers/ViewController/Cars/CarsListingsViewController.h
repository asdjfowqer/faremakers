//
//  CarsListingsViewController.h
//  Faremakers
//
//  Created by Mac1 on 22/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CarSearchItem;

@interface CarsListingsViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>

@property (nonatomic) CarSearchItem *searchItem;
@property (nonatomic) NSArray *carsList;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIToolbar *toolBar;

- (IBAction)filterButtonSelector:(id)sender;
- (void)searchCarDataWithItem:(NSDictionary*)item;
@property (weak, nonatomic) IBOutlet UIView *notFoundView;

@end
