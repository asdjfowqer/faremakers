//
//  CarTransferCollectionCell.h
//  Faremakers
//
//  Created by Mac1 on 23/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//


#import <UIKit/UIKit.h>

@class TransferVehicles;

@interface CarTransferCollectionCell : UICollectionViewCell

@property (strong, nonatomic) TransferVehicles *transferVehicle;
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;
@property (weak, nonatomic) IBOutlet UILabel *baggageLabel;
@property (weak, nonatomic) IBOutlet UILabel *passengerLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

-(IBAction)bookCarButtonPressed:(id)sender;

@end
