//
//  CarsSearchViewController.h
//  Faremakers
//
//  Created by Mac1 on 16/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+RESideMenu.h"


@interface CarsSearchViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *swapeBusButton;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentController;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint* searchBtnBottomMargin;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint* optionsLblBottomMargin;
@property (weak, nonatomic) IBOutlet UIButton *acOnlyButton;
@property (weak, nonatomic) IBOutlet UIButton *nonAcButton;


- (IBAction)searchButtonSelector:(id)sender;
- (IBAction)tripSegmentValueChange:(id)sender;
- (IBAction)swapBusButtonSelector:(id)sender;
- (IBAction)nonAcBtnSelector:(id)sender;
- (IBAction)acOnlyBtnSelector:(id)sender;

@end
