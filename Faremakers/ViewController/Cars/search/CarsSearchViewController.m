//
//  CarsSearchViewController.m
//  Faremakers
//
//  Created by Mac1 on 16/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "CarsSearchViewController.h"
#import "CitySerachViewController.h"
#import "CarsListingsViewController.h"
#import "DoubleDateTableCell.h"
#import "DoubleDateCellViewDelegate.h"
#import "UILabel+FontName.h"
#import "Constants.h"
#import "CarSearchItem.h"

@interface CarsSearchViewController ()<DoubleDateCellViewDelegate>
{
    CarSearchItem *item;
}

@end

@implementation CarsSearchViewController

#define KFlightDetailCell @"FlightDetailCell"

-(void)viewDidLoad
{
    [super viewDidLoad];
    item = [[CarSearchItem alloc] init];
    [self.tableView registerNib:[UINib nibWithNibName:@"DoubleDateTableCell" bundle:nil] forCellReuseIdentifier:KDoubleDateTableCell];
    //[self.swapeBusButton removeFromSuperview];
    [self.tableView addSubview:self.swapeBusButton];
    [self.tableView bringSubviewToFront:self.swapeBusButton];

    [self adjustMarginsForLagerHeights];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section)
        return 1;
    return 2;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section)
        return 77;
    return 44;
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!indexPath.section) {
        
        UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:KFlightDetailCell];
       // [cell.textLabel changeFontFamily];
        switch (indexPath.row) {
            case 0:{
                cell.textLabel.text = item.homeCity;
            }
                break;
            case 1:{
                cell.textLabel.text = item.DestinationCity;
            }
                break;
                
            default:
                break;
        }
        return cell;
    }
    else
    {
        DoubleDateTableCell *cell = [self.tableView dequeueReusableCellWithIdentifier:KDoubleDateTableCell];
        cell.doubleDateItem = item.doubleDate;
        [cell showRoundTripLables:item.roundTrip];
        cell.adelegate = self;
        return cell;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CitySerachViewController *citySearch = [self.storyboard instantiateViewControllerWithIdentifier:@"CitySerachViewController"];
    // citySearch.delegate = self;
    [self.navigationController pushViewController:citySearch animated:YES];
}
- (IBAction)tripSegmentValueChange:(id)sender
{
    if ([sender selectedSegmentIndex])
    {
        item.roundTrip = YES;
        DoubleDateTableCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:1]];
        [cell showRoundTripLables:YES];
    }
    else
    {
        item.roundTrip = NO;
        DoubleDateTableCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:1]];
        [cell showRoundTripLables:NO];
    }
}

- (IBAction)swapBusButtonSelector:(id)sender
{
    NSString *homeCity = item.homeCity;
    item.homeCity = item.DestinationCity;
    item.DestinationCity = homeCity;
    [self.tableView reloadData];
}

- (IBAction)searchButtonSelector:(id)sender
{
    if ([item checkRequiredItems]) {
       // <#statements#>
    }
    CarsListingsViewController *carListingView = [self.storyboard instantiateViewControllerWithIdentifier:@"CarsListingsViewController"];
    carListingView.searchItem = item;
    [carListingView searchCarDataWithItem:[item preparePostData]];
    [self.navigationController pushViewController:carListingView animated:YES];

}

- (void) adjustMarginsForLagerHeights
{
    if ([UIScreen mainScreen].bounds.size.height != 480)
    {
       // self.searchBtnBottomMargin.constant = 100;
      //  self.optionsLblBottomMargin.constant = 70;
    }
}
#pragma mark DoubleDateCellViewDelegate

-(void) doubleDateReturnComponentDidSelected:(id)sender
{
    item.roundTrip = YES;
    self.segmentController.selectedSegmentIndex = 1;

}

- (IBAction)acOnlyBtnSelector:(id)sender {
    [self.acOnlyButton setSelected:YES];
    [self.nonAcButton setSelected:NO];
}

- (IBAction)nonAcBtnSelector:(id)sender {
    [self.acOnlyButton setSelected:NO];
    [self.nonAcButton setSelected:YES];

}
@end
