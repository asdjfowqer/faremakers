//
//  CarListingsDetailCell.m
//  Faremakers
//
//  Created by Mac1 on 22/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "CarListingsDetailCell.h"
#import "UILabel+FontName.h"
#import "TransferVehicles.h"
#import "CarSearchItem.h"
#import "CarsItem.h"
#import "Cities.h"

@implementation CarListingsDetailCell

- (void)awakeFromNib
{
    [self.maxPassLabel changeFontFamily];
    [self.maxLuggLabel changeFontFamily];
    [self.vehicleTypeLabel changeFontFamily];
    [self.travellCityLabel changeFontFamily];
    [self.durationLabel changeFontFamily];
    [self.availableLabel changeFontFamily];
    [self.priceLabel changeFontFamily];
    
}
-(void)setTransferVehical:(TransferVehicles *)transferVehical
{
    _transferVehical = transferVehical;
    
    self.availableLabel.text = self.transferVehical.confirmationDescription;//@"";
    self.maxPassLabel.text = [NSString stringWithFormat:@"Max Passengers %@" , self.transferVehical.maximumPassengers];//@"";
    self.maxLuggLabel.text = [NSString stringWithFormat:@"Max Luggage %@" ,self.transferVehical.maximumLuggage];//@"";
    self.vehicleTypeLabel.text = self.transferVehical.vehicleValue;
    self.priceLabel.text = [NSString stringWithFormat:@"%@ %@", self.transferVehical.vehicleItemPriceValue, self.transferVehical.vehicleItemPriceCurrency];
    self.travellCityLabel.text = [[self.searchItem.destCity.text componentsSeparatedByString:@","] firstObject];
}
- (IBAction)bookingBtnSelector:(id)sender
{
    if ([self.adelegate respondsToSelector:@selector(BookCarButtonDidSelected:)])
    {
        [self.adelegate BookCarButtonDidSelected:sender];
    }
}

- (IBAction)pickndropBtnSelector:(id)sender
{
    
}

@end
