//
//  CarListingsCell.h
//  Faremakers
//
//  Created by Mac1 on 01/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CarsItem;
@class CarSearchItem;

@interface CarListingsCell : UITableViewCell

@property (nonatomic)CarsItem *item;
@property (nonatomic)CarSearchItem *searchitem;

@property (weak, nonatomic) IBOutlet UILabel *valueTitleLabel;

@end
