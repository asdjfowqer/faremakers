//
//  CarTransferDetailCell.m
//  Faremakers
//
//  Created by Mac1 on 23/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "CarTransferDetailCell.h"
#import "CarTransferCollectionCell.h"
#import "UILabel+FontName.h"
#import "CarsItem.h"

@interface CarTransferDetailCell () <UICollectionViewDataSource,UICollectionViewDelegate>
{
    NSInteger totalStops;
    NSInteger totalOverLay;
    NSArray *flightSegmentsArr;
}

@end

@implementation CarTransferDetailCell

- (void)awakeFromNib
{
    [self.itemValueLabel changeFontFamily];
}

- (void)setCarsSearItem:(CarsItem *)carItem withTripIndex:(NSInteger)index;
{
    _carsItem = carItem;
    totalStops = 3;
    [self.stopsCollectionView reloadData];
}
-(void)LoadMenuItemsForColltionView:(UICollectionView*)collectionView
{
    totalStops = 4;
    totalOverLay = totalStops - 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return totalStops + totalOverLay;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    float width = 190;
    
    return CGSizeMake(width,self.stopsCollectionView.frame.size.height);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{

    {
        CarTransferCollectionCell *cell = [self.stopsCollectionView dequeueReusableCellWithReuseIdentifier:@"CarTransferCollectionCell" forIndexPath:indexPath];
        NSInteger index = (indexPath.row)?(indexPath.row/2):indexPath.row;
     //   cell.flightSegmentItem = flightSegmentsArr[index];
        return cell;
    }
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self.aDelegate collectionViewDidSelectMenuItem:indexPath.row];
    
}
@end
