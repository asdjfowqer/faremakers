//
//  CarSearchItem.h
//  Faremakers
//
//  Created by Mac1 on 01/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Cities;

@interface CarSearchItem : NSObject

@property (nonatomic) NSString *DestinationCity;
@property (nonatomic) NSDate *departDate;
@property (nonatomic) NSString *noOfPassenger;
@property (nonatomic) Cities *destCity;
@property (nonatomic) NSString *pickUp;
@property (nonatomic) NSString *dropOff;
@property (nonatomic) BOOL nonAC;
@property (nonatomic) BOOL roundTrip;

-(NSDictionary*)preparePostData;
-(BOOL)checkRequiredItems;
-(id)initWithTempData;

@end
