//
//  CarListingsCell.m
//  Faremakers
//
//  Created by Mac1 on 01/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "CarListingsCell.h"
#import "UILabel+FontName.h"
#import "CarsItem.h"


@implementation CarListingsCell

- (void)awakeFromNib
{
    [self.valueTitleLabel changeFontFamily];
}

-(void)setItem:(CarsItem *)item
{
    _item = item;
    self.valueTitleLabel.text = item.itemValue;
}
@end
