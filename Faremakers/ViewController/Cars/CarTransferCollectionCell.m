//
//  CarTransferCollectionCell.m
//  Faremakers
//
//  Created by Mac1 on 23/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "CarTransferCollectionCell.h"
#import "UILabel+FontName.h"
#import "TransferVehicles.h"

@implementation CarTransferCollectionCell

- (void)awakeFromNib
{
    [self.typeLabel changeFontFamily];
    [self.baggageLabel changeFontFamily];
    [self.passengerLabel changeFontFamily];
    [self.priceLabel changeFontFamily];
    
}
- (void)setTransferVehicle:(TransferVehicles *)transferVehicle
{
    _transferVehicle = transferVehicle;
}
- (void)populateSegment
{
    
}

-(IBAction)bookCarButtonPressed:(id)sender
{
    
}

@end
