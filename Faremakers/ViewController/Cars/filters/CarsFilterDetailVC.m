//
//  CarsFilterDetailVC.m
//  Faremakers
//
//  Created by Mac1 on 21/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "CarsFilterDetailVC.h"
#import "CarsFilterItem.h"
#import "FilterDetailViewController.h"


@interface CarsFilterDetailVC ()
{
    NSMutableArray *dataList;
}

@end


@implementation CarsFilterDetailVC

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    dataList = [[NSMutableArray alloc] init];
    
    int i = 0;
    
    for (NSString *str in self.transferTypes)
    {
        SelectionItem *sItem = [[SelectionItem alloc] init];
        sItem.item = str;
        BOOL isContain = [self.item.transferTypes containsObject:str];
        sItem.isSelected = isContain;
        [dataList addObject:sItem];
        i++;
    }
    
    UIBarButtonItem *doneButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneButtonClicked:)];
    self.navigationItem.rightBarButtonItem = doneButtonItem;
    
}

-(void)doneButtonClicked:(id)sender
{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    for (SelectionItem *sItem in dataList) {
        
        if (sItem.isSelected)
            [array addObject:sItem.item];
    }
    
    self.item.transferTypes = [[NSArray alloc] initWithArray:array];
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    SelectionItem *sItem = [dataList objectAtIndex:indexPath.row];
    [cell.textLabel setText:sItem.item];
    
    if (sItem.isSelected) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
        cell.accessoryType = UITableViewCellAccessoryNone;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SelectionItem *sItem = dataList[indexPath.row];
    sItem.isSelected = !sItem.isSelected;
    [self.tableView reloadData];
    
}

@end
