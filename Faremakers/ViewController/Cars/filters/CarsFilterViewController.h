//
//  CarsFilterViewController.h
//  Faremakers
//
//  Created by Mac1 on 18/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CarsFilterViewDelegate <NSObject>

- (void) carFilterViewDidDissmissWithFilterItem:(id)item;

@end

@class CarsFilterItem;

@interface CarsFilterViewController : UIViewController

@property (weak, nonatomic) id<CarsFilterViewDelegate> adelegate;

@property (strong, nonatomic) NSArray *carSearchData;

@property (strong, nonatomic) CarsFilterItem *filterItem;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)ApplyFilterButtonPressed:(id)sender;

@end
