//
//  CarsFilterItem.m
//  Faremakers
//
//  Created by Mac1 on 02/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "CarsFilterItem.h"

@implementation CarsFilterItem

- (id)copyWithZone:(NSZone *)zone
{
    CarsFilterItem *copy = [[[self class] allocWithZone:zone] init];
    if (copy) {
        copy.transferTypes = [[NSMutableArray alloc] initWithArray:self.transferTypes];
        copy.price = [self.price copy];
    }
    return copy;
}

@end
