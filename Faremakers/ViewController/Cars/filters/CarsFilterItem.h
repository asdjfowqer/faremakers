//
//  CarsFilterItem.h
//  Faremakers
//
//  Created by Mac1 on 02/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CarsFilterItem : NSObject  <NSCopying>

@property (nonatomic) NSString *price;
@property (nonatomic) NSArray *transferTypes;

@end
