//
//  CarsFilterCell.m
//  Faremakers
//
//  Created by Mac1 on 02/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "CarsFilterCell.h"
#import "CarsFilterItem.h"
#import "Constants.h"
#import "PriceFilterColletionCell.h"
#import "UILabel+FontName.h"


@implementation CarsFilterCell

- (void)awakeFromNib {
    
    [self.locationsLabel changeFontFamily];
    [self.locationTitleLabel changeFontFamily];
}
- (void)loadCellWithIdentifier:(NSString *)cellIdentifier ForIndexPath:(NSIndexPath*)index
{
    switch (index.section) {
        case 0:
            [self loadPriceCollectionCell];
            break;
        case 1:
            [self loadOptionsFirstCell];
            break;
        default:
            break;
    }
}

-(void)loadPriceCollectionCell
{
    _priceCollectionView.delegate = self;
    _priceCollectionView.dataSource = self;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 3;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout  *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.frame.size.width/3, 55);
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    PriceFilterColletionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CarsPriceFilterColletionCell" forIndexPath:indexPath];
    [cell loadCellForPrice:self.item.price withAllPrices:CarsFilterPriceStrings atIndexPath:indexPath ];
    return cell;
    
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
   
    {
        
        NSString *price = CarsFilterPriceStrings[indexPath.row];
        
        if (self.item.price && [self.item.price isEqualToString:price]) {
            self.item.price = nil;
        }
        else
            self.item.price = CarsFilterPriceStrings[indexPath.row];
        
    }
    
    [collectionView reloadData];
    
}

-(void)loadOptionsFirstCell
{
    _locationTitleLabel.text = @"Transfer Type";
    
    NSMutableString *locations = [[NSMutableString alloc] init];
    
    int i = 0;
    for (NSString *str in self.item.transferTypes)
    {
        [locations appendString:str];
        i++;
        if (self.item.transferTypes.count < i)
        [locations appendString:@","];
    }
    _locationsLabel.text = (self.item.transferTypes.count)?locations:@"Select Transfer Type";
}


@end
