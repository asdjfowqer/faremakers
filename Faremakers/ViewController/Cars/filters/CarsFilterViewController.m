//
//  CarsFilterViewController.h
//  Faremakers
//
//  Created by Mac1 on 18/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "CarsFilterViewController.h"
#import "CarsFilterDetailVC.h"
#import "CarsFilterCell.h"
#import "CarsFilterItem.h"
#import "CarsItem.h"
#import "TransferVehicles.h"

@interface CarsFilterViewController()<UITableViewDataSource,UITabBarControllerDelegate>
{
    NSArray *headerTitles;
    NSInteger selectedIndex;
    NSArray *uniqueAirLines;
}

@end

@implementation CarsFilterViewController


-(void) viewDidLoad
{
    [super viewDidLoad];
    
    headerTitles = @[@"Select Price Range",@"Select Transfer Type"];
    self.navigationItem.title = @"Filter";
    UIBarButtonItem *doneButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleDone target:self action:@selector(doneButtonClicked:)];
    self.navigationItem.rightBarButtonItem = doneButtonItem;
    selectedIndex = -1;
    
}

-(void)doneButtonClicked:(id)sender
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return headerTitles.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 25;
}
- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return headerTitles[section];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger index = indexPath.section;
    
    NSString *cellIdentifier = [@"CarsFilterCell" stringByAppendingFormat:@"%ld",(long)index ];
    
    CarsFilterCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    cell.item = self.filterItem;
    [cell loadCellWithIdentifier:cellIdentifier ForIndexPath:indexPath];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section > 0)
    {
        CarsFilterDetailVC *detailView = [[CarsFilterDetailVC alloc] initWithStyle:UITableViewStylePlain];
        detailView.item = self.filterItem;
        detailView.transferTypes = [self getAllTransfersType];
        [self.navigationController pushViewController:detailView animated:YES];
    }
    
}
-(NSArray*)getAllTransfersType
{
    NSMutableArray *transferTypeArray = [[NSMutableArray alloc] init];
    
    for (CarsItem *carObject in self.carSearchData) {
        
        [transferTypeArray addObjectsFromArray:carObject.transferVehicles];
    }
    
    transferTypeArray = [transferTypeArray valueForKeyPath:@"@distinctUnionOfObjects.vehicleValue"];
    
    return transferTypeArray;
    
}
- (IBAction)ApplyFilterButtonPressed:(id)sender;
{
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        [self.adelegate carFilterViewDidDissmissWithFilterItem:self.filterItem];
        
    }];
}

@end
