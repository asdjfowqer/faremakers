//
//  CarsFilterDetailVC.h
//  Faremakers
//
//  Created by Mac1 on 21/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef enum {
    Location,
    Themes
} CarsFilterType;


@class CarsFilterItem;

@interface CarsFilterDetailVC : UITableViewController

@property (nonatomic,strong)CarsFilterItem *item;

@property (nonatomic) CarsFilterType filterType;

@property (nonatomic) NSArray *transferTypes;



@end
