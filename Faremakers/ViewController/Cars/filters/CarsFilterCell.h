//
//  CarsFilterCell.h
//  Faremakers
//
//  Created by Mac1 on 02/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <UIKit/UIKit.h>


@class CarsFilterItem;

@interface CarsFilterCell : UITableViewCell <UICollectionViewDataSource,UICollectionViewDelegate>

@property (nonatomic) CarsFilterItem *item;

#pragma mark CarsFilterItem 0
@property (weak, nonatomic) IBOutlet UICollectionView *priceCollectionView;

#pragma mark CarsFilterItem 1
@property (weak, nonatomic) IBOutlet UILabel *locationsLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationTitleLabel;

- (void)loadCellWithIdentifier:(NSString *)cellIdentifier ForIndexPath:(NSIndexPath*)index;

@end
