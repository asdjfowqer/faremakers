//
//  CarsListingsViewController.m
//  Faremakers
//
//  Created by Mac1 on 22/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import <SVProgressHUD/SVProgressHUD.h>
#import "CarsListingsViewController.h"
#import "CarListingsDetailCell.h"
#import "CarBookingViewController.h"
#import "CarsFilterViewController.h"
#import <MRProgress/MRProgress.h>
#import "NSDate+DateComponents.h"
#import "TransferVehicles.h"
#import "AppDelegate.h"
#import "CarsDataApiManger.h"
#import "CarListingsCell.h"
#import "CarSearchItem.h"
#import "CarsFilterItem.h"
#import "CarsItem.h"
#import "Constants.h"

@interface CarsListingsViewController()<CarListingsDetailCellDelegate,CarsDataApiSearchDelegate,CarsFilterViewDelegate>
{
    CarsDataApiManger *carsApiMangaer;
    CarsFilterItem *filterItem;
    NSArray *filteredCarsList;
    UIActivityIndicatorView *activityIndicator;
}

@end

@implementation CarsListingsViewController

//pickup_icn,
- (void)viewDidLoad
{
    [super viewDidLoad];
    
   // [self.tableView registerNib:[UINib nibWithNibName:@"ListingsHeaderView" bundle:nil] forHeaderFooterViewReuseIdentifier:@"ListingHeaderView"];
   
    filterItem = [[CarsFilterItem alloc] init];
    
    [self ShowActivityView];
    [self updateNavigationBarTitle];
   // [[MRNavigationBarProgressView progressViewForNavigationController:self.navigationController] setProgress:0.01 animated:YES];
    
}
-(void)updateNavigationBarTitle
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 5, self.view.frame.size.width, 44)];
    label.backgroundColor = [UIColor clearColor];
    label.numberOfLines = 2;
    label.font = [UIFont fontWithName:KCustomeRagularFont size:16.0];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor blackColor];
    label.text = [NSString stringWithFormat:@"%@ - %@ \n %@ %@ %@",self.searchItem.pickUp , self.searchItem.dropOff,[self.searchItem.departDate getDayOfTheWeek],[self.searchItem.departDate getDayOfTheMonth],[self.searchItem.departDate getMonth]];
    [label sizeToFit];
    self.navigationItem.titleView = label;
    
}
- (NSInteger)totalTransfersTypeInSearchData
{
    NSInteger totalTransfer = self.carsList.count;
    
    for (CarsItem *carObj in self.carsList) {
        
        totalTransfer+=carObj.transferVehicles.count;
    }
    return totalTransfer;
}

- (void)searchCarDataWithItem:(NSDictionary*)item
{
    carsApiMangaer = [[CarsDataApiManger alloc] init];
    carsApiMangaer.adelegate = self;
    [carsApiMangaer searchCarsDataForItem:item];
}

-(void)setCarsList:(NSArray *)carsList
{
    _carsList = carsList;
}
-(void) CarsDataManagerDidRecieveResponse:(id)object
{
    _carsList = [[NSArray alloc] initWithArray:object];
    filteredCarsList = [self.carsList mutableCopy];
    [self.tableView reloadData];
    [self.notFoundView setHidden:YES];
        
    [self ShowActivityView];
    
}
-(void) CarsDataManagerDidFail:(id)object
{
    [self ShowActivityView];
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"No Data Found!" preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [self.navigationController popViewControllerAnimated:YES];
    }]];
    [self.navigationController presentViewController:alertController animated:YES completion:nil];
}
-(void) CarsDataManagerStartProgressing:(float)progress
{
    dispatch_async(dispatch_get_main_queue(), ^(void){
       // [[MRNavigationBarProgressView progressViewForNavigationController:self.navigationController] setProgress:progress animated:YES];
    });
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return filteredCarsList.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    CarsItem *carObj = filteredCarsList[section];
    return carObj.transferVehicles.count + 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
        return 35;
    
    CarsItem *carObj = filteredCarsList[indexPath.section];
    
    return (indexPath.row == carObj.transferVehicles.count)?80:90;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // NSInteger Index = [self calculateIndexPath:indexPath];
    
    if (indexPath.row == 0)
    {
        CarListingsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CarListingsCell"];
        cell.item = filteredCarsList[indexPath.row];
        return cell;
        
    }
    else
    {
        CarListingsDetailCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"CarListingsDetailCell" forIndexPath:indexPath];
        CarsItem *carObj = filteredCarsList[indexPath.section];
        cell.searchItem = self.searchItem;
        cell.transferVehical = carObj.transferVehicles[indexPath.row - 1];
        cell.adelegate = self;
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}


- (void)DeleteRow:(NSIndexPath*)indexPath
{
    [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
}
- (void)insertNewRow:(NSIndexPath*)indexPath
{
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationTop];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController setToolbarHidden:YES];
}

- (IBAction)filterButtonSelector:(id)sender
{
    CarsFilterViewController *filterView = [self.storyboard instantiateViewControllerWithIdentifier:@"CarsFilterViewController"];
    filterView.adelegate = self;
    filterView.carSearchData = self.carsList;
    filterView.filterItem = filterItem;

    UINavigationController *navigationController = [[UINavigationController alloc] initWithNavigationBarClass:[FMNavigationBar class] toolbarClass:nil];
    [navigationController setViewControllers:@[filterView]];

    [self.navigationController presentViewController:navigationController animated:YES completion:nil];
}
-(void)ShowActivityView
{
    UIView *view = [self.view viewWithTag:707];
    if (view)
    {
        [view removeFromSuperview];
        [activityIndicator stopAnimating];
        [[AppDelegate sharedDelegate] hideLoadingViewForView:self];
        
    }
    else
    {
        view = [[UIView alloc] initWithFrame:self.view.bounds];
        view.tag = 707;
        view.alpha = 0.5;
        
        [view setBackgroundColor:[UIColor whiteColor]];
        [self.view addSubview:view];
        [self.view bringSubviewToFront:view];
        [[AppDelegate sharedDelegate] showLoadingViewForView:self withText:@"Searching Cars..."];
    }
}
- (void)barButtonWithActivityIndicator
{
    activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    [activityIndicator hidesWhenStopped];
    UIBarButtonItem *activityItem = [[UIBarButtonItem alloc] initWithCustomView:activityIndicator];
    self.navigationItem.rightBarButtonItem = activityItem;
}

-(NSInteger)calculateIndexPath:(NSIndexPath*)indexPath
{
    return indexPath.row;
}
#pragma mark carFilterViewDelegate

- (void)carFilterViewDidDissmissWithFilterItem:(id)item
{
    
    filterItem = (CarsFilterItem*)item;
    
    
    [SVProgressHUD showWithStatus:@"Applying Filters"];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        NSArray *SearchListCopy = [self.carsList mutableCopy];
        NSMutableArray *array = [[NSMutableArray alloc] init];
        
        BOOL filterApplied = false;
        
        if (filterItem.transferTypes.count)
        {
            [SearchListCopy enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop)
             {
                 CarsItem *carItemObj = (CarsItem*)obj;
                 for (TransferVehicles *transferType in carItemObj.transferVehicles)
                 {
                     if ([filterItem.transferTypes containsObject:transferType.vehicleValue])
                     {
                         [array addObject:carItemObj];
                         break;
                     }
                     
                 }
             }];
            
            filteredCarsList = [array mutableCopy];
            filterApplied = true;
        }
        if(filterItem.price)
        {
            int startingPrice = 0;
            int endingPrice = 0;
            switch ([CarsFilterPriceStrings indexOfObject:filterItem.price]) {
                case 0:{
                    startingPrice = 0;
                    endingPrice = 200;
                    DLog(@" case 0");
                }
                    break;
                case 1:
                {
                    startingPrice = 200;
                    endingPrice = 500;
                    DLog(@" case 0");
                }
                    break;
                case 2:
                {
                    startingPrice = 500;
                    endingPrice = 999999;
                    DLog(@" case 0");
                }
                    break;
                default:
                    break;
            }
            [SearchListCopy enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop)
             {
                 CarsItem *carObj = (CarsItem*)obj;
                 
                 for (TransferVehicles *transferType in carObj.transferVehicles) {
                     
                     if (transferType.vehicleItemPriceValue.integerValue > startingPrice && transferType.vehicleItemPriceValue.integerValue <= endingPrice) {
                         [array addObject:carObj];
                         break;
                     }
                 }
                 
                 
             }];
            
            filteredCarsList = [array mutableCopy];
            filterApplied = true;
            
        }
        if (!filterApplied)
        {
            filteredCarsList = [self.carsList mutableCopy];
        }
        [self.tableView reloadData];
        
        [SVProgressHUD showSuccessWithStatus:@"Done"];
    });
    
    
}
#pragma mark CarListingsDetailCellDelegate

-(void) BookCarButtonDidSelected:(id)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];

    CarBookingViewController *carBookingView = [self.storyboard instantiateViewControllerWithIdentifier:@"CarBookingViewController"];
    carBookingView.carItem = self.carsList[indexPath.section];
    carBookingView.searchItem = self.searchItem;
    carBookingView.SelectTransferVehicle = indexPath.row - 1;
    [self.navigationController pushViewController:carBookingView animated:YES];
}
@end
