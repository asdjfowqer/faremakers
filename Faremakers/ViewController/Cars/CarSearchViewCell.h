//
//  CarSearchViewCell.h
//  Faremakers
//
//  Created by Mac1 on 23/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CarSearchItem;

@interface CarSearchViewCell : UITableViewCell

@property (strong,nonatomic)CarSearchItem *carSearchItem;

#pragma mark CarSearchViewCell 0

@property (weak, nonatomic) IBOutlet UILabel *destTitleLabel;

#pragma mark CarSearchViewCell 1

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;


-(void)setCarSearchItem:(CarSearchItem *)carSearchItem withIndexPath:(NSIndexPath*)indexPath;

-(void)showPickerViewForIndex:(NSInteger)tag;
-(void)showDatePickerViewForIndex:(NSIndexPath*)indexPath;

@end
