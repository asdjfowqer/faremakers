//
//  CarSearchItem.m
//  Faremakers
//
//  Created by Mac1 on 01/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "CarSearchItem.h"
#import "DoubleDateItem.h"
#import "FlightSegment.h"
#import "Cities.h"
#import "Constants.h"

@implementation CarSearchItem

-(id)init
{
    if (self = [super init])
    {
        self.DestinationCity = @"";
        self.departDate = [[NSDate date] dateByAddingTimeInterval:KTimeInterval*15];
        self.nonAC = NO;
        self.roundTrip = NO;
        self.noOfPassenger = @"1";
    }
    return self;
}
-(id)initWithTempData
{
    if (self = [super init])
    {
        self.DestinationCity = @"NEW YORK CITY";
        self.departDate = [[NSDate date] dateByAddingTimeInterval:KTimeInterval*15];
        self.destCity = [Cities itemWithDictionary:[self prepareTempCityDictionary:0]];
        self.nonAC = NO;
        self.roundTrip = NO;
        self.noOfPassenger = @"1";
        self.pickUp = @"Airport";
        self.dropOff = @"Port";

    }
    return self;
}
-(NSDictionary*)prepareTempCityDictionary:(BOOL)isDestination
{
    
    return [NSDictionary dictionaryWithObjectsAndKeys:  @"NEW YORK CITY",@"airtportName",
            @"NYC", @"cityCode" ,
            @"UNITED STATES", @"countryName" ,
            @"US", @"coutryCode",
            @"NEW YORK CITY,US (NYC)", @"text", nil];
}

/*
 
 "PaxQuantity": "2",
 "Pickoff": "A",
 "Dropoff": "H",
 }
 Airport  Accomodation  Port Station  Other
 
 */

- (NSString*)getDropOfCode:(NSString*)point
{
    if ([point isEqualToString:@"Airport"])
        return @"A";
    else if ([point isEqualToString:@"Accomodation"])
        return @"H";
    else if ([point isEqualToString:@"Port"])
        return @"P";
    else if ([point isEqualToString:@"Station"])
        return @"S";
    return @"O";
}
-(NSDictionary*)preparePostData
{
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:[FlightSegment parseDateOnly:self.departDate] forKey:@"DepartureDate"];
    [dict setObject:[FlightSegment parseDateOnly:self.departDate] forKey:@"RetrunDate"];
    [dict setObject:self.noOfPassenger forKey:@"PaxQuantity"];
    [dict setObject:self.destCity.text forKey:@"FromCity"];
    [dict setObject:self.destCity.text forKey:@"ToCity"];
    [dict setObject:[self getDropOfCode:self.pickUp] forKey:@"Pickoff"];
    [dict setObject:[self getDropOfCode:self.dropOff] forKey:@"Dropoff"];
    
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict
                                                       options:(NSJSONWritingOptions) NSJSONWritingPrettyPrinted
                                                         error:&error];
    
    if (! jsonData) {
        DLog(@"bv_jsonStringWithPrettyPrint: error: %@", error.localizedDescription);
    } else {
        DLog(@"%@", [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]);
    }
    return dict;
}
-(BOOL)checkRequiredItems
{
    BOOL error = NO;
    NSString *errorString = @"";
    
    if (!self.destCity)
    {
        errorString = @"Require Destination location";
        error = YES;
    }
    else if (!self.departDate)
    {
        errorString = @"Require Departure Date";
        error = YES;
    }
    else if (!self.pickUp)
    {
        errorString = @"Require PickUP location";
        error = YES;
    }
    else if (!self.pickUp)
    {
        errorString = @"Require DropOff location";
        error = YES;
    }
    else if (!self.noOfPassenger)
    {
        errorString = @"Require Number of Passengers";
        error = YES;
    }
    
    if (error)
    {
        [[[UIAlertView alloc] initWithTitle:@"Error" message:errorString delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil] show];
    }
    return error;
}

@end
