//
//  CarSearchViewController.m
//  Faremakers
//
//  Created by Mac1 on 16/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "CarSearchViewController.h"
#import "CitySerachViewController.h"
#import "CarsListingsViewController.h"
#import "CarSearchViewCell.h"
#import "CarSearchItem.h"
#import "Cities.h"

@interface CarSearchViewController ()<CityNameSearchDelegate>
{
    CarSearchItem *item;
    CitySerachViewController *citySearchView;
}

@end

@implementation CarSearchViewController

#define KFlightDetailCell @"CarSearchViewCell"

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    citySearchView =  [self.storyboard instantiateViewControllerWithIdentifier:@"CitySerachViewController"];
    citySearchView.title = @"Select Destination";
    citySearchView.adelegate = self;

    item = [[CarSearchItem alloc] initWithTempData];
    [self adjustMarginsForLagerHeights];    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([UIScreen mainScreen].bounds.size.height == 480)
        return 70;
    if (indexPath.row == 0)
    {
        return ([UIScreen mainScreen].bounds.size.height == 568)?80:100;
    }
    return 80;
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger index = (indexPath.row)?1:0;
    NSString *identifier = [KFlightDetailCell stringByAppendingFormat:@"%ld", (long)index];
    CarSearchViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:identifier];
    [cell setCarSearchItem:item withIndexPath:indexPath];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    switch (indexPath.row) {
        case 0:{
            [self showCityViewController];
        }
            break;
        case 1:{
            
            CarSearchViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
            [cell showPickerViewForIndex:(indexPath.row-1)];
        }
            break;
        case 2:{
            
            CarSearchViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
            [cell showPickerViewForIndex:(indexPath.row-1)];
        }
            break;
        case 3:{
            
            CarSearchViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
            [cell showPickerViewForIndex:(indexPath.row-1)];
        }
            break;
        case 4:{
            CarSearchViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
            [cell showDatePickerViewForIndex:indexPath];
            
        }
            break;
        default:
            break;
    }
    
}
- (void) showCityViewController
{
    [self.navigationController pushViewController:citySearchView animated:YES];
}

-(void) CitySearchViewController:(id)viewController didSelectCity:(id)city
{
    CitySerachViewController *Controller = (CitySerachViewController*)viewController;
    [item setDestCity:city];
    item.DestinationCity = [[item.destCity.text componentsSeparatedByString:@","] firstObject];
    [self.tableView reloadData];
    [Controller.navigationController popViewControllerAnimated:YES];
}
- (IBAction)searchButtonSelector:(id)sender
{
    if (![item checkRequiredItems])
    {
        CarsListingsViewController *listingView = [self.storyboard instantiateViewControllerWithIdentifier:@"CarsListingsViewController"];
        listingView.searchItem = item;
        [listingView searchCarDataWithItem:[item preparePostData]];
        [self.navigationController pushViewController:listingView animated:YES];
    }
   
}
- (void) adjustMarginsForLagerHeights
{
    if ([UIScreen mainScreen].bounds.size.height == 480) {
        
        //self.roomsTableTopMargin.constant = 20;
        self.searchBtnBottomMargin.constant = 0;
    }
}

@end
