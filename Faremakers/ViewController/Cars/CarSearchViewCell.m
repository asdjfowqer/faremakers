//
//  CarSearchViewCell.m
//  Faremakers
//
//  Created by Mac1 on 23/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "CarSearchViewCell.h"
#import "IQActionSheetPickerView.h"
#import "NSDate+DateComponents.h"
#import "UILabel+FontName.h"
#import "CarSearchItem.h"
#import "Constants.h"

@interface CarSearchViewCell()<IQActionSheetPickerViewDelegate>

@end

@implementation CarSearchViewCell

- (void)awakeFromNib
{
    [self.titleLabel changeFontFamily];
    [self.destTitleLabel changeFontFamily];
    [self.detailLabel changeFontFamily];
    
}
-(void)setCarSearchItem:(CarSearchItem *)carSearchItem withIndexPath:(NSIndexPath*)indexPath
{
    _carSearchItem = carSearchItem;
    
    [self loadCellWithIdentifier:indexPath.row];
}
#pragma mark - Private Method.

- (void)loadCellWithIdentifier:(NSInteger)value
{
    
    switch (value)
    {
        case 0:
            [self loadFirstCell];
            break;
        case 1:
            [self loadSecondCell];
            break;
        case 2:
            [self loadThirdCell];
            break;
        case 3:
            [self loadFourthCell];
            break;
        case 4:
            [self loadFifthCell];
            break;
        default:
            break;
    }
    
}

- (void)loadFirstCell
{
    self.destTitleLabel.text = (self.carSearchItem.DestinationCity)?:@"Select Destination";
    ;
}
- (void)loadSecondCell
{
    self.titleLabel.text = @"Passengers";
    self.detailLabel.text = (self.carSearchItem.noOfPassenger)?:@"Select";
}
- (void)loadThirdCell
{
    self.titleLabel.text = @"PickUp Point";
    self.detailLabel.text = (self.carSearchItem.pickUp)?:@"Select";
    
}
- (void)loadFourthCell
{
    self.titleLabel.text = @"Dropof Point";
    self.detailLabel.text = (self.carSearchItem.dropOff)?:@"Select";
    
}
- (void)loadFifthCell
{
    self.titleLabel.text = @"Date";
    NSDate *date = self.carSearchItem.departDate;
    self.detailLabel.text = [NSString stringWithFormat:@"%@ %@ ' %@", [date getDayOfTheMonth],[date getMonth],[date getYear]];
}
-(void)showPickerViewForIndex:(NSInteger)tag
{
    [self showPickUpPickerViewUsingTag:tag];
}
-(void)showDatePickerViewForIndex:(NSIndexPath*)indexPath
{
    [self showDatePicker];
}

- (void)showDatePicker
{
    IQActionSheetPickerView *picker = [[IQActionSheetPickerView alloc] initWithTitle:@"Select Date" delegate:self];
    [picker setMinimumDate:[NSDate date]];
    [picker setMaximumDate:[[NSDate date] dateByAddingTimeInterval:KTimeInterval * 365]];

    [picker setTag:6];
    [picker setActionSheetPickerStyle:IQActionSheetPickerStyleDatePicker];
    [picker show];
}

-(void)actionSheetPickerView:(IQActionSheetPickerView *)pickerView didSelectTitles:(NSArray *)titles
{
    self.detailLabel.text = [NSString stringWithFormat:@"%@", [titles objectAtIndex:0]];
    switch ([pickerView tag]) {
        case 0:
            self.carSearchItem.noOfPassenger = [titles objectAtIndex:0];
            break;
        case 1:
            self.carSearchItem.pickUp = [titles objectAtIndex:0];
            break;
        case 2:
            self.carSearchItem.dropOff = [titles objectAtIndex:0];
            break;
        default:
            break;
    }
}

-(void)actionSheetPickerView:(IQActionSheetPickerView *)pickerView didSelectDate:(NSDate *)date
{
    self.detailLabel.text = [NSString stringWithFormat:@"%@ %@ ' %@", [date getDayOfTheMonth],[date getMonth],[date getYear]];
    self.carSearchItem.departDate = date;
}
- (void)showPickUpPickerViewUsingTag:(NSInteger)tag
{
    NSArray *titlesComponents;
    NSString *title;
    switch (tag) {
        case 0:
            titlesComponents = @[@[@"1",@"2", @"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"11",@"12", @"13",@"14",@"15",@"16",@"17",@"18",@"19",@"20",@"21",@"22",@"23",@"24",@"25"]];
            title = @"Select Passengers";
            break;
        case 1:
            titlesComponents = @[@[@"Airport",@"Accomodation", @"Port", @"Station",@"Other"]];
            title = @"PickUp Point";
            break;
        case 2:
            titlesComponents = @[@[@"Airport",@"Accomodation", @"Port", @"Station",@"Other"]];
            title = @"DropOff Point";
            break;
        default:
            break;
    }
    IQActionSheetPickerView *picker = [[IQActionSheetPickerView alloc] initWithTitle:title delegate:self];
    picker.titleFont = [UIFont fontWithName:KCustomeFont size:14];
    [picker setTitlesForComponents:titlesComponents];
    [picker setTag:tag];
    [picker show];
}

@end
