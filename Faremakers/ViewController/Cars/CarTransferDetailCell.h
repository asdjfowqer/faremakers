//
//  CarTransferDetailCell.h
//  Faremakers
//
//  Created by Mac1 on 23/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainColloctionViewDelegate.h"

@class CarsItem;

@interface CarTransferDetailCell : UITableViewCell

@property (strong, nonatomic) CarsItem *carsItem;

@property (weak, nonatomic) IBOutlet UILabel *itemValueLabel;

@property (nonatomic,weak)id<MainColloctionViewDelegate> aDelegate;

@property (weak, nonatomic) IBOutlet UICollectionView *stopsCollectionView;

-(void)LoadMenuItemsForColltionView:(UICollectionView*)collectionView;

- (void)setCarsSearItem:(CarsItem *)carItem withTripIndex:(NSInteger)index;

@end
