//
//  FlightBookingCell.h
//  Faremakers
//
//  Created by Mac1 on 30/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TransferVehicles;
@class CarsItem;
@class CarSearchItem;

@interface CarBookingCell : UITableViewCell

@property (nonatomic) TransferVehicles *transferVehicle;
@property (nonatomic) CarsItem *carItem;
@property (nonatomic) CarSearchItem *searchItem;
@property (nonatomic) NSString *reUseIdentifier;


#pragma mark CarBookingCell0

@property (weak, nonatomic) IBOutlet UILabel *travellingTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *deparTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *deparDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *deparCityLabel;
@property (weak, nonatomic) IBOutlet UILabel *travellTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *destTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *destDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *destCityLabel;

#pragma mark CarBookingCell1
@property (weak, nonatomic) IBOutlet UILabel *travellCompanyTitle;
@property (weak, nonatomic) IBOutlet UILabel *carDetailLabel;
@property (weak, nonatomic) IBOutlet UILabel *numberOfSeatsLabel;
@property (weak, nonatomic) IBOutlet UILabel *boardingTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *boardingDetailLabel;

#pragma mark CarBookingCell2
@property (weak, nonatomic) IBOutlet UILabel *PoliciesLabel;

#pragma mark CarBookingCell3
@property (weak, nonatomic) IBOutlet UILabel *chargesTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *chargesCostLabel;

-(void)setTransferVehicle:(TransferVehicles *)transferVehicle forIndex:(NSInteger)index;

@end
