//
//  CarListingsDetailCell.h
//  Faremakers
//
//  Created by Mac1 on 22/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CarListingsDetailCellDelegate <NSObject>

- (void) BookCarButtonDidSelected:(id)sender;

@end

@class CarsItem;
@class CarSearchItem;
@class TransferVehicles;

@interface CarListingsDetailCell : UITableViewCell

@property (nonatomic)CarsItem *item;
@property (nonatomic,strong)CarSearchItem *searchItem;
@property (nonatomic) TransferVehicles *transferVehical;
@property (weak, nonatomic) id<CarListingsDetailCellDelegate> adelegate;

@property (weak, nonatomic) IBOutlet UILabel *maxPassLabel;
@property (weak, nonatomic) IBOutlet UILabel *maxLuggLabel;
@property (weak, nonatomic) IBOutlet UILabel *vehicleTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *travellCityLabel;
@property (weak, nonatomic) IBOutlet UILabel *durationLabel;
@property (weak, nonatomic) IBOutlet UILabel *availableLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

- (IBAction)bookingBtnSelector:(id)sender;
@end
