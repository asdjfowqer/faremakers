//
//  FlightBookingCell.m
//  Faremakers
//
//  Created by Mac1 on 30/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "CarBookingCell.h"
#import "UILabel+FontName.h"
#import "TransferVehicles.h"
#import "CarsItem.h"
#import "CarSearchItem.h"
#import "NSDate+DateComponents.h"

@implementation CarBookingCell

- (void)awakeFromNib
{
    [self.travellingTitleLabel changeFontFamily];
    [self.deparTimeLabel changeFontFamily];
    [self.deparDateLabel changeFontFamily];
    [self.deparCityLabel changeFontFamily];
    [self.travellTimeLabel changeFontFamily];
    [self.destTimeLabel changeFontFamily];
    [self.destDateLabel changeFontFamily];
    [self.destCityLabel changeFontFamily];
    [self.travellCompanyTitle changeFontFamily];
    [self.carDetailLabel changeFontFamily];
    [self.numberOfSeatsLabel changeFontFamily];
    [self.boardingTitleLabel changeFontFamily];
    [self.boardingDetailLabel changeFontFamily];
    [self.PoliciesLabel changeFontFamily];
    [self.chargesTitleLabel changeFontFamily];
    [self.chargesCostLabel changeFontFamily];
}

-(void)setTransferVehicle:(TransferVehicles *)transferVehicle forIndex:(NSInteger)index
{
    _transferVehicle = transferVehicle;
    
    int value = [[_reUseIdentifier substringFromIndex: [_reUseIdentifier length] - 1] intValue];
    
    switch (value)
    {
        case 0:
            [self loadFirstCell];
            break;
        case 1:
            [self loadSecondCell];
            break;
        case 2:
            [self loadSecondCell];
            break;
        case 3:
            [self loadThirdCell:index];
            break;
        default:
            break;
    }
    
}

-(void)loadFirstCell
{
    self.travellingTitleLabel.text = self.transferVehicle.vehicleValue;
    self.deparCityLabel.text = self.searchItem.pickUp;
    self.destCityLabel.text = self.searchItem.dropOff;
    self.travellTimeLabel.text = self.carItem.approxTransValue;
    self.deparDateLabel.text = [NSString stringWithFormat:@"%@ %@ %@'%@",[self.searchItem.departDate getDayOfTheWeek],[self.searchItem.departDate getDayOfTheMonth],[self.searchItem.departDate getMonth],[self.searchItem.departDate getYear]];
    self.destDateLabel.text = [NSString stringWithFormat:@"%@ %@ %@'%@",[self.searchItem.departDate getDayOfTheWeek],[self.searchItem.departDate getDayOfTheMonth],[self.searchItem.departDate getMonth],[self.searchItem.departDate getYear]];
    self.deparTimeLabel.text = self.searchItem.DestinationCity;
    self.destTimeLabel.text = self.searchItem.DestinationCity;
        
}
-(void)loadSecondCell
{
    // self.destCityLabel.text = self.transferVehicle;
    
    self.travellCompanyTitle.text = [NSString stringWithFormat:@"Availabilty:" ];
    self.boardingDetailLabel.text = self.transferVehicle.confirmationDescription;
    self.carDetailLabel.text = [NSString stringWithFormat:@"Max Passengers: %@" , self.transferVehicle.maximumPassengers];//@"";
    self.numberOfSeatsLabel.text = [NSString stringWithFormat:@"Max Luggage: %@" ,self.transferVehicle.maximumLuggage];//@"";
    self.boardingTitleLabel.text = self.carItem.itemValue;
    
}
-(void)loadThirdCell:(NSInteger)index
{
    if (index == 0) {
        self.chargesCostLabel.text = [NSString stringWithFormat:@"%@ %@",self.transferVehicle.vehicleItemPriceValue,self.transferVehicle.vehicleItemPriceCurrency];
        
    }
    else if (index == 3)
        self.chargesCostLabel.text = [NSString stringWithFormat:@"%@ %@",self.transferVehicle.vehicleItemPriceValue,self.transferVehicle.vehicleItemPriceCurrency];
    else
        self.chargesCostLabel.text = @"0";
    
    
}
@end
