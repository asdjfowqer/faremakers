//
//  FlightBookingViewController.m
//  Faremakers
//
//  Created by Mac1 on 29/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "CarBookingViewController.h"
#import "CheckOutRootController.h"
#import "TransitionDelegate.h"
#import "ListingHeaderView.h"
#import "TransferVehicles.h"
#import "BookingPromtView.h"
#import "CheckOutItems.h"
#import "CarBookingCell.h"
#import "CarsItem.h"
#import "CarSearchItem.h"
#import "Cities.h"
#import "FMUser.h"

@interface CarBookingViewController ()
{
    NSArray *SectionsData;
    NSArray *DataArray;
    NSArray *flightsDataArray;

}
@property (nonatomic, strong) TransitionDelegate *transitionController;

@end

@implementation CarBookingViewController

//layover_img@2x,devider@3x
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Review Booking";
    [self.tableView registerNib:[UINib nibWithNibName:@"ListingsHeaderView" bundle:nil] forHeaderFooterViewReuseIdentifier:@"ListingHeaderView"];
    _transferVehicle = self.carItem.transferVehicles[self.SelectTransferVehicle];
    SectionsData = @[@"Booking Detail",@"Fare Details"];
    flightsDataArray = @[@"a",@"a"];
    DataArray = @[@[@"Baggage",@"Fare Rules"],
                  @[@"Base Fare",@"Taxes",@"Grand Total"],
                  @[@"$ 200",@"$ 20",@"$ 35",@"$ 235"]];
	
	
	[self addCarTransaction];
}

-(void)addCarTransaction
{
	TransactionBookingPaidCar* transaction = [TransactionBookingPaidCar anInstance];
	
	transaction.vehicleCode = self.carItem.vehicleCodes;
	transaction.vehicleValues = self.carItem.vehicleValues;
	transaction.transferId = self.transferVehicle.transferId;
	transaction.vehicleItemPriceCurrency = self.transferVehicle.vehicleItemPriceCurrency;
	transaction.vehicleItemPriceValue = self.transferVehicle.vehicleItemPriceValue;
	transaction.departureDate = self.searchItem.departDate;
	transaction.fromCity = self.searchItem.DestinationCity;
	transaction.paxQuantity = [NSNumber numberWithInt:[self.searchItem.noOfPassenger intValue]];
	transaction.origin = self.searchItem.destCity.airPortName;
	transaction.desination = self.searchItem.destCity.text;
	transaction.orderInfo = @"Book Car";
	transaction.orderName = @"Car";
	transaction.firstName = [FMUser defaultUser].firstName;
	transaction.lastName = [FMUser defaultUser].lastName;
	transaction.phoneNumber = [FMUser defaultUser].phoneNumber;
	transaction.email = [FMUser defaultUser].email;
	transaction.amount = self.carItem.maximumPrice;

	transaction.hours = @(0);
	transaction.min = @(0);
	transaction.flightNo = @"";
	transaction.specialRequests = @"";

	[TransactionBookingManager shared].currentTransaction = transaction;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0)
        return 44;
    else
        return 30;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 0)
    {
        ListingHeaderView *header=[tableView dequeueReusableHeaderFooterViewWithIdentifier:@"ListingHeaderView"];
        [header showSectionLabel:NO];
        [header.refundView setHidden:YES];
        return header;
    }
    else{
        ListingHeaderView *header=[tableView dequeueReusableHeaderFooterViewWithIdentifier:@"ListingHeaderView"];
        [header showSectionLabel:YES];
        [header.sectionTitleLabel setText:SectionsData[section]];
        return header;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *array = [DataArray objectAtIndex:section];
    if (section == 0)
      return  flightsDataArray.count + array.count;
    return array.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row < 2)
    {
        return  120;
    }
    return 55;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self prepareCellForTableView:tableView forIndexPath:indexPath];
}
- (id)prepareCellForTableView:(UITableView*)tableView forIndexPath:(NSIndexPath*)indexPath
{
    CarBookingCell *cell;
    
    
    if (indexPath.section == 0)
    {
        if (indexPath.row == 0)
        {
            cell = [self.tableView dequeueReusableCellWithIdentifier:@"CarBookingCell0" forIndexPath:indexPath];
            cell.reUseIdentifier = @"CarBookingCell0";
            
        }
        else  if (indexPath.row == 1)
        {
            cell = [self.tableView dequeueReusableCellWithIdentifier:@"CarBookingCell1" forIndexPath:indexPath];
            cell.reUseIdentifier = @"CarBookingCell1";

        }
        else
        {
            
            cell = [self.tableView dequeueReusableCellWithIdentifier:@"CarBookingCell2" forIndexPath:indexPath];
            cell.PoliciesLabel.text = DataArray [indexPath.section][indexPath.row -2];
            cell.reUseIdentifier = @"CarBookingCell2";
            
        }
    }
    else
    {
        cell = [self.tableView dequeueReusableCellWithIdentifier:@"CarBookingCell3" forIndexPath:indexPath];
        cell.chargesTitleLabel.text = DataArray [indexPath.section][indexPath.row];
        cell.chargesCostLabel.text = DataArray [indexPath.section + 1][indexPath.row];
        cell.reUseIdentifier = @"CarBookingCell3";
        
    }
    cell.carItem = self.carItem;
    cell.searchItem = self.searchItem;
    [cell setTransferVehicle:self.transferVehicle forIndex:indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (IBAction)continueBtnSelector:(id)sender
{
    CheckOutRootController *bookingController = [[CheckOutRootController alloc] init];
    [bookingController setCheckOutItem:[[CheckOutItems alloc] initWithType:KCarItem withSearchItem:nil andSelectedItem:nil SubSelectedItem:nil andGrandTotal:[NSString stringWithFormat:@"%@ %@",self.transferVehicle.vehicleItemPriceValue,self.transferVehicle.vehicleItemPriceCurrency]]];
    [bookingController pushProcesBookingViewController:self.navigationController];
  //  [self showFareRuleViewController];

}

@end
