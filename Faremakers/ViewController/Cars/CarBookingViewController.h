//
//  FlightBookingViewController.h
//  Faremakers
//
//  Created by Mac1 on 29/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TransferVehicles;
@class CarsItem;
@class CarSearchItem;

@interface CarBookingViewController : UIViewController <UITableViewDataSource , UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *view1;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic) TransferVehicles *transferVehicle;
@property (nonatomic) CarsItem *carItem;
@property (nonatomic) CarSearchItem *searchItem;
@property (nonatomic) NSInteger SelectTransferVehicle;


- (IBAction)continueBtnSelector:(id)sender;


@end
