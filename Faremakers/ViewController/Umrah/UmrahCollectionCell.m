//
//  UmrahCollectionCell.m
//  Faremakers
//
//  Created by Mac1 on 15/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "UmrahCollectionCell.h"
#import <SDWebImage/SDWebImagePrefetcher.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "UILabel+FontName.h"
#import "UmrahPackage.h"
#import "Constants.h"

@implementation UmrahCollectionCell

-(void)awakeFromNib
{
    [self.titleLabel changeFontFamily];
}
-(void)setItem:(UmrahPackage*)dict ForIndex:(NSIndexPath*)index
{
    self.titleLabel.text = dict.packageName;
    if (dict.imageUrls.count)
    {
        SDWebImagePrefetcher *prefetcher = [SDWebImagePrefetcher sharedImagePrefetcher];
        NSString *urlString = [[BASE_URL stringByDeletingLastPathComponent] stringByAppendingPathComponent:dict.imageUrls[0]];
        NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        [prefetcher prefetchURLs:@[url]];
        
        [self.imageview sd_setImageWithURL:[NSURL URLWithString:urlString]
                                   placeholderImage:self.imageview.image
                                            options:SDWebImageRetryFailed
                                          completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
             [self.activityView stopAnimating];
             
             if (error)
                 ;//self.PromotionImageView.image = placeholderImage;
             else
                 self.imageview.image = image;
             
         }];
    }
}
@end
