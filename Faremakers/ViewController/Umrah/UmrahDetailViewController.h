//
//  UmrahDetailViewController.h
//  Faremakers
//
//  Created by Mac1 on 03/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MWPhotoBrowser.h"


@class UmrahPackage;
@class UmrahDetailHeaderView;

@interface UmrahDetailViewController : UIViewController

@property (nonatomic)UmrahPackage *umrahpackage;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UmrahDetailHeaderView *umrahDetailHeader;
- (IBAction)bookNowButtonPressed:(id)sender;

@end
