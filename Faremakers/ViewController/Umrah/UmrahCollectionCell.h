//
//  UmrahCollectionCell.h
//  Faremakers
//
//  Created by Mac1 on 15/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <UIKit/UIKit.h>

@class UmrahPackage;

@interface UmrahCollectionCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageview;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityView;

-(void)setItem:(UmrahPackage*)dict ForIndex:(NSIndexPath*)index;

@end
