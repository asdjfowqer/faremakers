//
//  UmrahPackageCellView.h
//  Faremakers
//
//  Created by Mac1 on 18/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <UIKit/UIKit.h>

@class UmrahPackage;

@interface UmrahPackageCellView : UITableViewCell

@property (nonatomic) UmrahPackage *umrahpackage;
@property (weak, nonatomic) IBOutlet UIImageView *ImageView;
@property (weak, nonatomic) IBOutlet UILabel *packageNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *packageFareLabel;
@property (weak, nonatomic) IBOutlet UILabel *packageDateLabel;
@property (weak,nonatomic) IBOutlet UIActivityIndicatorView *activityView;

@end
