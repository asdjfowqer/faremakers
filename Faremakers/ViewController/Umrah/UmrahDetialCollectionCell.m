//
//  UmrahDetialCollectionCell.m
//  Faremakers
//
//  Created by Mac1 on 03/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "UmrahDetialCollectionCell.h"
#import "UmrahPackage.h"
#import "Constants.h"
#import <SDWebImage/SDWebImagePrefetcher.h>
#import <SDWebImage/UIImageView+WebCache.h>


@implementation UmrahDetialCollectionCell

-(void)setItem:(UmrahPackage*)dict ForIndex:(NSIndexPath*)index
{
    [self.activityView startAnimating];

    if (dict.imageUrls.count)
    {
        NSString *urlString = [[BASE_URL stringByDeletingLastPathComponent] stringByAppendingPathComponent:dict.imageUrls[index.row]];
        
        [self.imageview sd_setImageWithURL:[NSURL URLWithString:urlString]
                          placeholderImage:self.imageview.image
                                   options:SDWebImageRetryFailed
                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
             [self.activityView stopAnimating];
             
             if (error)
                 ;//self.imageview.image = [UIImage imageNamed:@"no-images.png"];
             else
                 self.imageview.image = image;
             
         }];
    }
}

@end
