//
//  UmrahDetailCell.h
//  Faremakers
//
//  Created by Mac1 on 03/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <UIKit/UIKit.h>

@class UmrahPackage;

@interface UmrahDetailCell : UITableViewCell

#pragma mark UmrahDetailCell 0

@property (nonatomic,strong)UmrahPackage *umrahpackage;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;

#pragma mark UmrahDetailCell 1
@property (weak, nonatomic) IBOutlet UILabel *packageTitle;
@property (weak, nonatomic) IBOutlet UILabel *packageDetail;

-(void)setUmrahpackage:(UmrahPackage *)umrahpackage withIndexPath:(NSIndexPath*)indexPath;

@end
