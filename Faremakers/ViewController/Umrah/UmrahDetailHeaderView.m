//
//  UmrahDetailHeaderView.m
//  Faremakers
//
//  Created by Mac1 on 03/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "UmrahDetailHeaderView.h"
#import <SDWebImage/SDWebImagePrefetcher.h>
#import "UmrahPackage.h"
#import "UmrahDetialCollectionCell.h"
#import "Constants.h"
#import "NSDate+DateComponents.h"

@implementation UmrahDetailHeaderView

-(void)setUmrahpackage:(UmrahPackage *)umrahpackage
{
    _umrahpackage = umrahpackage;
    
    NSDateFormatter *dateFormater = [[NSDateFormatter alloc] init];
    [dateFormater setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    
    NSDate *date = [dateFormater dateFromString:_umrahpackage.packagerDateTime];
    self.dateLable.text = [NSString stringWithFormat:@"%@ %@ %@'%@",[date getDayOfTheWeek],[date getDayOfTheMonth],[date getMonth],[date getYear]];
    self.typeLabel.text = _umrahpackage.packageFare;
    self.collectionView.delegate =self;
    self.collectionView.dataSource = self;
    [self.collectionView reloadData];
    self.pager.numberOfPages = self.umrahpackage.imageUrls.count;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.umrahpackage.imageUrls.count;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout  *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    return CGSizeMake(self.frame.size.width, collectionView.frame.size.height);
    
}
- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    self.pager.currentPage = indexPath.row;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UmrahDetialCollectionCell *collectionCell = [self.collectionView dequeueReusableCellWithReuseIdentifier:@"UmrahDetialCollectionCell" forIndexPath:indexPath];
    [collectionCell setItem:self.umrahpackage ForIndex:indexPath];
    return collectionCell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    [self.adelegate UmrahDetailHeaderViewDidReceiveTap:indexPath];
}

@end
