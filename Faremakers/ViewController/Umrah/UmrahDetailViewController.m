//
//  UmrahDetailViewController.m
//  Faremakers
//
//  Created by Mac1 on 03/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "UmrahDetailViewController.h"
#import <SDWebImage/SDWebImagePrefetcher.h>
#import "TransitionDelegate.h"
#import "BookingPromtView.h"
#import "UmrahDetailHeaderView.h"
#import "NSString+Height.h"
#import "UmrahDetailCell.h"
#import "UmrahPackage.h"
#import "Constants.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "TransactionBookingPaidUmrah.h"
#import "BookingApiManager.h"

@interface UmrahDetailViewController () <MWPhotoBrowserDelegate, UmrahDetailHeaderViewDelagate>
{
    NSMutableArray *photos;
}

@property (nonatomic, strong) TransitionDelegate *transitionController;

@end

@implementation UmrahDetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 160.0;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

    SDWebImagePrefetcher *prefetcher = [SDWebImagePrefetcher sharedImagePrefetcher];

    NSMutableArray *urls = [NSMutableArray array];
    for (int i = 0; i < self.umrahpackage.imageUrls.count; i++)
    {
        NSString *urlString = [[BASE_URL stringByDeletingLastPathComponent] stringByAppendingPathComponent:self.umrahpackage.imageUrls[i]];
        NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        [urls addObject:url];
    }
    [prefetcher prefetchURLs:urls];

    [self loadHeaderView];
}

- (void)loadHeaderView
{
    [self.umrahDetailHeader setUmrahpackage:self.umrahpackage];
    self.umrahDetailHeader.adelegate = self;
    self.tableView.tableHeaderView = self.umrahDetailHeader;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)animateTableViewToTop
{
    DLog(@"%.2f", self.view.frame.origin.y);
    [self.tableView setContentOffset:CGPointZero];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row)
    {
        case 0: {
            float height = 46;
            UIFont *font = [UIFont fontWithName:KCustomeRagularFont size:12.0];
            CGFloat width = self.tableView.frame.size.width - 30;
            CGRect titleRect = [NSString
                                setAttributeWithString:self.umrahpackage.itenerary
                                withLineSpacing:kLineHeightSpacing
                                withSize:CGSizeMake(width, CGFLOAT_MAX)
                                withFont:font];

            CGFloat titleHeight = ceilf(titleRect.size.height);

            if (titleHeight > 20)
            {
                titleHeight -= 17;
            }
            return height + titleHeight;
        }
            break;

        default:
            return 60;
            break;
    }
    //return (indexPath.row)?60:68;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger index = (indexPath.row) ? 1 : 0;
    NSString *resueIdentifier = [NSString stringWithFormat:@"UmrahDetailCell%ld", index];
    UmrahDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:resueIdentifier];
    [cell setUmrahpackage:self.umrahpackage withIndexPath:indexPath];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (IBAction)filterButtonPressed:(id)sender
{
}

- (void)UmrahDetailHeaderViewDidReceiveTap:(NSIndexPath *)indexPath
{
    [self showImageGallary:indexPath.row];
}

- (void)showImageGallary:(NSInteger)index
{
    if (self.umrahpackage.imageUrls.count < 1)
    {
        return;
    }
    photos = [[NSMutableArray alloc] init];
    MWPhoto *photo;
    for (NSString *imageLink in self.umrahpackage.imageUrls)
    {
        NSString *urlString = [[BASE_URL stringByDeletingLastPathComponent] stringByAppendingPathComponent:imageLink];
        photo = [MWPhoto photoWithURL:[NSURL URLWithString:urlString]];
        [photos addObject:photo];
    }
    if (photos.count)
    {
        // Create browser
        MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
        browser.displayActionButton = NO;
        browser.displayNavArrows = NO;
        browser.displaySelectionButtons = NO;
        browser.alwaysShowControls = NO;
        browser.zoomPhotosToFill = YES;
        browser.enableGrid = NO;
        browser.startOnGrid = NO;
        browser.enableSwipeToDismiss = NO;
        browser.autoPlayOnAppear = NO;
        [browser setCurrentPhotoIndex:index];
        {
            // Modal
            UINavigationController *navigationControlller = [[UINavigationController alloc] initWithNavigationBarClass:[FMNavigationBar class] toolbarClass:nil];
            [navigationControlller setViewControllers:@[browser]];
            navigationControlller.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            [self presentViewController:navigationControlller animated:YES completion:nil];
        }
    }
}

- (IBAction)bookNowButtonPressed:(id)sender
{
    if ([[FMUser defaultUser] isSessionAlive]) {
        [self showBookingPromtViewController];
    }
    else{
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Login" message:@"Please login to book" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alertView show];
    }
}

- (void)showBookingPromtViewController
{
    [SVProgressHUD show];

    TransactionBookingPaidUmrah *transaction = [[TransactionBookingPaidUmrah alloc] init];
    transaction.packageId = self.umrahpackage.packageId;

    [TransactionBookingManager shared].currentTransaction = transaction;

    [BookingApiManager postPackageBooking:transaction
                         withSuccessBlock: ^(NSDictionary *responseobject) {
                             [SVProgressHUD dismiss];

                             NSNumber *statusCode = [[responseobject objectForKey:@"data"] objectForKey:@"statusCode"];
                             if ([statusCode integerValue] == 200)
                             {
                                 dispatch_async(dispatch_get_main_queue(), ^{
                                     [self showAlertControllerWithMessage:[[responseobject objectForKey:@"data"] objectForKey:@"serviceResponse"]];
                                     [self.navigationController popToRootViewControllerAnimated:YES];
                                 });
                             }
                             else
                             {
                                 [self showErrorAlertControllerWithMessage:[[responseobject objectForKey:@"data"] objectForKey:@"serviceResponse"]];
                             }
                         }
                             failureBlock: ^(NSError *error) {
                                 [SVProgressHUD dismiss];
                                 [self showErrorAlertControllerWithMessage:error.localizedRecoverySuggestion];
                             }];
}

- (void)showAlertControllerWithMessage:(NSString *)message
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Success" message:message delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil];
    [alertView show];
}

- (void)showErrorAlertControllerWithMessage:(NSString *)message
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:message delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil];
    [alertView show];
}

#pragma mark - MWPhotoBrowserDelegate

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser
{
    return photos.count;
}

- (id <MWPhoto> )photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index
{
    if (index < photos.count)
    {
        return [photos objectAtIndex:index];
    }
    return nil;
}

- (void)photoBrowser:(MWPhotoBrowser *)photoBrowser didDisplayPhotoAtIndex:(NSUInteger)index
{
    DLog(@"Did start viewing photo at index %lu", (unsigned long)index);
}

- (void)photoBrowserDidFinishModalPresentation:(MWPhotoBrowser *)photoBrowser
{
    // If we subscribe to this method we must dismiss the view controller ourselves
    DLog(@"Did finish modal presentation");
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end