//
//  UmrahDetialCollectionCell.h
//  Faremakers
//
//  Created by Mac1 on 03/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <UIKit/UIKit.h>

@class UmrahPackage;

@interface UmrahDetialCollectionCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageview;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityView;

-(void)setItem:(UmrahPackage*)dict ForIndex:(NSIndexPath*)index;
@end
