//
//  UmrahPackageViewController.h
//  Faremakers
//
//  Created by Mac1 on 15/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RESideMenu.h"

@interface UmrahPackageViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UITabBarDelegate,UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UICollectionView *pakageCollectionView;
@property (nonatomic) NSArray *packages;
@end
