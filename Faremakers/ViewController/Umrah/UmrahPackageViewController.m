//
//  UmrahPackageViewController.m
//  Faremakers
//
//  Created by Mac1 on 15/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "UmrahPackageViewController.h"
#import "HolidayFilterViewController.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "UmrahDetailViewController.h"
#import "PromotionDataApiManager.h"
#import "UmrahPackageCellView.h"
#import "HolidayFilterItem.h"
#import "UmrahPackage.h"
#import "AppDelegate.h"

@interface UmrahPackageViewController()<HolidaysFilterViewDelegate>
{
    HolidayFilterItem *filterItem;
    NSArray *tmpHotelList;
    NSArray *minMaxPrices;
    
}
@end
@implementation UmrahPackageViewController


-(void)viewDidLoad
{
    [super viewDidLoad];
    
    filterItem = [[HolidayFilterItem alloc] init];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 300;
    
    self.tableView.tableFooterView = [UIView new];
    [self loadUmrahPackages];
    [self ShowActivityView];
}
-(void)loadUmrahPackages
{
    [PromotionDataApiManager getUmrahPackagesWithsuccessBlock:^(id responseobject)
     {
         self.packages = [[NSArray alloc] initWithArray:responseobject];
         
         if (!self.packages.count) {
             [self ShowActivityView];
         }
         else
         {
             [self.tableView reloadData];
             [self ShowActivityView];
             tmpHotelList = [self.packages mutableCopy];
             [self prepareFilterPriceRanges];
         }
         
     } failureBlock:^(NSError *error) {
         [self ShowActivityView];
         
     }];
}

-(void)prepareFilterPriceRanges
{
    minMaxPrices =[UmrahPackage findMinMaxPrices:tmpHotelList];
    filterItem.minPrice = minMaxPrices[0];
    filterItem.maxprice = minMaxPrices[1];
    [filterItem preparePriceFilterRanges];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.packages.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UmrahPackageCellView *cell = [self.tableView dequeueReusableCellWithIdentifier:@"UmrahPackageCellView" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.umrahpackage = self.packages[indexPath.row];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UmrahDetailViewController *detailView = [self.storyboard instantiateViewControllerWithIdentifier:@"UmrahDetailViewController"];
    detailView.umrahpackage = self.packages[indexPath.row];
    [self.navigationController pushViewController:detailView animated:YES];
}
-(void)ShowActivityView
{
    UIView *view = [self.view viewWithTag:707];
    if (view)
    {
        [view removeFromSuperview];
        [[AppDelegate sharedDelegate] hideLoadingViewForView:self];
        
    }
    else
    {
        view = [[UIView alloc] initWithFrame:self.view.bounds];
        view.tag = 707;
        view.alpha = 0.5;
        
        [view setBackgroundColor:[UIColor whiteColor]];
        [self.view addSubview:view];
        [self.view bringSubviewToFront:view];
        [[AppDelegate sharedDelegate] showLoadingViewForView:self withText:@"Loading Umrah Packages..."];
    }
}
-(void)sortUmrahPackages
{
    NSArray *sortedArray;
    
    sortedArray = [self.packages sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        
        UmrahPackage *package1 = obj1;
        UmrahPackage *package2 = obj2;
        
        if ( package1.packageFare.integerValue < package2.packageFare.integerValue) {
            
            return (NSComparisonResult)NSOrderedAscending;
            
        }
        else if (package1.packageFare.integerValue > package2.packageFare.integerValue) {
            
            return (NSComparisonResult)NSOrderedDescending;
            
        }
        return (NSComparisonResult)NSOrderedSame;
    }];
    _packages = [sortedArray mutableCopy];
    [self.tableView reloadData];
}
- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    NSInteger tag = [item tag];
    
    switch (tag) {
        case 0:
            [self sortUmrahPackages];
            break;
        case 1:
        {
            [self filterButtonSelector:nil];
        }
            break;
        default:
            break;
    }
}
- (void)filterButtonSelector:(id)sender
{
    HolidayFilterViewController *filterView = [self.storyboard instantiateViewControllerWithIdentifier:@"HolidayFilterViewController"];
    filterView.filterItem = filterItem;
    filterView.adelegate = self;
    
    UINavigationController *navigationControlller = [[UINavigationController alloc] initWithNavigationBarClass:[FMNavigationBar class] toolbarClass:nil];
    [navigationControlller setViewControllers:@[filterView]];
    
    [self.navigationController presentViewController:navigationControlller animated:YES completion:nil];
}

#pragma mark filterViewDelegate

- (void)holidaysFilterViewDidDissmissWithFilterItem:(id)item
{
    filterItem = (HolidayFilterItem*)item;
    [self perfomFilters];
}

- (void) holidaysfilterViewDidSelectClrAllFilters:(BOOL)clear
{
    [filterItem clearAllFilters];
    
    [SVProgressHUD showWithStatus:@"Applying Filters"];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        _packages = [tmpHotelList mutableCopy];
        [self.tableView reloadData];
        [SVProgressHUD showSuccessWithStatus:@"Done"];
    });
}

- (void) perfomFilters
{
    
    [SVProgressHUD showWithStatus:@"Applying Filters"];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        NSArray *SearchListCopy = [self.packages mutableCopy];
        NSMutableArray *array = [[NSMutableArray alloc] init];
        BOOL filterApplied = false;
        
        if(filterItem.price)
        {
            NSNumber *startingPrice;
            NSNumber *endingPrice ;
            switch ([[filterItem preparePriceRangeStrings] indexOfObject:filterItem.price]) {
                case 0:{
                    startingPrice = [NSNumber numberWithInteger:0];
                    endingPrice = filterItem.priceFilterRange[0];
                    DLog(@" case 0");
                }
                    break;
                case 1:
                {
                    startingPrice = filterItem.priceFilterRange[0];
                    endingPrice = filterItem.priceFilterRange[1];
                    DLog(@" case 1");
                }
                    break;
                case 2:
                {
                    startingPrice = filterItem.priceFilterRange[1];
                    endingPrice = filterItem.maxprice;
                    DLog(@" case 2");
                }
                    break;
                default:
                    break;
            }
            [SearchListCopy enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop)
             {
                 UmrahPackage *Obj = (UmrahPackage*)obj;
                 if (Obj.packageFare.integerValue > startingPrice.integerValue && Obj.packageFare.integerValue <= endingPrice.integerValue) {
                     [array addObject:Obj];
                 }
                 
             }];
            
            tmpHotelList = [array mutableCopy];
            filterApplied = true;
            
        }
        if (!filterApplied)
        {
            tmpHotelList = [self.packages mutableCopy];
        }
        [self.tableView reloadData];
        
        [SVProgressHUD showSuccessWithStatus:@"Done"];
    });
    
}

@end
