//
//  UmrahDetailHeaderView.h
//  Faremakers
//
//  Created by Mac1 on 03/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//


#import <UIKit/UIKit.h>

@protocol UmrahDetailHeaderViewDelagate <NSObject>

- (void)UmrahDetailHeaderViewDidReceiveTap:(NSIndexPath*)indexPath;

@end

@class UmrahPackage;

@interface UmrahDetailHeaderView : UIView <UICollectionViewDataSource,UICollectionViewDelegate>

@property (strong,nonatomic)UmrahPackage *umrahpackage;
@property (nonatomic, assign) id<UmrahDetailHeaderViewDelagate>  adelegate;
@property (weak,nonatomic)IBOutlet UICollectionView *collectionView;
@property (weak,nonatomic)IBOutlet UIPageControl *pager;
@property (weak,nonatomic)IBOutlet UILabel *dateLable;
@property (weak,nonatomic)IBOutlet UILabel *typeLabel;

@end
