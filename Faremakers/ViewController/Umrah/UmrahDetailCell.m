//
//  UmrahDetailCell.m
//  Faremakers
//
//  Created by Mac1 on 03/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "UmrahDetailCell.h"
#import "UmrahPackage.h"
#import "UILabel+FontName.h"
#import "NSDate+DateComponents.h"

@implementation UmrahDetailCell

- (void)awakeFromNib
{
    [self.titleLabel changeFontFamily];
    [self.detailLabel changeFontFamily];
    [self.packageTitle changeFontFamily];
    [self.packageDetail changeFontFamily];
}

-(void)setUmrahpackage:(UmrahPackage *)umrahpackage withIndexPath:(NSIndexPath*)indexPath
{
    _umrahpackage = umrahpackage;
   
    [self loadCellWithIdentifier:indexPath.row];
}
#pragma mark - Private Method.

- (void)loadCellWithIdentifier:(NSInteger)value
{
    
    switch (value)
    {
        case 0:
            [self loadFirstCollectionCell];
            break;
        case 1:
            [self loadSecondCollectionCell];
            break;
        case 2:
            [self loadThirdCollectionCell];
        default:
            break;
    }
    
}

- (void)loadFirstCollectionCell
{
    _titleLabel.text = _umrahpackage.packageName;
    _detailLabel.text = [NSString stringWithFormat:@"Itinerary: %@",_umrahpackage.itenerary];

}
- (void)loadSecondCollectionCell
{
    _packageTitle.text = @"Package Date";
    NSDate *date = [self parseDateFor];
    _packageDetail.text = [NSString stringWithFormat:@"%@ %@ %@'%@",[date getDayOfTheWeek],[date getDayOfTheMonth],[date getMonth],[date getYear]];
}
- (void)loadThirdCollectionCell
{
    _packageTitle.text = @"Package Price";
    _packageDetail.text = [NSString stringWithFormat:@"%@ %@",_umrahpackage.packageFare,_umrahpackage.packageCurrencyCode];

}
- (NSDate*)parseDateFor

{
    NSDateFormatter *dateFormater = [[NSDateFormatter alloc] init];
    [dateFormater setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    
    NSDate *date = [dateFormater dateFromString:_umrahpackage.packagerDateTime];
    
    if (!date) {
        _umrahpackage.packagerDateTime = [[_umrahpackage.packagerDateTime componentsSeparatedByString:@" - "] firstObject];
        [dateFormater setDateFormat:@"yyyy-MM-dd"];

        date = [dateFormater dateFromString:_umrahpackage.packagerDateTime];
        
        if (!date) {
            [dateFormater setDateFormat:@"yyyy-MM-dd '-' HH:mm:ss"];
            date = [dateFormater dateFromString:_umrahpackage.packagerDateTime];
        }
    }
    
    return date;

}
@end
