//
//  UmrahPackageCellView.m
//  Faremakers
//
//  Created by Mac1 on 18/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "UmrahPackageCellView.h"
#import <SDWebImage/SDWebImagePrefetcher.h>
#import <UIImageView+WebCache.h>
#import "NSDate+DateComponents.h"
#import "UmrahPackage.h"
#import "Constants.h"

@implementation UmrahPackageCellView

- (void)awakeFromNib
{
    [self.packageDateLabel changeFontFamily];
    [self.packageFareLabel changeFontFamily];
    [self.packageNameLabel changeFontFamily];
}

-(void)setUmrahpackage:(UmrahPackage *)umrahpackage
{
    _umrahpackage = umrahpackage;
    self.packageNameLabel.text = self.umrahpackage.packageName;
    
    NSDateFormatter *dateFormater = [[NSDateFormatter alloc] init];
    [dateFormater setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    
    NSDate *date = [dateFormater dateFromString:self.umrahpackage.packagerDateTime];
    
    self.packageDateLabel.text = [NSString stringWithFormat:@"%@ %@'%@ %@",[date getDayOfTheWeek],[date getDayOfTheMonth],[date getMonth],[date getYear]];//self.umrahpackage.packagerDateTime;
    self.packageFareLabel.text = [NSString stringWithFormat:@"%@ %@",self.umrahpackage.packageFare,self.umrahpackage.packageCurrencyCode];
    [self loadUmrahImage];
}

- (void)loadUmrahImage
{
    if (self.umrahpackage.imageUrls.count)
    {
        SDWebImagePrefetcher *prefetcher = [SDWebImagePrefetcher sharedImagePrefetcher];
        NSString *urlString = [[BASE_URL stringByDeletingLastPathComponent] stringByAppendingPathComponent:self.umrahpackage.imageUrls[0]];
        NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        [prefetcher prefetchURLs:@[url]];
        
        UIImage *placeHolder = [UIImage imageNamed:@"placeholder"];
        
        [self.ImageView sd_setImageWithURL:[NSURL URLWithString:urlString]
                          placeholderImage:placeHolder
                                   options:SDWebImageRetryFailed
                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
         {
             [self.activityView stopAnimating];
             
             if (error)
                 self.ImageView.image = placeHolder;
             else
                 self.ImageView.image = image;
             
         }];
    }
}

@end
