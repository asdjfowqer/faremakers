//
//  BookingProcessViewController.h
//  Faremakers
//
//  Created by Mac1 on 23/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Passengers;
@class CheckOutItems;

@interface BookingProcessViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *grandTotalLabel;
@property (nonatomic , strong) CheckOutItems *checkOutItem;

- (IBAction)proceedButtonPressed:(id)sender;

@end
