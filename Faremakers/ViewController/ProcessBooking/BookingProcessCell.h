//
//  BookingProcessCell.h
//  Faremakers
//
//  Created by Mac1 on 23/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Passengers;


@protocol BookingProcessCellDelegate <NSObject>

-(void)userEnteredPromocode:(NSString*)promocode;
-(void)userEnteredEmailAddress:(NSString*)email;

@end

@interface BookingProcessCell : UITableViewCell <UITextFieldDelegate>

@property (nonatomic,strong)Passengers *passenger;
@property (weak, nonatomic) IBOutlet UIImageView *imageview;
@property (weak, nonatomic) IBOutlet UITextField *detailField;

@property (nonatomic,weak) id <BookingProcessCellDelegate> delegate;

-(void)loadCellWithIndex:(NSInteger)index;

@end
