//
//  CheckOutRootController.m
//  Faremakers
//
//  Created by Mac1 on 23/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "CheckOutRootController.h"
#import "BookingProcessViewController.h"
#import "CheckOutItems.h"


@implementation CheckOutRootController

static NSString *kUserViewController = @"BookingProcessViewController";


- (void)pushProcesBookingViewController:(UINavigationController *)parentViewController
{
    BookingProcessViewController *userController = [self userViewControllerFromStoryboard];
    self.bookingProcessController = userController;
    self.bookingProcessController.checkOutItem = self.checkOutItem;
    [parentViewController pushViewController:userController animated:YES];
}

- (void)popDetailViewController
{
    [self.bookingProcessController dismissViewControllerAnimated:YES completion:nil];
}

- (BookingProcessViewController *)userViewControllerFromStoryboard
{
    UIStoryboard *storyboard = [self mainStoryboard];
    return [storyboard instantiateViewControllerWithIdentifier:kUserViewController];;
}

- (UIStoryboard *)mainStoryboard
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"ProcessBooking"
                                                         bundle:[NSBundle mainBundle]];
    return storyboard;
}

@end
