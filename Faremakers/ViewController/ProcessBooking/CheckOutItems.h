//
//  CheckOutItems.h
//  Faremakers
//
//  Created by Mac1 on 26/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    KCarItem = 0,
    KHotelItem,
    KHolidayItem,
} CheckOutType;

@class Passengers;

@interface CheckOutItems : NSObject

@property (nonatomic) CheckOutType bookingCheckOutType;
@property (nonatomic) NSString *grandTotalPrice;
@property (nonatomic) Passengers *passenger;
@property (nonatomic) id SubselectedItem;
@property (nonatomic) id selectedItem;
@property (nonatomic) id seachItem;


-(id)initWithType:(CheckOutType)checkOutType withSearchItem:(id)searchItem andSelectedItem:(id)sItem SubSelectedItem:(id)subSItem andGrandTotal:(NSString*)grandtotal;
-(NSString*)getCheckOutTitle;

@end
