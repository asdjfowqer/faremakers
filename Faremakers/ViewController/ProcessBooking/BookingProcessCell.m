//
//  BookingProcessCell.m
//  Faremakers
//
//  Created by Mac1 on 23/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "BookingProcessCell.h"
#import "NSString+Email.h"
#import "Travellers.h"
#import "FMUser.h"

@implementation BookingProcessCell

- (void)awakeFromNib {
    self.detailField.delegate = self;
}

-(void)loadCellWithIndex:(NSInteger)index
{
    self.detailField.tag = index;
    switch (index) {
        case 0:
            self.detailField.enabled = false;
            self.detailField.placeholder = @"Enter Your Information";
            [self loadFirstCell];
            break;
        case 1:
            self.detailField.enabled = true;
            self.detailField.placeholder = @"Enter Email";
            [self loadSecondCell];
            break;
        case 2:
            self.detailField.enabled = true;
            self.detailField.placeholder = @"Enter Phone";
            [self loadThirdCell];
		    break;
	    case 3:
		    self.detailField.enabled = true;
		    self.detailField.placeholder = @"Enter promo code";
		    [self loadFourthCell];
		    break;
	    case 4:
		    self.detailField.enabled = true;
		    self.detailField.placeholder = @"Enter Flight No.";
		    [self loadFourthCell];
		    break;
        default:
            break;
    }
}


-(void)loadFirstCell
{
    self.imageview.image = [UIImage imageNamed:@"traveller_icn"];
    self.detailField.text = [self.passenger convertIntoString];
}
-(void)loadSecondCell
{
    self.imageview.image = [UIImage imageNamed:@"email_icn"];
    if ([FMUser defaultUser].email) {
        self.detailField.enabled = true;
        self.detailField.text = [FMUser defaultUser].email;
    }
}
-(void)loadThirdCell
{
    self.imageview.image = [UIImage imageNamed:@"mobile_icn"];
    if ([FMUser defaultUser].phoneNumber) {
        self.detailField.text = [FMUser defaultUser].phoneNumber;
    }
}

-(void)loadFourthCell
{
	self.imageview.image = nil;
}

-(void)loadFifthCell
{
	self.imageView.image = nil;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField.text.length > 0)
    {
        if (textField.tag ==1 && ![textField.text isValidEmail])
        {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"Invalid Email Address" preferredStyle:UIAlertControllerStyleAlert];
            
            [alertController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                textField.text = @"";
            }]];
        }
        else
            [self saveUserInfoText:textField.text forIndex:textField.tag];
    }
}
- (void)saveUserInfoText:(NSString*)text forIndex:(NSInteger)tag
{
	if (tag ==1)
	{
		[FMUser defaultUser].email = text;
        if (_delegate)
        {
            if ([_delegate respondsToSelector:@selector(userEnteredEmailAddress:)])
            {
                [_delegate userEnteredEmailAddress:text];
            }
        }
	}
	else if (tag == 2)
	{
		[FMUser defaultUser].phoneNumber = text;
	}
	else if (tag == 3)
	{
		DLog(@"saving called");
		if (_delegate)
		{
			if ([_delegate respondsToSelector:@selector(userEnteredPromocode:)])
			{
				[_delegate userEnteredPromocode:text];
			}
		}
	}
	else if (tag == 4 && [[TransactionBookingManager shared].currentTransaction isKindOfClass:[TransactionBookingPaidCar class]])
	{
		TransactionBookingPaidCar* transaction = (TransactionBookingPaidCar*)[TransactionBookingManager shared].currentTransaction;
		transaction.flightNo = text;
	}
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

@end
