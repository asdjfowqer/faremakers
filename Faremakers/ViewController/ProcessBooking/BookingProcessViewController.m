//
//  BookingProcessViewController.m
//  Faremakers
//
//  Created by Mac1 on 23/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "BookingProcessViewController.h"
#import "LogInWithEmailViewController.h"
#import "EditTravellerViewController.h"
#import "NSString+PhoneNumber.h"
#import "FMWebViewController.h"
#import "BookingProcessCell.h"
#import "TransitionDelegate.h"
#import "BookingPromtView.h"
#import "NSString+Email.h"
#import "CheckOutItems.h"
#import "AppDelegate.h"
#import "Travellers.h"
#import "FMUser.h"
#import <SVProgressHUD/SVProgressHUD.h>

#import "PaymentApiManager.h"
#import "CarsDataApiManger.h"
#import "HotelDataApiManger.h"
#import "BookingProcessCellArrivalTime.h"
#import "BookingProcessCellSpecialRequests.h"
#import "TransactionBookingPaidPackage.h"
#import "BookingApiManager.h"

@interface BookingProcessViewController () <BookingPromtViewDelegate, FMWebViewDelegate, BookingProcessCellDelegate>
{
    CGFloat grandtotal;
    NSDictionary *emailResponse;
    BOOL isloggedIn;
    UIBarButtonItem *LoginbarItem;
}
@property (nonatomic, strong) TransitionDelegate *transitionController;
@end

#define KBookingProcessCell @"BookingProcessCell"
#define KBookingProcessCellArrivalTime @"BookingProcessCellArrivalTime"
#define KBookingProcessCellSpecialRequests @"BookingProcessCellSpecialRequests"

@implementation BookingProcessViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = [self.checkOutItem getCheckOutTitle];
    self.grandTotalLabel.text = self.checkOutItem.grandTotalPrice;
    
    if (![[FMUser defaultUser] isSessionAlive])
    {
        LoginbarItem = [[UIBarButtonItem alloc] initWithTitle:@"Login" style:UIBarButtonItemStylePlain target:self action:@selector(LoginBarButtonPressed:)];
        self.navigationItem.rightBarButtonItem = LoginbarItem;
    }
    else
        isloggedIn = true;
    
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    grandtotal = [[TransactionBookingManager shared].currentTransaction.amount floatValue];
    
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.tableView reloadData];
    if ([[FMUser defaultUser] isSessionAlive]){
        LoginbarItem.enabled = false;
        self.navigationItem.rightBarButtonItem = nil;
    }
}

- (void)LoginBarButtonPressed:(id)sender
{
    LogInWithEmailViewController *loginVC = [[[AppDelegate sharedDelegate] getManStoryBoard] instantiateViewControllerWithIdentifier:@"LogInWithEmailViewController"];
    [loginVC setUpRightNavigationItem];
    [self.navigationController presentViewController:[[AppDelegate sharedDelegate] setUpNavigationControllerWithRootView:loginVC]  animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)userEnteredPromocode:(NSString *)promocode
{
    NSString *typeinfo;
    if ([[TransactionBookingManager shared].currentTransaction isKindOfClass:[TransactionBookingPaidCar class]])
    {
        typeinfo = @"Cars";
    }
    else
    {
        typeinfo = @"Hotels";
    }
    
    [PaymentApiManager getPromoCode:promocode type:typeinfo withSuccessBlock: ^(id responseobject) {
        if ([responseobject floatValue] == -1)
        {
            [self showErrorMessage:@"The promocode entered is not valid."];
        }
        else
        {
            [TransactionBookingManager shared].currentTransaction.promocodePercentage = [NSNumber numberWithFloat:[responseobject floatValue]];
            CGFloat amountNew = [[TransactionBookingManager shared].currentTransaction.amount floatValue];
            CGFloat discount = [responseobject floatValue];
            
            amountNew = amountNew - (amountNew * (discount / 100));
            
            self.grandTotalLabel.text = [NSString stringWithFormat:@"%.02f %@", amountNew, [TransactionBookingManager shared].currentTransaction.currency];
            
            grandtotal = amountNew;
            
            [TransactionBookingManager shared].currentTransaction.promocodePercentage = [NSNumber numberWithFloat:discount];
            [TransactionBookingManager shared].currentTransaction.promocode = promocode;
        }
    }
                       failureBlock: ^(NSError *error) {
                           [self showErrorMessage:@"An error occured while trying to verify promocode."];
                       }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
    {
        return 4;
    }
    else if (section == 1)
    {
        return 2;
    }
    else if (section == 2)
    {
        return 1;
    }
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([[TransactionBookingManager shared].currentTransaction isKindOfClass:[TransactionBookingPaidCar class]])
    {
        return 3;
    }
    else
    {
        return 1;
    }
}

- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section;
{
    if (section == 1)
    {
        return @"Flight information";
    }
    else if (section == 2)
    {
        return @"Estimated Arrival time";
    }
    return nil;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        BookingProcessCell *cell = [tableView dequeueReusableCellWithIdentifier:KBookingProcessCell];
        cell.passenger = self.checkOutItem.passenger;
        [cell loadCellWithIndex:indexPath.row];
        cell.delegate = self;
        return cell;
    }
    else if (indexPath.section == 1)
    {
        if (indexPath.row == 0)
        {
            BookingProcessCell *cell = [tableView dequeueReusableCellWithIdentifier:KBookingProcessCell];
            cell.passenger = self.checkOutItem.passenger;
            [cell loadCellWithIndex:4];
            cell.delegate = self;
            return cell;
        }
        else
        {
            BookingProcessCellSpecialRequests *cell = [tableView dequeueReusableCellWithIdentifier:KBookingProcessCellSpecialRequests];
            return cell;
        }
    }
    else if (indexPath.section == 2)
    {
        BookingProcessCellArrivalTime *cell = [tableView dequeueReusableCellWithIdentifier:KBookingProcessCellArrivalTime];
        return cell;
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row == 0 && indexPath.section == 0)
    {
        EditTravellerViewController *editTravellerView = [[EditTravellerViewController alloc]initWithNibName:@"EditTravellerViewController" bundle:nil];
        editTravellerView.passenger = self.checkOutItem.passenger;
        editTravellerView.person = KAdult;
        [self.navigationController pushViewController:editTravellerView animated:YES];
    }
}

- (IBAction)proceedButtonPressed:(id)sender
{
    [self.tableView endEditing:YES];
    [self validateData];
    
    //    if ([FMUser defaultUser].authToken)
    //    {
    //        [self validateData];
    //    }
    //    else
    //    {
    //        [self showErrorMessage:@"You must be logged in to continue"];
    //    }
}

- (void)validateData
{
    if (![self.checkOutItem.passenger isPassengerInfoAvaialble])
    {
        [self showErrorMessage:[NSString stringWithFormat:@"Please Enter %@", self.title]];
    }
    else if (![[FMUser defaultUser].phoneNumber isValidPhoneNumber])
    {
        [self showErrorMessage:@"Please Enter valid Phone number"];
    }
    else if (![[FMUser defaultUser].email isValidEmail])
    {
        [self showErrorMessage:@"Please Enter valid Email"];
    }
    else
    {
        if ([[TransactionBookingManager shared].currentTransaction isKindOfClass:[TransactionBookingPaidCar class]])
        {
            [self verifyCarBookingDetails];
        }
        else
        {
            [self showConfirmDialogue];
        }
    }
}

- (void)verifyCarBookingDetails
{
    TransactionBookingPaidCar *transaction = (TransactionBookingPaidCar *)[TransactionBookingManager shared].currentTransaction;
    if (transaction.flightNo.length == 0)
        
    {
        [self showErrorMessage:@"Please Enter your flight number"];
    }
    else if (!(transaction.hours) && !(transaction.min))
    {
        [self showErrorMessage:@"Please Enter your esitmated arrival time"];
    }
    else
    {
        [self showConfirmDialogue];
    }
}

- (void)showBookingWebView:(NSString *)html
{
    FMWebViewController *webVC = [[FMWebViewController alloc] initWithNibName:@"FMWebViewController" bundle:nil];
    webVC.html = html;
    webVC.adelegate = self;
    [self.navigationController presentViewController:webVC animated:YES completion:nil];
}

- (void)postBookingData
{
    [SVProgressHUD show];
    
    TransactionBookingPaid *transaction = [TransactionBookingManager shared].currentTransaction;
    transaction.firstName = [FMUser defaultUser].phoneNumber;
    transaction.lastName = [FMUser defaultUser].firstName;
    transaction.phoneNumber = [FMUser defaultUser].lastName;
    
    
    NSDictionary *postData ;
    if (!isloggedIn) {
        
        if (![(emailResponse[@"isEmailExists"]) boolValue])
        {
            Passengers *pass = self.checkOutItem.passenger;
            
            transaction.firstName = pass.fName;
            transaction.lastName = pass.lName;
            transaction.phoneNumber = [FMUser defaultUser].phoneNumber;
        }
        else
        {
            transaction.phoneNumber = emailResponse[@"data"][@"phone"];
            transaction.firstName = emailResponse[@"data"][@"firstName"];
            transaction.lastName = emailResponse[@"data"][@"lastName"];
            
        }
        
        postData = [transaction prePaymentDictionaryWithNewUser:emailResponse];
    }
    else
        postData = [transaction prePaymentDictionary];
    
    if (![transaction isKindOfClass:[TransactionBookingPaidPackage class]])
    {
        //DLog(@"[transaction prePaymentDictionary] %@", [transaction prePaymentDictionary]);
        
        [PaymentApiManager uploadBookingDetailData:postData success: ^(id responseobject) {
            [SVProgressHUD dismiss];
            
            DLog(@"%@", responseobject);
            
            if (responseobject)
            {
                DLog(@"%@", responseobject[@"data"][@"html"]);
                
                if ([(responseobject[@"data"][@"corporateUser"]) boolValue])
                {
                    [self postDataAfterPayment:YES];
                }
                else
                {
                    [[TransactionBookingManager shared].currentTransaction addPaymentInformationFromDictionary:responseobject];
                    if (responseobject[@"data"][@"authToken"]) {
                        [FMUser defaultUser].authToken = responseobject[@"data"][@"authToken"];
                    }
                    [self showBookingWebView:responseobject[@"data"][@"html"]];
                    //[self postDataAfterPayment:false];
                }
            }
        }
                                      failureBlock: ^(NSError *error) {
                                          [SVProgressHUD dismiss];
                                          UIAlertView *av = [[UIAlertView alloc] initWithTitle:error.userInfo[@"com.alamofire.serialization.response.error.data"] message:@"An error occured. Please try again." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                          [av show];
                                      }];
    }
    else
    {
        //        TransactionBookingPaidPackage
        [BookingApiManager postPackageBooking:(TransactionBookingPaidPackage *)transaction
                             withSuccessBlock: ^(id responseobject) {
                                 [SVProgressHUD dismiss];
                                 
                                 UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Booking Query Submitted" message:@"" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                 [av show];
                                 [self.navigationController popToRootViewControllerAnimated:YES];
                             }
                                 failureBlock: ^(NSError *error) {
                                     [SVProgressHUD dismiss];
                                     UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Booking Failed" message:@"An error occured. Please try again." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                     [av show];
                                 }];
    }
}

- (void)fmWebViewDidRedirectedToPaymentURL
{
    DLog(@"fmWebViewDidRedirectedToPaymentURL");
    [self postDataAfterPayment:NO];
}

- (void)postDataAfterPayment:(BOOL)isCorporateUser
{
    [SVProgressHUD show];
    
    TransactionBookingPaid *transaction = [TransactionBookingManager shared].currentTransaction;
    
    NSDictionary *dictionary;
    
    if (isCorporateUser)
    {
        dictionary = [transaction postPaymentDictionaryCorporate];
    }
    else
    {
        dictionary = [transaction postPaymentDictionary];
    }
    
    if ([transaction isKindOfClass:[TransactionBookingPaidHotel class]])
    {
        [HotelDataApiManger uploadPaidBookingDetail:dictionary success: ^(id responseobject) {
            DLog(@"Done uploading data");
            DLog(@"responseobject %@", responseobject);
            [SVProgressHUD dismiss];
            
            NSString *velocityCheckMessage = [[responseobject objectForKey:@"data"] valueForKey:@"velocityCheckMessage"];
            NSString *errorMessage = [[responseobject objectForKey:@"data"] valueForKey:@"errorMessage"];
            NSString *bookingID = [[responseobject objectForKey:@"data"] valueForKey:@"bookingID"];
            NSString *bookingStatus = [[responseobject objectForKey:@"data"] valueForKey:@"bookingStatus"];
            NSString *transactionId = [[responseobject objectForKey:@"data"] valueForKey:@"paymentTransactionId"];
            NSString *transactionStatus = [[responseobject objectForKey:@"data"] valueForKey:@"paymentTransactionStatus"];
            
            NSString *messageTitle = @"Booking Failed";
            NSString *messageDetail = @"Please try again later.";
            
            if (errorMessage)
            {
                messageTitle = @"Booking Failed";
                messageDetail = errorMessage;
            }
            else if (velocityCheckMessage)
            {
                messageTitle = @"Booking Failed";
                messageDetail = velocityCheckMessage;
            }
            else
            {
                messageTitle = @"Booking Status";
                messageDetail = [NSString stringWithFormat:@"Booking Status: %@\nBooking ID: %@\nTransaction Status: %@\nTransaction ID: %@", bookingStatus, bookingID, transactionStatus, transactionId ? : @"Corporate User"];
                
                [self.navigationController popToRootViewControllerAnimated:YES];
            }
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:messageTitle
                                                                message:messageDetail
                                                               delegate:nil
                                                      cancelButtonTitle:@"Dismiss"
                                                      otherButtonTitles:nil];
            
            [alertView show];
        }
                                       failureBlock: ^(NSError *error) {
                                           if (error.code != -1001)
                                           {
                                               UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Booking" message:@"Booking failed" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                               [av show];
                                           }
                                           else
                                           {
                                               UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Booking" message:@"Booking succeeded" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                               [av show];
                                           }
                                           
                                           DLog(@"error %@", error);
                                           DLog(@"failed uploading data");
                                           [SVProgressHUD dismiss];
                                       }];
    }
    else
    {
        [CarsDataApiManger uploadPaidBookingDetail:dictionary success: ^(id responseobject) {
            DLog(@"Done uploading data");
            [SVProgressHUD dismiss];
            
            if (responseobject[@"data"][@"errorMessage"]) {
                UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Booking" message:responseobject[@"data"][@"errorMessage"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [av show];
            }
            else{
                UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Booking" message:@"Booking succeeded" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [av show];
            }
        }
                                      failureBlock: ^(NSError *error) {
                                          DLog(@"failed uploading data");
                                          [SVProgressHUD dismiss];
                                          
                                          if (error.code != -1001)
                                          {
                                              UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Booking" message:@"Booking failed" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                              [av show];
                                          }
                                          else
                                          {
                                              UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Booking" message:@"Booking succeeded" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                                              [av show];
                                          }
                                      }];
    }
}

#pragma mark - Booking Promt

- (void)userSubmitResponse:(BOOL)submit
{
    if (submit)
    {
        [TransactionBookingManager shared].currentTransaction.amount = [NSNumber numberWithFloat:grandtotal];

        if ([[FMUser defaultUser] isSessionAlive])//([FMUser defaultUser].authToken)
        {
            [self postBookingData];
        }
        else
        {
            [self showAlertView];
        }
    }
}

- (void)showConfirmDialogue
{
    BookingPromtView *rulesView = [[[AppDelegate sharedDelegate] getManStoryBoard] instantiateViewControllerWithIdentifier:@"BookingPromtView"];
    rulesView.delegate = self;
    rulesView.checkOutItem = self.checkOutItem;
    rulesView.view.backgroundColor = [UIColor clearColor];
    [rulesView setTransitioningDelegate:_transitionController];
    rulesView.modalPresentationStyle = UIModalPresentationCustom;
    [self.navigationController presentViewController:rulesView animated:YES completion:nil];//:rulesView animated:YES];
}

#pragma mark - NewUSerBooking

-(void)userEnteredEmailAddress:(NSString *)email
{
    if ([[FMUser defaultUser] isSessionAlive])////([FMUser defaultUser].authToken)
        return;
    
    emailResponse = nil;
    [self checkUserExistance];
}
- (void)checkUserExistance
{
    if (emailResponse) {
        [self showAlertView];
    }
    else{
        [SVProgressHUD show];
        
        DLog(@"%@", [transaction prePaymentDictionary]);
        
        [PaymentApiManager prepareNewUSerForBooking:[FMUser defaultUser].email success: ^(id responseobject) {
            [SVProgressHUD dismiss];
            
            DLog(@"%@", responseobject);
            
            if (responseobject)
            {
                emailResponse = responseobject[@"data"];
                
                if ([(responseobject[@"data"][@"isEmailExists"]) boolValue])
                {
                    [FMUser defaultUser].phoneNumber = responseobject[@"data"][@"data"][@"phone"];
                    [self.tableView reloadData];
                }
                else
                {
                    
                }
            }
        }
                                       failureBlock: ^(NSError *error) {
                                           [SVProgressHUD dismiss];
                                           
                                       }];
    }
}

-(void)showAlertView
{
    if (!emailResponse) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Opps" message:@"Something went Wrong please try later!" preferredStyle:UIAlertControllerStyleAlert];
        
        [alertController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            //textField.text = @"";
        }]];
    }
    else{
        NSString *message ;
        if ([(emailResponse[@"isEmailExists"]) boolValue])
        {
            message  = @"Your email Exists! you will be proceed to payment by using existing registed Data against this information";
        }
        else
        {
            message  = @"Your account will be created by provided information";
        }
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Alert" message:message preferredStyle:UIAlertControllerStyleAlert];
        
        [alertController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [self postBookingData];
        }]];
        [self.navigationController presentViewController:alertController animated:YES completion:nil];
    }
    
}

#pragma mark - alertView

- (void)showErrorMessage:(NSString *)message
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:message preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler: ^(UIAlertAction *_Nonnull action) {
        DLog(@"cancled");
    }]];
    [self.navigationController presentViewController:alertController animated:YES completion:nil];
}

@end