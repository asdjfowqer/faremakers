//
//  BookingProcessCellSpecialRequests.h
//  Faremakers
//
//  Created by Mac Mini - 2 on 15/04/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookingProcessCellSpecialRequests : UITableViewCell<UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UITextView *textviewSpecialRequests;

@end
