//
//  CheckOutItems.m
//  Faremakers
//
//  Created by Mac1 on 26/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "CheckOutItems.h"
#import "Travellers.h"

@implementation CheckOutItems


-(id)initWithType:(CheckOutType)checkOutType withSearchItem:(id)searchItem andSelectedItem:(id)sItem SubSelectedItem:(id)subSItem andGrandTotal:(NSString*)grandtotal
{
    self = [super init];
    if (self)
    {
        _passenger = [[Passengers alloc]init];
        _grandTotalPrice = grandtotal;
        _bookingCheckOutType = checkOutType;
        _seachItem = searchItem;
        _selectedItem = sItem;
        _SubselectedItem = subSItem;
    }
    return self;
}

-(NSString*)getCheckOutTitle
{
    switch (self.bookingCheckOutType) {
        case KCarItem:
            return @"Passengers Detail";
            break;
        case KHotelItem:
        case KHolidayItem:
            return @"Guest Detail";
            break;
        default:
            break;
    }
    return @"";
}
@end
