//
//  BookingProcessCellSpecialRequests.m
//  Faremakers
//
//  Created by Mac Mini - 2 on 15/04/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "BookingProcessCellSpecialRequests.h"

@implementation BookingProcessCellSpecialRequests

- (void)awakeFromNib {
    // Initialization code
	self.textviewSpecialRequests.delegate = self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)textViewDidEndEditing:(UITextView *)textView;
{
	TransactionBookingPaidCar* transaction = (TransactionBookingPaidCar*)[TransactionBookingManager shared].currentTransaction;
	
	transaction.specialRequests = textView.text;
}


@end
