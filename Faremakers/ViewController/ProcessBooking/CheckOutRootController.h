//
//  CheckOutRootController.h
//  Faremakers
//
//  Created by Mac1 on 23/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BookingProcessViewController;
@class CheckOutItems;

@interface CheckOutRootController : NSObject

@property (nonatomic , strong) BookingProcessViewController *bookingProcessController;
@property (nonatomic , strong) CheckOutItems *checkOutItem;


- (void)pushProcesBookingViewController:(UINavigationController *)parentViewController;

@end
