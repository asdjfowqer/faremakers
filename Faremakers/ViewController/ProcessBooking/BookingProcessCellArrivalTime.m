//
//  BookingProcessCellArrivalTime.m
//  Faremakers
//
//  Created by Mac Mini - 2 on 15/04/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "BookingProcessCellArrivalTime.h"
#import "IQActionSheetPickerView.h"

@interface BookingProcessCellArrivalTime ()<IQActionSheetPickerViewDelegate>
{
	NSArray* hours;
	NSArray* minutes;
}

@end

@implementation BookingProcessCellArrivalTime

- (void)awakeFromNib {
    // Initialization code
	
	hours = @[@"0",];
	
	NSMutableArray* minutesarray = [NSMutableArray array];
	for (int i = 0; i<60; i++) {
		[minutesarray addObject:[NSString stringWithFormat:@"%02d",i]];
	}
	
	NSMutableArray* hourssarray = [NSMutableArray array];
	for (int i = 0; i<24; i++) {
		[hourssarray addObject:[NSString stringWithFormat:@"%02d",i]];
	}

	hours =  @[hourssarray];
	minutes = @[minutesarray];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)btnHours:(id)sender
{
	[self showPickUpPickerForHours];
}

- (IBAction)btnMinutes:(id)sender
{
	[self showPickUpPickerForMinutes];
}

-(void)actionSheetPickerView:(IQActionSheetPickerView *)pickerView didSelectTitles:(NSArray *)titles
{
	TransactionBookingPaidCar* transaction = (TransactionBookingPaidCar*)[TransactionBookingManager shared].currentTransaction;

	if (pickerView.tag == 0)
	{
		NSString* hoursString = [titles objectAtIndex:0];
		[self.btnHours setTitle:hoursString forState:UIControlStateNormal];
		[self.btnHours setTitle:hoursString forState:UIControlStateSelected];
		transaction.hours = [NSNumber numberWithInteger:[hoursString integerValue]];
	}
	else if (pickerView.tag ==1)
	{
		NSString* minsString = [titles objectAtIndex:0];
		[self.btnMinutes setTitle:minsString forState:UIControlStateNormal];
		[self.btnMinutes setTitle:minsString forState:UIControlStateSelected];
		transaction.min = [NSNumber numberWithInteger:[minsString integerValue]];
	}
		
}

- (void)showPickUpPickerForHours
{
	NSString *title = @"Hours";
	
	IQActionSheetPickerView *picker = [[IQActionSheetPickerView alloc] initWithTitle:title delegate:self];
	picker.titleFont = [UIFont fontWithName:KCustomeFont size:14];
	[picker setTitlesForComponents:hours];
	[picker show];
	[picker setTag:0];
}

- (void)showPickUpPickerForMinutes
{
	NSString *title = @"Minutes";
	
	IQActionSheetPickerView *picker = [[IQActionSheetPickerView alloc] initWithTitle:title delegate:self];
	picker.titleFont = [UIFont fontWithName:KCustomeFont size:14];
	[picker setTitlesForComponents:minutes];
	[picker setTag:1];
	[picker show];
}


@end
