//
//  BookingProcessCellArrivalTime.h
//  Faremakers
//
//  Created by Mac Mini - 2 on 15/04/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookingProcessCellArrivalTime : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *btnHours;
@property (weak, nonatomic) IBOutlet UIButton *btnMinutes;
@property (weak, nonatomic) IBOutlet UILabel *lblHours;
@property (weak, nonatomic) IBOutlet UILabel *lblMinutes;

@end
