//
//  DoubleDateItem.h
//  Faremakers
//
//  Created by Mac1 on 29/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DoubleDateItem : NSObject

@property (nonatomic , retain) NSDate *depDate;
@property (nonatomic , retain) NSDate *retDate;

-(id)initWithDoubleDate:(NSDate*)date1 :(NSDate*)date2;
-(void)updateAllDates;
-(NSInteger)noOfDays;

@end
