//
//  HolidaysItem.m
//  Faremakers
//
//  Created by Mac1 on 08/03/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "HolidayItem.h"

@implementation HolidayTours

+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary
{
    HolidayTours *tourItem = [[HolidayTours alloc] init];
    
    if (tourItem)
    {
        tourItem.tourTitle = [dictionary objectForKey:@"tourTitle"];
        tourItem.tourDetails = [dictionary objectForKey:@"tourDetails"];
        tourItem.tourIncludes = [dictionary objectForKey:@"tourIncludes"];
        
    }
    
    return tourItem;
}
+ (NSArray*)itemWithArray:(NSArray*)itemArray
{
    NSMutableArray *tourItemItemList = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in itemArray)
    {
        [tourItemItemList addObject:[self itemWithDictionary:dict]];
    }
    
    return [[NSArray alloc] initWithArray:tourItemItemList];
}

@end

@implementation HolidayVisa

+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary
{
    HolidayVisa *visaItem = [[HolidayVisa alloc] init];
    
    if (visaItem)
    {
        visaItem.visaRate = [dictionary objectForKey:@"visaRate"];
        visaItem.city = [dictionary objectForKey:@"city"];
    }
    
    return visaItem;
}
+ (NSArray*)itemWithArray:(NSArray*)itemArray
{
    NSMutableArray *visasItemItemList = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in itemArray)
    {
        [visasItemItemList addObject:[self itemWithDictionary:dict]];
    }
    
    return [[NSArray alloc] initWithArray:visasItemItemList];
}

@end

@implementation HolidayTicketDetail

+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary
{
    HolidayTicketDetail *ticketItem = [[HolidayTicketDetail alloc] init];
    
    if (ticketItem)
    {
        ticketItem.adultTicketRate = [dictionary objectForKey:@"adultTicketRate"];
        ticketItem.childTicketRate = [dictionary objectForKey:@"childTicketRate"];
        ticketItem.extraTicketDetails = [dictionary objectForKey:@"extraTicketDetails"];
        ticketItem.routeDetails = [dictionary objectForKey:@"routeDetails"];
    }
    
    return ticketItem;
}
+ (NSArray*)itemWithArray:(NSArray*)itemArray
{
    NSMutableArray *visasItemItemList = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in itemArray)
    {
        [visasItemItemList addObject:[self itemWithDictionary:dict]];
    }
    
    return [[NSArray alloc] initWithArray:visasItemItemList];
}

@end


@implementation HolidayItem

+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary
{
    HolidayItem *holiItem = [[HolidayItem alloc] init];
    
    if (holiItem)
    {
        holiItem.packagePrice = (NSNumber*)[dictionary objectForKey:@"packagePrice"];
        holiItem.familyType = [dictionary objectForKey:@"familyType"];
        holiItem.roomType = [dictionary objectForKey:@"roomType"];
        holiItem.noOfRooms = [dictionary objectForKey:@"noOfRooms"];
        holiItem.nightsStay = [dictionary objectForKey:@"nights"];
        holiItem.holidayImageUrl = [dictionary objectForKey:@"imageUrl"];
        holiItem.holidayPackage = [dictionary objectForKey:@"packageName"];//@"3 nights in Fortune pear hotel 3 star(Dubai)  3 nights in Fortune pear hotel 3 star(Dubai)";//
        holiItem.packageID = [dictionary objectForKey:@"packageId"];
        holiItem.priceInclude = [dictionary objectForKey:@"priceIncludes"];
        holiItem.termsAndConditions = [dictionary objectForKey:@"termsAndConditions"];
        holiItem.totalNightsStay = [dictionary objectForKey:@"totalNightsStay"];
        holiItem.totalNightsStay = [dictionary objectForKey:@"totalNightsStay"];
        holiItem.holidaysTourList = [HolidayTours itemWithArray:[dictionary objectForKey:@"tours"]];
        holiItem.visasList  = [HolidayVisa itemWithArray:[dictionary objectForKey:@"visas"]];
        holiItem.holidayTicketDetail  = [HolidayTicketDetail itemWithDictionary:[dictionary objectForKey:@"ticketDetail"]];
        if ([dictionary objectForKey:@"priceCode"])
            holiItem.currencyCode = [dictionary objectForKey:@"priceCode"];
        else
            holiItem.currencyCode = @"PKR";
        
        holiItem.holidayDetail = [NSString stringWithFormat:@"%@ %@ (s), For %@ Nights",holiItem.noOfRooms,holiItem.roomType,holiItem.totalNightsStay];
        holiItem.holidayAccomodDetail = [HolidayItem prepareAccomodationDetail:holiItem];
        holiItem.holidayPriceDetail = [HolidayItem preparePriceDetail:holiItem];
        holiItem.holidayVisaDetail = [HolidayItem prepareVisaDetail:holiItem];
        holiItem.holidayTicketNote = [HolidayItem prepareTicketDetail:holiItem];
        
        
        //        NSArray *stringArray = @[ @" 1 adult 1 double  bed room",@" 3 nights in Fortune pear hotel 3 star(Dubai)",@" 3 Nights in Arena Star Luxury 3 star (Kuala Lumpur)",@" 3 Nights in Natural park resort 3 star (Pattaya)",@" 3 Nights in Woraburi Sukhumvist hotel 3 star (Bangkok)"];
        //        DLog(@"stringArray %@",stringArray);
        //        DLog(@"Converted %@",[self getStringByAddingBulletsIntoArrayOfString:stringArray]);
    }
    
    return holiItem;
}
+(NSString*)preparePriceDetail:(HolidayItem*)holidayItem
{
    NSMutableString *accoStr = [[NSMutableString alloc] init];
    
    [accoStr appendString:[self getStringByAddingBullets:[NSString stringWithFormat:@"Package Price %@ For %@\n",holidayItem.packagePrice,holidayItem.familyType]]];
    
    [accoStr appendString:[self getStringByAddingBulletsIntoArrayOfString:holidayItem.priceInclude]];
    
    return accoStr;
}
+(NSString*)prepareAccomodationDetail:(HolidayItem*)holidayItem
{
    NSMutableString *accoStr = [[NSMutableString alloc] init];
    
    [accoStr appendString:[self getStringByAddingBullets:[NSString stringWithFormat:@"Package For %@ \n",holidayItem.familyType]]];
    
    [accoStr appendString:[self getStringByAddingBulletsIntoArrayOfString:holidayItem.nightsStay]];
    
    return accoStr;
}
+(NSString*)prepareVisaDetail:(HolidayItem*)holidayItem
{
    NSMutableString *accoStr = [[NSMutableString alloc] init];
    
    for (HolidayVisa *visa in holidayItem.visasList)
    {
        [accoStr appendString:[self getStringByAddingBullets:[NSString stringWithFormat:@"%@ Visa in %@\n",visa.city,visa.visaRate]]];
    }
    return accoStr;
}
+(NSString*)prepareTicketDetail:(HolidayItem*)holidayItem
{
    NSMutableString *accoStr = [[NSMutableString alloc] init];
    
    [accoStr appendString:[self getStringByAddingBullets:[NSString stringWithFormat:@"Ticket Rate %@ For Adult\n",holidayItem.holidayTicketDetail.adultTicketRate]]];
    if (holidayItem.holidayTicketDetail.childTicketRate.integerValue != 0)
    {
        [accoStr appendString:[self getStringByAddingBullets:[NSString stringWithFormat:@"Ticket Rate %@ For Child\n",holidayItem.holidayTicketDetail.childTicketRate]]];
    }
    if (holidayItem.holidayTicketDetail.extraTicketDetails)
    {
        [accoStr appendString:[self getStringByAddingBullets:[NSString stringWithFormat:@"%@ \n",holidayItem.holidayTicketDetail.extraTicketDetails]]];
    }
    
    [accoStr appendString:[self getStringByAddingBullets:[NSString stringWithFormat:@"Route: %@",holidayItem.holidayTicketDetail.routeDetails]]];
    
    return accoStr;
}

+ (NSArray*)itemWithArray:(NSArray*)itemArray
{
    NSMutableArray *HolidaysItemList = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in itemArray)
    {
        [HolidaysItemList addObject:[self itemWithDictionary:dict]];
    }
    
    return [[NSArray alloc] initWithArray:HolidaysItemList];
}

+(NSString*)getStringByAddingBullets:(NSString*)str
{
    return [NSString stringWithFormat:@" \u2022 %@",str];
}
+(NSString*)getStringByAddingBulletsIntoArrayOfString:(NSArray*)stringArray
{
    NSMutableString *str = [[NSMutableString alloc] init];
    for (NSString *text in stringArray)
    {
        [str appendString:[NSString stringWithFormat:@"%@\n",[self getStringByAddingBullets:text]]];
    }
    if ([str containsString:@"\n"])
    {
        NSRange range = [str rangeOfString:@"\n" options:NSBackwardsSearch];
        if (range.location != NSNotFound)
        {
            [str deleteCharactersInRange:range];
        }
    }
    return str;
}
+ (NSArray*)findMinMaxPrices:(NSArray*)array
{
    HolidayItem *holidayItem = array[0];
    __block NSInteger lowest = holidayItem.packagePrice.integerValue;
    __block NSInteger highest = holidayItem.packagePrice.integerValue;
    
    [array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        HolidayItem *holidayItem = (HolidayItem*)obj;
        
        DLog(@"%@",holidayItem.packagePrice);
        
        if (holidayItem.packagePrice.integerValue > highest) {
            
            highest = holidayItem.packagePrice.integerValue;
            
        }
        else if (holidayItem.packagePrice.integerValue < lowest)
        {
            lowest = holidayItem.packagePrice.integerValue;
        }
    }];
    return [NSArray arrayWithObjects:[NSNumber numberWithInteger:lowest],[NSNumber numberWithInteger:highest],nil];
}

@end
