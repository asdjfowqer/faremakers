//
//  TransactionBookingPaidUmrah.m
//  Faremakers
//
//  Created by Afnan Ahmad on 22/04/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "TransactionBookingPaidUmrah.h"

@implementation TransactionBookingPaidUmrah

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.type = @"Ummrahs";
    }

    return self;
}

@end