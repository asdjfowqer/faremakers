//
//  TransactionBookingPaidHolidays.h
//  Faremakers
//
//  Created by Afnan Ahmad on 22/04/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "TransactionBookingPaid.h"
#import "TransactionBookingPaidPackage.h"

@interface TransactionBookingPaidHolidays : TransactionBookingPaidPackage

@end
