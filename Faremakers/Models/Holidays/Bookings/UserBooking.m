//
//  UserBooking.m
//  Faremakers
//
//  Created by Mac Mini - 2 on 16/04/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "UserBooking.h"

@implementation UserBooking


+(instancetype)anInstance
{
	return [[self alloc] init];
}

+(NSArray*)instanceArrayWithDictionary:(NSDictionary*)dictionary
{
	NSDictionary* data = dictionary[@"data"];
	NSMutableArray* array = [data objectForKey:[self subDictionaryKey]];
	NSMutableArray* retArray = [NSMutableArray array];
	
	for (NSDictionary* dictionary in array)
	{
		UserBooking* book = [[self alloc] initWithDictionary:dictionary];
		book.userCompleteName = [data objectForKey:@"userName"];
		[retArray addObject:book];
	}
	return retArray;
}

-(instancetype)initWithDictionary:(NSDictionary*)dictionary
{
	if (self = [self init])
	{
		_transactionDate = [dictionary objectForKey:@"transactionDate"];
		_maskedCardNumber = [dictionary objectForKey:@"maskedCardNumber"];
		_bookStatus = [dictionary objectForKey:@"bookStatus"];
		_bookId = [dictionary objectForKey:@"bookID"];
		_paymentStatus = [dictionary objectForKey:@"paymentStatus"];
		_cancelStatus = [dictionary objectForKey:@"cancel"];
		_orderId = [dictionary objectForKey:@"id"];
		_userCompleteName = [dictionary objectForKey:@"userName"];
        _price = [dictionary objectForKey:@"transactionAmount" ];//@"$100";
        _currency = [dictionary objectForKey:@"currency"];
        _transactionReference = [dictionary objectForKey:@"transactionReference"];
        _packageName = [dictionary objectForKey:@"packageName"];
        _packageAmount = [dictionary objectForKey:@"amount"];
        _orderDate = [dictionary objectForKey:@"orderDate"];
        
        if ([dictionary objectForKey:@"isRefundable"]) {
            
            self.IsRefundable = [[dictionary objectForKey:@"isRefundable"] boolValue];
        }
	}
	return self;
}


+(NSString*)subDictionaryKey
{
	return @"flights";
}


-(instancetype)init
{
	if (self = [super init])
	{
		_transactionDate = [[NSDate alloc] init];
		_maskedCardNumber = [[NSNumber alloc] init];
		_bookStatus = [[NSString alloc] init];
		_bookId = [[NSString alloc] init];
		_paymentStatus = [[NSString alloc] init];
		_cancelStatus = [[NSString alloc] init];
		_orderId = [[NSNumber alloc] init];
		_userCompleteName = [[NSString alloc] init];
		_price = [[NSString alloc] init];
        _packageAmount = [[NSNumber alloc] init];
        _packageName = [[NSString alloc] init];
        _currency = [[NSString alloc] init];
        _orderDate = [[NSDate alloc] init];
        _transactionReference = [[NSString alloc] init];
        _currency = [[NSString alloc] init];
	}
	return self;
}

-(NSString*)dateString:(NSDate*)date
{
	DLog(@"%@",date);
	
//	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//	[formatter setDateFormat:@"yyyy-MM-dd"];
//	NSString *string = [formatter stringFromDate:date];
	NSString *string = [NSString stringWithFormat:@"%@",date];
    if (!string) {
        string = [string substringToIndex:10];
    }
	return string;
}


@end
