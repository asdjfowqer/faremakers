//
//  TransactionBookingPaidPackage.m
//  Faremakers
//
//  Created by Afnan Ahmad on 22/04/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "TransactionBookingPaidPackage.h"

@implementation TransactionBookingPaidPackage

- (NSDictionary *)prePaymentDictionary
{
    return @{
             @"authToken": [FMUser defaultUser].authToken,
             @"type": self.type,
             @"packageId": self.packageId
             };
}

- (NSDictionary *)postPaymentDictionary
{
    return @{
             @"authToken": [FMUser defaultUser].authToken,
             @"type": self.type,
             @"packageId": self.packageId
             };
}

- (NSDictionary *)postPaymentDictionaryCorporate
{
    return @{
             @"authToken": [FMUser defaultUser].authToken,
             @"type": self.type,
             @"packageId": self.packageId
             };
}

@end
