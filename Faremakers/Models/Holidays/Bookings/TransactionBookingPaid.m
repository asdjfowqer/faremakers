//
//  TransactionBookingPaid.m
//  Faremakers
//
//  Created by Mac Mini - 2 on 14/04/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "TransactionBookingPaid.h"

@implementation TransactionBookingPaid


+(instancetype)anInstance
{
    return [[self alloc] init];
}


-(instancetype)init
{
    if (self = [super init])
    {
        _hotelName = [[NSString alloc] init];
        _email = [[NSString alloc] init];
        _country = [[NSString alloc] init];
        _paymentType = [[NSString alloc] init];
        _paymentTransactionId = [[NSNumber alloc] init];
        
        
        _phoneNumber = [[NSString alloc] init];
        _amount = [[NSNumber alloc] init];
        _firstName = [[NSString alloc] init];
        _lastName = [[NSString alloc] init];
        _currency = [[NSString alloc] init];
        _orderInfo = [[NSString alloc] init];
        _orderName = [[NSString alloc] init];
        
        _promocode = [[NSString alloc] init];
        _promocodePercentage = [[NSNumber alloc] init];
        
    }
    return self;
}

-(NSDictionary*)prePaymentDictionary
{
    //	if (!_phoneNumber || !_amount || !_firstName || !_lastName || !_currency || _orderInfo || !_orderName)
    //	{
    //		return nil;
    //	}
    
    return @{
             @"phoneNumber":_phoneNumber,
             @"amount":_amount,
             @"firstName":_firstName,
             @"lastName":_lastName,
             @"Currency":_currency,
             @"OrderInfo":_orderInfo,
             @"OrderName":_orderName,
             @"authToken":[FMUser defaultUser].authToken,
             @"ApplicationType":@"IPhoneApp"
             };
}

- (NSDictionary *)prePaymentDictionaryWithNewUser:(NSDictionary*)response
{
    if (![response[@"isEmailExists"] boolValue]) {
        return @{
                 @"phoneNumber":_phoneNumber,
                 @"amount":_amount,
                 @"firstName":_firstName,
                 @"lastName":_lastName,
                 @"Currency":_currency,
                 @"OrderInfo":_orderInfo,
                 @"OrderName":_orderName,
                 @"authToken":@"",
                 @"userID":@"",
                 @"ApplicationType":@"IPhoneApp",
                 @"isNewUser":@"1",
                 @"Email": [FMUser defaultUser].email
                 };
    }
    return @{
             @"phoneNumber":_phoneNumber,
             @"amount":_amount,
             @"firstName":_firstName,
             @"lastName":_lastName,
             @"Currency":_currency,
             @"OrderInfo":_orderInfo,
             @"OrderName":_orderName,
             @"authToken":@"",
             @"userID":response[@"data"][@"userID"],
             @"ApplicationType":@"IPhoneApp",
             @"isNewUser":@"0",
             @"Email": [FMUser defaultUser].email
             };
}


-(NSDictionary*)postPaymentDictionary
{
    return nil;
}

-(NSDictionary*)postPaymentDictionaryCorporate
{
    return nil;
}


-(void)addPaymentInformationFromDictionary:(NSDictionary*)paymentInfo
{
    DLog(@"paymentInfo %@",paymentInfo);
    
    _paymentTransactionId = paymentInfo[@"data"][@"transactionId"];
    _paymentType = @"Etisalat";
}

-(NSString*)dateString:(NSDate*)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *string = [formatter stringFromDate:date];
    return string;
}

-(NSString*)timeStampString:(NSDate*)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd'T'HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    NSString* datestring =  [dateFormatter stringFromDate:date];
    return datestring;
}

@end
