//
//  TransactionBookingPaidPackage.h
//  Faremakers
//
//  Created by Afnan Ahmad on 22/04/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "TransactionBookingPaid.h"

@interface TransactionBookingPaidPackage : TransactionBookingPaid

@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSNumber *packageId;

@end
