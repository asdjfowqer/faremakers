//
//  UserBookingUmrah.m
//  Faremakers
//
//  Created by Afnan Ahmad on 22/04/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "UserBookingUmrah.h"

@implementation UserBookingUmrah

+(instancetype)anInstance
{
    return [[self alloc] init];
}

+(NSArray*)instanceArrayWithDictionary:(NSDictionary*)dictionary
{
    NSDictionary* data = dictionary[@"data"];
    NSMutableArray* array = [data objectForKey:[self subDictionaryKey]];
    NSMutableArray* retArray = [NSMutableArray array];

    for (NSDictionary* dictionary in array)
    {
        UserBooking* book = [[self alloc] initWithDictionary:dictionary];
        book.userCompleteName = [data objectForKey:@"userName"];
        [retArray addObject:book];
    }
    return retArray;
}

+(NSString*)subDictionaryKey
{
    return @"ummrahs";
}

@end
