//
//  TransactionBookingCarPaid.h
//  Faremakers
//
//  Created by Mac Mini - 2 on 13/04/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "TransactionBookingPaid.h"

@interface TransactionBookingPaidCar : TransactionBookingPaid

@property (nonatomic,strong) NSString* vehicleCode;
@property (nonatomic,strong) NSString* vehicleValues;
@property (nonatomic,strong) NSString* transferId;
@property (nonatomic,strong) NSString* vehicleItemPriceCurrency;
@property (nonatomic,strong) NSNumber* vehicleItemPriceValue;
@property (nonatomic,strong) NSDate* departureDate;
@property (nonatomic,strong) NSString* fromCity;
@property (nonatomic,strong) NSNumber* paxQuantity;
@property (nonatomic,strong) NSString* origin;
@property (nonatomic,strong) NSString* desination;
@property (nonatomic,strong) NSNumber* hours;
@property (nonatomic,strong) NSNumber* min;
@property (nonatomic,strong) NSString* flightNo;
@property (nonatomic,strong) NSString* specialRequests;




@end
