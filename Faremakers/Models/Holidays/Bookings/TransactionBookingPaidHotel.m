//
//  TransactionBookingHotelPaid.m
//  Faremakers
//
//  Created by Mac Mini - 2 on 13/04/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "TransactionBookingPaidHotel.h"

@implementation TransactionBookingPaidHotel

+(instancetype)anInstance
{
	return [[self alloc] init];
}


-(instancetype)init
{
	if (self = [super init])
	{
		_hotelCode = [[NSString alloc] init];
		_hotelId = [[NSString alloc] init];
		_price = [[NSNumber alloc] init];
		_totalNights = [[NSNumber alloc] init];
		_roomType = [[NSString alloc] init];
		_checkInDate = [[NSDate alloc] init];
		_checkOutDate = [[NSDate alloc] init];
		_city = [[NSString alloc] init];
		_adults = [[NSNumber alloc] init];
		_childs = [[NSNumber alloc] init];
		_numberOfRooms = [[NSNumber alloc] init];
	}
	return self;
}

-(NSDictionary*)postPaymentDictionaryCorporate
{
	return @{
		    @"hotelBookingModel": @{
				    @"hotel": @{
						    @"HotelCode": self.hotelCode,
						    @"HotelName": self.hotelName,
						    @"id": self.hotelId,
						    @"currency": self.currency,
						    @"price": self.price,
						    @"totalNights": self.totalNights,
						    @"roomType": self.roomType
						    },
				    @"searchModel": @{
						    @"CheckInDate": [self dateString:self.checkInDate],
						    @"CheckOutDate": [self dateString:self.checkOutDate],
						    @"City": self.city,
						    @"Adults": self.adults,
						    @"Childs": self.childs,
						    @"NumberOfRooms": self.numberOfRooms,
						    @"Country": self.country,
						    }
				    },
		    @"bookingDetailsModel": @{
				    @"FirstName": self.firstName,
				    @"LastName": self.lastName,
				    @"Email": self.email,
				    @"authToken":[FMUser defaultUser].authToken
				    },
		    
		    };
}


-(NSDictionary*)postPaymentDictionary
{
	return @{
		    @"hotelBookingModel": @{
				    @"hotel": @{
						    @"HotelCode": self.hotelCode,
						    @"HotelName": self.hotelName,
						    @"id": self.hotelId,
						    @"currency": self.currency,
						    @"price": self.price,
						    @"totalNights": self.totalNights,
						    @"roomType": self.roomType
						    },
				    @"searchModel": @{
						    @"CheckInDate": [self dateString:self.checkInDate],
						    @"CheckOutDate": [self dateString:self.checkOutDate],
						    @"City": self.city,
						    @"Adults": self.adults,
						    @"Childs": self.childs,
						    @"NumberOfRooms": self.numberOfRooms,
						    @"Country": self.country,
						    @"PaymentType": @"Etisalat",
						    @"PaymentTransactionId":self.paymentTransactionId
						    
						    }
				    },
		    @"bookingDetailsModel": @{
				    @"FirstName": self.firstName,
				    @"LastName": self.lastName,
				    @"Email": self.email,
				    @"authToken":[FMUser defaultUser].authToken,
                    @"PromoCode":([self.promocode isEqualToString:@""]) ? [NSNull null] : self.promocode
				    }
		    
		    };
}

@end
