//
//  HolidaysItem.h
//  Faremakers
//
//  Created by Mac1 on 08/03/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface UserBookingItem : NSObject

@property (nonatomic) NSString *bookingType;

+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary;
+ (NSArray*)itemWithArray:(NSArray*)itemArray;


@end
