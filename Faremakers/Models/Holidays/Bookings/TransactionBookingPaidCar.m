//
//  TransactionBookingCarPaid.m
//  Faremakers
//
//  Created by Mac Mini - 2 on 13/04/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "TransactionBookingPaidCar.h"

@implementation TransactionBookingPaidCar

+(instancetype)anInstance
{
	return [[self alloc] init];
}

-(instancetype)init
{
	if (self = [super init])
	{
		_vehicleCode = [[NSString alloc] init];
		_vehicleValues = [[NSString alloc] init];
		_transferId = [[NSString alloc] init];
		_vehicleItemPriceCurrency = [[NSString alloc] init];
		_vehicleItemPriceValue = [[NSNumber alloc] init];
		_departureDate = [[NSDate alloc] init];
		_fromCity = [[NSString alloc] init];
		_paxQuantity = [[NSNumber alloc] init];
		_origin = [[NSString alloc] init];
		_desination = [[NSString alloc] init];
		_hours = [[NSNumber alloc] init];
		_min = [[NSNumber alloc] init];
		_flightNo = [[NSString alloc] init];
		_specialRequests = [[NSString alloc] init];
	}
	return self;
}


-(NSDictionary*)postPaymentDictionaryCorporate
{
	return @{
		    @"carBookingModel": @{
				    @"car": @{
						    @"vehicleCode": self.vehicleCode,
						    @"vehicleValues": self.vehicleValues,
						    @"HotelName": self.hotelName,
						    @"transferId": self.transferId,
						    @"vehicleItemPriceCurrency": self.vehicleItemPriceCurrency,
						    @"vehicleItemPriceValue": self.vehicleItemPriceValue
						    },
				    @"searchModel": @{
						    @"DepartureDate": [self dateString:self.departureDate],
						    @"FromCity": self.fromCity,
						    @"PaxQuantity": self.paxQuantity,
						    @"Country": self.country,
						    }
				    },
		    @"bookingDetailsModel": @{
				    @"FirstName": self.firstName,
				    @"LastName": self.lastName,
				    @"Email": self.email,
				    @"origin": self.origin,
				    @"desination": self.desination,
				    @"hours": self.hours,
				    @"min": self.min,
				    @"flightNo": self.flightNo,
				    @"specialRequests": self.specialRequests,
				    @"authToken":[FMUser defaultUser].authToken
				    },
		    };
}

-(NSDictionary*)postPaymentDictionary
{
	return @{
		@"carBookingModel": @{
			@"car": @{
				@"vehicleCode": self.vehicleCode,
				@"vehicleValues": self.vehicleValues,
				@"HotelName": self.hotelName,
				@"transferId": self.transferId,
				@"vehicleItemPriceCurrency": self.vehicleItemPriceCurrency,
				@"vehicleItemPriceValue": self.vehicleItemPriceValue
			},
			@"searchModel": @{
				@"DepartureDate": [self dateString:self.departureDate],
				@"FromCity": self.fromCity,
				@"PaxQuantity": self.paxQuantity,
				@"Country": self.country,
				@"PaymentType": @"Etisalat",
				@"PaymentTransactionId": self.paymentTransactionId
			}
		},
		@"bookingDetailsModel": @{
			@"FirstName": self.firstName,
			@"LastName": self.lastName,
			@"Email": self.email,
			@"origin": self.origin,
			@"desination": self.desination,
			@"hours": self.hours,
			@"min": self.min,
			@"flightNo": self.flightNo,
			@"specialRequests": self.specialRequests,
			@"authToken":[FMUser defaultUser].authToken,
			@"PromoCode":self.promocode
		},
		
	};
}

@end

