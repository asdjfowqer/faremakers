//
//  TransactionBookingPaidHolidays.m
//  Faremakers
//
//  Created by Afnan Ahmad on 22/04/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "TransactionBookingPaidHolidays.h"
#import "FMUser.h"

@implementation TransactionBookingPaidHolidays

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.type = @"Holidays";
    }

    return self;
}

@end