//
//  TransactionBookingHotelPaid.h
//  Faremakers
//
//  Created by Mac Mini - 2 on 13/04/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "TransactionBookingPaid.h"

@interface TransactionBookingPaidHotel : TransactionBookingPaid

@property (nonatomic,strong) NSString* hotelCode;
@property (nonatomic,strong) NSString* hotelId;
@property (nonatomic,strong) NSNumber* price;
@property (nonatomic,strong) NSNumber* totalNights;
@property (nonatomic,strong) NSString* roomType;
@property (nonatomic,strong) NSDate* checkInDate;
@property (nonatomic,strong) NSDate* checkOutDate;
@property (nonatomic,strong) NSString* city;
@property (nonatomic,strong) NSNumber* adults;
@property (nonatomic,strong) NSNumber* childs;
@property (nonatomic,strong) NSNumber* numberOfRooms;




@end
