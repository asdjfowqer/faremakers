//
//  TransactionBookingFlightPaid.m
//  Faremakers
//
//  Created by Mac Mini - 2 on 13/04/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "TransactionBookingPaidFlight.h"

@implementation TransactionBookingPaidFlight

+ (instancetype)anInstance
{
    return [[self alloc] init];
}

- (instancetype)init
{
    if (self = [super init])
    {
        _adults = [NSMutableArray array];
        _children = [NSMutableArray array];
        _infants = [NSMutableArray array];

        //        _departureDateTime = [[NSDate alloc] init];
        //        _arrivalDateTime  = [[NSDate alloc] init];
        //        _operatingAirlineFlightNumber = [[NSString alloc] init];
        //        _operatingAirlineFlightCode = [[NSString alloc] init];
        //        _flightReservationBookDesignationCode  = [[NSString alloc] init];
        //        _arrivalAirportLocationCode = [[NSString alloc] init];
        //        _departureAirportLocationCode = [[NSString alloc] init];
        //        _airEquipmentType = [[NSString alloc] init];
        //        _commission = [[NSString alloc] init];
    }

    return self;
}

- (NSDictionary *)postPaymentDictionaryCorporate
{
    NSDictionary *dictBookingModel = [self bookingModelDictionaryCorporate];//[self bookingModelDictionary];

    NSMutableArray *originDestinationOptionsDictionary = [NSMutableArray array];

    for (FlightSegment *segment in self.originDestinationOptions)
    {
        [originDestinationOptionsDictionary addObject:[segment toJSONDictionary]];
    }

    return @{
             //             @"originDestinationOptions": @{
             //                     @"DepartureDateTime" : [self timeStampString:self.departureDateTime],
             //                     @"ArrivalDateTime" : [self timeStampString:self.arrivalDateTime],
             //                     @"OperatingAirlineFlightNumber" : self.operatingAirlineFlightNumber,
             //                     @"OperatingAirlineFlightCode" : self.operatingAirlineFlightCode,
             //                     @"flightResBookDesigCode" : self.flightReservationBookDesignationCode,
             //                     @"ArrivalAirportLocationCode" : self.arrivalAirportLocationCode,
             //                     @"DepartureAirportLocationCode" : self.departureAirportLocationCode,
             //                     @"AirEquipmentType" : self.airEquipmentType
             //                     },
             @"originDestinationOptions": originDestinationOptionsDictionary,
             @"IsRefundable" : [NSNumber numberWithBool:self.IsRefundable],
             @"bookingModel":dictBookingModel
             };
}

- (NSDictionary *)postPaymentDictionary
{
    NSDictionary *dictBookingModel = [self bookingModelDictionary];

    NSMutableArray *originDestinationOptionsDictionary = [NSMutableArray array];

    for (FlightSegment *segment in self.originDestinationOptions)
    {
        [originDestinationOptionsDictionary addObject:[segment toJSONDictionary]];
    }

    return @{
             //             @"originDestinationOptions": @{
             //                     @"DepartureDateTime" : [self timeStampString:self.departureDateTime],
             //                     @"ArrivalDateTime" : [self timeStampString:self.arrivalDateTime],
             //                     @"OperatingAirlineFlightNumber" : self.operatingAirlineFlightNumber,
             //                     @"OperatingAirlineFlightCode" : self.operatingAirlineFlightCode,
             //                     @"flightResBookDesigCode" : self.flightReservationBookDesignationCode,
             //                     @"ArrivalAirportLocationCode" : self.arrivalAirportLocationCode,
             //                     @"DepartureAirportLocationCode" : self.departureAirportLocationCode,
             //                     @"AirEquipmentType" : self.airEquipmentType
             //                     },
             @"originDestinationOptions": originDestinationOptionsDictionary,
//             @"TotalFare" : self.amount.stringValue,
//             @"Commission" : self.commission,
             @"IsRefundable" : [NSNumber numberWithBool:self.IsRefundable],
             @"bookingModel":dictBookingModel
             };
}

- (NSDictionary *)bookingModelDictionary
{
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithDictionary:@{
                                                                                      @"PhoneNumber": self.phoneNumber,
                                                                                      @"Email": self.email,
                                                                                      @"authToken": [FMUser defaultUser].authToken,
                                                                                      @"TransactionID": self.paymentTransactionId
                                                                                      }];
    NSMutableArray *arrayAdults = [NSMutableArray array];
    NSMutableArray *arrayChildren = [NSMutableArray array];
    NSMutableArray *arrayInfants = [NSMutableArray array];

    for (Passengers *ps in _adults)
    {
        [arrayAdults addObject:[ps getPassengersDetailForFlightTransaction]];
    }
    for (Passengers *ps in _children)
    {
        [arrayChildren addObject:[ps getPassengersDetailForFlightTransaction]];
    }
    for (Passengers *ps in _infants)
    {
        [arrayInfants addObject:[ps getPassengersDetailForFlightTransaction]];
    }

    [dictionary setObject:arrayAdults forKey:@"Adults"];
    [dictionary setObject:arrayChildren forKey:@"Children"];
    [dictionary setObject:arrayInfants forKey:@"Infants"];
    [dictionary setObject:self.amount forKey:@"TotalFare"];
    [dictionary setObject:self.commission forKey:@"Commission"];

    return dictionary;
}

- (NSDictionary *)bookingModelDictionaryCorporate
{
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithDictionary:@{
                                                                                      @"PhoneNumber": self.phoneNumber,
                                                                                      @"Email": self.email,
                                                                                      @"authToken": [FMUser defaultUser].authToken,
                                                                                      }];
    NSMutableArray *arrayAdults = [NSMutableArray array];
    NSMutableArray *arrayChildren = [NSMutableArray array];
    NSMutableArray *arrayInfants = [NSMutableArray array];

    for (Passengers *ps in _adults)
    {
        [arrayAdults addObject:[ps getPassengersDetailForFlightTransaction]];
    }
    for (Passengers *ps in _children)
    {
        [arrayChildren addObject:[ps getPassengersDetailForFlightTransaction]];
    }
    for (Passengers *ps in _infants)
    {
        [arrayInfants addObject:[ps getPassengersDetailForFlightTransaction]];
    }

    [dictionary setObject:arrayAdults forKey:@"Adults"];
    [dictionary setObject:arrayChildren forKey:@"Children"];
    [dictionary setObject:arrayInfants forKey:@"Infants"];
    [dictionary setObject:self.amount forKey:@"TotalFare"];
    [dictionary setObject:self.commission forKey:@"Commission"];

    return dictionary;
}

//-(NSDictionary*)bookingModelDictionary
//{
//			return @{
//			  @"Adults": @[
//					  @{
//						  @"Title": @"Mr",
//						  @"FirstName": @"asdasd",
//						  @"LastName": @"asdasdas",
//						  @"Day": @"1",
//						  @"Month": @"1",
//						  @"Year": @"1960"
//						  }
//					  ],
//			  @"Children": @[
//					  @{
//						  @"Title": @"Mr",
//						  @"FirstName": @"asdasd",
//						  @"LastName": @"asdasdas",
//						  @"Day": @"1",
//						  @"Month": @"1",
//						  @"Year": @"1960"
//						  }
//					  ],
//			  @"Infants": @[
//					  @{
//						  @"Title": @"Mr",
//						  @"FirstName": @"asdasd",
//						  @"LastName": @"asdasdas",
//						  @"Day": @"1",
//						  @"Month": @"1",
//						  @"Year": @"1960"
//						  }
//					  ],
//			  @"PhoneNumber": self.phoneNumber,
//			  @"Email": self.email,
//			  @"authToken": [FMUser defaultUser].authToken,
//			  @"TransactionID": self.paymentTransactionId
//			  };
//}

@end