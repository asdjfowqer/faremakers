//
//  TransactionBookingFlightPaid.h
//  Faremakers
//
//  Created by Mac Mini - 2 on 13/04/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "TransactionBookingPaid.h"
#import "Travellers.h"
#import "FlightSegment.h"


@interface TransactionBookingPaidFlight : TransactionBookingPaid

@property (nonatomic,strong) NSMutableArray <Passengers*>* adults;
@property (nonatomic,strong) NSMutableArray <Passengers*>* children;
@property (nonatomic,strong) NSMutableArray <Passengers*>* infants;
//
//
//@property (nonatomic,strong) NSDate* departureDateTime;
//@property (nonatomic,strong) NSDate* arrivalDateTime;
//@property (nonatomic,strong) NSString* operatingAirlineFlightNumber;
//@property (nonatomic,strong) NSString* operatingAirlineFlightCode;
//@property (nonatomic,strong) NSString* flightReservationBookDesignationCode;
//@property (nonatomic,strong) NSString* arrivalAirportLocationCode;
//@property (nonatomic,strong) NSString* departureAirportLocationCode;
//@property (nonatomic,strong) NSString* airEquipmentType;
@property (nonatomic, strong) NSString* commission;
@property (nonatomic, strong) NSArray <FlightSegment *> * originDestinationOptions;
@property (nonatomic) BOOL IsRefundable;

@end
