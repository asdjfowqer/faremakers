//
//  UserBooking.h
//  Faremakers
//
//  Created by Mac Mini - 2 on 16/04/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserBooking : NSObject

@property (nonatomic,strong) NSDate* transactionDate;
@property (nonatomic,strong) NSNumber* maskedCardNumber;
@property (nonatomic,strong) NSString* bookStatus;
@property (nonatomic,strong) NSString* bookId;//pnr/bookid
@property (nonatomic,strong) NSString* paymentStatus;
@property (nonatomic,strong) NSString* cancelStatus;
@property (nonatomic,strong) NSNumber* orderId;
@property (nonatomic,strong) NSString* userCompleteName;
@property (nonatomic,strong) NSString* price;
@property (nonatomic,strong) NSString* currency;
@property (nonatomic,strong) NSDate* orderDate;
@property (nonatomic,strong) NSString* packageName;
@property (nonatomic,strong) NSNumber* packageAmount;
@property (nonatomic,strong) NSString* transactionReference;
@property (nonatomic) BOOL IsRefundable;


-(instancetype)initWithDictionary:(NSDictionary*)dictionary;

+(NSString*)subDictionaryKey;
+(instancetype)anInstance;
+(NSArray*)instanceArrayWithDictionary:(NSDictionary*)dictionary;
-(NSString*)dateString:(NSDate*)date;

@end
