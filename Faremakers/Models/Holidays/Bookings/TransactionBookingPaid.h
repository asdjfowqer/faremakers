//
//  TransactionBookingPaid.h
//  Faremakers
//
//  Created by Mac Mini - 2 on 14/04/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "FMUser.h"

@interface TransactionBookingPaid : NSObject

@property (nonatomic, strong) NSString *phoneNumber;
@property (nonatomic, strong) NSNumber *amount;
@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *currency;
@property (nonatomic, strong) NSString *orderInfo;
@property (nonatomic, strong) NSString *orderName;

@property (nonatomic, strong) NSString *promocode;
@property (nonatomic, strong) NSNumber *promocodePercentage;

@property (nonatomic, strong) NSString *hotelName;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *country;
@property (nonatomic, strong) NSString *paymentType;
@property (nonatomic, strong) NSNumber *paymentTransactionId;


- (NSDictionary *)prePaymentDictionary;
- (NSDictionary *)postPaymentDictionary;
- (NSDictionary *)postPaymentDictionaryCorporate;
- (NSDictionary *)prePaymentDictionaryWithNewUser:(NSDictionary*)response;

- (void)addPaymentInformationFromDictionary:(NSDictionary *)paymentInfo;

+ (instancetype)anInstance;


- (NSString *)dateString:(NSDate *)date;
- (NSString *)timeStampString:(NSDate *)date;

@end