//
//  TransferVehicles.h
//  Faremakers
//
//  Created by Mac1 on 08/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TransferVehicles : NSObject

@property (nonatomic) NSString *confirmationDescription;
@property (nonatomic) NSString *maximumLuggage;
@property (nonatomic) NSString *maximumPassengers;
@property (nonatomic) NSString *transferId;
@property (nonatomic) NSString *vehicleCode;
@property (nonatomic) NSString *vehicleItemPriceCurrency;
@property (nonatomic) NSNumber *vehicleItemPriceValue;
@property (nonatomic) NSString *vehicleValue;

+ (NSArray*)itemWithArray:(NSArray*)itemArray;

@end