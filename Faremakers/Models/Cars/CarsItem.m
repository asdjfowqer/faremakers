//
//  CarsItem.m
//  Faremakers
//
//  Created by Mac1 on 08/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "CarsItem.h"
#import "TransferVehicles.h"

@implementation CarsItem

+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary
{
    CarsItem *carsItem = [[CarsItem alloc] init];
    
    if (carsItem)
    {
        carsItem.approxTransTime = [dictionary objectForKey:@"approximateTransferTime"];
        carsItem.approxTransValue = [dictionary objectForKey:@"approximateTransferValue"];
        carsItem.itemCode = [dictionary objectForKey:@"itemCode"];
        carsItem.itemValue = [dictionary objectForKey:@"itemValue"];
        carsItem.maximumPrice = [dictionary objectForKey:@"maximumPrice"];
        carsItem.minimumPrice = [dictionary objectForKey:@"minimumPrice"];
        carsItem.totalVehicles = [dictionary objectForKey:@"totalVehicles"];
        carsItem.transferVehicles = [TransferVehicles itemWithArray:[dictionary objectForKey:@"transferVehicles"]];
        carsItem.vehicleCodes = [dictionary objectForKey:@"vehicleCodes"];
        carsItem.vehicleValues = [dictionary objectForKey:@"vehicleValues"];
    }
    
    return carsItem;
}
+ (NSArray*)itemWithArray:(NSArray*)itemArray
{
    NSMutableArray *CarsItemList = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in itemArray)
    {
        [CarsItemList addObject:[self itemWithDictionary:dict]];
    }
    
    return [[NSArray alloc] initWithArray:CarsItemList];
}


@end
