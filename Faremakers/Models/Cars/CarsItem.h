//
//  CarsItem.h
//  Faremakers
//
//  Created by Mac1 on 08/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CarsItem : NSObject

@property (nonatomic) NSString *approxTransTime;
@property (nonatomic) NSString *approxTransValue;
@property (nonatomic) NSString *itemCode;
@property (nonatomic) NSString *itemValue;
@property (nonatomic) NSNumber *maximumPrice;
@property (nonatomic) NSNumber *minimumPrice;
@property (nonatomic) NSNumber *totalVehicles;
@property (nonatomic) NSArray *transferVehicles;
@property (nonatomic) NSString *vehicleCodes;
@property (nonatomic) NSString *vehicleValues;

+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary;
+ (NSArray*)itemWithArray:(NSArray*)itemArray;

@end
