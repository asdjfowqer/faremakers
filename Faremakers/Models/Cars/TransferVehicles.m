//
//  TransferVehicles.m
//  Faremakers
//
//  Created by Mac1 on 08/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "TransferVehicles.h"


@implementation TransferVehicles

+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary
{
    TransferVehicles *transferVehicles = [[TransferVehicles alloc] init];
    
    if (outline)
    {
        transferVehicles.confirmationDescription = [dictionary objectForKey:@"confirmationDescription"];
        transferVehicles.maximumLuggage = [dictionary objectForKey:@"maximumLuggage"];
        transferVehicles.maximumPassengers = [dictionary objectForKey:@"maximumPassengers"];
        transferVehicles.transferId = [dictionary objectForKey:@"transferId"];
        transferVehicles.vehicleCode = [dictionary objectForKey:@"vehicleCode"];
        transferVehicles.vehicleItemPriceCurrency = [dictionary objectForKey:@"vehicleItemPriceCurrency"];
        transferVehicles.vehicleItemPriceValue = [dictionary objectForKey:@"vehicleItemPriceValue"];
        transferVehicles.vehicleValue = [dictionary objectForKey:@"vehicleValue"];
    }
    
    return transferVehicles;
}
+ (NSArray*)itemWithArray:(NSArray*)itemArray
{
    NSMutableArray *transferVehiclesArray = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in itemArray)
    {
        [transferVehiclesArray addObject:[self itemWithDictionary:dict]];
    }
    
    return [[NSArray alloc] initWithArray:transferVehiclesArray];
}

@end
