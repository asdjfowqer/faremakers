//
//  HotelEssentialInfo.h
//  Faremakers
//
//  Created by Mac1 on 04/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HotelEssentialInfo : NSObject

@property (nonatomic)NSString *essentialInformationText;
@property (nonatomic)NSString *essentialInformationFromDate;
@property (nonatomic)NSString *essentialInformationToDate;

+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary;
+ (NSArray*)itemsWithArray:(NSArray*)array;


@end
