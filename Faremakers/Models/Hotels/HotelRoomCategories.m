//
//  HotelRoomCategories.m
//  Faremakers
//
//  Created by Mac1 on 04/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "HotelRoomCategories.h"

@implementation HotelRoomCategories

+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary
{
    HotelRoomCategories *hotel = [[HotelRoomCategories alloc] init];
    
    if (hotel)
    {
        hotel.hotelCode  = [dictionary objectForKey:@"hotelCode"];
        hotel.roomID = [dictionary objectForKey:@"id"];
        hotel.roomDescription = [dictionary objectForKey:@"description"];
        hotel.currency = [dictionary objectForKey:@"currency"];
        hotel.price = [dictionary objectForKey:@"price"];
        hotel.totalNights = [dictionary objectForKey:@"totalNights"];
        hotel.confirmationCode = [dictionary objectForKey:@"confirmationCode"];
        hotel.basisCode = [dictionary objectForKey:@"basisCode"];
        hotel.confirmationDescription = [dictionary objectForKey:@"confirmationDescription"];
        hotel.basisDescription = [dictionary objectForKey:@"basisDescription"];
        hotel.sharing = [dictionary objectForKey:@"sharing"];
        hotel.roomType = [dictionary objectForKey:@"roomType"];
        hotel.offer = [dictionary objectForKey:@"offer"];
        hotel.hotelName  = [dictionary objectForKey:@"hotelName"];
    }
    
    return hotel;
}

+ (NSArray*)itemsWithArray:(NSArray*)itemArray
{
    NSMutableArray *cities = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in itemArray)
    {
        [cities addObject:[self itemWithDictionary:dict]];
    }
    
    return [[NSArray alloc] initWithArray:cities];
}
- (NSString*)calculatePricePerNight:(NSString*)price
{
    NSInteger totalNight = self.totalNights.integerValue;
    
    if (totalNight)
    {
        return [NSString stringWithFormat:@"%ld",(price.integerValue / self.totalNights.integerValue)];

    }
    return self.price;
}

@end
