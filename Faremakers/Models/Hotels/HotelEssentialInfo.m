//
//  HotelEssentialInfo.m
//  Faremakers
//
//  Created by Mac1 on 04/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "HotelEssentialInfo.h"

@implementation HotelEssentialInfo


+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary
{
    HotelEssentialInfo *hotel = [[HotelEssentialInfo alloc] init];
    
    if (hotel)
    {
        hotel.essentialInformationText  = [dictionary objectForKey:@"essentialInformationText"];
        hotel.essentialInformationFromDate = [dictionary objectForKey:@"essentialInformationFromDate"];
        hotel.essentialInformationToDate = [dictionary objectForKey:@"essentialInformationToDate"];
    }
    
    return hotel;
}

+ (NSArray*)itemsWithArray:(NSArray*)itemArray
{
    NSMutableArray *cities = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in itemArray)
    {
        [cities addObject:[self itemWithDictionary:dict]];
    }
    
    return [[NSArray alloc] initWithArray:cities];
}


@end
