//
//  FMHotels.m
//  Faremakers
//
//  Created by Mac1 on 04/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "FMHotels.h"
#import "HotelRoomCategories.h"
#import "HotelEssentialInfo.h"
#import <CoreLocation/CoreLocation.h>
#import "Cities.h"
#import "HotelImageItem.h"

@implementation FMHotels

+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary
{
    FMHotels *hotel = [[FMHotels alloc] init];
    
    if (hotel)
    {
        NSString *latLang = [dictionary objectForKey:@"hotelLatLong"];
        
        if (latLang) {
            hotel.hotelLongitude = [[latLang componentsSeparatedByString:@","] lastObject];
            hotel.hotelLatitude = [[latLang componentsSeparatedByString:@","] firstObject];
        }
        hotel.hotelCode = [dictionary objectForKey:@"hotelCode"];
        hotel.hotelName = [dictionary objectForKey:@"hotelName"];
        hotel.hasExtraInfo = [dictionary objectForKey:@"hasExtraInfo"];
        hotel.hasMap = [dictionary objectForKey:@"hasMap"];
        hotel.hotelCode = [dictionary objectForKey:@"hotelCode"];
        hotel.hotelName = [dictionary objectForKey:@"hotelName"];
        hotel.hasExtraInfo = [dictionary objectForKey:@"hasExtraInfo"];
        hotel.hasMap= [dictionary objectForKey:@"hasMap"];
        hotel.hasPicture= [dictionary objectForKey:@"hasPicture"];
        hotel.rating= [dictionary objectForKey:@"rating"];
        hotel.city= [dictionary objectForKey:@"city"];
        hotel.roomCategories= [HotelRoomCategories itemsWithArray:[dictionary objectForKey:@"roomCategories"]];
        hotel.essentialInformation = [HotelEssentialInfo itemsWithArray:[dictionary objectForKey:@"essentialInformation"]];
        hotel.price= [dictionary objectForKey:@"price"];
        hotel.offer= [dictionary objectForKey:@"offer"];
        hotel.currencyCode= [dictionary objectForKey:@"currencyCode"];
        hotel.baseFare= [dictionary objectForKey:@"baseFare"];
        hotel.minimumPrice= [dictionary objectForKey:@"minimumPrice"];
        hotel.maximumPrice= [dictionary objectForKey:@"maximumPrice"];
        hotel.hotelImageUrl = [dictionary objectForKey:@"hotelImageUrl"];
        hotel.hotelImages = [HotelImageItem itemsWithArray:[dictionary objectForKey:@"hotelImages"]];
        
    }
    
    return hotel;
}
- (void)getLocationWithsuccessBlock:(FMHotelsSuccessBlock)successBlock failureBlock:(FMHotelsFailureBlock)failureBlock;
{
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    CLLocation *location = [[CLLocation alloc] initWithLatitude:self.hotelLatitude.doubleValue longitude:self.hotelLongitude.doubleValue];
    [geocoder reverseGeocodeLocation:location
                   completionHandler:^(NSArray *placemarks, NSError *error) {
                       
                       CLPlacemark *placemark = [placemarks objectAtIndex:0];
                       
                       self.locationTitle = [NSString stringWithFormat:@"%@ , %@",placemark.administrativeArea,placemark.country];
                       successBlock(self.locationTitle);
                   }];
}
+ (NSArray*)itemsWithArray:(NSArray*)itemArray
{
    NSMutableArray *cities = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in itemArray)
    {
        [cities addObject:[self itemWithDictionary:dict]];
    }
    
    return [[NSArray alloc] initWithArray:cities];
}
+ (NSArray*)findMinMaxPrices:(NSArray*)array
{
    FMHotels *hotelItem = array[0];
    
    __block NSInteger lowest = hotelItem.baseFare.integerValue;
    __block NSInteger highest = hotelItem.baseFare.integerValue;
    
    [array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        FMHotels *hotelItem = (FMHotels*)obj;
        
        DLog(@"%@",hotelItem.baseFare);
        
        if (hotelItem.baseFare.integerValue > highest) {
            
            highest = hotelItem.baseFare.integerValue;
            
        }
        else if (hotelItem.baseFare.integerValue < lowest)
        {
            lowest = hotelItem.baseFare.integerValue;
        }
    }];
    return [NSArray arrayWithObjects:[NSNumber numberWithInteger:lowest],[NSNumber numberWithInteger:highest],nil];
}


@end
