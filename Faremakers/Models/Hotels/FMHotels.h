//
//  FMHotels.h
//  Faremakers
//
//  Created by Mac1 on 04/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^FMHotelsSuccessBlock)(id responseobject);
typedef void (^FMHotelsFailureBlock)(NSError *error);

@class Cities;

@interface FMHotels : NSObject

@property (nonatomic) NSString *hotelCode;
@property (nonatomic) NSString *hotelName;
@property (nonatomic) NSString *hasExtraInfo;
@property (nonatomic) NSString *hasMap;
@property (nonatomic) NSString *hasPicture;
@property (nonatomic) NSNumber *rating;
@property (nonatomic) NSDictionary *city;
@property (nonatomic) NSArray *roomCategories;
@property (nonatomic) NSArray *essentialInformation;
@property (nonatomic) NSString *price;
@property (nonatomic) NSString *offer;
@property (nonatomic) NSString *currencyCode;
@property (nonatomic) NSNumber *baseFare;
@property (nonatomic) NSNumber *minimumPrice;
@property (nonatomic) NSNumber *maximumPrice;
@property (nonatomic) NSString *hotelImageUrl;
@property (nonatomic) NSArray *hotelImages;
@property (nonatomic) NSString *hotelLatitude;
@property (nonatomic) NSString *hotelLongitude;
@property (nonatomic) NSString *locationTitle;


+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary;
+ (NSArray*)itemsWithArray:(NSArray*)array;
+ (NSArray*)findMinMaxPrices:(NSArray*)array;

- (void)getLocationWithsuccessBlock:(FMHotelsSuccessBlock)successBlock failureBlock:(FMHotelsFailureBlock)failureBlock;


@end
