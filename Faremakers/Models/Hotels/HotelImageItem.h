//
//  HotelImageItem.h
//  Faremakers
//
//  Created by Mac1 on 16/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HotelImageItem : NSObject

@property(nonatomic) NSString *imageDescription;
@property(nonatomic) NSString *imageUrl;

+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary;
+ (NSArray*)itemsWithArray:(NSArray*)array;

@end
