//
//  HotelImageItem.m
//  Faremakers
//
//  Created by Mac1 on 16/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "HotelImageItem.h"

@implementation HotelImageItem

+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary
{
    HotelImageItem *hotelImageItem = [[HotelImageItem alloc] init];
    
    if (hotelImageItem)
    {
        hotelImageItem.imageDescription= [dictionary objectForKey:@"imageDescription"];
        hotelImageItem.imageUrl = [dictionary objectForKey:@"imageUrl"];
    }
    
    return hotelImageItem;
}
+ (NSArray*)itemsWithArray:(NSArray*)itemArray
{
    NSMutableArray *Images = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in itemArray)
    {
        [Images addObject:[self itemWithDictionary:dict]];
    }
    
    return [[NSArray alloc] initWithArray:Images];
}

@end
