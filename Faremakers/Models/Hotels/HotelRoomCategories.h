//
//  HotelRoomCategories.h
//  Faremakers
//
//  Created by Mac1 on 04/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HotelRoomCategories : NSObject

@property (nonatomic)NSString *roomID;
@property (nonatomic)NSString *roomDescription;
@property (nonatomic)NSString *currency;
@property (nonatomic)NSString *price;
@property (nonatomic)NSString *confirmationCode;
@property (nonatomic)NSString *basisCode;//            "totalNights": "1",
@property (nonatomic)NSString *totalNights;
@property (nonatomic)NSString *confirmationDescription;
@property (nonatomic)NSString *basisDescription;
@property (nonatomic)NSString *sharing;
@property (nonatomic)NSString *hotelCode;
@property (nonatomic)NSString *roomType;
@property (nonatomic)NSString *offer;
@property (nonatomic)NSString *hotelName;


+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary;
+ (NSArray*)itemsWithArray:(NSArray*)array;
- (NSString*)calculatePricePerNight:(NSString*)price;


@end
