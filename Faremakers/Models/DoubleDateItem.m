//
//  DoubleDateItem.m
//  Faremakers
//
//  Created by Mac1 on 29/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import "DoubleDateItem.h"
#import "Constants.h"

@implementation DoubleDateItem

-(id)initWithDoubleDate:(NSDate*)date1 :(NSDate*)date2
{
    if (self = [super init])
    {
        self.depDate = date1;
        self.retDate = date2;
    }
    return self;
}

- (void)updateAllDates
{
    if ([self isCurrentDate:self.depDate olderThanDate:[NSDate date]])
    {
        self.depDate = [NSDate date];
    }
    if ([self isCurrentDate:self.retDate olderThanDate:[NSDate date]])
    {
        self.retDate = [NSDate date];
    }
}
- (BOOL)isCurrentDate:(NSDate*)date1 olderThanDate:(NSDate*)date2
{
    return [date1 timeIntervalSinceDate:date2] < 0;
}
-(NSInteger)noOfDays
{
    NSTimeInterval interval = [self.retDate timeIntervalSinceDate:self.depDate];
    return floor(interval/KTimeInterval);
}

@end
