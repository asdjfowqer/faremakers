//
//  Cities.h
//  Faremakers
//
//  Created by Mac1 on 11/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Cities : NSObject

@property (nonatomic)NSString *text;//":"23000",
@property (nonatomic)NSString *cityCode;//":"PKR"
@property (nonatomic)NSString *countryCode;//":"PKR"
@property (nonatomic)NSString *countryName;//":"PKR"
@property (nonatomic)NSString *airPortName;

+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary;
+ (NSArray*)itemsWithArray:(NSArray*)array;
+ (NSString*)stringWithCityNameOnly:(NSString *)string;


@end
