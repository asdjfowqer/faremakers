//
//  Cities.m
//  Faremakers
//
//  Created by Mac1 on 11/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "Cities.h"

@implementation Cities

+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary
{
    Cities *city = [[Cities alloc] init];
    
    if (city)
    {
        city.text = [dictionary objectForKey:@"text"];
        city.cityCode = [dictionary objectForKey:@"cityCode"];
        city.countryCode = [dictionary objectForKey:@"coutryCode"];
        city.countryName = [dictionary objectForKey:@"countryName"];
        city.airPortName = [dictionary objectForKey:@"airtportName"];
    }
    
    return city;
}

+ (NSArray*)itemsWithArray:(NSArray*)itemArray
{
    NSMutableArray *cities = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in itemArray)
    {
        [cities addObject:[self itemWithDictionary:dict]];
    }
    
    return [[NSArray alloc] initWithArray:cities];
}
//[[[newcity.text componentsSeparatedByString:@","] firstObject] localizedUppercaseString]
+ (NSString*)stringWithCityNameOnly:(NSString *)string
{
    string = [[[string componentsSeparatedByString:@"," ] firstObject] lowercaseString];
    return [string capitalizedString];
}
@end
