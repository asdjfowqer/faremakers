//
//  User+CoreDataProperties.m
//  Faremakers
//
//  Created by Mac1 on 07/10/2016.
//  Copyright © 2016 Self. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "User+CoreDataProperties.h"

@implementation User (CoreDataProperties)

@dynamic authToken;
@dynamic dateOfBirth;
@dynamic email;
@dynamic firstName;
@dynamic imageUrl;
@dynamic lastName;
@dynamic phoneNumber;
@dynamic userName;
@dynamic userType;

@end
