//
//  HolidaysItem.h
//  Faremakers
//
//  Created by Mac1 on 08/03/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HolidayTours : NSObject

@property (nonatomic) NSString *tourDetails;
@property (nonatomic) NSString *tourIncludes;
@property (nonatomic) NSString *tourTitle;

+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary;
+ (NSArray*)itemWithArray:(NSArray*)itemArray;

@end

@interface HolidayVisa : NSObject

@property (nonatomic) NSString *city;
@property (nonatomic) NSNumber *visaRate;

+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary;
+ (NSArray*)itemWithArray:(NSArray*)itemArray;

@end

@interface HolidayTicketDetail : NSObject

@property (nonatomic) NSNumber *adultTicketRate;
@property (nonatomic) NSNumber *childTicketRate;
@property (nonatomic) NSString *extraTicketDetails;
@property (nonatomic) NSString *routeDetails;


+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary;
+ (NSArray*)itemWithArray:(NSArray*)itemArray;

@end

@interface HolidayItem : NSObject

@property (nonatomic) NSString *holidayAccomodDetail;
@property (nonatomic) NSString *termsAndConditions;
@property (nonatomic) NSString *holidayPackage;
@property (nonatomic) HolidayTicketDetail *holidayTicketDetail;
@property (nonatomic) NSString *holidayPriceDetail;
@property (nonatomic) NSString *holidayPriceNote;
@property (nonatomic) NSString *holidayVisaDetail;
@property (nonatomic) NSString *holidayTicketNote;
@property (nonatomic) NSString *holidayImageUrl;
@property (nonatomic) NSString *holidayDetail;
@property (nonatomic) NSString *currencyCode;
@property (nonatomic) NSString *familyType;
@property (nonatomic) NSString *roomType;
@property (nonatomic) NSNumber *packagePrice;
@property (nonatomic) NSNumber *totalNightsStay;
@property (nonatomic) NSNumber *packageID;
@property (nonatomic) NSNumber *noOfRooms;
@property (nonatomic) NSArray *holidaysTourList;
@property (nonatomic) NSArray *nightsStay;
@property (nonatomic) NSArray *priceInclude;
@property (nonatomic) NSArray *visasList;




+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary;
+ (NSArray*)itemWithArray:(NSArray*)itemArray;
+ (NSString*)getStringByAddingBulletsIntoArrayOfString:(NSArray*)stringArray;
+ (NSString*)getStringByAddingBullets:(NSString*)str;
+ (NSArray*)findMinMaxPrices:(NSArray*)array;

@end
