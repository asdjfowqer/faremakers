//
//  AirPort.h
//  Faremakers
//
//  Created by Mac1 on 18/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AirPort : NSObject

@property (nonatomic) NSString *locationCode;
@property (nonatomic) NSString *codeContext;
@property (nonatomic) NSString *airPortName;//Getting from Local Json

+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary;

@end