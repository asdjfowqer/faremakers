//
//  AirItinTotalFare.h
//  Faremakers
//
//  Created by Mac1 on 07/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AirBaseFare;

@interface AirItinTotalFare : NSObject

@property (nonatomic) AirBaseFare *baseFare;//":{ },
@property (nonatomic) AirBaseFare *equivFare;//":{ },
@property (nonatomic) NSArray *taxes;
@property (nonatomic) AirBaseFare *totalFare;
@property (nonatomic) BOOL negotiatedFare;
@property (nonatomic) AirBaseFare *commission;

+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary;

@end