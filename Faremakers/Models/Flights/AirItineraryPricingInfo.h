//
//  AirItineraryPricingInfo.h
//  Faremakers
//
//  Created by Mac1 on 07/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AirBaseFare;
@class AirItinTotalFare;

@interface AirPassengerTypeQuantity : NSObject

@property (nonatomic) NSString *code;//":"23000",
@property (nonatomic) NSString *quantity;//":"PKR"
@property (nonatomic) BOOL changeable;
@property (nonatomic) NSString *passengerType;


+ (NSArray*)itemWithArray:(NSArray*)itemArray;
+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary;

@end

@interface FareBasisCodes : NSObject

@property (nonatomic) NSString *bookingCode;//":{ }
@property (nonatomic) NSString *value;//":{ }
@property (nonatomic) NSString *departureAirportCode;//":{ }
@property (nonatomic) NSString *arrivalAirportCode;//":{ }

+ (NSArray*)itemWithArray:(NSArray*)itemArray;
+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary;
- (NSDictionary*)prepareDictForPostDataWithDate:(NSString*)date andAirlineCode:(NSString*)airCode;

@end


@interface Endorsements : NSObject

@property (nonatomic) BOOL nonRefundableIndicator;
@property (nonatomic) BOOL nonRefundableIndicatorSpecified;
@property (nonatomic) BOOL nonEndorsableIndicatorSpecified;

+ (NSArray*)itemWithArray:(NSArray*)itemArray;
+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary;

@end
/*
"FareInfos":[
             {
                 "DepartureDate":null,
                 "FareReference":"Y",
                 "RuleInfo":null,
                 "FilingAirline":null,
                 "MarketingAirline":null,
                 "DepartureAirport":null,
                 "ArrivalAirport":null,
                 "TPA_Extensions":{
                     "SeatsRemaining":{
                         "Number":8,
                         "NumberSpecified":true,
                         "BelowMin":false,
                         "BelowMinSpecified":true
                     },
                     "Cabin":{
                         "Cabin":"Y"
                     },
                     "FareNote":null,
                     "Meal":{
                         "Code":"M"
                     },
                     "Rule":null
                 },
                 "NegotiatedFare":false,
                 "NegotiatedFareCode":null
             }
             ],
*/
@interface FareInfos : NSObject

@property (nonatomic) NSNumber *seatsRemaining;
@property (nonatomic) NSString *cabinCode;
@property (nonatomic) NSString *cabin;
@property (nonatomic) NSString *meal;//tpA_Extensions//info

+ (NSArray*)itemWithArray:(NSArray*)itemArray;
+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary;

@end


@interface PricingInfo_Extensions : NSObject

@property (nonatomic) BOOL indicator;
@property (nonatomic) BOOL indicatorSpecified;

+ (NSArray*)itemWithArray:(NSArray*)itemArray;
+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary;

@end



@interface AirItineraryPricingInfo : NSObject

@property (nonatomic) AirItinTotalFare *itinTotalFare;//":{ },
@property (nonatomic) NSArray *ptC_FareBreakdown;//FareBreakdowns
@property (nonatomic) NSString *pricingSource;//":{ }
@property (nonatomic) BOOL alternateCityOption;
@property (nonatomic) BOOL cachedItin;
@property (nonatomic) BOOL fareReturnedSpecified;
@property (nonatomic) PricingInfo_Extensions *tpA_Extensions;
@property (nonatomic) NSArray *fareInfos;



+ (NSArray*)itemWithArray:(NSArray*)itemArray;
+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary;

- (BOOL)checkNonRefunableFlight;


@end
