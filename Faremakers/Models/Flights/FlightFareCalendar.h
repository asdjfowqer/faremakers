//
//  FlightFareCalendar.h
//  Chart
//
//  Created by Mac1 on 15/02/2016.
//  Copyright © 2016 Hipolito Arias. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AirlineCodes : NSObject

@property (nonatomic) NSNumber *Fare;
@property (nonatomic) NSArray *airlineCodes;

+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary;
+ (NSArray*)itemWithArray:(NSArray*)itemArray;

@end

@interface LowestFare : NSObject

@property (nonatomic) NSNumber *Fare;
@property (nonatomic) NSArray *airlineCodes;

+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary;
+ (NSArray*)itemWithArray:(NSArray*)itemArray;

@end


@interface FlightFareCalendar : NSObject

@property (nonatomic) NSString *CurrencyCode;
@property (nonatomic) NSDate *DepartureDateTime;
@property (nonatomic) NSDate *ReturnDateTime;
@property (nonatomic) LowestFare *lowestFare;

+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary;
+ (NSArray*)itemWithArray:(NSArray*)itemArray;
+ (NSArray*)itemWithArray:(NSArray*)itemArray withBaseCurrency:(NSString*)currency;
+ (NSArray*)getFareCalendarArray:(NSArray*)itemArray forCurrentDate:(NSString*)date;

@end
