//
//  AirLinePricedItinerary.h
//  Faremakers
//
//  Created by Mac1 on 07/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AirFreBasis : NSObject

@property (nonatomic)NSString *code;//":"23000",
@property (nonatomic)NSString *farePassengerType;//":"PKR"
@property (nonatomic)NSString *fareType;
@property (nonatomic)NSString *filingCarrier;//":"PKR"
@property (nonatomic)NSString *globalInd;//":"PKR"
@property (nonatomic)NSString *market;//":"PKR"

@end




@interface MarketingAirline : NSObject

@property (nonatomic) NSString *code;

+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary;

@end

@interface OperatingAirline : NSObject

@property (nonatomic) NSString *code;
@property (nonatomic) NSString *flightNumber;

+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary;
+ (NSArray*)itemWithArray:(NSArray*)itemArray;

@end




@interface AirEquipment : NSObject

@property (nonatomic) NSString *airEquipType;
@property (nonatomic) BOOL changeofGauge;

+ (NSArray*)itemWithArray:(NSArray*)itemArray;

@end


@interface ETicket : NSObject

@property (nonatomic) BOOL ind;
@property (nonatomic) BOOL indSpecified;


+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary;

@end

@interface AirItinerary_Extensions : NSObject

@property (nonatomic) ETicket *eTicket;

+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary;


@end

@interface OriginDestination : NSObject //"originDestinationOptions"

@property (nonatomic) NSArray *flightSegments;
@property (nonatomic) NSNumber *stopQuantity;
@property (nonatomic) NSArray *layOverTimes;
@property (nonatomic) NSArray *duration;

+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary;

@end

@interface AirItinerary : NSObject //"originDestinationOptions"

@property (nonatomic) NSArray<OriginDestination *> *originDestinationOptions;
@property (nonatomic) NSNumber *directionInd;
@property (nonatomic) BOOL directionIndSpecified;

@end

@interface AirheaderInformation : NSObject
@property (nonatomic) NSString *validatingCarrier;
@property (nonatomic) NSArray *text;

+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary;

@end

@interface Air_Extensions : NSObject

@property (nonatomic) NSString *code;


+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary;

@end

@interface AirTicketingInfo : NSObject

@property (nonatomic) NSNumber *ticketType;
@property (nonatomic) NSNumber *validInterline;


+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary;

@end

@interface AirLinePricedItinerary : NSObject

@property (nonatomic) NSArray *airItineraryPricingInfo;
@property (nonatomic) AirItinerary *airItinerary;
@property (nonatomic) Air_Extensions *tpA_Extensions;
@property (nonatomic) NSArray *customeArray;

+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary;

+(NSArray*)filterFlightData:(NSArray*)flightArray forItem:(NSArray*)items withKey:(NSString*)key andIndex:(NSInteger)index;

+(NSArray*)filterNonRefundableFlight:(NSArray*)flightArray;

+(NSArray*)FilterFlightsArray:(NSArray*)flightArray forDayTimingsWithStartInterv:(NSTimeInterval)startTime andEndInterv:(NSTimeInterval)endingInterv withTripIndex:(NSInteger)oneWayIndex;

+(NSArray*)filterFlightForDuration:(NSArray*)flightArray withDuration:(NSInteger)duration withTripIndex:(NSInteger)oneWayIndex;

+(NSArray*)filterFlightForLayoverDuration:(NSArray*)flightArray withLayoverDuration:(NSInteger)duration withTripIndex:(NSInteger)oneWayIndex;

@end



