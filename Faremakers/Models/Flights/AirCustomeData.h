//
//  AirCustomeData.h
//  Faremakers
//
//  Created by Mac1 on 18/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface AirCustomeData : NSObject

@property (nonatomic) NSNumber *totalStops;
@property (nonatomic) NSString *duration;
@property (nonatomic) NSDate *departureDateTime;
@property (nonatomic) NSString *departureTime;
@property (nonatomic) NSDate *arrivalDateTime;
@property (nonatomic) NSString *arrivalTime;
@property (nonatomic) NSString *DepartureAirport;
@property (nonatomic) NSString *airLine;
@property (nonatomic) NSString *airLineFlightNo;
@property (nonatomic) NSString *connectingFlight;
@property (nonatomic) NSString *connectingFlighNo;
@property (nonatomic) BOOL isMultiAirline;
@property (nonatomic) NSString *arrivalAirport;
@property (nonatomic) NSArray *allAirLines;
@property (nonatomic) NSArray *layOverTimes;




+ (NSArray*)itemWithArray:(NSArray*)itemArray;
+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary;
- (NSString*)getTimeOnlyFromDate:(NSDate*)date;
- (NSTimeInterval)returnDayTimingInterval:(NSDate*)todayDate;
- (NSTimeInterval)converFlightLayoverDurationToInterval:(NSString*)layover;
- (NSTimeInterval)converDurationToInterval;

@end
