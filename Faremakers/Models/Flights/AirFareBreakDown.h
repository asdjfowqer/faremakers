//
//  AirFareBreakDown.h
//  Faremakers
//
//  Created by Mac1 on 18/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AirItinTotalFare;
@class Endorsements;
@class AirPassengerTypeQuantity;

@interface AirFareBreakDown : NSObject

@property (nonatomic) AirPassengerTypeQuantity *passengerTypeQuantity;
@property (nonatomic) NSArray *fareBasisCodes;
@property (nonatomic) AirItinTotalFare *passengerFare;
@property (nonatomic) Endorsements *endorsements;
@property (nonatomic) NSString *tpA_Extensions;//tpA_Extensions//info
@property (nonatomic) BOOL indicatorSpecified;

+ (NSArray*)itemWithArray:(NSArray*)itemArray;
+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary;


@end
