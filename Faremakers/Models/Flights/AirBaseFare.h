//
//  AirBaseFare.h
//  Faremakers
//
//  Created by Mac1 on 07/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AirBaseFare : NSObject

@property (nonatomic)NSString *amount;//":"23000",
@property (nonatomic)NSString *currencyCode;//":"PKR"
@property (nonatomic)NSString *decimalPlaces;//":"PKR"
@property (nonatomic) BOOL amountSpecified;

+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary;

@end
