//
//  AirCustomeData.m
//  Faremakers
//
//  Created by Mac1 on 18/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "AirCustomeData.h"
#import "AirLinePricedItinerary.h"
#import "FlightSegment.h"
#import "AirLines.h"
#import "Constants.h"

@implementation AirCustomeData

+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary
{
    AirCustomeData *airCustomeData = [[AirCustomeData alloc] init];
    
    if (airCustomeData)
    {
        airCustomeData.totalStops = (NSNumber*)[dictionary objectForKey:@"totalStops"];
        airCustomeData.duration = [dictionary objectForKey:@"Duration"];
	    
	    DLog(@"%@",dictionary);
	    
        airCustomeData.departureDateTime = [FlightSegment parseDateFromString:[dictionary objectForKey:@"DepartureDateTime"]];
        airCustomeData.departureTime = [dictionary objectForKey:@"DepartureTime"];
        airCustomeData.arrivalDateTime = [FlightSegment parseDateFromString:[dictionary objectForKey:@"ArrivalDateTime"]];
        airCustomeData.arrivalTime = [dictionary objectForKey:@"ArrivalTime"];
        airCustomeData.DepartureAirport = [dictionary objectForKey:@"DepartureAirport"];
        airCustomeData.airLine = [dictionary objectForKey:@"airline"];
        airCustomeData.airLineFlightNo = [dictionary objectForKey:@"airlineFlightNo"];
        airCustomeData.connectingFlight = [dictionary objectForKey:@"connectingFlight"];
        airCustomeData.connectingFlighNo = [dictionary objectForKey:@"connectingfFlightNo"];
        airCustomeData.isMultiAirline = (BOOL)[dictionary objectForKey:@"MultiAirline"];
        airCustomeData.arrivalAirport = [dictionary objectForKey:@"ArrivalAirport"];
        airCustomeData.allAirLines = [AirLines itemWithArray:[dictionary objectForKey:@"Airlines"]];
        airCustomeData.layOverTimes = [AirCustomeData findLayOverTimes:dictionary withTotalStops:airCustomeData.totalStops.integerValue];
    }
    
    return airCustomeData;
}
+ (NSArray*)findLayOverTimes:(NSDictionary*)dictionary withTotalStops:(NSInteger)totalStops
{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    NSInteger i = 0;
    while (i < totalStops)
    {
        NSString *key = [NSString stringWithFormat:@"LayOuverTime%ld",(i+1)];
        NSString *layover = [dictionary objectForKey:key];
        
        if (layover != nil) {
            [array addObject:layover];
        }
        ++i;
    }
    
    return array;
}

+ (NSArray*)itemWithArray:(NSArray*)itemArray
{
    NSMutableArray *airCustomeArray = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in itemArray)
    {
        [airCustomeArray addObject:[self itemWithDictionary:dict]];
    }
    
    return [[NSArray alloc] initWithArray:airCustomeArray];
}
- (NSString*)getTimeOnlyFromDate:(NSDate*)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    NSString *str = [dateFormatter stringFromDate:date];
    return str;
}
-(NSString*)stringByReplacingStringsFromDictionary:(NSDictionary*)dict WithString:(NSString*)str
{
    NSMutableString *string = str.mutableCopy;
    for(NSString *key in dict)
        [string replaceOccurrencesOfString:key withString:dict[key] options:0 range:NSMakeRange(0, string.length)];
    return string.copy;
}
// Used while Duration Sortings
- (NSTimeInterval)returnInterval:(NSString*)str
{
    NSTimeInterval timerInterval = 0;
    NSMutableArray *chunks = [[NSMutableArray alloc] initWithArray:[str componentsSeparatedByString:@" "]];
    
    if (3 - chunks.count== 0)
    {
        NSString *str = chunks[0];
        timerInterval = timerInterval + (str.integerValue *24*60*60);
        [chunks removeObjectAtIndex:0];
        
    }
    if (2 - chunks.count == 0)
    {
        NSString *str = chunks[0];
        timerInterval = timerInterval + (str.integerValue *60*60);
        [chunks removeObjectAtIndex:0];
        
    }
    if (1 - chunks.count == 0)
    {
        NSString *str = chunks[0];
        timerInterval = timerInterval + (str.integerValue *60);
        [chunks removeObjectAtIndex:0];
        
    }
    
    return timerInterval;
}
- (NSTimeInterval)returnChunksInterval:(NSMutableArray*)chunks
{
    NSTimeInterval timerInterval = 0;
    
    if (3 - chunks.count== 0)
    {
        NSString *str = chunks[0];
        timerInterval = timerInterval + (str.integerValue *60*60);
        [chunks removeObjectAtIndex:0];
        
    }
    if (2 - chunks.count == 0)
    {
        NSString *str = chunks[0];
        timerInterval = timerInterval + (str.integerValue *60);
        [chunks removeObjectAtIndex:0];
        
    }
    if (1 - chunks.count == 0)
    {
        NSString *str = chunks[0];
        timerInterval = timerInterval + (str.integerValue);
        [chunks removeObjectAtIndex:0];
        
    }
    
    return timerInterval;
}

//3h 55 m
- (NSTimeInterval)converDurationToInterval
{
    NSDictionary *replacements = [NSDictionary dictionaryWithObjectsAndKeys:
                                  @"", @"day",
                                  @"", @"h",
                                  @"", @"m",
                                  nil];
    NSString *str = [self stringByReplacingStringsFromDictionary:replacements WithString:self.duration];

   return [self returnInterval:str];
}
- (NSTimeInterval)converFlightLayoverDurationToInterval:(NSString*)layover
{
    NSMutableArray *chunks = [[NSMutableArray alloc]initWithArray:[layover componentsSeparatedByString:@":"]];
    return [self returnChunksInterval:chunks];
}

-(NSTimeInterval)returnDayTimingInterval:(NSDate*)todayDate
{
//    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//   [formatter setDateFormat:@"yyyy-MM-dd HH.mm.ss Z"];
//    NSDate *newdate = [formatter dateFromString:newDate];
    
    NSTimeInterval interval2 = [self.departureDateTime timeIntervalSinceDate:todayDate];
    
    if (interval2 > KTimeInterval) {
        double multipler = interval2/KTimeInterval;
        interval2 = KTimeInterval *(multipler - floor(multipler));
    }
    
    return interval2;
}
@end
