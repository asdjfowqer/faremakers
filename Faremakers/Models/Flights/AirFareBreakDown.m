//
//  AirFareBreakDown.m
//  Faremakers
//
//  Created by Mac1 on 18/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "AirFareBreakDown.h"
#import "AirItinTotalFare.h"
#import "AirItineraryPricingInfo.h"

@implementation AirFareBreakDown

+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary
{
    AirFareBreakDown *fareBreakdowns = [[AirFareBreakDown alloc] init];
    
    if (fareBreakdowns)
    {
        fareBreakdowns.indicatorSpecified = (BOOL)[dictionary objectForKey:@"IndicatorSpecified"];
        fareBreakdowns.tpA_Extensions = [[[dictionary objectForKey:@"TpA_Extensions"] objectForKey:@"FareCalcLine"] objectForKey:@"Info"];
        fareBreakdowns.endorsements = [Endorsements itemWithDictionary:[dictionary objectForKey:@"Endorsements"]];
        fareBreakdowns.passengerFare = [AirItinTotalFare itemWithDictionary:[dictionary objectForKey:@"Passengerfare"]];
        fareBreakdowns.passengerTypeQuantity = [AirPassengerTypeQuantity itemWithDictionary:[dictionary objectForKey:@"Passengertypequantity"]];
        fareBreakdowns.fareBasisCodes = [FareBasisCodes itemWithArray:[dictionary objectForKey:@"Farebasiscode"]];
        //error
    }
    
    return fareBreakdowns;
}

+ (NSArray*)itemWithArray:(NSArray*)itemArray
{
    NSMutableArray *fareBreakdowns = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in itemArray)
    {
        [fareBreakdowns addObject:[self itemWithDictionary:dict]];
    }
    
    return [[NSArray alloc] initWithArray:fareBreakdowns];
}


@end
