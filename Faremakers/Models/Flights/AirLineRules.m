//
//  AirLineRules.m
//  Faremakers
//
//  Created by Mac1 on 03/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "AirLineRules.h"

@implementation AirLineRules

+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary
{
    AirLineRules *outline = [[AirLineRules alloc] init];
    
    if (outline)
    {
        outline.ruleTitle = [dictionary objectForKey:@"title"];
        outline.ruleText = [dictionary objectForKey:@"text"];
    }
    
    return outline;
}
+ (NSArray*)itemWithArray:(NSArray*)itemArray
{
    NSMutableArray *operatingAirlineArray = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in itemArray)
    {
        [operatingAirlineArray addObject:[self itemWithDictionary:dict]];
    }
    
    return [[NSArray alloc] initWithArray:operatingAirlineArray];
}


@end
