//
//  FlightSegment.m
//  Faremakers
//
//  Created by Mac1 on 18/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "FlightSegment.h"
#import "NSDate+DateComponents.h"
#import "AirLinePricedItinerary.h"
#import "AirPort.h"
#import "AirLinesNameData.h"
#import "AirTimeZone.h"

@implementation FlightSegment

- (NSString *)stringFromTimeInterval:(NSTimeInterval)interval
{
    NSInteger ti = (NSInteger)interval;
    // NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);

    return [NSString stringWithFormat:@"%02ldh %02ldm", (long)hours, (long)minutes];
}

+ (NSDate *)parseDateFromString:(NSString *)stringDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd'T'HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    NSDate *date =  [dateFormatter dateFromString:stringDate];
    return date;
}

+ (NSDate *)parseTimeFromString:(NSString *)stringDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    NSDate *date =  [dateFormatter dateFromString:stringDate];
    return date;
}

+ (NSString *)parseDateOnly:(NSDate *)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd"];
    return [dateFormatter stringFromDate:date];
}

+ (NSString *)parseStringFromDate:(NSDate *)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd'T'HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    return [dateFormatter stringFromDate:date];
}

+ (instancetype)itemWithDictionary:(NSDictionary *)dictionary
{
    FlightSegment *flightSegment = [[FlightSegment alloc]init];

    if (flightSegment)
    {
        flightSegment.operatingAirline = [OperatingAirline itemWithDictionary:[dictionary objectForKey:@"OperatingAirline"]];
        flightSegment.departureTimeZone = [AirTimeZone itemWithDictionary:[dictionary objectForKey:@"Departuretimezone"]];
        flightSegment.tpA_Extensions = [AirItinerary_Extensions itemWithDictionary:[dictionary objectForKey:@"TpA_Extensions"]];
        flightSegment.arrivalTimeZone = [AirTimeZone itemWithDictionary:[dictionary objectForKey:@"Arrivaltimezone"]];
        flightSegment.arrivalDateTime = [self parseDateFromString:[dictionary objectForKey:@"ArrivalDateTime"]];
        flightSegment.departureDateTime = [self parseDateFromString:[dictionary objectForKey:@"DepartureDateTime"]];
        flightSegment.departureAirport = [AirPort itemWithDictionary:[dictionary objectForKey:@"DepartureAirport"]];
        flightSegment.arrivalAirport = [AirPort itemWithDictionary:[dictionary objectForKey:@"ArrivalAirport"]];
        DLog(@"%@", [dictionary objectForKey:@"Equipment"]);
        flightSegment.equipment = [AirEquipment itemWithArray:[dictionary objectForKey:@"Equipment"]];
        flightSegment.elapsedTimeSpecified = (BOOL)[dictionary objectForKey:@"ElapsedTimeSpecified"];
        flightSegment.actionCodeSpecified = (BOOL)[dictionary objectForKey:@"ActionCodeSpecified"];
        flightSegment.resBookDesigCode = [dictionary objectForKey:@"ResBookDesigCode"];
        flightSegment.elapsedTime = (NSNumber *)[dictionary objectForKey:@"ElapsedTime"];

        flightSegment.flightNumber = [dictionary objectForKey:@"FlightNumber"];
        flightSegment.stopQuantity = [dictionary objectForKey:@"StopQuantity"];
        flightSegment.marriageGrp = [dictionary objectForKey:@"MarriageGrp"];

        flightSegment.layOverTime = -1;

        // flightSegment.airlineName = [[AirLinesNameData defaultAirLinesData] getAirlineNameByCode:flightSegment.operatingAirline.code];
    }
    return flightSegment;
}

+ (void)calculateLayoverTime:(NSMutableArray *)itemArray
{
    // DLog(@"segment count %@",itemArray);

    if (itemArray.count <= 1)
    {
        return;
    }

    for (int i = 0; i < itemArray.count - 1; i++)
    {
        FlightSegment *_1stflightSegment = [itemArray objectAtIndex:i];
        FlightSegment *_2ndflightsegment = [itemArray objectAtIndex:(i + 1)];

        NSTimeInterval timeInterval = [_2ndflightsegment.departureDateTime timeIntervalSinceDate:_1stflightSegment.arrivalDateTime];

        _1stflightSegment.layOverTime = timeInterval;
    }
}

+ (NSArray *)itemWithArray:(NSArray *)itemArray
{
    NSMutableArray *segmentArray = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in itemArray)
    {
        [segmentArray addObject:[self itemWithDictionary:dict]];
    }
    [self calculateLayoverTime:segmentArray];
    return [[NSArray alloc] initWithArray:segmentArray];
}

+ (NSString *)getTimeStringFromDate:(NSDate *)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    [dateFormatter setDateFormat:@"HH:mm"];
    return [dateFormatter stringFromDate:date];
}

// Tue 29 Dec 2014
- (NSString *)getDateStringFromDate:(NSDate *)date
{
    return [NSString stringWithFormat:@"%@ %@ %@ %@", [date getDayOfTheWeek], [date getDayOfTheMonth], [date getMonth], [date getYear]];
}

- (NSDictionary *)toJSONDictionary
{
    return @{
             @"DepartureDateTime": [FlightSegment parseStringFromDate:self.departureDateTime],
             @"ArrivalDateTime": [FlightSegment parseStringFromDate:self.arrivalDateTime],
             @"OperatingAirlineFlightNumber": self.operatingAirline.flightNumber,
             @"OperatingAirlineFlightCode": self.operatingAirline.code,
             @"flightResBookDesigCode": self.resBookDesigCode,
             @"ArrivalAirportLocationCode": self.arrivalAirport.locationCode,
             @"DepartureAirportLocationCode": self.departureAirport.locationCode,
             @"AirEquipmentType": [self.equipment firstObject].airEquipType,
             };
}

@end