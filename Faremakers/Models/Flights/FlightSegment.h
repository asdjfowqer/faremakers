//
//  FlightSegment.h
//  Faremakers
//
//  Created by Mac1 on 18/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AirPort;
@class OperatingAirline;
@class MarketingAirline;
@class AirItinerary_Extensions;
@class AirTimeZone;
@class AirEquipment;

@interface FlightSegment : NSObject

@property (nonatomic) AirPort *departureAirport;
@property (nonatomic) AirPort *arrivalAirport;
@property (nonatomic) OperatingAirline *operatingAirline;
@property (nonatomic) NSArray<AirEquipment *> *equipment;
@property (nonatomic) NSString *marriageGrp;
@property (nonatomic) AirTimeZone *departureTimeZone;
@property (nonatomic) AirTimeZone *arrivalTimeZone;
@property (nonatomic) AirItinerary_Extensions *tpA_Extensions;
@property (nonatomic) NSDate *departureDateTime;//":"01-09T15:30:00",
@property (nonatomic) NSDate *arrivalDateTime;//":"01-09T16:25:00",
@property (nonatomic) NSString *stopQuantity;//":"0"
@property (nonatomic) NSString *flightNumber;
@property (nonatomic) NSString *resBookDesigCode;//":"H",
@property (nonatomic) BOOL actionCodeSpecified;//":"H",
@property (nonatomic) NSNumber *elapsedTime;//":"00.55",
@property (nonatomic) BOOL elapsedTimeSpecified;
@property (nonatomic) NSTimeInterval layOverTime;
@property (nonatomic) NSString *airlineName;//Getting from Local Json


+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary;
+ (NSArray*)itemWithArray:(NSArray*)itemArray;
+ (NSString*)getTimeStringFromDate:(NSDate*)date;
- (NSString *)stringFromTimeInterval:(NSTimeInterval)interval;
- (NSString*)getDateStringFromDate:(NSDate*)date;
+ (NSDate*)parseDateFromString:(NSString*)stringDate;
+ (NSDate*)parseTimeFromString:(NSString*)stringDate;
+ (NSString*)parseDateOnly:(NSDate*)date;

- (NSDictionary *)toJSONDictionary;

@end
