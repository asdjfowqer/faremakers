//
//  AirLines.m
//  Faremakers
//
//  Created by Mac1 on 20/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "AirLines.h"

@implementation AirLines

+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary
{
    AirLines *airline = [[AirLines alloc] init];
    
    if (airline)
    {
        airline.airLine = [dictionary objectForKey:@"airline"];
        airline.flightNo = [dictionary objectForKey:@"flightNo"];
    }
    
    return airline;
}
+ (NSArray*)itemWithArray:(NSArray*)itemArray
{
    NSMutableArray *operatingAirlineArray = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in itemArray)
    {
        [operatingAirlineArray addObject:[self itemWithDictionary:dict]];
    }
    
    return [[NSArray alloc] initWithArray:operatingAirlineArray];
}

@end
