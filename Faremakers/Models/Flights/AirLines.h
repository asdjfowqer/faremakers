//
//  AirLines.h
//  Faremakers
//
//  Created by Mac1 on 20/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AirLines : NSObject

@property (nonatomic) NSString *airLine;
@property (nonatomic) NSString *flightNo;
@property (nonatomic) NSString *airLineName;


+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary;
+ (NSArray*)itemWithArray:(NSArray*)itemArray;

@end
