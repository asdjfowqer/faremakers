//
//  AirLineRules.h
//  Faremakers
//
//  Created by Mac1 on 03/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AirLineRules : NSObject

@property (nonatomic) NSString *ruleTitle;
@property (nonatomic) NSString *ruleText;

+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary;
+ (NSArray*)itemWithArray:(NSArray*)itemArray;

@end
