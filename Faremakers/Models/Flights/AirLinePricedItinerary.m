//
//  AirLinePricedItinerary.m
//  Faremakers
//
//  Created by Mac1 on 07/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "AirLinePricedItinerary.h"
#import "AirItineraryPricingInfo.h"
#import "NSDate+DateComponents.h"
#import "AirFareBreakDown.h"
#import "AirLinesNameData.h"
#import "AirCustomeData.h"
#import "FlightSegment.h"
#import "AirLines.h"
#import "Constants.h"

@implementation AirFreBasis


+ (instancetype)itemWithDictionary:(NSDictionary *)dictionary
{
    AirFreBasis *airFreBasis = [[AirFreBasis alloc] init];

    if (airFreBasis)
    {
        airFreBasis.code = [dictionary objectForKey:@"Code"];
        airFreBasis.farePassengerType = [dictionary objectForKey:@"FarePassengerType"];
        airFreBasis.fareType = [dictionary objectForKey:@"FareType"];
        airFreBasis.filingCarrier = [dictionary objectForKey:@"FilingCarrier"];
        airFreBasis.globalInd = [dictionary objectForKey:@"GlobalInd"];
        airFreBasis.market = [dictionary objectForKey:@"Market"];
    }

    return airFreBasis;
}

+ (NSArray *)itemWithArray:(NSArray *)itemArray
{
    NSMutableArray *AirFreBasisArray = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in itemArray)
    {
        [AirFreBasisArray addObject:[self itemWithDictionary:dict]];
    }

    return [[NSArray alloc] initWithArray:AirFreBasisArray];
}

@end




@implementation MarketingAirline

+ (instancetype)itemWithDictionary:(NSDictionary *)dictionary
{
    MarketingAirline *marketingAirline = [[MarketingAirline alloc] init];

    if (marketingAirline)
    {
        marketingAirline.code = [dictionary objectForKey:@"Code"];
        // marketingAirline.flightNumber = [dictionary objectForKey:@"flightNumber"];
    }

    return marketingAirline;
}

@end

@implementation ETicket : NSObject

+ (instancetype)itemWithDictionary:(NSDictionary *)dictionary
{
    ETicket *eTicket = [[ETicket alloc] init];

    if (eTicket)
    {
        eTicket.ind = (BOOL)[dictionary objectForKey:@"Ind"];
        eTicket.indSpecified = (BOOL)[dictionary objectForKey:@"IndSpecified"];
    }

    return eTicket;
}

@end

@implementation AirItinerary_Extensions

+ (instancetype)itemWithDictionary:(NSDictionary *)dictionary
{
    AirItinerary_Extensions *extensions = [[AirItinerary_Extensions alloc] init];

    if (extensions)
    {
        extensions.eTicket = [ETicket itemWithDictionary:[dictionary objectForKey:@"ETicket"]];
        // marketingAirline.flightNumber = [dictionary objectForKey:@"flightNumber"];
    }

    return extensions;
}

@end


@implementation AirEquipment : NSObject


+ (instancetype)itemWithDictionary:(NSDictionary *)dictionary
{
    AirEquipment *airEquipment = [[AirEquipment alloc] init];

    if (airEquipment)
    {
        airEquipment.airEquipType = (NSString *)[dictionary objectForKey:@"AirEquipType"];
        airEquipment.changeofGauge = (BOOL)[dictionary objectForKey:@"ChangeofGauge"];
    }

    return airEquipment;
}

+ (NSArray *)itemWithArray:(NSArray *)itemArray
{
    NSMutableArray *airEquipmentArray = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in itemArray)
    {
        [airEquipmentArray addObject:[self itemWithDictionary:dict]];
    }

    return [[NSArray alloc] initWithArray:airEquipmentArray];
}

@end


@implementation OperatingAirline

+ (instancetype)itemWithDictionary:(NSDictionary *)dictionary
{
    OperatingAirline *operatingAirline = [[OperatingAirline alloc] init];

    if (operatingAirline)
    {
        operatingAirline.code = [dictionary objectForKey:@"Code"];
        operatingAirline.flightNumber = [dictionary objectForKey:@"FlightNumber"];
    }

    return operatingAirline;
}

+ (NSArray *)itemWithArray:(NSArray *)itemArray
{
    NSMutableArray *operatingAirlineArray = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in itemArray)
    {
        [operatingAirlineArray addObject:[self itemWithDictionary:dict]];
    }

    return [[NSArray alloc] initWithArray:operatingAirlineArray];
}

@end


@implementation AirheaderInformation

+ (instancetype)itemWithDictionary:(NSDictionary *)dictionary
{
    AirheaderInformation *airheaderInformation = [[AirheaderInformation alloc] init];

    if (airheaderInformation)
    {
        airheaderInformation.validatingCarrier = [[dictionary objectForKey:@"ValidatingCarrier"] objectForKey:@"Code"];

        NSMutableArray *textArray = [[NSMutableArray alloc] init];

        for (NSString *text in[dictionary objectForKey:@"Text"])
        {
            [textArray addObject:text];
        }
        // airheaderInformation.text = [textArray copy];
        airheaderInformation.text = [[NSArray alloc] initWithArray:textArray];
    }
    return airheaderInformation;
}

@end

@implementation OriginDestination


+ (instancetype)itemWithDictionary:(NSDictionary *)dictionary
{
    OriginDestination *origionDestination = [[OriginDestination alloc] init];

    if (origionDestination)
    {
        origionDestination.flightSegments = [FlightSegment itemWithArray:[dictionary objectForKey:@"Origindestination"]];
        origionDestination.stopQuantity = [dictionary objectForKey:@"stopQuantity"];
        origionDestination.duration = [dictionary objectForKey:@"duration"];
        origionDestination.layOverTimes = [dictionary objectForKey:@"LayoverTime"];
    }
    return origionDestination;
}

@end

@implementation AirItinerary

+ (NSArray *)originDestinationArray:(NSArray *)itemArray
{
    NSMutableArray *OrigindestinationArray = [[NSMutableArray alloc] init];

    for (NSDictionary *item in itemArray)
    {
        [OrigindestinationArray addObject:[OriginDestination itemWithDictionary:item]];
    }

    return [[NSArray alloc] initWithArray:OrigindestinationArray];
}

+ (instancetype)itemWithDictionary:(NSDictionary *)dictionary
{
    AirItinerary *airItinerary = [[AirItinerary alloc] init];

    if (airItinerary)
    {
        airItinerary.directionInd = (NSNumber *)[dictionary objectForKey:@"DirectionInd"];
        airItinerary.directionIndSpecified = (BOOL)[dictionary objectForKey:@"DirectionIndSpecified"];
        airItinerary.originDestinationOptions = [self originDestinationArray:[dictionary objectForKey:@"OriginDestinationOptions"]];
    }
    return airItinerary;
}

@end

@implementation Air_Extensions : NSObject


+ (instancetype)itemWithDictionary:(NSDictionary *)dictionary
{
    Air_Extensions *air_Extensions = [[Air_Extensions alloc] init];

    if (air_Extensions)
    {
        air_Extensions.code = [[dictionary objectForKey:@"ValidatingCarrier"] objectForKey:@"Code"];
    }
    return air_Extensions;
}

@end

@implementation AirTicketingInfo : NSObject


+ (instancetype)itemWithDictionary:(NSDictionary *)dictionary
{
    AirTicketingInfo *airTicketingInfo = [[AirTicketingInfo alloc] init];

    if (airTicketingInfo)
    {
        airTicketingInfo.ticketType = (NSNumber *)[dictionary objectForKey:@"TicketType"];
        airTicketingInfo.validInterline = (NSNumber *)[dictionary objectForKey:@"ValidInterline"];
    }
    return airTicketingInfo;
}

@end


@implementation AirLinePricedItinerary

+ (instancetype)itemWithDictionary:(NSDictionary *)dictionary
{
    AirLinePricedItinerary *airLinePricedItinerary = [[AirLinePricedItinerary alloc] init];


    DLog(@"%@", dictionary);
    if (airLinePricedItinerary)
    {
        airLinePricedItinerary.airItineraryPricingInfo = [AirItineraryPricingInfo itemWithArray:[dictionary objectForKey:@"AirItineraryPricingInfo"]];
        airLinePricedItinerary.airItinerary = [AirItinerary itemWithDictionary:[dictionary objectForKey:@"Airitinerary"]];
        airLinePricedItinerary.tpA_Extensions = [Air_Extensions itemWithDictionary:[dictionary objectForKey:@"TpA_Extensions"]];
        airLinePricedItinerary.customeArray = [AirCustomeData itemWithArray:[dictionary objectForKey:@"CustomeArray"]];
    }

    return airLinePricedItinerary;
}

#pragma mark Sorting/Filtring

+ (NSArray *)filterFlightData:(NSArray *)flightArray forItem:(NSArray *)items withKey:(NSString *)key andIndex:(NSInteger)index
{
    NSMutableArray *array = [[NSMutableArray alloc]init];

    if ([key isEqualToString:@"airLine"])
    {
        [flightArray enumerateObjectsUsingBlock: ^(id _Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {
            AirLinePricedItinerary *pricedIt = (AirLinePricedItinerary *)obj;
            AirCustomeData *customeData = pricedIt.customeArray[index];

            BOOL isFound = false;

            for (AirLines *airline in customeData.allAirLines)
            {
                for (NSString *filterAirLine in items)
                {
                    if ([airline.airLine isEqualToString:filterAirLine])
                    {
                        isFound = true;
                        break;
                    }
                }
                if (isFound)
                {
                    [array addObject:pricedIt];
                    break;
                }
            }
        }];
    }

    return [[NSArray alloc] initWithArray:array];
}

+ (NSArray *)filterNonRefundableFlight:(NSArray *)flightArray
{
    NSMutableArray *array = [[NSMutableArray alloc]init];
    {
        [flightArray enumerateObjectsUsingBlock: ^(id _Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {
            AirLinePricedItinerary *pricedIt = (AirLinePricedItinerary *)obj;
            AirItineraryPricingInfo *pricingInfo = pricedIt.airItineraryPricingInfo[0];

            if ([pricingInfo checkNonRefunableFlight])//(endorsment.nonRefundableIndicator)//If true then Non-Refundable
            {
                [array addObject:pricedIt];
            }
        }];
    }

    return [[NSArray alloc] initWithArray:array];
}

+ (NSArray *)filterFlightForDuration:(NSArray *)flightArray withDuration:(NSInteger)duration withTripIndex:(NSInteger)oneWayIndex
{
    NSMutableArray *array = [[NSMutableArray alloc]init];
    {
        [flightArray enumerateObjectsUsingBlock: ^(id _Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {
            AirLinePricedItinerary *pricedIt = (AirLinePricedItinerary *)obj;

            AirCustomeData *data = pricedIt.customeArray[oneWayIndex];

            NSTimeInterval interval1 = [data converDurationToInterval];
            NSTimeInterval interval2 = (duration * 60 * 60);


            if (interval1 <= interval2)
            {
                [array addObject:pricedIt];
            }
        }];
    }

    return [[NSArray alloc] initWithArray:array];
}

+ (NSArray *)filterFlightForLayoverDuration:(NSArray *)flightArray withLayoverDuration:(NSInteger)duration withTripIndex:(NSInteger)oneWayIndex
{
    NSMutableArray *array = [[NSMutableArray alloc]init];
    {
        [flightArray enumerateObjectsUsingBlock: ^(id _Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {
            AirLinePricedItinerary *pricedIt = (AirLinePricedItinerary *)obj;

            AirCustomeData *data = pricedIt.customeArray[oneWayIndex];

            BOOL isObjectFound = true;

            for (NSString *layover in data.layOverTimes)
            {
                NSTimeInterval interval1 = [data converFlightLayoverDurationToInterval:layover];
                NSTimeInterval interval2 = (duration * 60 * 60);
                if (interval1 > interval2)
                {
                    isObjectFound = false;
                }
            }
            if (isObjectFound)
            {
                [array addObject:pricedIt];
            }
        }];
    }

    return [[NSArray alloc] initWithArray:array];
}

//Filtering According to time
- (NSDate *)dateStyle
{
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH.mm.ss"];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    NSString *dateString = [dateFormatter stringFromDate:date];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];

    date = [dateFormatter dateFromString:dateString];
    //NSTimeInterval interval = [date timeIntervalSinceNow];
    return date;
}

- (void)converDepartureTimeToDates
{
    //    [self dateStyle];
    NSDate *today = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *string = [formatter stringFromDate:today];

    [formatter setDateFormat:@"yyyy-MM-dd 00.01.00"];
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];


    today = [formatter dateFromString:string];

    NSTimeInterval interval = [today timeIntervalSinceNow];

    NSString *newDate = @"2016-02-12 04:25:00 +0000";
    [formatter setDateFormat:@"yyyy-MM-dd HH.mm.ss Z"];
    NSDate *newdate = [formatter dateFromString:newDate];

    NSTimeInterval interval2 = [newdate timeIntervalSinceDate:today];
}

+ (NSArray *)FilterFlightsArray:(NSArray *)flightArray forDayTimingsWithStartInterv:(NSTimeInterval)startTime andEndInterv:(NSTimeInterval)endingInterv withTripIndex:(NSInteger)oneWayIndex
{
    NSDate *today = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *string = [formatter stringFromDate:today];
    [formatter setDateFormat:@"yyyy-MM-dd 00.00.00"];
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    today = [formatter dateFromString:string];//current date + 00:00:00 (start fo current day)



    NSMutableArray *array = [[NSMutableArray alloc]init];
    {
        [flightArray enumerateObjectsUsingBlock: ^(id _Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {
            AirLinePricedItinerary *pricedIt = (AirLinePricedItinerary *)obj;
            AirCustomeData *airCustome = pricedIt.customeArray[oneWayIndex];

            NSTimeInterval flightInterval = [airCustome returnDayTimingInterval:today];
            if (flightInterval >= startTime && flightInterval <= endingInterv)
            {
                [array addObject:pricedIt];
            }
        }];
    }

    return [[NSArray alloc] initWithArray:array];
}

@end