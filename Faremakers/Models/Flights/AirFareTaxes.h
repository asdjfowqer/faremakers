//
//  AirFareTaxes.h
//  Faremakers
//
//  Created by Mac1 on 08/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AirFareTaxes : NSObject

@property (nonatomic) NSString *taxCode;
@property (nonatomic) NSNumber *amount;
@property (nonatomic) BOOL amountSpecified;
@property (nonatomic) NSString *currencyCode;
@property (nonatomic) NSString *decimalPlaces;

+ (NSArray*)itemWithArray:(NSArray*)itemArray;
+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary;

@end
