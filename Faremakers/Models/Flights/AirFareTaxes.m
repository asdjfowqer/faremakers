//
//  AirFareTaxes.m
//  Faremakers
//
//  Created by Mac1 on 08/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "AirFareTaxes.h"

@implementation AirFareTaxes


+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary
{
    AirFareTaxes *airFareTaxes = [[AirFareTaxes alloc] init];
    
    if (airFareTaxes)
    {
        airFareTaxes.taxCode = [dictionary objectForKey:@"IndicatorSpecified"];
        airFareTaxes.amount = (NSNumber*)[dictionary objectForKey:@"Amount"];
        airFareTaxes.amountSpecified = (BOOL)[dictionary objectForKey:@"AmountSpecified"];
        airFareTaxes.currencyCode = [dictionary objectForKey:@"CurrencyCode"];
        airFareTaxes.decimalPlaces = [dictionary objectForKey:@"DecimalPlaces"];
        
    }
    
    return airFareTaxes;
}

+ (NSArray*)itemWithArray:(NSArray*)itemArray
{
    NSMutableArray *airFareTaxes = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in itemArray)
    {
        [airFareTaxes addObject:[self itemWithDictionary:dict]];
    }
    
    return [[NSArray alloc] initWithArray:airFareTaxes];
}

@end
