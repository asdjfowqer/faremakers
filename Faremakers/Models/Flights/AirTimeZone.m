//
//  AirTimeZone.m
//  Faremakers
//
//  Created by Mac1 on 18/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "AirTimeZone.h"

@implementation AirTimeZone

+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary
{
    AirTimeZone *departureTimeZone = [[AirTimeZone alloc] init];
    
    if (departureTimeZone)
    {
        departureTimeZone.gmtOffset = (NSNumber*)[dictionary objectForKey:@"GmtOffset"];
        departureTimeZone.gmtOffsetSpecified = (BOOL)[dictionary objectForKey:@"GmtOffsetSpecified"];
    }
    
    return departureTimeZone;
}



@end
