//
//  FlightFareCalendar.m
//  Chart
//
//  Created by Mac1 on 15/02/2016.
//  Copyright © 2016 Hipolito Arias. All rights reserved.
//

#import "FlightFareCalendar.h"
#import "FlightSegment.h"

@implementation AirlineCodes


+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary
{
    AirlineCodes *flightCalendar = [[AirlineCodes alloc] init];
    if (flightCalendar) {
        flightCalendar.Fare = [dictionary objectForKey:@"CurrencyCode"];
    }
    return flightCalendar;
}
+ (NSArray*)itemWithArray:(NSArray*)itemArray
{
    NSMutableArray *flightCalendarArray = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in itemArray)
    {
        [flightCalendarArray addObject:[self itemWithDictionary:dict]];
    }
    
    return [[NSArray alloc] initWithArray:flightCalendarArray];
}


@end

@implementation LowestFare


+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary
{
    LowestFare *lowestFare = [[LowestFare alloc] init];
    
    if (lowestFare) {
        lowestFare.Fare = [dictionary objectForKey:@"Fare"];
       // lowestFare.airlineCodes = [AirlineCodes itemWithArray:[dictionary objectForKey:@"airlineCodes"]];
    }
    return lowestFare;
}
+ (NSArray*)itemWithArray:(NSArray*)itemArray
{
    NSMutableArray *flightCalendarArray = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in itemArray)
    {
        [flightCalendarArray addObject:[self itemWithDictionary:dict]];
    }
    
    return [[NSArray alloc] initWithArray:flightCalendarArray];
}


@end


@implementation FlightFareCalendar


+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary
{
    FlightFareCalendar *flightCalendar = [[FlightFareCalendar alloc] init];
    if (flightCalendar) {
        flightCalendar.CurrencyCode = [dictionary objectForKey:@"CurrencyCode"];
        flightCalendar.DepartureDateTime = [FlightSegment parseDateFromString:[dictionary objectForKey:@"DepartureDateTime"]];
        flightCalendar.ReturnDateTime = [FlightSegment parseDateFromString:[dictionary objectForKey:@"ReturnDateTime"]];
        flightCalendar.lowestFare = [LowestFare itemWithDictionary:[dictionary objectForKey:@"LowestFare"]];
    }
    return flightCalendar;
}
+ (NSArray*)itemWithArray:(NSArray*)itemArray
{
    NSMutableArray *flightCalendarArray = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in itemArray)
    {
        [flightCalendarArray addObject:[self itemWithDictionary:dict]];
    }
    
    return [[NSArray alloc] initWithArray:flightCalendarArray];
}
+ (NSArray*)itemWithArray:(NSArray*)itemArray withBaseCurrency:(NSString*)currency
{
    NSMutableArray *flightCalendarArray = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in itemArray)
    {
        [flightCalendarArray addObject:[self itemWithDictionary:dict withBaseCurrency:currency]];
    }
    
    return [[NSArray alloc] initWithArray:flightCalendarArray];
}
+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary withBaseCurrency:(NSString*)currency
{
    FlightFareCalendar *flightCalendar = [[FlightFareCalendar alloc] init];
    if (flightCalendar) {
        flightCalendar.CurrencyCode = [dictionary objectForKey:@"CurrencyCode"];
        flightCalendar.DepartureDateTime = [FlightSegment parseDateFromString:[dictionary objectForKey:@"DepartureDateTime"]];
        flightCalendar.ReturnDateTime = [FlightSegment parseDateFromString:[dictionary objectForKey:@"ReturnDateTime"]];
        flightCalendar.lowestFare = [LowestFare itemWithDictionary:[dictionary objectForKey:@"LowestFare"]];
//        if (![flightCalendar.CurrencyCode isEqualToString:currency])
//        {
//            flightCalendar.lowestFare.Fare = [NSNumber numberWithLong:(flightCalendar.lowestFare.Fare.integerValue * 100)];
//        }
    }
    return flightCalendar;
}
+ (NSDate*)parseDateFromString:(NSString*)stringDate // Don't use it Outside the campare method
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd'T'HH:mm"];
  //  [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    NSDate *date =  [dateFormatter dateFromString:stringDate];
    return date;
}
+(BOOL)compareCalanderDeparDate:(NSDate*)calenderDate withDeprDate:(NSString*)date
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSInteger comps = (NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear);
    
    NSDateComponents *date1Components = [calendar components:comps
                                                    fromDate: calenderDate];
    NSDateComponents *date2Components = [calendar components:comps
                                                    fromDate: [FlightFareCalendar parseDateFromString:date]];
    
    calenderDate = [calendar dateFromComponents:date1Components];
    NSDate *date2 = [calendar dateFromComponents:date2Components];
    
    if([calenderDate compare:date2] == NSOrderedAscending)
    {
        return false;
    }
    return true;
}
+ (NSArray*)getFareCalendarArray:(NSArray*)itemArray forCurrentDate:(NSString*)date
{
    NSMutableArray *flightCalendarArray = [[NSMutableArray alloc] init];
    for (FlightFareCalendar *flightCalendar in itemArray)
    {
        if ([FlightFareCalendar compareCalanderDeparDate:flightCalendar.DepartureDateTime withDeprDate:date])
        {
            [flightCalendarArray addObject:flightCalendar];
        }
    }
    
    return [[NSArray alloc] initWithArray:flightCalendarArray];
}
@end

