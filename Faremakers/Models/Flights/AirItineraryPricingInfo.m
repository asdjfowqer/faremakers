//
//  AirItineraryPricingInfo.m
//  Faremakers
//
//  Created by Mac1 on 07/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "AirItineraryPricingInfo.h"
#import "AirItinTotalFare.h"
#import "AirFareBreakDown.h"
#import "FlightSegment.h"

@implementation AirPassengerTypeQuantity


+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary
{
    AirPassengerTypeQuantity *airPassengerTypeQuantity = [[AirPassengerTypeQuantity alloc] init];
    
    if (airPassengerTypeQuantity)
    {
        airPassengerTypeQuantity.code = [dictionary objectForKey:@"Code"];
      //  airPassengerTypeQuantity.passengerType = [self getPassengerType];
        airPassengerTypeQuantity.quantity = [dictionary objectForKey:@"Quantity"];
        airPassengerTypeQuantity.changeable = (BOOL)[dictionary objectForKey:@"Changeable"];
    }
    
    return airPassengerTypeQuantity;
}
+ (NSArray*)itemWithArray:(NSArray*)itemArray
{
    NSMutableArray *fareBasisCodes = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in itemArray)
    {
        [fareBasisCodes addObject:[self itemWithDictionary:dict]];
    }
    
    return [[NSArray alloc] initWithArray:fareBasisCodes];
}

@end

@implementation FareBasisCodes


+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary
{
    FareBasisCodes *fareBasisCodes = [[FareBasisCodes alloc] init];
    
    if (fareBasisCodes)
    {
        fareBasisCodes.bookingCode = [dictionary objectForKey:@"BookingCode"];
        fareBasisCodes.value = [dictionary objectForKey:@"Value"];
        fareBasisCodes.departureAirportCode = [dictionary objectForKey:@"DepartureAirportCode"];
        fareBasisCodes.arrivalAirportCode = [dictionary objectForKey:@"ArrivalAirportCode"];

    }
    
    return fareBasisCodes;
}

+ (NSArray*)itemWithArray:(NSArray*)itemArray
{
    NSMutableArray *fareBasisCodes = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in itemArray)
    {
        [fareBasisCodes addObject:[self itemWithDictionary:dict]];
    }
    
    return [[NSArray alloc] initWithArray:fareBasisCodes];
}

- (NSDictionary*)prepareDictForPostDataWithDate:(NSString*)date andAirlineCode:(NSString*)airCode
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:self.arrivalAirportCode forKey:@"DestinationLocation"];
    [dict setObject:self.departureAirportCode forKey:@"OriginLocation"];
    [dict setObject:airCode forKey:@"MarketingCarrier"];
    [dict setObject:self.value forKey:@"FareBasisCode"];
    [dict setObject:date forKey:@"DepartureDateTime"];

    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict
                                                       options:(NSJSONWritingOptions) NSJSONWritingPrettyPrinted
                                                         error:&error];
    
    if (! jsonData) {
        DLog(@"bv_jsonStringWithPrettyPrint: error: %@", error.localizedDescription);
    } else {
        DLog(@"%@", [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]);
    }
    return dict;
}
@end


@implementation Endorsements

+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary
{
    Endorsements *endorsements = [[Endorsements alloc] init];
    
    if (endorsements)
    {
        endorsements.nonRefundableIndicator = [[dictionary objectForKey:@"NonRefundableIndicator"] boolValue];
        endorsements.nonRefundableIndicatorSpecified = [[dictionary objectForKey:@"NonRefundableIndicatorSpecified"] boolValue];
        endorsements.nonEndorsableIndicatorSpecified = [[dictionary objectForKey:@"NonEndorsableIndicatorSpecified"] boolValue];
    }
    
    return endorsements;
}

+ (NSArray*)itemWithArray:(NSArray*)itemArray
{
    NSMutableArray *fareBasisCodes = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in itemArray)
    {
        [fareBasisCodes addObject:[self itemWithDictionary:dict]];
    }
    
    return [[NSArray alloc] initWithArray:fareBasisCodes];
}

@end


@implementation FareInfos


+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary
{
    FareInfos *fareInfos = [[FareInfos alloc] init];
    
    if (fareInfos)
    {
        fareInfos.seatsRemaining = (NSNumber*)[[[dictionary objectForKey:@"Tpa_Extensions2"] objectForKey:@"Seatsremaining"] objectForKey:@"Number"];
        fareInfos.cabinCode =(NSString*)[[[dictionary objectForKey:@"Tpa_Extensions2"] objectForKey:@"Cabin"]objectForKey:@"CabinCode"];
        NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:@"Business",@"C",@"Economy",@"Y",@"Premium Economy",@"S",@"Premium First",@"P",@"Premium Business",@"J",nil];
        fareInfos.cabin =[dict objectForKey:fareInfos.cabinCode];

        NSDictionary *meal = [[dictionary objectForKey:@"TPA_Extensions"] objectForKey:@"Meal"];
        if (![meal isKindOfClass:[NSNull class]]) {
            fareInfos.meal = (NSString*)[meal objectForKey:@"Code"];
        }
        else
            fareInfos.meal = @"N/A";
        
        //error
    }
    
    return fareInfos;
}

+ (NSArray*)itemWithArray:(NSArray*)itemArray
{
    NSMutableArray *fareInfos = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in itemArray)
    {
        [fareInfos addObject:[self itemWithDictionary:dict]];
    }
    
    return [[NSArray alloc] initWithArray:fareInfos];
}


@end


@implementation PricingInfo_Extensions


+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary
{
    PricingInfo_Extensions *pricingInfo_Extensions = [[PricingInfo_Extensions alloc] init];
    
    if (pricingInfo_Extensions)
    {
        pricingInfo_Extensions.indicator = (BOOL)[[dictionary objectForKey:@"DivideInParty"] objectForKey:@"Indicator"];
        pricingInfo_Extensions.indicatorSpecified = (BOOL)[[dictionary objectForKey:@"DivideInParty"] objectForKey:@"IndicatorSpecified"];
    }
    
    return pricingInfo_Extensions;
}
+ (NSArray*)itemWithArray:(NSArray*)itemArray
{
    NSMutableArray *fareBasisCodes = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in itemArray)
    {
        [fareBasisCodes addObject:[self itemWithDictionary:dict]];
    }
    
    return [[NSArray alloc] initWithArray:fareBasisCodes];
}

@end

@implementation AirItineraryPricingInfo


+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary
{
    AirItineraryPricingInfo *airItineraryPricingInfo = [[AirItineraryPricingInfo alloc] init];
    
    if (airItineraryPricingInfo)
    {
    //    airItineraryPricingInfo.alternateCityOption = (BOOL)[dictionary objectForKey:@"AlternateCityOption"];
 //       airItineraryPricingInfo.cachedItin = (BOOL)[dictionary objectForKey:@"CachedItin"];
 //       airItineraryPricingInfo.fareReturnedSpecified = (BOOL)[dictionary objectForKey:@"FareReturnedSpecified"];
        airItineraryPricingInfo.pricingSource = [dictionary objectForKey:@"PricingSource"];

//        airItineraryPricingInfo.tpA_Extensions = [PricingInfo_Extensions itemWithDictionary:[dictionary objectForKey:@"TpA_Extensions"]];
        airItineraryPricingInfo.itinTotalFare = [AirItinTotalFare itemWithDictionary:[dictionary objectForKey:@"ItinTotalFare"]];
        airItineraryPricingInfo.ptC_FareBreakdown = [AirFareBreakDown itemWithArray:[dictionary objectForKey:@"PTC_FareBreakdowns"]];
        
        airItineraryPricingInfo.fareInfos = [FareInfos itemWithArray:[dictionary objectForKey:@"FareInfos"]];

    }
    
    return airItineraryPricingInfo;
}

+ (NSArray*)itemWithArray:(NSArray*)itemArray
{
    NSMutableArray *pricingInfoArray = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in itemArray)
    {
        [pricingInfoArray addObject:[self itemWithDictionary:dict]];
    }
    
    return [[NSArray alloc] initWithArray:pricingInfoArray];
}
- (BOOL)checkNonRefunableFlight
{
    AirFareBreakDown *fareBreakDown = self.ptC_FareBreakdown[0];
    Endorsements *endorsment = fareBreakDown.endorsements;
    return endorsment.nonRefundableIndicator;
    
}


@end


