//
//  AirTimeZone.h
//  Faremakers
//
//  Created by Mac1 on 18/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AirTimeZone : NSObject

@property (nonatomic) NSNumber *gmtOffset;
@property (nonatomic) BOOL gmtOffsetSpecified;

+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary;

@end
