//
//  AirPort.m
//  Faremakers
//
//  Created by Mac1 on 18/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "AirPort.h"
#import "AirLinesNameData.h"

@implementation AirPort

+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary
{
    AirPort *airPort = [[AirPort alloc] init];
    
    if (airPort)
    {
        airPort.locationCode = [dictionary objectForKey:@"LocationCode"];
        airPort.codeContext = [dictionary objectForKey:@"CodeContext"];
       // airPort.airPortName = [[AirLinesNameData defaultAirLinesData] getAirPortNameByCode:airPort.locationCode];
    }
    
    return airPort;
}

@end

