//
//  AirItinTotalFare.m
//  Faremakers
//
//  Created by Mac1 on 07/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "AirItinTotalFare.h"
#import "AirBaseFare.h"
#import "AirFareTaxes.h"

@implementation AirItinTotalFare


+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary
{
    AirItinTotalFare *airItinTotalFare = [[AirItinTotalFare alloc] init];
    
    if (airItinTotalFare)
    {
        airItinTotalFare.baseFare = [AirBaseFare itemWithDictionary:[dictionary objectForKey:@"BaseFare"]];
        airItinTotalFare.equivFare = [AirBaseFare itemWithDictionary:[dictionary objectForKey:@"EquivFare"]];
        airItinTotalFare.totalFare = [AirBaseFare itemWithDictionary:[dictionary objectForKey:@"TotalFare"]];
        airItinTotalFare.commission = [AirBaseFare itemWithDictionary:[dictionary objectForKey:@"Commission"]];
        if ([dictionary objectForKey:@"Taxes"])
            airItinTotalFare.taxes = [AirFareTaxes itemWithArray:[[dictionary objectForKey:@"Taxes"] objectForKey:@"Tax"]];
    }
    
    return airItinTotalFare;
}

@end
