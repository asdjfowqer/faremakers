//
//  AirBaseFare.m
//  Faremakers
//
//  Created by Mac1 on 07/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "AirBaseFare.h"

@implementation AirBaseFare

/*
 "amount":721,
 "amountSpecified":true,
 "currencyCode":"USD",
 "decimalPlaces":"2"
 */

+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary
{
    AirBaseFare *airBaseFare = [[AirBaseFare alloc] init];
    
    if (airBaseFare)
    {
        airBaseFare.amount = [dictionary objectForKey:@"Amount"];
        airBaseFare.currencyCode = [dictionary objectForKey:@"CurrencyCode"];
        airBaseFare.decimalPlaces = [dictionary objectForKey:@"DecimalPlaces"];
 //       airBaseFare.amountSpecified = (BOOL)[dictionary objectForKey:@"DecimalPlaces"];

    }
    
    return airBaseFare;
}


@end
