//
//  Promotions.h
//  Faremakers
//
//  Created by Mac1 on 07/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Promotions : NSObject

@property (nonatomic)NSNumber *promotionId;
@property (nonatomic)NSString *promotionName;
@property (nonatomic)NSString *promDescription;
@property (nonatomic)NSString *validityP;
@property (nonatomic)NSString *promotionType;
@property (nonatomic)NSString *termsAndConditions;
@property (nonatomic)NSString *File;
@property (nonatomic)NSString *imageUrl;
@property (nonatomic)NSString *code;
@property (nonatomic)NSNumber *promotionPercentage;


+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary;
+ (NSArray*)itemWithArray:(NSArray*)itemArray;
-(NSInteger)getCurrentOfferMenuIndex;

@end