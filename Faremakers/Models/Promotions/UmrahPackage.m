//
//  UmrahPackage.m
//  Faremakers
//
//  Created by Mac1 on 03/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "UmrahPackage.h"

@implementation UmrahPackage

+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary
{
    UmrahPackage *package = [[UmrahPackage alloc] init];
    
    if (package)
    {
        package.packageId = [dictionary objectForKey:@"packageId"];
        package.packageName = [dictionary objectForKey:@"packageName"];
        package.packagerDateTime = [dictionary objectForKey:@"packagerDateTime"];
        package.packageFare = [dictionary objectForKey:@"packageFare"];
        package.itenerary = [dictionary objectForKey:@"itenerary"];
        package.type = [dictionary objectForKey:@"type"];
        package.File = [dictionary objectForKey:@"File"];
        package.imageUrls = [dictionary objectForKey:@"imageUrls"];
        package.packageCurrencyCode = @"PKR";
    }
    
    return package;
}

+ (NSArray*)itemWithArray:(NSArray*)itemArray
{
    NSMutableArray *PromotionsArray = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in itemArray)
    {
        [PromotionsArray addObject:[self itemWithDictionary:dict]];
    }
    
    return [[NSArray alloc] initWithArray:PromotionsArray];
}
+ (NSArray*)findMinMaxPrices:(NSArray*)array
{
    UmrahPackage *umrahItem = array[0];
    __block NSInteger lowest = umrahItem.packageFare.integerValue;
    __block NSInteger highest = umrahItem.packageFare.integerValue;
    
    [array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        UmrahPackage *umrahItem = (UmrahPackage*)obj;
        
        DLog(@"%@",umrahItem.packageFare);
        
        if (umrahItem.packageFare.integerValue > highest) {
            
            highest = umrahItem.packageFare.integerValue;
            
        }
        else if (umrahItem.packageFare.integerValue < lowest)
        {
            lowest = umrahItem.packageFare.integerValue;
        }
    }];
    return [NSArray arrayWithObjects:[NSNumber numberWithInteger:lowest],[NSNumber numberWithInteger:highest],nil];
}
@end
