//
//  UmrahPackage.h
//  Faremakers
//
//  Created by Mac1 on 03/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UmrahPackage : NSObject

@property (nonatomic)NSString *packageName;
@property (nonatomic)NSString *packageFare;
@property (nonatomic)NSString *packagerDateTime;
@property (nonatomic)NSString *itenerary;
@property (nonatomic)NSString *type;
@property (nonatomic)NSString *File;
@property (nonatomic)NSNumber *packageId;
@property (nonatomic)NSArray *imageUrls;
@property (nonatomic)NSString *packageCurrencyCode;

+ (instancetype)itemWithDictionary:(NSDictionary*)dictionary;
+ (NSArray*)itemWithArray:(NSArray*)itemArray;
+ (NSArray*)findMinMaxPrices:(NSArray*)array;

@end
