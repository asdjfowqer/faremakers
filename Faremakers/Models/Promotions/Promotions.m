//
//  Promotions.m
//  Faremakers
//
//  Created by Mac1 on 07/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "Promotions.h"

@implementation Promotions

+ (instancetype)itemWithDictionary:(NSDictionary *)dictionary
{
    Promotions *promotion = [[Promotions alloc] init];

    if (promotion)
    {
        promotion.promotionId = [dictionary objectForKey:@"promotionId"];
        promotion.promotionName = [dictionary objectForKey:@"promotionName"];
        promotion.promDescription = [dictionary objectForKey:@"description"];//@"Best offer for offering umrah in summer Best offer for offering umrah in summer";//
        promotion.validityP = [dictionary objectForKey:@"validity"];
        promotion.promotionType = [dictionary objectForKey:@"promotionType"];
        promotion.termsAndConditions = [dictionary objectForKey:@"termsAndConditions"];
        promotion.File = [dictionary objectForKey:@"File"];
        promotion.imageUrl = [dictionary objectForKey:@"imageUrl"];
        NSString *code = [dictionary objectForKey:@"promotionCode"];
        promotion.code = [code isKindOfClass:[NSNull class]] ? @"----" : code;
        promotion.promotionPercentage = dictionary[@"promotionPercentage"];
    }

    return promotion;
}

+ (NSArray *)itemWithArray:(NSArray *)itemArray
{
    NSMutableArray *PromotionsArray = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in itemArray)
    {
        [PromotionsArray addObject:[self itemWithDictionary:dict]];
    }

    return [[NSArray alloc] initWithArray:PromotionsArray];
}

- (NSInteger)getCurrentOfferMenuIndex
{
    if ([self.promotionType containsString:@"Flight"])
    {
        return 1;
    }
    if ([self.promotionType containsString:@"Car"])
    {
        return 2;
    }
    if ([self.promotionType containsString:@"Holiday"])
    {
        return 3;
    }
    return 0;
}

@end