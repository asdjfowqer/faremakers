//
//  HotelDetailBottomView.m
//  Faremakers
//
//  Created by Mac1 on 03/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "HotelDetailBottomView.h"

@implementation HotelDetailBottomView

- (IBAction)optionsButtonPressed:(id)sender
{
    if ([sender tag] == 0) {
        [self.adelegate hotelDetailBottomViewRoomSelectoreClicked:sender];
    }
    else
        [self.adelegate hotelDetailBottomViewPriceSelectoreClicked:sender];
}
@end
