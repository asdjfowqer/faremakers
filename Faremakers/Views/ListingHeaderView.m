//
//  ListingHeaderView.m
//  Faremakers
//
//  Created by Mac1 on 01/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "ListingHeaderView.h"
#import "NSDate+DateComponents.h"
#import "AirLinePricedItinerary.h"
#import "FlightSegment.h"

@implementation ListingHeaderView
@synthesize iconImageView;
@synthesize goingLabel;
@synthesize calenderImageView;
@synthesize sectionTitleLabel;
@synthesize dateLabel;
@synthesize refundLabel;


- (void)awakeFromNib {
    NSDate *date = [NSDate date];
    self.dateLabel.text = [NSString stringWithFormat:@"%@ %@ %@",[date getDayOfTheWeek],[date getDayOfTheMonth],[date getMonth]];
}
-(void)showSectionLabel:(BOOL)show
{
    [self.sectionTitleLabel setHidden:!show];
    [self.iconImageView setHidden:show];
    [self.goingLabel setHidden:show];
    [self.calenderImageView setHidden:show];
    [self.dateLabel setHidden:show];
    [self.refundView setHidden:show];
    [self.refundLabel setHidden:show];

}
-(void)setDepartureDate:(NSDate*)date
{
    self.dateLabel.text = [self getDateTime:date];
}
-(void)setArivalDate:(NSDate*)date
{
    self.dateLabel.text = [self getDateTime:date];
}
-(void)setrefundedView:(BOOL)nonRefunded
{
    if (nonRefunded) {
        [self.refundView setBackgroundColor:[UIColor redColor]];
        self.refundLabel.text = @"Non-Refunded";
    }
    else
    {
        [self.refundView setBackgroundColor:[UIColor colorWithRed:24.0/255 green:74.0/255 blue:150.0/255 alpha:1]];
        self.refundLabel.text = @"Refundable";
    }
}
-(NSString*)getDateTime:(NSDate*)date
{
    return [NSString stringWithFormat:@"%@ %@ %@",[date getDayOfTheWeek],[date getDayOfTheMonth],[date getMonth]];
}
@end
