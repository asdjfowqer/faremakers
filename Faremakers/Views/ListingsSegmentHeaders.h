//
//  ListingHeaderView.h
//  Faremakers
//
//  Created by Mac1 on 01/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ListingsSegmentHeaders : UITableViewHeaderFooterView
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *goingLabel;
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;

-(void)setDepartureDate:(NSDate*)date;
-(void)changeDepartureTitle:(BOOL)change;

@end
