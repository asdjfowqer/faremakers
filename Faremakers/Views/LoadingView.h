//
//  LoadingView.h
//  Faremakers
//
//  Created by Mac1 on 03/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LoadingViewDelegate <NSObject>

- (void) loadingViewRoomSelectoreClicked:(id)sender;
- (void) loadingViewPriceSelectoreClicked:(id)sender;

@end

@interface LoadingView : UIView
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;

@property (nonatomic,weak) id<LoadingViewDelegate> adelegate;
@property (weak, nonatomic) IBOutlet UIImageView *loadingImage;

- (IBAction)optionsButtonPressed:(id)sender;
- (void)loadAnimation;
- (void)removeAnimation;

@end
