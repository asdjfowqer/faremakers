//
//  HotelDetailBottomView.h
//  Faremakers
//
//  Created by Mac1 on 03/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HotelDetailBottomViewDelegate <NSObject>

- (void) hotelDetailBottomViewRoomSelectoreClicked:(id)sender;
- (void) hotelDetailBottomViewPriceSelectoreClicked:(id)sender;

@end

@interface HotelDetailBottomView : UIView
@property (weak, nonatomic) IBOutlet UILabel *roomsLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

@property (nonatomic,weak) id<HotelDetailBottomViewDelegate> adelegate;
- (IBAction)optionsButtonPressed:(id)sender;

@end
