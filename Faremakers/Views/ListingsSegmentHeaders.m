//
//  ListingHeaderView.m
//  Faremakers
//
//  Created by Mac1 on 01/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "ListingsSegmentHeaders.h"
#import "NSDate+DateComponents.h"

@implementation ListingsSegmentHeaders
@synthesize iconImageView;
@synthesize goingLabel;
@synthesize dateLabel;


- (void)awakeFromNib
{
    NSDate *date = [NSDate date];
    self.dateLabel.text = [NSString stringWithFormat:@"%@ %@ %@",[date getDayOfTheWeek],[date getDayOfTheMonth],[date getMonth]];
}

-(void)setDepartureDate:(NSDate*)date
{
    self.dateLabel.text = [NSString stringWithFormat:@"%@ %@ %@",[date getDayOfTheWeek],[date getDayOfTheMonth],[date getMonth]];

}
-(void)changeDepartureTitle:(BOOL)change
{
    if (change) {
        self.goingLabel.text = @"Return";
    }
}
@end
