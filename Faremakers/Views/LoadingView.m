//
//  LoadingView.m
//  Faremakers
//
//  Created by Mac1 on 03/02/2016.
//  Copyright © 2016 Self. All rights reserved.
//

#import "LoadingView.h"

@implementation LoadingView

- (IBAction)optionsButtonPressed:(id)sender
{
    if ([sender tag] == 0) {
        [self.adelegate loadingViewRoomSelectoreClicked:sender];
    }
    else
        [self.adelegate loadingViewPriceSelectoreClicked:sender];
}
-(void)loadAnimation
{
    [self runSpinAnimationOnView:self.loadingImage duration:1 rotations:1 repeat:-1];
}
-(void)removeAnimation
{
    [self.loadingImage.layer removeAllAnimations];
}
- (void) runSpinAnimationOnView:(UIView*)view duration:(CGFloat)duration rotations:(CGFloat)rotations repeat:(float)repeat;
{
    
    CABasicAnimation *rotation;
    rotation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    rotation.fromValue = [NSNumber numberWithFloat:0];
    rotation.toValue = [NSNumber numberWithFloat:(M_PI * 2.0)];
    rotation.duration = duration; // Speed
    rotation.repeatCount = HUGE_VALF; // Repeat forever. Can be a finite number.
    [view.layer addAnimation:rotation forKey:@"Spin"];

}

@end
