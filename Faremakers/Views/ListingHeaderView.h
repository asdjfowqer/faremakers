//
//  ListingHeaderView.h
//  Faremakers
//
//  Created by Mac1 on 01/01/2016.
//  Copyright © 2016 Self. All rights reserved.
//



@class FlightSegment;

@interface ListingHeaderView : UITableViewHeaderFooterView
@property (weak, nonatomic) IBOutlet UILabel *sectionTitleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *calenderImageView;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *goingLabel;
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UIView *refundView;
@property (weak, nonatomic) IBOutlet UILabel *refundLabel;

-(void)showSectionLabel:(BOOL)show;
-(void)setDepartureDate:(NSDate*)date;
-(void)setArivalDate:(NSDate*)date;
-(void)setrefundedView:(BOOL)nonRefunded;

@end
