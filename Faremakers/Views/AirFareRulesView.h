//
//  AirFareRulesView.h
//  Faremakers
//
//  Created by Mac1 on 22/12/2015.
//  Copyright © 2015 Self. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AirFareRulesView : NSObject

-(void)showPopupCentered:(id)sender;

@end
